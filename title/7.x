<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Title</title>
  <short_name>title</short_name>
  <dc:creator>plach</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/title</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>title 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta4.tar.gz</download_link>
      <date>1614446626</date>
      <mdhash>4d3892216c5881607e423c77769956fe</mdhash>
      <filesize>27175</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4d3892216c5881607e423c77769956fe</md5>
          <size>27175</size>
          <filedate>1614446626</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>307a9efea4463d754f6884ee2e237f37</md5>
          <size>34423</size>
          <filedate>1614446626</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta3.tar.gz</download_link>
      <date>1571406487</date>
      <mdhash>3841ccf0ec5cca401e128cb9f16f930d</mdhash>
      <filesize>27155</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3841ccf0ec5cca401e128cb9f16f930d</md5>
          <size>27155</size>
          <filedate>1571406487</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>379f3c0892b006a20f1b0716401f391a</md5>
          <size>34406</size>
          <filedate>1571406487</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta2.tar.gz</download_link>
      <date>1570952286</date>
      <mdhash>4c2f973ca442f3b87204b0a21c1a79ad</mdhash>
      <filesize>27149</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c2f973ca442f3b87204b0a21c1a79ad</md5>
          <size>27149</size>
          <filedate>1570952286</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b9dc7298628ae698f5ebe2802bcb019e</md5>
          <size>34383</size>
          <filedate>1570952286</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta1.tar.gz</download_link>
      <date>1568461685</date>
      <mdhash>28447ec28b43563b32b0d10644de929a</mdhash>
      <filesize>26823</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>28447ec28b43563b32b0d10644de929a</md5>
          <size>26823</size>
          <filedate>1568461685</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b09795fc6e56375d4280818dd0982bfe</md5>
          <size>34051</size>
          <filedate>1568461685</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha9</name>
      <version>7.x-1.0-alpha9</version>
      <tag>7.x-1.0-alpha9</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha9.tar.gz</download_link>
      <date>1484302983</date>
      <mdhash>dce551145ee2cbc21f59326dd677f148</mdhash>
      <filesize>27311</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dce551145ee2cbc21f59326dd677f148</md5>
          <size>27311</size>
          <filedate>1484302983</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha9.zip</url>
          <archive_type>zip</archive_type>
          <md5>fff30b4132fea01eba91822fa5e65b91</md5>
          <size>33326</size>
          <filedate>1484302983</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha8</name>
      <version>7.x-1.0-alpha8</version>
      <tag>7.x-1.0-alpha8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha8.tar.gz</download_link>
      <date>1459177439</date>
      <mdhash>fa2006a74e7aaf1f76d9928c4bb761b8</mdhash>
      <filesize>27220</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa2006a74e7aaf1f76d9928c4bb761b8</md5>
          <size>27220</size>
          <filedate>1459177439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha8.zip</url>
          <archive_type>zip</archive_type>
          <md5>8217184e0fac25b95e5207e7a1c8379e</md5>
          <size>33048</size>
          <filedate>1459177439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha7</name>
      <version>7.x-1.0-alpha7</version>
      <tag>7.x-1.0-alpha7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha7.tar.gz</download_link>
      <date>1363626024</date>
      <mdhash>fa24dae8b15c8f5001ff05a5033b4e8d</mdhash>
      <filesize>24062</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa24dae8b15c8f5001ff05a5033b4e8d</md5>
          <size>24062</size>
          <filedate>1363626024</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>a5a9d5e70abe915c03b2d80994f32891</md5>
          <size>29008</size>
          <filedate>1363626024</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha6</name>
      <version>7.x-1.0-alpha6</version>
      <tag>7.x-1.0-alpha6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha6</release_link>
      <download_link/>
      <date>1363625422</date>
      <mdhash>a256e336af8be3b61a4cc66970e731a1</mdhash>
      <filesize>26045</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>a256e336af8be3b61a4cc66970e731a1</md5>
          <size>26045</size>
          <filedate>1363625422</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>e2d8d5f99ae806cc5871ccd817517a24</md5>
          <size>30936</size>
          <filedate>1363625422</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1355827149</date>
      <mdhash>11a5dfbd2bb196e4fdda1223afea3a23</mdhash>
      <filesize>23601</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11a5dfbd2bb196e4fdda1223afea3a23</md5>
          <size>23601</size>
          <filedate>1355827149</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>77f988d1e1d9ba5e5ba9fa0bd7726ea9</md5>
          <size>28588</size>
          <filedate>1355827149</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha4</release_link>
      <download_link/>
      <date>1344682073</date>
      <mdhash>6feba247e8c2ca531a0ceaadccad1a91</mdhash>
      <filesize>21336</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>6feba247e8c2ca531a0ceaadccad1a91</md5>
          <size>21336</size>
          <filedate>1344682073</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>5fd1b8e5b2e85fa85d34538491b90dd2</md5>
          <size>25868</size>
          <filedate>1344682073</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1338898035</date>
      <mdhash>4d03d30ea6ef6c319f9cdcb2be870e15</mdhash>
      <filesize>18205</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4d03d30ea6ef6c319f9cdcb2be870e15</md5>
          <size>18205</size>
          <filedate>1338898035</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d90e95db574551a995421a3f6de48691</md5>
          <size>21894</size>
          <filedate>1338898035</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1315581706</date>
      <mdhash>269af3d3fea50863a0429a781ec5c914</mdhash>
      <filesize>15651</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>269af3d3fea50863a0429a781ec5c914</md5>
          <size>15651</size>
          <filedate>1315581706</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e74381b54745f172bb10d2ec2e6199b8</md5>
          <size>19201</size>
          <filedate>1315581706</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1315386730</date>
      <mdhash>2985639c45efaf634dcbce4852862f38</mdhash>
      <filesize>15429</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2985639c45efaf634dcbce4852862f38</md5>
          <size>15429</size>
          <filedate>1315386730</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>bd1586e275aa943b484fbd0d2591feeb</md5>
          <size>18938</size>
          <filedate>1315386730</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>title 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/title/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/title-7.x-1.x-dev.tar.gz</download_link>
      <date>1700934283</date>
      <mdhash>7ad588a535f02b5959b3a69f5750e2b1</mdhash>
      <filesize>27230</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ad588a535f02b5959b3a69f5750e2b1</md5>
          <size>27230</size>
          <filedate>1700934283</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/title-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>28827731c5085588dff915cbc7c979e3</md5>
          <size>34495</size>
          <filedate>1700934283</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
