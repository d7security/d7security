<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>SAML Authentication</title>
  <short_name>samlauth</short_name>
  <dc:creator>cweagans</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/samlauth</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>samlauth 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/samlauth/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/samlauth-7.x-1.1.tar.gz</download_link>
      <date>1619622238</date>
      <mdhash>cff0e70903e523c3aa3f1231f5d07ee9</mdhash>
      <filesize>16426</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cff0e70903e523c3aa3f1231f5d07ee9</md5>
          <size>16426</size>
          <filedate>1619622238</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>23602f43edeeb287b17c3ca1f00ce833</md5>
          <size>18747</size>
          <filedate>1619622238</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>samlauth 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/samlauth/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0.tar.gz</download_link>
      <date>1619028354</date>
      <mdhash>92ae1a7034d940970f87740fcd6729ea</mdhash>
      <filesize>16361</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92ae1a7034d940970f87740fcd6729ea</md5>
          <size>16361</size>
          <filedate>1619028354</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>bd3e13a523e888798a82ce255d363228</md5>
          <size>18675</size>
          <filedate>1619028354</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>samlauth 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/samlauth/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0-beta1.tar.gz</download_link>
      <date>1528387384</date>
      <mdhash>d9e5eab5723cf37231385941bbca8145</mdhash>
      <filesize>12841</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9e5eab5723cf37231385941bbca8145</md5>
          <size>12841</size>
          <filedate>1528387384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c164d0c8944e55112f19ca63ef945712</md5>
          <size>15023</size>
          <filedate>1528387384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>samlauth 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/samlauth/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/samlauth-7.x-1.x-dev.tar.gz</download_link>
      <date>1645292752</date>
      <mdhash>22dae474712b9dc22677a828313b387c</mdhash>
      <filesize>16786</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>22dae474712b9dc22677a828313b387c</md5>
          <size>16786</size>
          <filedate>1645292752</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/samlauth-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab3adbdbfcc2fbb9de2c996db3f163c6</md5>
          <size>19157</size>
          <filedate>1645292752</filedate>
        </file>
      </files>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
  </releases>
</project>
