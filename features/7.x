<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Features</title>
  <short_name>features</short_name>
  <dc:creator>mpotter</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/features</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>features 7.x-2.15</name>
      <version>7.x-2.15</version>
      <tag>7.x-2.15</tag>
      <version_major>2</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.15.tar.gz</download_link>
      <date>1691595136</date>
      <mdhash>76ee65e3a160a72f9dae0957d6e1e34a</mdhash>
      <filesize>112589</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>76ee65e3a160a72f9dae0957d6e1e34a</md5>
          <size>112589</size>
          <filedate>1691595136</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>49938aee6cbeae14a0cd23f36fd39613</md5>
          <size>132167</size>
          <filedate>1691595136</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.14</name>
      <version>7.x-2.14</version>
      <tag>7.x-2.14</tag>
      <version_major>2</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.14.tar.gz</download_link>
      <date>1645021494</date>
      <mdhash>e0905b3d86b9c4a201e729abc3653086</mdhash>
      <filesize>111721</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e0905b3d86b9c4a201e729abc3653086</md5>
          <size>111721</size>
          <filedate>1645021494</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>54aa085f6006982d08eff5ea98bb6c8d</md5>
          <size>131295</size>
          <filedate>1645021494</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.13</name>
      <version>7.x-2.13</version>
      <tag>7.x-2.13</tag>
      <version_major>2</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.13.tar.gz</download_link>
      <date>1605854052</date>
      <mdhash>eb4770fdc5ff62effb21960ec45641a6</mdhash>
      <filesize>111571</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb4770fdc5ff62effb21960ec45641a6</md5>
          <size>111571</size>
          <filedate>1605854052</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf9349f27492121ba4744b9138e84e3d</md5>
          <size>131153</size>
          <filedate>1605854052</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.12</name>
      <version>7.x-2.12</version>
      <tag>7.x-2.12</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.12.tar.gz</download_link>
      <date>1604202290</date>
      <mdhash>45d91b29c393f09435ca46109b85578c</mdhash>
      <filesize>111102</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>45d91b29c393f09435ca46109b85578c</md5>
          <size>111102</size>
          <filedate>1604202290</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>965494eff340a28634055fa87079883b</md5>
          <size>130620</size>
          <filedate>1604202290</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.11.tar.gz</download_link>
      <date>1541050680</date>
      <mdhash>4e57b3a3720445fd1053ba6740855436</mdhash>
      <filesize>96813</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e57b3a3720445fd1053ba6740855436</md5>
          <size>96813</size>
          <filedate>1541050680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab758ea16526f3ec010abbee9910e468</md5>
          <size>115422</size>
          <filedate>1541050680</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.10.tar.gz</download_link>
      <date>1461011639</date>
      <mdhash>5641e5545020932570aed464fbaebf6a</mdhash>
      <filesize>97102</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5641e5545020932570aed464fbaebf6a</md5>
          <size>97102</size>
          <filedate>1461011639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d7389ae61caaea5a2d3e41c7573fe77</md5>
          <size>115335</size>
          <filedate>1461011639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.9.tar.gz</download_link>
      <date>1460560739</date>
      <mdhash>27fbaf1a31c83f0d18fbe114c2f3fc8a</mdhash>
      <filesize>97030</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>27fbaf1a31c83f0d18fbe114c2f3fc8a</md5>
          <size>97030</size>
          <filedate>1460560739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>b4d0919e4a20b0bc005947e1d38a9974</md5>
          <size>115264</size>
          <filedate>1460560739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.8.tar.gz</download_link>
      <date>1458744539</date>
      <mdhash>ad38d9cf2588bdd7821fc662217d6763</mdhash>
      <filesize>96932</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ad38d9cf2588bdd7821fc662217d6763</md5>
          <size>96932</size>
          <filedate>1458744539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>87af5320fb6201144bedf5bcd8929766</md5>
          <size>115155</size>
          <filedate>1458744539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.7.tar.gz</download_link>
      <date>1444829339</date>
      <mdhash>dcfcc0cede1d281c23346662029015a5</mdhash>
      <filesize>97292</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dcfcc0cede1d281c23346662029015a5</md5>
          <size>97292</size>
          <filedate>1444829339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>f194084c1622aca9cf7bbe02b3aee2b8</md5>
          <size>115553</size>
          <filedate>1444829339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.6.tar.gz</download_link>
      <date>1435165980</date>
      <mdhash>3a41eba4ca26f0d3a0e00e97dd8722ba</mdhash>
      <filesize>96581</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3a41eba4ca26f0d3a0e00e97dd8722ba</md5>
          <size>96581</size>
          <filedate>1435165980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c7e6a58f647914006a556b7a1bfe215</md5>
          <size>114296</size>
          <filedate>1435165980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.6-rc1</name>
      <version>7.x-2.6-rc1</version>
      <tag>7.x-2.6-rc1</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.6-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.6-rc1.tar.gz</download_link>
      <date>1433533080</date>
      <mdhash>ac68a860da89bd8a54f0cb5b7a2668ce</mdhash>
      <filesize>96592</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.6-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac68a860da89bd8a54f0cb5b7a2668ce</md5>
          <size>96592</size>
          <filedate>1433533080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.6-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>11bbd16a9db0fe7ccd26a2e46eb36822</md5>
          <size>114296</size>
          <filedate>1433533080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.5.tar.gz</download_link>
      <date>1428943813</date>
      <mdhash>c6fea06d61fe6f4c9d1943a5fcb99f61</mdhash>
      <filesize>92795</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c6fea06d61fe6f4c9d1943a5fcb99f61</md5>
          <size>92795</size>
          <filedate>1428943813</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>61a59f85b37236589770e0bdc19a87d2</md5>
          <size>110498</size>
          <filedate>1428943813</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.4.tar.gz</download_link>
      <date>1425500581</date>
      <mdhash>af6f0fd6117b5c0ff15ef7c1923d116f</mdhash>
      <filesize>92229</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af6f0fd6117b5c0ff15ef7c1923d116f</md5>
          <size>92229</size>
          <filedate>1425500581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>805967bfba8401713591a31b3e52fde6</md5>
          <size>109857</size>
          <filedate>1425500581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.3.tar.gz</download_link>
      <date>1420492080</date>
      <mdhash>562c8c81a512e45d5fb04b4ee33858bf</mdhash>
      <filesize>92013</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>562c8c81a512e45d5fb04b4ee33858bf</md5>
          <size>92013</size>
          <filedate>1420492080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>328b74de8c0616ca4bf432176e022e01</md5>
          <size>109598</size>
          <filedate>1420492080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.2.tar.gz</download_link>
      <date>1407338927</date>
      <mdhash>1eaa764a12b7ad846ab3ffa70512c567</mdhash>
      <filesize>92041</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1eaa764a12b7ad846ab3ffa70512c567</md5>
          <size>92041</size>
          <filedate>1407338927</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c0b0f6a844239444daee6bccb6e36dad</md5>
          <size>108797</size>
          <filedate>1407338927</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.1.tar.gz</download_link>
      <date>1406655227</date>
      <mdhash>4e95d6671dbdaa0e8c8bd1ce9cf04ead</mdhash>
      <filesize>92052</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e95d6671dbdaa0e8c8bd1ce9cf04ead</md5>
          <size>92052</size>
          <filedate>1406655227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>61463b200d64bfce49455bc38512d4d6</md5>
          <size>108791</size>
          <filedate>1406655227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0.tar.gz</download_link>
      <date>1382018758</date>
      <mdhash>2ce7c8cb75d3fc57da069721f6a9a052</mdhash>
      <filesize>90313</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2ce7c8cb75d3fc57da069721f6a9a052</md5>
          <size>90313</size>
          <filedate>1382018758</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>cd96ce6f7d82b618a172a53887b53e72</md5>
          <size>106729</size>
          <filedate>1382018759</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-2.0-rc5</name>
      <version>7.x-2.0-rc5</version>
      <tag>7.x-2.0-rc5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc5.tar.gz</download_link>
      <date>1381157303</date>
      <mdhash>faf76e87122ff5140a7a8300c96e8389</mdhash>
      <filesize>90231</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>faf76e87122ff5140a7a8300c96e8389</md5>
          <size>90231</size>
          <filedate>1381157303</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea2344de72c2fffd233e04c5d6aed12a</md5>
          <size>106649</size>
          <filedate>1381157303</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-rc4</name>
      <version>7.x-2.0-rc4</version>
      <tag>7.x-2.0-rc4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc4.tar.gz</download_link>
      <date>1380815748</date>
      <mdhash>71aa54041d07e2b773efb692c2d13476</mdhash>
      <filesize>90706</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>71aa54041d07e2b773efb692c2d13476</md5>
          <size>90706</size>
          <filedate>1380815748</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>6bd2e94a599382769713c06ce42503af</md5>
          <size>107242</size>
          <filedate>1380815748</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-rc3</name>
      <version>7.x-2.0-rc3</version>
      <tag>7.x-2.0-rc3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc3.tar.gz</download_link>
      <date>1377548845</date>
      <mdhash>d92bf152081ba92b2c352b0164198e17</mdhash>
      <filesize>88117</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d92bf152081ba92b2c352b0164198e17</md5>
          <size>88117</size>
          <filedate>1377548845</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a489d76bec31ae385b85a565b05e5cc4</md5>
          <size>104489</size>
          <filedate>1377548846</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc2.tar.gz</download_link>
      <date>1375464067</date>
      <mdhash>63db49c3d8285277b787afa237e96454</mdhash>
      <filesize>87769</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>63db49c3d8285277b787afa237e96454</md5>
          <size>87769</size>
          <filedate>1375464067</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d7752c14f0484839fc4ec8b3f518e51</md5>
          <size>104103</size>
          <filedate>1375464067</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc1.tar.gz</download_link>
      <date>1369094412</date>
      <mdhash>303cd7d10ec0a84e500fd1693d2d3fd0</mdhash>
      <filesize>85917</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>303cd7d10ec0a84e500fd1693d2d3fd0</md5>
          <size>85917</size>
          <filedate>1369094412</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3bd0dbd35f5acf795c878ab944e46c92</md5>
          <size>101426</size>
          <filedate>1369094412</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta2.tar.gz</download_link>
      <date>1364589018</date>
      <mdhash>d889b7006668c261294e58a31b40c53e</mdhash>
      <filesize>85069</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d889b7006668c261294e58a31b40c53e</md5>
          <size>85069</size>
          <filedate>1364589018</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b960d0f7bc289815440b5d01a5c0dd94</md5>
          <size>100471</size>
          <filedate>1364589018</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta1.tar.gz</download_link>
      <date>1352395006</date>
      <mdhash>654e54d22922d4cdbdf1b78be19c5cda</mdhash>
      <filesize>83547</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>654e54d22922d4cdbdf1b78be19c5cda</md5>
          <size>83547</size>
          <filedate>1352395006</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9fc5b98a103563e0bae6c86b35dc793c</md5>
          <size>99018</size>
          <filedate>1352395006</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1351907522</date>
      <mdhash>f588de7611e09d4460a6cd92bf133200</mdhash>
      <filesize>83533</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f588de7611e09d4460a6cd92bf133200</md5>
          <size>83533</size>
          <filedate>1351907522</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>75f0975e0f6d16f99c42dc53f7562de3</md5>
          <size>98994</size>
          <filedate>1351907522</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0.tar.gz</download_link>
      <date>1343240292</date>
      <mdhash>00984913badb19fee69d3ee0ac27eee8</mdhash>
      <filesize>72114</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00984913badb19fee69d3ee0ac27eee8</md5>
          <size>72114</size>
          <filedate>1343240292</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>645d8a02169e771f40d9d18d5667d6ef</md5>
          <size>87662</size>
          <filedate>1343240292</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc3.tar.gz</download_link>
      <date>1339680981</date>
      <mdhash>bb8ac718377cd098474e37f202181b03</mdhash>
      <filesize>71806</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb8ac718377cd098474e37f202181b03</md5>
          <size>71806</size>
          <filedate>1339680981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f50540d8f577299df28ccf5b29b40b3</md5>
          <size>87102</size>
          <filedate>1339680981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc2.tar.gz</download_link>
      <date>1334345807</date>
      <mdhash>0b559938e0aa604729eb2b2fb4840f1b</mdhash>
      <filesize>69926</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b559938e0aa604729eb2b2fb4840f1b</md5>
          <size>69926</size>
          <filedate>1334345807</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>84193babb9e9cb525d634ccd5e548fb3</md5>
          <size>85114</size>
          <filedate>1334345807</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc1.tar.gz</download_link>
      <date>1332022247</date>
      <mdhash>a4f24fe0f0ac5e099b18c667253fa8bc</mdhash>
      <filesize>68737</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a4f24fe0f0ac5e099b18c667253fa8bc</md5>
          <size>68737</size>
          <filedate>1332022247</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e3fed1cd520a03555dbe0692493f3f02</md5>
          <size>83271</size>
          <filedate>1332022247</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta6.tar.gz</download_link>
      <date>1325627143</date>
      <mdhash>d65f3131ad8d397bd5437fe4ab7dba0a</mdhash>
      <filesize>68900</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d65f3131ad8d397bd5437fe4ab7dba0a</md5>
          <size>68900</size>
          <filedate>1325627143</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>acfdd0abd47f50c1f6a45918bbbe7360</md5>
          <size>83541</size>
          <filedate>1325627143</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta5.tar.gz</download_link>
      <date>1324573544</date>
      <mdhash>10053380081d77c6e1cc107ea17bd81e</mdhash>
      <filesize>68894</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10053380081d77c6e1cc107ea17bd81e</md5>
          <size>68894</size>
          <filedate>1324573544</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>8cc14ca07f7db7dbe9b18ff7ce5d1500</md5>
          <size>83532</size>
          <filedate>1324573544</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta4.tar.gz</download_link>
      <date>1316565919</date>
      <mdhash>fbce2ff07b59723d6688ceeac97a967a</mdhash>
      <filesize>67007</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fbce2ff07b59723d6688ceeac97a967a</md5>
          <size>67007</size>
          <filedate>1316565919</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>55345f9401081ddec6cd80a6019913bc</md5>
          <size>81615</size>
          <filedate>1316565919</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta3.tar.gz</download_link>
      <date>1308598915</date>
      <mdhash>20db76b147faf56cc2c1c413cf187db3</mdhash>
      <filesize>63951</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20db76b147faf56cc2c1c413cf187db3</md5>
          <size>63951</size>
          <filedate>1308598915</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c9dd98c777b579113469fd2d236b513d</md5>
          <size>78443</size>
          <filedate>1308598916</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta2.tar.gz</download_link>
      <date>1302049346</date>
      <mdhash>333424ca64d3278cb51319b842e218b2</mdhash>
      <filesize>63505</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>333424ca64d3278cb51319b842e218b2</md5>
          <size>63505</size>
          <filedate>1302049346</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c66e7a5bd3eb56c419ba87b930ba5323</md5>
          <size>77932</size>
          <filedate>1302049346</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta1.tar.gz</download_link>
      <date>1298073121</date>
      <mdhash>a5a58e87c3e18dd77ae0e2fb512531d1</mdhash>
      <filesize>68372</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a5a58e87c3e18dd77ae0e2fb512531d1</md5>
          <size>68372</size>
          <filedate>1298073121</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>be6ac4ee87c03f7316c39a3de825a734</md5>
          <size>85588</size>
          <filedate>1298073121</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1285791063</date>
      <mdhash>09651bc4edb61969d1caa23c76c2df00</mdhash>
      <filesize>67846</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>09651bc4edb61969d1caa23c76c2df00</md5>
          <size>67846</size>
          <filedate>1285791063</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>fa74719fbb2c166de80eabdf45937894</md5>
          <size>84447</size>
          <filedate>1293231362</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1285602662</date>
      <mdhash>b7bcfb6b3a7ac54cac600153642c7aab</mdhash>
      <filesize>67869</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b7bcfb6b3a7ac54cac600153642c7aab</md5>
          <size>67869</size>
          <filedate>1285602662</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>095b814a14f53396c071a72c9d83cafb</md5>
          <size>84472</size>
          <filedate>1293231363</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1284158807</date>
      <mdhash>829c91a7111886b3f53278fd44346ec7</mdhash>
      <filesize>67845</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>829c91a7111886b3f53278fd44346ec7</md5>
          <size>67845</size>
          <filedate>1284158807</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>026c98f7c45caffc75756e9c58e49633</md5>
          <size>84544</size>
          <filedate>1293231360</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-2.x-dev.tar.gz</download_link>
      <date>1690411236</date>
      <mdhash>1d24db47327f3f42dd37eda77c3e4336</mdhash>
      <filesize>112581</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1d24db47327f3f42dd37eda77c3e4336</md5>
          <size>112581</size>
          <filedate>1690411236</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6839f729112ec083d71a5fef22668ddb</md5>
          <size>132177</size>
          <filedate>1690411236</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features-7.x-1.x-dev.tar.gz</download_link>
      <date>1380578130</date>
      <mdhash>baaa489bb72393a5168683c4c27f29dc</mdhash>
      <filesize>73847</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>baaa489bb72393a5168683c4c27f29dc</md5>
          <size>73847</size>
          <filedate>1380578130</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>bfb39fbfd6db545bbd201b3a9a9049f4</md5>
          <size>89426</size>
          <filedate>1380578131</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
