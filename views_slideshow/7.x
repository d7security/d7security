<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Slideshow</title>
  <short_name>views_slideshow</short_name>
  <dc:creator>redndahead</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/views_slideshow</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content Display</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_slideshow 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/views_slideshow/-/releases/7.x-3.11</release_link>
      <download_link>https://gitlab.com/api/v4/projects/61064481/packages/generic/views_slideshow/7.x-3.11/views_slideshow-7.x-3.11.tar.gz</download_link>
      <date>1724357474</date>
      <mdhash>3e71220d3bdeda1f0e97957601089310</mdhash>
      <filesize>43251</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/61064481/packages/generic/views_slideshow/7.x-3.11/views_slideshow-7.x-3.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3e71220d3bdeda1f0e97957601089310</md5>
          <size>43251</size>
          <filedate>1724357474</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/61064481/packages/generic/views_slideshow/7.x-3.11/views_slideshow-7.x-3.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>7403899f205692ef037bbb5d7c7fde3c</md5>
          <size>60632</size>
          <filedate>1724357474</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.10.tar.gz</download_link>
      <date>1586783965</date>
      <mdhash>016d649e241a1d9515fdc454cd4f2304</mdhash>
      <filesize>42362</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>016d649e241a1d9515fdc454cd4f2304</md5>
          <size>42362</size>
          <filedate>1586783965</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>7be3a7d78ab7a61d082c1872e3243b0f</md5>
          <size>60391</size>
          <filedate>1586783965</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.9</name>
      <version>7.x-3.9</version>
      <tag>7.x-3.9</tag>
      <version_major>3</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.9.tar.gz</download_link>
      <date>1496893142</date>
      <mdhash>fc1edcb8ee04923d93cdaa36ca848563</mdhash>
      <filesize>42352</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fc1edcb8ee04923d93cdaa36ca848563</md5>
          <size>42352</size>
          <filedate>1496893142</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>39160781cabf39c93c0667034912a2f9</md5>
          <size>60407</size>
          <filedate>1496893142</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.8</name>
      <version>7.x-3.8</version>
      <tag>7.x-3.8</tag>
      <version_major>3</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.8.tar.gz</download_link>
      <date>1488825783</date>
      <mdhash>ad0e1665ad39b234df35954f96be4d78</mdhash>
      <filesize>42099</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ad0e1665ad39b234df35954f96be4d78</md5>
          <size>42099</size>
          <filedate>1488825783</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f6e52a195b26835633a2a255e96c85e</md5>
          <size>60135</size>
          <filedate>1488825783</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.7.tar.gz</download_link>
      <date>1488323883</date>
      <mdhash>3c9f89add1c975c6820f155a855d109d</mdhash>
      <filesize>41306</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3c9f89add1c975c6820f155a855d109d</md5>
          <size>41306</size>
          <filedate>1488323883</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>656e46b5efbf20e77a23073ade957ad6</md5>
          <size>58541</size>
          <filedate>1488323883</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.6.tar.gz</download_link>
      <date>1487211484</date>
      <mdhash>cb17433b7585c42c64c4ce4cbaaa186d</mdhash>
      <filesize>40102</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb17433b7585c42c64c4ce4cbaaa186d</md5>
          <size>40102</size>
          <filedate>1487211484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>da731d27c0e5fc41bf2bd23fa27e0824</md5>
          <size>55973</size>
          <filedate>1487211484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.5.tar.gz</download_link>
      <date>1484857383</date>
      <mdhash>3123e0b522aadb07b7e791eea34c27de</mdhash>
      <filesize>38808</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3123e0b522aadb07b7e791eea34c27de</md5>
          <size>38808</size>
          <filedate>1484857383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>584906615b4135558f6f069e5fd119db</md5>
          <size>54559</size>
          <filedate>1484857383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.4.tar.gz</download_link>
      <date>1483641842</date>
      <mdhash>c793ed088af5d093df616fee77b86b41</mdhash>
      <filesize>38045</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c793ed088af5d093df616fee77b86b41</md5>
          <size>38045</size>
          <filedate>1483641842</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9d02aaf01efcc004465a3dd2546e8ccb</md5>
          <size>53592</size>
          <filedate>1483641842</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.3.tar.gz</download_link>
      <date>1483562342</date>
      <mdhash>8642510e04bb94fa5e61033b6ed6645c</mdhash>
      <filesize>37849</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8642510e04bb94fa5e61033b6ed6645c</md5>
          <size>37849</size>
          <filedate>1483562342</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>56bb5711a0d76e68dff797fac2cf80a4</md5>
          <size>53414</size>
          <filedate>1483562342</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.2.tar.gz</download_link>
      <date>1482784083</date>
      <mdhash>81adf86198a6a0dc61aa43c961726dbe</mdhash>
      <filesize>37459</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81adf86198a6a0dc61aa43c961726dbe</md5>
          <size>37459</size>
          <filedate>1482784083</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>cac9f955d83593682ed8b9ffba33b5d9</md5>
          <size>52237</size>
          <filedate>1482784083</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.1.tar.gz</download_link>
      <date>1382584827</date>
      <mdhash>a807ec5d346350d1f21b84be38480faf</mdhash>
      <filesize>35949</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a807ec5d346350d1f21b84be38480faf</md5>
          <size>35949</size>
          <filedate>1382584827</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8bf6be747a8793d8e67892d979e685ef</md5>
          <size>48574</size>
          <filedate>1382584827</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0.tar.gz</download_link>
      <date>1319589616</date>
      <mdhash>6bf9363dacee1b55b97c0294b70b5507</mdhash>
      <filesize>33692</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6bf9363dacee1b55b97c0294b70b5507</md5>
          <size>33692</size>
          <filedate>1319589616</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>0bd93cf5bd4b32110f0bd6bb8a8d779b</md5>
          <size>45495</size>
          <filedate>1319589616</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1299893769</date>
      <mdhash>a6b97871bde1ee41b63b60d0953b2e8b</mdhash>
      <filesize>29502</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a6b97871bde1ee41b63b60d0953b2e8b</md5>
          <size>29502</size>
          <filedate>1299893769</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9cfc2f0b183c7ab82c4ea83c316add0a</md5>
          <size>41119</size>
          <filedate>1299893769</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_slideshow 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_slideshow/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.x-dev.tar.gz</download_link>
      <date>1510976285</date>
      <mdhash>fd06837eb70fbd98239f19fca30a0ccd</mdhash>
      <filesize>42349</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd06837eb70fbd98239f19fca30a0ccd</md5>
          <size>42349</size>
          <filedate>1510976285</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b86af2dbc166234777e14b62b8aa8c93</md5>
          <size>60403</size>
          <filedate>1510976285</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
