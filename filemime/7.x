<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>File MIME</title>
  <short_name>filemime</short_name>
  <dc:creator>mfb</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/filemime</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>filemime 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filemime/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filemime-7.x-1.3.tar.gz</download_link>
      <date>1673518520</date>
      <mdhash>090219375a9d8f88e504cda0c4236e51</mdhash>
      <filesize>10193</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>090219375a9d8f88e504cda0c4236e51</md5>
          <size>10193</size>
          <filedate>1673518520</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a17875a80d2cf48f177fc9de83a41055</md5>
          <size>11983</size>
          <filedate>1673518520</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filemime 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filemime/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filemime-7.x-1.2.tar.gz</download_link>
      <date>1564953785</date>
      <mdhash>ef9439a9218c33033fcc8e9b98fc6fc9</mdhash>
      <filesize>10168</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef9439a9218c33033fcc8e9b98fc6fc9</md5>
          <size>10168</size>
          <filedate>1564953785</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f277f8d24870f26250278a919a405718</md5>
          <size>11968</size>
          <filedate>1564953785</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filemime 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filemime/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filemime-7.x-1.1.tar.gz</download_link>
      <date>1368168912</date>
      <mdhash>e617a3a06e527abc15b6a100007e8d1d</mdhash>
      <filesize>9832</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e617a3a06e527abc15b6a100007e8d1d</md5>
          <size>9832</size>
          <filedate>1368168912</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9be90d601839a5ffc63e306a0f56a3cc</md5>
          <size>11161</size>
          <filedate>1368168912</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filemime 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filemime/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filemime-7.x-1.0.tar.gz</download_link>
      <date>1294280458</date>
      <mdhash>dd25ed8e79c963acee7790188e547841</mdhash>
      <filesize>7742</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dd25ed8e79c963acee7790188e547841</md5>
          <size>7742</size>
          <filedate>1294280458</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b47020216c98b10ca082f52576b5164</md5>
          <size>9258</size>
          <filedate>1294280458</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filemime 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filemime/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filemime-7.x-1.x-dev.tar.gz</download_link>
      <date>1673386094</date>
      <mdhash>d741bb19be8d95bff9fe6a0fcceea6e0</mdhash>
      <filesize>10196</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d741bb19be8d95bff9fe6a0fcceea6e0</md5>
          <size>10196</size>
          <filedate>1673386094</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filemime-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>af75b23742af41c7a41330a04d7fa063</md5>
          <size>11988</size>
          <filedate>1673386094</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
