<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Field reference delete</title>
  <short_name>field_reference_delete</short_name>
  <dc:creator>David_Rothstein</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/field_reference_delete</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>field_reference_delete 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/field_reference_delete/-/releases/7.x-1.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66184191/packages/generic/field_reference_delete/7.x-1.0/field_reference_delete-7.x-1.0.tar.gz</download_link>
      <date>1736960036</date>
      <mdhash>f058acc4c3c1a6ab12012c9a77d94c8a</mdhash>
      <filesize>9076</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66184191/packages/generic/field_reference_delete/7.x-1.0/field_reference_delete-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f058acc4c3c1a6ab12012c9a77d94c8a</md5>
          <size>9076</size>
          <filedate>1736960036</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66184191/packages/generic/field_reference_delete/7.x-1.0/field_reference_delete-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0a0a3fa6177af8c70a44ca7aea6b510</md5>
          <size>10097</size>
          <filedate>1736960036</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>field_reference_delete 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_reference_delete/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.0-beta1.tar.gz</download_link>
      <date>1421774108</date>
      <mdhash>c82a6327adbc45d8d2b6728d599a4f0e</mdhash>
      <filesize>8970</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c82a6327adbc45d8d2b6728d599a4f0e</md5>
          <size>8970</size>
          <filedate>1421774108</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>36f6c0961be1d8b40098779c9a67c7a3</md5>
          <size>9780</size>
          <filedate>1421774108</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>field_reference_delete 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_reference_delete/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.x-dev.tar.gz</download_link>
      <date>1421775180</date>
      <mdhash>ec71c64d74431b4bb293e4ef7c0aa20e</mdhash>
      <filesize>8975</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec71c64d74431b4bb293e4ef7c0aa20e</md5>
          <size>8975</size>
          <filedate>1421775180</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_reference_delete-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>49f6b9072201db22efb8e6a023f58cdf</md5>
          <size>9784</size>
          <filedate>1421775180</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
