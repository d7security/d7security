<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Services Entity API</title>
  <short_name>services_entity</short_name>
  <dc:creator>pcambra</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/services_entity</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>services_entity 7.x-2.0-alpha8</name>
      <version>7.x-2.0-alpha8</version>
      <tag>7.x-2.0-alpha8</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha8.tar.gz</download_link>
      <date>1407747227</date>
      <mdhash>26c4a9e43d3cfdb4dce9870b8b4faa22</mdhash>
      <filesize>29374</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>26c4a9e43d3cfdb4dce9870b8b4faa22</md5>
          <size>29374</size>
          <filedate>1407747227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha8.zip</url>
          <archive_type>zip</archive_type>
          <md5>a575a57251b572c8ddaf849c9aa4a61c</md5>
          <size>37872</size>
          <filedate>1407747227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha7</name>
      <version>7.x-2.0-alpha7</version>
      <tag>7.x-2.0-alpha7</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha7.tar.gz</download_link>
      <date>1387704804</date>
      <mdhash>f66e6267bd60863c79f3855e363144d7</mdhash>
      <filesize>28283</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f66e6267bd60863c79f3855e363144d7</md5>
          <size>28283</size>
          <filedate>1387704804</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>42408fe8e6b243fb0ca2fdd3caf68b84</md5>
          <size>37215</size>
          <filedate>1387704804</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha6</name>
      <version>7.x-2.0-alpha6</version>
      <tag>7.x-2.0-alpha6</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha6.tar.gz</download_link>
      <date>1384271005</date>
      <mdhash>e9bf49a45fec48d472e62d69f588fa5c</mdhash>
      <filesize>26537</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e9bf49a45fec48d472e62d69f588fa5c</md5>
          <size>26537</size>
          <filedate>1384271005</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>748bfdffb80cda459d3e5a9bc9408f31</md5>
          <size>35104</size>
          <filedate>1384271005</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha5</name>
      <version>7.x-2.0-alpha5</version>
      <tag>7.x-2.0-alpha5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha5.tar.gz</download_link>
      <date>1375891030</date>
      <mdhash>2036e2aa4ce571fdfe92dd28fad4ed1f</mdhash>
      <filesize>19232</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2036e2aa4ce571fdfe92dd28fad4ed1f</md5>
          <size>19232</size>
          <filedate>1375891030</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>c3fd76c12a80499a14dff2277cb74494</md5>
          <size>25619</size>
          <filedate>1375891030</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha4</name>
      <version>7.x-2.0-alpha4</version>
      <tag>7.x-2.0-alpha4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha4.tar.gz</download_link>
      <date>1369221317</date>
      <mdhash>ad66edda06c5c9e42a4f31aabab0e122</mdhash>
      <filesize>19133</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ad66edda06c5c9e42a4f31aabab0e122</md5>
          <size>19133</size>
          <filedate>1369221317</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ee9e1f19f977b6f9e3b7625a0085024a</md5>
          <size>25508</size>
          <filedate>1369221317</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha3</name>
      <version>7.x-2.0-alpha3</version>
      <tag>7.x-2.0-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha3.tar.gz</download_link>
      <date>1364575816</date>
      <mdhash>8faae3f1f658036e180873f006409711</mdhash>
      <filesize>18993</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8faae3f1f658036e180873f006409711</md5>
          <size>18993</size>
          <filedate>1364575816</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d53438281c27e97f0ad12b6404bc749b</md5>
          <size>25339</size>
          <filedate>1364575816</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1364574617</date>
      <mdhash>d5a9c7e240abda879f540520abac6081</mdhash>
      <filesize>18941</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d5a9c7e240abda879f540520abac6081</md5>
          <size>18941</size>
          <filedate>1364574617</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d1dbf42c0280e56df44af102672fef87</md5>
          <size>25297</size>
          <filedate>1364574617</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1353529125</date>
      <mdhash>a622ab1167f6dd5a6f984291a7ca7bb3</mdhash>
      <filesize>18076</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a622ab1167f6dd5a6f984291a7ca7bb3</md5>
          <size>18076</size>
          <filedate>1353529125</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>806e3065d211bee67730df756a0ec8c8</md5>
          <size>24223</size>
          <filedate>1353529125</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-2.x-dev.tar.gz</download_link>
      <date>1407746927</date>
      <mdhash>3530b396c5d918e314100ac7a30fed14</mdhash>
      <filesize>29385</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3530b396c5d918e314100ac7a30fed14</md5>
          <size>29385</size>
          <filedate>1407746927</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ecadda1ea68ce2f084e31e1d69f76d7b</md5>
          <size>37890</size>
          <filedate>1407746927</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>services_entity 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_entity/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_entity-7.x-1.x-dev.tar.gz</download_link>
      <date>1380627320</date>
      <mdhash>d83e427a1917380f7552ed302ddd741d</mdhash>
      <filesize>12284</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d83e427a1917380f7552ed302ddd741d</md5>
          <size>12284</size>
          <filedate>1380627320</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_entity-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>514c930ef1ce74f29164305ef34f3208</md5>
          <size>14002</size>
          <filedate>1380627320</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
