<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Flood Unblock</title>
  <short_name>flood_unblock</short_name>
  <dc:creator>fabianderijk</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/flood_unblock</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>flood_unblock 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.5.tar.gz</download_link>
      <date>1483704242</date>
      <mdhash>f0bfbfcd4117da87c6e6af9981fd5d3c</mdhash>
      <filesize>10264</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0bfbfcd4117da87c6e6af9981fd5d3c</md5>
          <size>10264</size>
          <filedate>1483704242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>945b2e071deed208eb220daf4927e98a</md5>
          <size>12932</size>
          <filedate>1483704242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.4.tar.gz</download_link>
      <date>1416035280</date>
      <mdhash>0e9a8c56fb3f21bbad5ae916a9abc4cd</mdhash>
      <filesize>9930</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0e9a8c56fb3f21bbad5ae916a9abc4cd</md5>
          <size>9930</size>
          <filedate>1416035280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2b70b0c4d97be79ad6ff941e1696f9ef</md5>
          <size>11293</size>
          <filedate>1416035280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.3.tar.gz</download_link>
      <date>1402330127</date>
      <mdhash>cb1bf59339e4828b86eff4451c39c501</mdhash>
      <filesize>9664</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb1bf59339e4828b86eff4451c39c501</md5>
          <size>9664</size>
          <filedate>1402330127</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0a4ed7ce502065dc61bf7e280a52571</md5>
          <size>10573</size>
          <filedate>1402330127</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.2.tar.gz</download_link>
      <date>1395076131</date>
      <mdhash>e2bb8264c78646aa81eeefd22578b5b0</mdhash>
      <filesize>8539</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e2bb8264c78646aa81eeefd22578b5b0</md5>
          <size>8539</size>
          <filedate>1395076131</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d331200b99962b331a7862258540fac</md5>
          <size>9444</size>
          <filedate>1395076131</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.1.tar.gz</download_link>
      <date>1380791552</date>
      <mdhash>99c30a6c3b578d66ae500eee554600c7</mdhash>
      <filesize>8056</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>99c30a6c3b578d66ae500eee554600c7</md5>
          <size>8056</size>
          <filedate>1380791552</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2e34c5f161b5b271769a5970ca120677</md5>
          <size>8573</size>
          <filedate>1380791552</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.0.tar.gz</download_link>
      <date>1358236655</date>
      <mdhash>59a38b2752417685e2c849f4c6e5631c</mdhash>
      <filesize>8037</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>59a38b2752417685e2c849f4c6e5631c</md5>
          <size>8037</size>
          <filedate>1358236655</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>155991bda1759c992ac515a1246a6d30</md5>
          <size>8550</size>
          <filedate>1358236655</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>flood_unblock 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/flood_unblock/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.x-dev.tar.gz</download_link>
      <date>1483703642</date>
      <mdhash>96bacdf1d13b559cfe533b4005153cde</mdhash>
      <filesize>10269</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>96bacdf1d13b559cfe533b4005153cde</md5>
          <size>10269</size>
          <filedate>1483703642</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/flood_unblock-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>dfc5e692aef66b8b6a54f9f9e4e55b4d</md5>
          <size>12942</size>
          <filedate>1483703642</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
