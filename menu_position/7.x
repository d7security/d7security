<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Position</title>
  <short_name>menu_position</short_name>
  <dc:creator>johnalbin</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/menu_position</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_position 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/menu_position/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67463315/packages/generic/menu_position/7.x-1.3/menu_position-7.x-1.3.tar.gz</download_link>
      <date>1740501058</date>
      <mdhash>ca44b20245404b39a2da9e26eac03ab6</mdhash>
      <filesize>31810</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67463315/packages/generic/menu_position/7.x-1.3/menu_position-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ca44b20245404b39a2da9e26eac03ab6</md5>
          <size>31810</size>
          <filedate>1740501058</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67463315/packages/generic/menu_position/7.x-1.3/menu_position-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9fb79f6acdaec659719959dc59aed499</md5>
          <size>46081</size>
          <filedate>1740501058</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>menu_position 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.2.tar.gz</download_link>
      <date>1448814839</date>
      <mdhash>1c30f002494286f300f31a43d0a54382</mdhash>
      <filesize>27582</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1c30f002494286f300f31a43d0a54382</md5>
          <size>27582</size>
          <filedate>1448814839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>42f2a50abfb897b2918fb501f6866108</md5>
          <size>40256</size>
          <filedate>1448814839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_position 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.1.tar.gz</download_link>
      <date>1329911144</date>
      <mdhash>4a3e3e909b6664fbb582d664433b28c5</mdhash>
      <filesize>25782</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a3e3e909b6664fbb582d664433b28c5</md5>
          <size>25782</size>
          <filedate>1329911144</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0e26b2e0c9b1e3c05cb3d55310f1eefa</md5>
          <size>36218</size>
          <filedate>1329911145</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_position 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0.tar.gz</download_link>
      <date>1320141035</date>
      <mdhash>aab419c28908ffd52546538dfb25fcd0</mdhash>
      <filesize>25041</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aab419c28908ffd52546538dfb25fcd0</md5>
          <size>25041</size>
          <filedate>1320141035</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc139a440d1cca93c2201290bb45937a</md5>
          <size>34131</size>
          <filedate>1320141035</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_position 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta4.tar.gz</download_link>
      <date>1318546301</date>
      <mdhash>36034039b3188dd72eb55273c035dc74</mdhash>
      <filesize>22591</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>36034039b3188dd72eb55273c035dc74</md5>
          <size>22591</size>
          <filedate>1318546301</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d1dbdcad9af99015307d4e8b83d1f70</md5>
          <size>30166</size>
          <filedate>1318546301</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_position 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta3.tar.gz</download_link>
      <date>1318252906</date>
      <mdhash>e27fa09ed0ea75a14f86f5a291e5d162</mdhash>
      <filesize>22352</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e27fa09ed0ea75a14f86f5a291e5d162</md5>
          <size>22352</size>
          <filedate>1318252906</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>00df329a91b47f7f0c734b49eacf7eac</md5>
          <size>29962</size>
          <filedate>1318252906</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_position 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta2.tar.gz</download_link>
      <date>1292517670</date>
      <mdhash>f02db4ab9e579957cc38dca81dab49a8</mdhash>
      <filesize>18130</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f02db4ab9e579957cc38dca81dab49a8</md5>
          <size>18130</size>
          <filedate>1292517670</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e9eea5423008c20ed57de3116c31e429</md5>
          <size>24655</size>
          <filedate>1293232939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_position 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta1.tar.gz</download_link>
      <date>1287702676</date>
      <mdhash>c02c5941223da790842d4eae4ebee9a6</mdhash>
      <filesize>17973</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c02c5941223da790842d4eae4ebee9a6</md5>
          <size>17973</size>
          <filedate>1287702676</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7582b286b63ed88cfd0a2dd1179ac556</md5>
          <size>24517</size>
          <filedate>1293232939</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_position 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-2.x-dev.tar.gz</download_link>
      <date>1480285683</date>
      <mdhash>186e2394a45f43c6cc11129e5339636d</mdhash>
      <filesize>25598</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>186e2394a45f43c6cc11129e5339636d</md5>
          <size>25598</size>
          <filedate>1480285683</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>0817232a6ef224a31cf26e7ac52fd283</md5>
          <size>36648</size>
          <filedate>1480285683</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_position 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_position/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_position-7.x-1.x-dev.tar.gz</download_link>
      <date>1472622239</date>
      <mdhash>309e41b6972acf26e27e2dca292574ce</mdhash>
      <filesize>30346</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>309e41b6972acf26e27e2dca292574ce</md5>
          <size>30346</size>
          <filedate>1472622239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_position-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>dc69ac3af495c64304e866d9b714ae81</md5>
          <size>43023</size>
          <filedate>1472622239</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
