<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Language Cookie</title>
  <short_name>language_cookie</short_name>
  <dc:creator>alexweber</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/language_cookie</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Accessibility</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>language_cookie 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0.tar.gz</download_link>
      <date>1447080839</date>
      <mdhash>5863515a73fe8855674e08fa6db53d64</mdhash>
      <filesize>11238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5863515a73fe8855674e08fa6db53d64</md5>
          <size>11238</size>
          <filedate>1447080839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>145f6a50b8ea0d23d0cf7b477ec863b6</md5>
          <size>13056</size>
          <filedate>1447080839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc2.tar.gz</download_link>
      <date>1432129285</date>
      <mdhash>3c135ffd3d284342785e0acce624872a</mdhash>
      <filesize>11238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3c135ffd3d284342785e0acce624872a</md5>
          <size>11238</size>
          <filedate>1432129285</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>40d8c431631814773905518fe06461f8</md5>
          <size>13044</size>
          <filedate>1432129285</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>language_cookie 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc1.tar.gz</download_link>
      <date>1427063581</date>
      <mdhash>35005bb022ff0fa2497987d8773f8028</mdhash>
      <filesize>11098</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35005bb022ff0fa2497987d8773f8028</md5>
          <size>11098</size>
          <filedate>1427063581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>39464e5b4eea81be7db11eae674be8ba</md5>
          <size>12894</size>
          <filedate>1427063581</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.9.tar.gz</download_link>
      <date>1432129285</date>
      <mdhash>f5315e4e9cb37fac044fa23357abbc8a</mdhash>
      <filesize>10759</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f5315e4e9cb37fac044fa23357abbc8a</md5>
          <size>10759</size>
          <filedate>1432129285</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>a06a1c554bfaa08bed096501457cc785</md5>
          <size>12371</size>
          <filedate>1432129285</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.8.tar.gz</download_link>
      <date>1386236006</date>
      <mdhash>e808dd46535e408e860c8d9169bac6a0</mdhash>
      <filesize>10617</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e808dd46535e408e860c8d9169bac6a0</md5>
          <size>10617</size>
          <filedate>1386236006</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>d3c12b4e72722aa7f6b1f5c436802de1</md5>
          <size>12085</size>
          <filedate>1386236006</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.7.tar.gz</download_link>
      <date>1382619927</date>
      <mdhash>9f8c6ff0ccaf20a50f37799ce4ce2554</mdhash>
      <filesize>10603</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9f8c6ff0ccaf20a50f37799ce4ce2554</md5>
          <size>10603</size>
          <filedate>1382619927</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b19612b1786a7663d643ad6b63536be</md5>
          <size>12083</size>
          <filedate>1382619927</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.6.tar.gz</download_link>
      <date>1350408114</date>
      <mdhash>1eeb6a7bade7f8b20c92119b2b4342c3</mdhash>
      <filesize>9951</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1eeb6a7bade7f8b20c92119b2b4342c3</md5>
          <size>9951</size>
          <filedate>1350408114</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>72fe9c50f39492c1be91c118ddf4e608</md5>
          <size>11110</size>
          <filedate>1350408114</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.5.tar.gz</download_link>
      <date>1344830852</date>
      <mdhash>283d5ac589a338323ef59dcfafe344cc</mdhash>
      <filesize>9790</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>283d5ac589a338323ef59dcfafe344cc</md5>
          <size>9790</size>
          <filedate>1344830852</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>9af245ee6be41bb3fc95b058f64625e1</md5>
          <size>10926</size>
          <filedate>1344830852</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.4.tar.gz</download_link>
      <date>1330905952</date>
      <mdhash>c8270c1b4d6589b163bee3a579019653</mdhash>
      <filesize>8883</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c8270c1b4d6589b163bee3a579019653</md5>
          <size>8883</size>
          <filedate>1330905952</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>397729e1993a7c460fed37ee65a3343b</md5>
          <size>10001</size>
          <filedate>1330905952</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.3.tar.gz</download_link>
      <date>1329702947</date>
      <mdhash>b425debf4fd5d1fe7ccca5fc52528092</mdhash>
      <filesize>8860</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b425debf4fd5d1fe7ccca5fc52528092</md5>
          <size>8860</size>
          <filedate>1329702947</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc7a2fab6d4ab7b0e81b1d30703a02aa</md5>
          <size>9976</size>
          <filedate>1329702947</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.2.tar.gz</download_link>
      <date>1328154945</date>
      <mdhash>5c87d0c1d4846a6424812757fe78f5db</mdhash>
      <filesize>8863</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5c87d0c1d4846a6424812757fe78f5db</md5>
          <size>8863</size>
          <filedate>1328154945</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3a72dedf0db808f540a1476d503b8709</md5>
          <size>9973</size>
          <filedate>1328154945</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.1.tar.gz</download_link>
      <date>1328154344</date>
      <mdhash>ed3548a37a7ba450d1b1c64249ba0970</mdhash>
      <filesize>8834</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ed3548a37a7ba450d1b1c64249ba0970</md5>
          <size>8834</size>
          <filedate>1328154344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>71dfab6e410401eeb5748b371f98c2be</md5>
          <size>9943</size>
          <filedate>1328154344</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0.tar.gz</download_link>
      <date>1312258023</date>
      <mdhash>9faf7858eda0757029c0c04a06bb0e3b</mdhash>
      <filesize>7840</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9faf7858eda0757029c0c04a06bb0e3b</md5>
          <size>7840</size>
          <filedate>1312258023</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9405814678cde6c9aaf3b6ded21d35a3</md5>
          <size>9104</size>
          <filedate>1312258023</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0-beta1.tar.gz</download_link>
      <date>1309739252</date>
      <mdhash>359dbf619061767452e5ec2b0dbd6eac</mdhash>
      <filesize>7163</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>359dbf619061767452e5ec2b0dbd6eac</md5>
          <size>7163</size>
          <filedate>1309739252</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1082367033410eae37b484d682c5fa89</md5>
          <size>7992</size>
          <filedate>1309739252</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>language_cookie 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.x-dev.tar.gz</download_link>
      <date>1447080839</date>
      <mdhash>0218f0b0898515fc2cc1756ed4be0fe3</mdhash>
      <filesize>11242</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0218f0b0898515fc2cc1756ed4be0fe3</md5>
          <size>11242</size>
          <filedate>1447080839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d98e291cf45968159f8401a41159bfd6</md5>
          <size>13063</size>
          <filedate>1447080839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>language_cookie 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/language_cookie/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.x-dev.tar.gz</download_link>
      <date>1431982381</date>
      <mdhash>1eb5cde0a1d210d8cca657e79c73bed8</mdhash>
      <filesize>10760</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1eb5cde0a1d210d8cca657e79c73bed8</md5>
          <size>10760</size>
          <filedate>1431982381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/language_cookie-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>90970b810fcc620208909b23ab6de2ef</md5>
          <size>12378</size>
          <filedate>1431982381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
