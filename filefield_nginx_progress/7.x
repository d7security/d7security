<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>FileField Nginx Progress</title>
  <short_name>filefield_nginx_progress</short_name>
  <dc:creator>smoothify</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/filefield_nginx_progress</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>filefield_nginx_progress 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/filefield_nginx_progress/-/releases/7.x-2.4</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66136762/packages/generic/filefield_nginx_progress/7.x-2.4/filefield_nginx_progress-7.x-2.4.tar.gz</download_link>
      <date>1736941368</date>
      <mdhash>9078567ceda8ecbe7f0d081beeb1e8f8</mdhash>
      <filesize>11181</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66136762/packages/generic/filefield_nginx_progress/7.x-2.4/filefield_nginx_progress-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9078567ceda8ecbe7f0d081beeb1e8f8</md5>
          <size>11181</size>
          <filedate>1736941368</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66136762/packages/generic/filefield_nginx_progress/7.x-2.4/filefield_nginx_progress-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>989a51dcc2a83afc7a450dbeb0dd7f0a</md5>
          <size>13056</size>
          <filedate>1736941368</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>filefield_nginx_progress 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_nginx_progress/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.3.tar.gz</download_link>
      <date>1350833210</date>
      <mdhash>ee2f3ca1e940eeee3b55ac9ca09258b2</mdhash>
      <filesize>11083</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee2f3ca1e940eeee3b55ac9ca09258b2</md5>
          <size>11083</size>
          <filedate>1350833210</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>34da8375bcb6682f58580c96a339ebaa</md5>
          <size>12550</size>
          <filedate>1350833210</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_nginx_progress 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_nginx_progress/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.1.tar.gz</download_link>
      <date>1350828109</date>
      <mdhash>10c1b42980190faf9c3c8a2a20bd5ccd</mdhash>
      <filesize>11110</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10c1b42980190faf9c3c8a2a20bd5ccd</md5>
          <size>11110</size>
          <filedate>1350828109</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e92c2ea0ed54fd42e0f5d2655a242b02</md5>
          <size>12549</size>
          <filedate>1350828109</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_nginx_progress 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_nginx_progress/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.0.tar.gz</download_link>
      <date>1350825412</date>
      <mdhash>927c6236c0ed0e4764efe76c2e93df2f</mdhash>
      <filesize>11179</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>927c6236c0ed0e4764efe76c2e93df2f</md5>
          <size>11179</size>
          <filedate>1350825412</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b3132a08ef1ccbf2ecff47f9e67fd1f1</md5>
          <size>12642</size>
          <filedate>1350825412</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_nginx_progress 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_nginx_progress/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.x-dev.tar.gz</download_link>
      <date>1380579093</date>
      <mdhash>5b5d87e366c3c87e735209f50ed9597a</mdhash>
      <filesize>11097</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5b5d87e366c3c87e735209f50ed9597a</md5>
          <size>11097</size>
          <filedate>1380579093</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>0e7476af618e7168acde9c0e361e7b32</md5>
          <size>12555</size>
          <filedate>1380579094</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_nginx_progress 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>master</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_nginx_progress/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-1.x-dev.tar.gz</download_link>
      <date>1380579097</date>
      <mdhash>dec045ca5b9ffd535815801548d7c4c1</mdhash>
      <filesize>10680</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dec045ca5b9ffd535815801548d7c4c1</md5>
          <size>10680</size>
          <filedate>1380579097</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_nginx_progress-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8ddb22e099d012ad072354b29eb557f7</md5>
          <size>12255</size>
          <filedate>1380579097</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
