<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Global Redirect</title>
  <short_name>globalredirect</short_name>
  <dc:creator>nicholasthompson</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/globalredirect</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site search</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>globalredirect 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6.tar.gz</download_link>
      <date>1518719580</date>
      <mdhash>8dc2f7e68616d7c2d7543a47cdb49c8c</mdhash>
      <filesize>20517</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8dc2f7e68616d7c2d7543a47cdb49c8c</md5>
          <size>20517</size>
          <filedate>1518719580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>dfbbdd3facdc4427d8e3c566892a5505</md5>
          <size>23515</size>
          <filedate>1518719580</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.6-beta1</name>
      <version>7.x-1.6-beta1</version>
      <tag>7.x-1.6-beta1</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.6-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6-beta1.tar.gz</download_link>
      <date>1510231385</date>
      <mdhash>20ceae8f28bd8efd90c751e7dd7136d6</mdhash>
      <filesize>20444</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20ceae8f28bd8efd90c751e7dd7136d6</md5>
          <size>20444</size>
          <filedate>1510231385</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.6-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a6c12aa02b989a7655f63dee242142c1</md5>
          <size>23410</size>
          <filedate>1510231385</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.5.tar.gz</download_link>
      <date>1339748779</date>
      <mdhash>11ad79ee4607b0939984e5c041e1a935</mdhash>
      <filesize>19685</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11ad79ee4607b0939984e5c041e1a935</md5>
          <size>19685</size>
          <filedate>1339748779</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>47d3d2e9a3017d53f1c8bed557ac0666</md5>
          <size>21962</size>
          <filedate>1339748779</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.4.tar.gz</download_link>
      <date>1324428083</date>
      <mdhash>f836c1db4887da48de8904d1c57d817a</mdhash>
      <filesize>18859</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f836c1db4887da48de8904d1c57d817a</md5>
          <size>18859</size>
          <filedate>1324428083</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9572d9d4563980dbd3420016edd9bd46</md5>
          <size>21151</size>
          <filedate>1324428083</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3.tar.gz</download_link>
      <date>1294242955</date>
      <mdhash>95f6a5eb77c2be9c60eee41d6c75aefd</mdhash>
      <filesize>20314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>95f6a5eb77c2be9c60eee41d6c75aefd</md5>
          <size>20314</size>
          <filedate>1294242955</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7629d2244ce88d47f42c7cca9ead30d1</md5>
          <size>28861</size>
          <filedate>1294242955</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.3-alpha1</name>
      <version>7.x-1.3-alpha1</version>
      <tag>7.x-1.3-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.3-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3-alpha1.tar.gz</download_link>
      <date>1278982812</date>
      <mdhash>acd27c2936eaf86f744d7f753f051669</mdhash>
      <filesize>19913</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>acd27c2936eaf86f744d7f753f051669</md5>
          <size>19913</size>
          <filedate>1278982812</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.3-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>52c19bee339ba7164761c0090fc7fb25</md5>
          <size>28200</size>
          <filedate>1293231972</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>globalredirect 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/globalredirect/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.x-dev.tar.gz</download_link>
      <date>1532000881</date>
      <mdhash>4c3591dcc8ef80e3fb40ee6c3223c223</mdhash>
      <filesize>20570</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c3591dcc8ef80e3fb40ee6c3223c223</md5>
          <size>20570</size>
          <filedate>1532000881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/globalredirect-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>db0cb7c0c467b93e97c467baf3841c90</md5>
          <size>23576</size>
          <filedate>1532000881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
