<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Better Formats</title>
  <short_name>better_formats</short_name>
  <dc:creator>dragonwize</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/better_formats</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>better_formats 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0.tar.gz</download_link>
      <date>1698874149</date>
      <mdhash>cfb3ccdfae4b1e10885cb77fd7da2230</mdhash>
      <filesize>18347</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cfb3ccdfae4b1e10885cb77fd7da2230</md5>
          <size>18347</size>
          <filedate>1698874149</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9590afbb882f51b9e5d06f8dbf2f2ba3</md5>
          <size>21032</size>
          <filedate>1698874149</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_formats 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-rc1.tar.gz</download_link>
      <date>1690920215</date>
      <mdhash>f254bd9ef23a89451fdb6c5a1656cc90</mdhash>
      <filesize>18342</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f254bd9ef23a89451fdb6c5a1656cc90</md5>
          <size>18342</size>
          <filedate>1690920215</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>31d1afeb00facf54835b65c37db030b6</md5>
          <size>21033</size>
          <filedate>1690920215</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta4.tar.gz</download_link>
      <date>1681734565</date>
      <mdhash>35aa24838b664389b0c0383ddcc1e4a6</mdhash>
      <filesize>18324</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35aa24838b664389b0c0383ddcc1e4a6</md5>
          <size>18324</size>
          <filedate>1681734565</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6cbc07038ce7c385129cad5e69bb048</md5>
          <size>21010</size>
          <filedate>1681734565</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta3.tar.gz</download_link>
      <date>1675043897</date>
      <mdhash>58146aaf03bb9c43262ec6c8a47e841f</mdhash>
      <filesize>18333</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>58146aaf03bb9c43262ec6c8a47e841f</md5>
          <size>18333</size>
          <filedate>1675043897</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a36ab5cbcfc671a8a5c6a697844695e</md5>
          <size>21008</size>
          <filedate>1675043897</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta2.tar.gz</download_link>
      <date>1675043448</date>
      <mdhash>a1d4a4ab0ac28aa9f7b0bee5a537c385</mdhash>
      <filesize>18332</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a1d4a4ab0ac28aa9f7b0bee5a537c385</md5>
          <size>18332</size>
          <filedate>1675043448</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>761d06f5147a0868ef42eaab3ad9466e</md5>
          <size>21010</size>
          <filedate>1675043448</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta1.tar.gz</download_link>
      <date>1675041471</date>
      <mdhash>1c9a9ef9d09bc84599e650f8409cb95c</mdhash>
      <filesize>14996</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1c9a9ef9d09bc84599e650f8409cb95c</md5>
          <size>14996</size>
          <filedate>1675041471</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>55308606389e23618224a1fc94bfd220</md5>
          <size>17373</size>
          <filedate>1675041471</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.2.tar.gz</download_link>
      <date>1698873984</date>
      <mdhash>fe7439285f2f974cab9f3465f1a20ac9</mdhash>
      <filesize>18321</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fe7439285f2f974cab9f3465f1a20ac9</md5>
          <size>18321</size>
          <filedate>1698873984</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fa0cf83eccd1b71620f72efa67e8ea12</md5>
          <size>21016</size>
          <filedate>1698873984</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_formats 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.1.tar.gz</download_link>
      <date>1698873410</date>
      <mdhash>df3397b9ad791454c4a70664dc2a28a9</mdhash>
      <filesize>18339</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df3397b9ad791454c4a70664dc2a28a9</md5>
          <size>18339</size>
          <filedate>1698873410</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>af107a212d5f952afdabe7ed3cd6ddb6</md5>
          <size>21025</size>
          <filedate>1698873410</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0.tar.gz</download_link>
      <date>1695740459</date>
      <mdhash>112f09499837eea15dfc1f160476af8b</mdhash>
      <filesize>18318</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>112f09499837eea15dfc1f160476af8b</md5>
          <size>18318</size>
          <filedate>1695740459</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>ef930d52a3cc933cc8be85bfc597abd5</md5>
          <size>20997</size>
          <filedate>1695740459</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta6.tar.gz</download_link>
      <date>1683612743</date>
      <mdhash>94d1c9aaa7969c69392c63069d68c850</mdhash>
      <filesize>18315</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94d1c9aaa7969c69392c63069d68c850</md5>
          <size>18315</size>
          <filedate>1683612743</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>ec65ebdef4d790cb43d785aac4ff24c6</md5>
          <size>21002</size>
          <filedate>1683612743</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta5.tar.gz</download_link>
      <date>1675044029</date>
      <mdhash>a8f520484de5da3d2ef7d0e8ca535b2a</mdhash>
      <filesize>18312</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a8f520484de5da3d2ef7d0e8ca535b2a</md5>
          <size>18312</size>
          <filedate>1675044029</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>b637e5a38e43872c48862c10d66259e3</md5>
          <size>20999</size>
          <filedate>1675044029</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta4.tar.gz</download_link>
      <date>1675043275</date>
      <mdhash>1069b744fa0e89b2b5a03560e5325338</mdhash>
      <filesize>18313</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1069b744fa0e89b2b5a03560e5325338</md5>
          <size>18313</size>
          <filedate>1675043275</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>298e1d9e84fb07d8230cf98d0a0cd3a2</md5>
          <size>21002</size>
          <filedate>1675043275</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta3.tar.gz</download_link>
      <date>1675041315</date>
      <mdhash>31d1c630440c6b28aa501a08c36eeac5</mdhash>
      <filesize>14984</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>31d1c630440c6b28aa501a08c36eeac5</md5>
          <size>14984</size>
          <filedate>1675041315</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0dba0ccdc067ab138091ef197a18009</md5>
          <size>17365</size>
          <filedate>1675041315</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta2.tar.gz</download_link>
      <date>1453922939</date>
      <mdhash>4e5425519c2a1c178b6feee6a04b5e8d</mdhash>
      <filesize>14695</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e5425519c2a1c178b6feee6a04b5e8d</md5>
          <size>14695</size>
          <filedate>1453922939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9b68aaaf09eb892ea46d3fd5a1b6ad6b</md5>
          <size>16872</size>
          <filedate>1453922939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta1.tar.gz</download_link>
      <date>1343262404</date>
      <mdhash>563f07fbdcceedfd3d9403ff4f4657f8</mdhash>
      <filesize>14679</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>563f07fbdcceedfd3d9403ff4f4657f8</md5>
          <size>14679</size>
          <filedate>1343262404</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>186ad57aecdb1d5812a1c4d8551c4450</md5>
          <size>16724</size>
          <filedate>1343262404</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-2.x-dev.tar.gz</download_link>
      <date>1690919859</date>
      <mdhash>6dc37170043a81a3cfc12cef4962eb4b</mdhash>
      <filesize>18351</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6dc37170043a81a3cfc12cef4962eb4b</md5>
          <size>18351</size>
          <filedate>1690919859</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>33a853f9ffa288514d04f0785fcc42d1</md5>
          <size>21041</size>
          <filedate>1690919859</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_formats 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_formats/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_formats-7.x-1.x-dev.tar.gz</download_link>
      <date>1698873854</date>
      <mdhash>19bea0ef0c5d20af5a572d7b1e5d7637</mdhash>
      <filesize>18325</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>19bea0ef0c5d20af5a572d7b1e5d7637</md5>
          <size>18325</size>
          <filedate>1698873854</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_formats-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f3d1453cc67da656b5852a6cf68b7b1</md5>
          <size>21022</size>
          <filedate>1698873854</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
