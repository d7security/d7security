<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Webform</title>
  <short_name>webform</short_name>
  <dc:creator>jrockowitz</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/webform</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>User engagement</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>webform 7.x-4.27</name>
      <version>7.x-4.27</version>
      <tag>7.x-4.27</tag>
      <version_major>4</version_major>
      <version_patch>27</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.27</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.27.tar.gz</download_link>
      <date>1733867207</date>
      <mdhash>77677cc271ca83a057d6a6963748db4c</mdhash>
      <filesize>255617</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.27.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>77677cc271ca83a057d6a6963748db4c</md5>
          <size>255617</size>
          <filedate>1733867207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.27.zip</url>
          <archive_type>zip</archive_type>
          <md5>45005145cd67f9848e8d9ac7c05ecc4d</md5>
          <size>307878</size>
          <filedate>1733867207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.27-rc1</name>
      <version>7.x-4.27-rc1</version>
      <tag>7.x-4.27-rc1</tag>
      <version_major>4</version_major>
      <version_patch>27</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.27-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.27-rc1.tar.gz</download_link>
      <date>1732647386</date>
      <mdhash>a221a1fbf08b0d2bf85ab593746f5b1b</mdhash>
      <filesize>255619</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.27-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a221a1fbf08b0d2bf85ab593746f5b1b</md5>
          <size>255619</size>
          <filedate>1732647386</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.27-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4a1df96fe6ed9d37a09290dfac32e65d</md5>
          <size>307881</size>
          <filedate>1732647386</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.26</name>
      <version>7.x-4.26</version>
      <tag>7.x-4.26</tag>
      <version_major>4</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.26.tar.gz</download_link>
      <date>1695923868</date>
      <mdhash>ca944984f2c0ba97fd901d0923dcdc49</mdhash>
      <filesize>255750</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ca944984f2c0ba97fd901d0923dcdc49</md5>
          <size>255750</size>
          <filedate>1695923868</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>8accfc48c652b947be6f44522be02fef</md5>
          <size>307873</size>
          <filedate>1695923868</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.26-rc1</name>
      <version>7.x-4.26-rc1</version>
      <tag>7.x-4.26-rc1</tag>
      <version_major>4</version_major>
      <version_patch>26</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.26-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.26-rc1.tar.gz</download_link>
      <date>1692375353</date>
      <mdhash>9291682382735c5d9959b74ed24ba19c</mdhash>
      <filesize>255752</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.26-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9291682382735c5d9959b74ed24ba19c</md5>
          <size>255752</size>
          <filedate>1692375353</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.26-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3b285e0cba74efd869dacc3b56d617eb</md5>
          <size>307876</size>
          <filedate>1692375353</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.25</name>
      <version>7.x-4.25</version>
      <tag>7.x-4.25</tag>
      <version_major>4</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.25.tar.gz</download_link>
      <date>1675119235</date>
      <mdhash>1844c950ded6b302acee7135c175f10c</mdhash>
      <filesize>255603</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1844c950ded6b302acee7135c175f10c</md5>
          <size>255603</size>
          <filedate>1675119235</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>d8f248b5b2c754dc68fd03a4fc81fe71</md5>
          <size>307859</size>
          <filedate>1675119235</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.25-rc1</name>
      <version>7.x-4.25-rc1</version>
      <tag>7.x-4.25-rc1</tag>
      <version_major>4</version_major>
      <version_patch>25</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.25-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.25-rc1.tar.gz</download_link>
      <date>1673666912</date>
      <mdhash>dce844f712d29d3620a355b8c25c44de</mdhash>
      <filesize>255600</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.25-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dce844f712d29d3620a355b8c25c44de</md5>
          <size>255600</size>
          <filedate>1673666912</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.25-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>06a2db05480c48836baf97011b35dbde</md5>
          <size>307865</size>
          <filedate>1673666912</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.24</name>
      <version>7.x-4.24</version>
      <tag>7.x-4.24</tag>
      <version_major>4</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.24.tar.gz</download_link>
      <date>1621619416</date>
      <mdhash>8b22efa7f015c90f8610d2f957997e2b</mdhash>
      <filesize>255788</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b22efa7f015c90f8610d2f957997e2b</md5>
          <size>255788</size>
          <filedate>1621619416</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>26e4b0120ece2948bcdacd4b79d17733</md5>
          <size>307875</size>
          <filedate>1621619416</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.24-rc1</name>
      <version>7.x-4.24-rc1</version>
      <tag>7.x-4.24-rc1</tag>
      <version_major>4</version_major>
      <version_patch>24</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.24-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.24-rc1.tar.gz</download_link>
      <date>1619802403</date>
      <mdhash>bce8ab4be6ca1130d733b4ada8d71a04</mdhash>
      <filesize>255794</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.24-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bce8ab4be6ca1130d733b4ada8d71a04</md5>
          <size>255794</size>
          <filedate>1619802403</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.24-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c0c6594cbcf1c28b4a050e50c83fbbb</md5>
          <size>307878</size>
          <filedate>1619802403</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.23</name>
      <version>7.x-4.23</version>
      <tag>7.x-4.23</tag>
      <version_major>4</version_major>
      <version_patch>23</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.23</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.23.tar.gz</download_link>
      <date>1591623584</date>
      <mdhash>95f3feb012284a7ec30aeb59ed2f3be8</mdhash>
      <filesize>255490</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.23.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>95f3feb012284a7ec30aeb59ed2f3be8</md5>
          <size>255490</size>
          <filedate>1591623584</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.23.zip</url>
          <archive_type>zip</archive_type>
          <md5>6bda43d064efa53d9114b742dc1f6a0e</md5>
          <size>307568</size>
          <filedate>1591623584</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.23-rc1</name>
      <version>7.x-4.23-rc1</version>
      <tag>7.x-4.23-rc1</tag>
      <version_major>4</version_major>
      <version_patch>23</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.23-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.23-rc1.tar.gz</download_link>
      <date>1590417675</date>
      <mdhash>2d395b8ca7c7b7f45840a6e18d72bc0c</mdhash>
      <filesize>255494</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.23-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2d395b8ca7c7b7f45840a6e18d72bc0c</md5>
          <size>255494</size>
          <filedate>1590417675</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.23-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>41ae7701d22b300dcd230b4448979be2</md5>
          <size>307571</size>
          <filedate>1590417675</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.22</name>
      <version>7.x-4.22</version>
      <tag>7.x-4.22</tag>
      <version_major>4</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.22.tar.gz</download_link>
      <date>1581709520</date>
      <mdhash>a310129fb2e420c8cc852de174dc1d15</mdhash>
      <filesize>253131</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a310129fb2e420c8cc852de174dc1d15</md5>
          <size>253131</size>
          <filedate>1581709520</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>af38afe0b8a715605b91fdc7175bcc09</md5>
          <size>305128</size>
          <filedate>1581709520</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.22-rc1</name>
      <version>7.x-4.22-rc1</version>
      <tag>7.x-4.22-rc1</tag>
      <version_major>4</version_major>
      <version_patch>22</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.22-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.22-rc1.tar.gz</download_link>
      <date>1580484593</date>
      <mdhash>e9fe8c6ea6818368031270c379aef416</mdhash>
      <filesize>253131</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.22-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e9fe8c6ea6818368031270c379aef416</md5>
          <size>253131</size>
          <filedate>1580484593</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.22-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ede6a52a23926c5b669ed059bacc5eb</md5>
          <size>305132</size>
          <filedate>1580484593</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.21</name>
      <version>7.x-4.21</version>
      <tag>7.x-4.21</tag>
      <version_major>4</version_major>
      <version_patch>21</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.21</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.21.tar.gz</download_link>
      <date>1576092784</date>
      <mdhash>c405d4d20c5a4c7f2e7b7efe27715a7e</mdhash>
      <filesize>252120</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.21.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c405d4d20c5a4c7f2e7b7efe27715a7e</md5>
          <size>252120</size>
          <filedate>1576092784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.21.zip</url>
          <archive_type>zip</archive_type>
          <md5>e2f98f7148d55ee1e3d93ee21d4f1872</md5>
          <size>304239</size>
          <filedate>1576092784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.20</name>
      <version>7.x-4.20</version>
      <tag>7.x-4.20</tag>
      <version_major>4</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.20.tar.gz</download_link>
      <date>1565386985</date>
      <mdhash>1657c76c0a8a3d01d22dcf7bbd2fdb82</mdhash>
      <filesize>252002</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1657c76c0a8a3d01d22dcf7bbd2fdb82</md5>
          <size>252002</size>
          <filedate>1565386985</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>f909a677190bedb28f26eb2d6750fe5d</md5>
          <size>304105</size>
          <filedate>1565386985</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.20-rc1</name>
      <version>7.x-4.20-rc1</version>
      <tag>7.x-4.20-rc1</tag>
      <version_major>4</version_major>
      <version_patch>20</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.20-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.20-rc1.tar.gz</download_link>
      <date>1564000686</date>
      <mdhash>0d7f7fdf95e328e4c2d8c8887f3110ca</mdhash>
      <filesize>252002</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.20-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d7f7fdf95e328e4c2d8c8887f3110ca</md5>
          <size>252002</size>
          <filedate>1564000686</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.20-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1328cf7bfcc52a4571172ea1ea388327</md5>
          <size>304108</size>
          <filedate>1564000686</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.19</name>
      <version>7.x-4.19</version>
      <tag>7.x-4.19</tag>
      <version_major>4</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.19.tar.gz</download_link>
      <date>1546876980</date>
      <mdhash>63b7df8f1be52e9d0b33a6c2f7b8c278</mdhash>
      <filesize>250218</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>63b7df8f1be52e9d0b33a6c2f7b8c278</md5>
          <size>250218</size>
          <filedate>1546876980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b10eb52e2fed5f1b77b4a36733b6b6d</md5>
          <size>301947</size>
          <filedate>1546876980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.19-rc1</name>
      <version>7.x-4.19-rc1</version>
      <tag>7.x-4.19-rc1</tag>
      <version_major>4</version_major>
      <version_patch>19</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.19-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.19-rc1.tar.gz</download_link>
      <date>1545064680</date>
      <mdhash>7db3564ebc40b5ee178ffa53cd454b48</mdhash>
      <filesize>250218</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.19-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7db3564ebc40b5ee178ffa53cd454b48</md5>
          <size>250218</size>
          <filedate>1545064680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.19-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>19d69af8ff6521054b7df12ff47c1db5</md5>
          <size>301950</size>
          <filedate>1545064680</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.18</name>
      <version>7.x-4.18</version>
      <tag>7.x-4.18</tag>
      <version_major>4</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.18.tar.gz</download_link>
      <date>1538403780</date>
      <mdhash>f7e2031e9ae8cd692a9b36734758f4e5</mdhash>
      <filesize>249328</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f7e2031e9ae8cd692a9b36734758f4e5</md5>
          <size>249328</size>
          <filedate>1538403780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>a3b6d8ba9934425be9fd11ace625dbc0</md5>
          <size>300899</size>
          <filedate>1538403780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.18-rc1</name>
      <version>7.x-4.18-rc1</version>
      <tag>7.x-4.18-rc1</tag>
      <version_major>4</version_major>
      <version_patch>18</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.18-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.18-rc1.tar.gz</download_link>
      <date>1536947280</date>
      <mdhash>11af64f12a228d660784f83a7db56916</mdhash>
      <filesize>249333</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.18-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11af64f12a228d660784f83a7db56916</md5>
          <size>249333</size>
          <filedate>1536947280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.18-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>87cf1c877228711879ca8f43a0433389</md5>
          <size>300902</size>
          <filedate>1536947280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.17</name>
      <version>7.x-4.17</version>
      <tag>7.x-4.17</tag>
      <version_major>4</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.17.tar.gz</download_link>
      <date>1524581885</date>
      <mdhash>1181d7165909930f5483ef4d49746dda</mdhash>
      <filesize>247520</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1181d7165909930f5483ef4d49746dda</md5>
          <size>247520</size>
          <filedate>1524581885</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>ef4c9acb7d254a224161e97a4804865c</md5>
          <size>298932</size>
          <filedate>1524581885</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.17-rc1</name>
      <version>7.x-4.17-rc1</version>
      <tag>7.x-4.17-rc1</tag>
      <version_major>4</version_major>
      <version_patch>17</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.17-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.17-rc1.tar.gz</download_link>
      <date>1523368685</date>
      <mdhash>4e3a918f7dfc9b0e9ae2f441338767d6</mdhash>
      <filesize>247524</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.17-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e3a918f7dfc9b0e9ae2f441338767d6</md5>
          <size>247524</size>
          <filedate>1523368685</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.17-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f6eb07b97faaa22e3ef4723dffa729c</md5>
          <size>298935</size>
          <filedate>1523368685</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.16</name>
      <version>7.x-4.16</version>
      <tag>7.x-4.16</tag>
      <version_major>4</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.16.tar.gz</download_link>
      <date>1507320544</date>
      <mdhash>d4d168b87b1ca12fb0d56126ac6aeb66</mdhash>
      <filesize>247838</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d4d168b87b1ca12fb0d56126ac6aeb66</md5>
          <size>247838</size>
          <filedate>1507320544</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>d72d8ca2b208a4dae20f7bf9e9ae3f56</md5>
          <size>299377</size>
          <filedate>1507320544</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.16-rc1</name>
      <version>7.x-4.16-rc1</version>
      <tag>7.x-4.16-rc1</tag>
      <version_major>4</version_major>
      <version_patch>16</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.16-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.16-rc1.tar.gz</download_link>
      <date>1505495644</date>
      <mdhash>0c45832337e5240efac9eac1ef56e3a4</mdhash>
      <filesize>247808</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.16-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0c45832337e5240efac9eac1ef56e3a4</md5>
          <size>247808</size>
          <filedate>1505495644</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.16-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>30a4bbcd15594515f3a2b0ff79f0e6c6</md5>
          <size>299333</size>
          <filedate>1505495644</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.15</name>
      <version>7.x-4.15</version>
      <tag>7.x-4.15</tag>
      <version_major>4</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.15.tar.gz</download_link>
      <date>1492107243</date>
      <mdhash>12556ecbae9cb7bf39d768d2bbd29694</mdhash>
      <filesize>247725</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>12556ecbae9cb7bf39d768d2bbd29694</md5>
          <size>247725</size>
          <filedate>1492107243</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>012b449a5510f375f5ebb46574707081</md5>
          <size>298908</size>
          <filedate>1492107243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.15-rc1</name>
      <version>7.x-4.15-rc1</version>
      <tag>7.x-4.15-rc1</tag>
      <version_major>4</version_major>
      <version_patch>15</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.15-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.15-rc1.tar.gz</download_link>
      <date>1490884682</date>
      <mdhash>4a0871210035b354cdaf24a7af9a072c</mdhash>
      <filesize>247731</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.15-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a0871210035b354cdaf24a7af9a072c</md5>
          <size>247731</size>
          <filedate>1490884682</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.15-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2fec96eaec921b517ae1d09d28373ebc</md5>
          <size>298912</size>
          <filedate>1490884682</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.14</name>
      <version>7.x-4.14</version>
      <tag>7.x-4.14</tag>
      <version_major>4</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.14.tar.gz</download_link>
      <date>1472386739</date>
      <mdhash>9d28b1cbc3d380074b6926181543e445</mdhash>
      <filesize>244516</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9d28b1cbc3d380074b6926181543e445</md5>
          <size>244516</size>
          <filedate>1472386739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>f847b8b6bb586eeda84c43d6977ea00b</md5>
          <size>295749</size>
          <filedate>1472386739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.13</name>
      <version>7.x-4.13</version>
      <tag>7.x-4.13</tag>
      <version_major>4</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.13.tar.gz</download_link>
      <date>1469140439</date>
      <mdhash>dae91401fb44079b6370451bb72652eb</mdhash>
      <filesize>243560</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dae91401fb44079b6370451bb72652eb</md5>
          <size>243560</size>
          <filedate>1469140439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>f3959adc42595d6499dd76e175376c7a</md5>
          <size>294773</size>
          <filedate>1469140439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.12</name>
      <version>7.x-4.12</version>
      <tag>7.x-4.12</tag>
      <version_major>4</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.12.tar.gz</download_link>
      <date>1445622240</date>
      <mdhash>5eed02c0782f36b042c205f4229a076c</mdhash>
      <filesize>240545</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5eed02c0782f36b042c205f4229a076c</md5>
          <size>240545</size>
          <filedate>1445622240</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>96570c0e74e2b9ad8c6fc649a6745e15</md5>
          <size>291676</size>
          <filedate>1445622240</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.11</name>
      <version>7.x-4.11</version>
      <tag>7.x-4.11</tag>
      <version_major>4</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.11.tar.gz</download_link>
      <date>1442953139</date>
      <mdhash>319613b667722186ff0b22f0f0cf2f95</mdhash>
      <filesize>235259</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>319613b667722186ff0b22f0f0cf2f95</md5>
          <size>235259</size>
          <filedate>1442953139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>20e8d8493184e465a4b5cc254f4a5da9</md5>
          <size>286509</size>
          <filedate>1442953139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.10</name>
      <version>7.x-4.10</version>
      <tag>7.x-4.10</tag>
      <version_major>4</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.10.tar.gz</download_link>
      <date>1437076439</date>
      <mdhash>a8947025d47f177154511c7406a8534e</mdhash>
      <filesize>234255</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a8947025d47f177154511c7406a8534e</md5>
          <size>234255</size>
          <filedate>1437076439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>34b9a665e5fdd13ef733f15b2bbf7508</md5>
          <size>285399</size>
          <filedate>1437076439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.9</name>
      <version>7.x-4.9</version>
      <tag>7.x-4.9</tag>
      <version_major>4</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.9.tar.gz</download_link>
      <date>1432669981</date>
      <mdhash>7a4771c9fb06bad7d0e7b51048d36e57</mdhash>
      <filesize>231377</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7a4771c9fb06bad7d0e7b51048d36e57</md5>
          <size>231377</size>
          <filedate>1432669981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>c39ef3f02de8e098811067e9a04b644d</md5>
          <size>282218</size>
          <filedate>1432669981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.8</name>
      <version>7.x-4.8</version>
      <tag>7.x-4.8</tag>
      <version_major>4</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.8.tar.gz</download_link>
      <date>1430334481</date>
      <mdhash>dbd68b085357a4802c55edb03188e22c</mdhash>
      <filesize>230957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dbd68b085357a4802c55edb03188e22c</md5>
          <size>230957</size>
          <filedate>1430334481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b7eb5f5d7606f7c8eab021a3cfbd118</md5>
          <size>281477</size>
          <filedate>1430334481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.7</name>
      <version>7.x-4.7</version>
      <tag>7.x-4.7</tag>
      <version_major>4</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.7.tar.gz</download_link>
      <date>1427387581</date>
      <mdhash>708325315325143ecf872803bb8fc97e</mdhash>
      <filesize>212482</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>708325315325143ecf872803bb8fc97e</md5>
          <size>212482</size>
          <filedate>1427387581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>063f25092c70f5e182519849bf8bdd59</md5>
          <size>261334</size>
          <filedate>1427387581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.6</name>
      <version>7.x-4.6</version>
      <tag>7.x-4.6</tag>
      <version_major>4</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.6.tar.gz</download_link>
      <date>1427029681</date>
      <mdhash>4c434af84de80e9886d9a0014bbde4dc</mdhash>
      <filesize>212172</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c434af84de80e9886d9a0014bbde4dc</md5>
          <size>212172</size>
          <filedate>1427029681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c703584308ad14a87cf7b079b700886</md5>
          <size>260988</size>
          <filedate>1427029681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.5</name>
      <version>7.x-4.5</version>
      <tag>7.x-4.5</tag>
      <version_major>4</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.5.tar.gz</download_link>
      <date>1426611015</date>
      <mdhash>8b864d7fd7de0abcc11f213bc28dcc6b</mdhash>
      <filesize>208218</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b864d7fd7de0abcc11f213bc28dcc6b</md5>
          <size>208218</size>
          <filedate>1426611015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>0eb618344c78f121e64bff2cee5e5d8d</md5>
          <size>256462</size>
          <filedate>1426611015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.4</name>
      <version>7.x-4.4</version>
      <tag>7.x-4.4</tag>
      <version_major>4</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.4.tar.gz</download_link>
      <date>1425406981</date>
      <mdhash>7933b2eaa6bfd7648b490c6c1e34f56c</mdhash>
      <filesize>207645</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7933b2eaa6bfd7648b490c6c1e34f56c</md5>
          <size>207645</size>
          <filedate>1425406981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7e70d11e1390e6574cc4f7bfe969e8f6</md5>
          <size>255926</size>
          <filedate>1425406981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.3</name>
      <version>7.x-4.3</version>
      <tag>7.x-4.3</tag>
      <version_major>4</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.3.tar.gz</download_link>
      <date>1424096281</date>
      <mdhash>5ed6d060b3cd57ba6a0dc6aac19d31be</mdhash>
      <filesize>204589</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ed6d060b3cd57ba6a0dc6aac19d31be</md5>
          <size>204589</size>
          <filedate>1424096281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>64b0678350a95ece3982d552ffba3e3a</md5>
          <size>251858</size>
          <filedate>1424096281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.2</name>
      <version>7.x-4.2</version>
      <tag>7.x-4.2</tag>
      <version_major>4</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.2.tar.gz</download_link>
      <date>1416925848</date>
      <mdhash>a48a33c79d3831b748342b1836617a74</mdhash>
      <filesize>202048</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a48a33c79d3831b748342b1836617a74</md5>
          <size>202048</size>
          <filedate>1416925848</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6a1d40cfee6142461185907c14c66243</md5>
          <size>249262</size>
          <filedate>1416925848</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.1</name>
      <version>7.x-4.1</version>
      <tag>7.x-4.1</tag>
      <version_major>4</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.1.tar.gz</download_link>
      <date>1412681928</date>
      <mdhash>e33d9a244c41c7b1bc6df8af00e08b7a</mdhash>
      <filesize>193676</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e33d9a244c41c7b1bc6df8af00e08b7a</md5>
          <size>193676</size>
          <filedate>1412681928</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e3e4ac32156a4cbd532dd1e23db51e97</md5>
          <size>238054</size>
          <filedate>1412681928</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0.tar.gz</download_link>
      <date>1409365728</date>
      <mdhash>20c266a95b99abd24bd5dd3facba61a8</mdhash>
      <filesize>193699</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20c266a95b99abd24bd5dd3facba61a8</md5>
          <size>193699</size>
          <filedate>1409365728</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc8e7386e320ff13a628a152ca3c7fd8</md5>
          <size>238066</size>
          <filedate>1409365728</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc6</name>
      <version>7.x-4.0-rc6</version>
      <tag>7.x-4.0-rc6</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc6.tar.gz</download_link>
      <date>1408408128</date>
      <mdhash>d6c9af053bb448d13db9ed4bda7373bf</mdhash>
      <filesize>192838</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d6c9af053bb448d13db9ed4bda7373bf</md5>
          <size>192838</size>
          <filedate>1408408128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc6.zip</url>
          <archive_type>zip</archive_type>
          <md5>89ce834bd09dc00645ac1f880ab38e0d</md5>
          <size>237070</size>
          <filedate>1408408128</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc5</name>
      <version>7.x-4.0-rc5</version>
      <tag>7.x-4.0-rc5</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc5.tar.gz</download_link>
      <date>1406922827</date>
      <mdhash>376fdf4bac5d797aba46ef7c6e984dad</mdhash>
      <filesize>192690</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>376fdf4bac5d797aba46ef7c6e984dad</md5>
          <size>192690</size>
          <filedate>1406922827</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ed7a14ee095f7714daefde5429590bb</md5>
          <size>234721</size>
          <filedate>1406922827</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc4</name>
      <version>7.x-4.0-rc4</version>
      <tag>7.x-4.0-rc4</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc4.tar.gz</download_link>
      <date>1403835228</date>
      <mdhash>a7433fae49177e403a2f9ea391dbff40</mdhash>
      <filesize>190874</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a7433fae49177e403a2f9ea391dbff40</md5>
          <size>190874</size>
          <filedate>1403835228</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2cb55b3c198e0742fcefe1714fe5650e</md5>
          <size>232794</size>
          <filedate>1403835228</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc3</name>
      <version>7.x-4.0-rc3</version>
      <tag>7.x-4.0-rc3</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc3.tar.gz</download_link>
      <date>1398161327</date>
      <mdhash>dc88f99bb75f0ccb278c43ff288ee20d</mdhash>
      <filesize>189321</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc88f99bb75f0ccb278c43ff288ee20d</md5>
          <size>189321</size>
          <filedate>1398161327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9742c54c1ded6ff4ee8b8638f7bdeea4</md5>
          <size>231194</size>
          <filedate>1398161327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc2</name>
      <version>7.x-4.0-rc2</version>
      <tag>7.x-4.0-rc2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc2.tar.gz</download_link>
      <date>1398128627</date>
      <mdhash>83463fc7c1c09db64491821d6ec70245</mdhash>
      <filesize>189359</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83463fc7c1c09db64491821d6ec70245</md5>
          <size>189359</size>
          <filedate>1398128627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>28893c28d0a665773ee667f3e8a33189</md5>
          <size>231254</size>
          <filedate>1398128627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-rc1</name>
      <version>7.x-4.0-rc1</version>
      <tag>7.x-4.0-rc1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc1.tar.gz</download_link>
      <date>1397285353</date>
      <mdhash>f531bd82a3e8a218e8ba6b9418a272fe</mdhash>
      <filesize>188567</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f531bd82a3e8a218e8ba6b9418a272fe</md5>
          <size>188567</size>
          <filedate>1397285353</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>194af78ed50f82df1f31c7423498fb06</md5>
          <size>230389</size>
          <filedate>1397285353</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-beta3</name>
      <version>7.x-4.0-beta3</version>
      <tag>7.x-4.0-beta3</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta3.tar.gz</download_link>
      <date>1392366205</date>
      <mdhash>ed5a9f251b148852a8604d3a27d312d0</mdhash>
      <filesize>183926</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ed5a9f251b148852a8604d3a27d312d0</md5>
          <size>183926</size>
          <filedate>1392366205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a79544ecd7e24114c8855c6c0615d81f</md5>
          <size>225821</size>
          <filedate>1392366205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-beta2</name>
      <version>7.x-4.0-beta2</version>
      <tag>7.x-4.0-beta2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta2.tar.gz</download_link>
      <date>1392182008</date>
      <mdhash>8aed2f45bc2c66d21b197fe0e25ee268</mdhash>
      <filesize>182803</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8aed2f45bc2c66d21b197fe0e25ee268</md5>
          <size>182803</size>
          <filedate>1392182008</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>61b6752b1aa339108f8c520eedc8a9c8</md5>
          <size>224630</size>
          <filedate>1392182008</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-beta1</name>
      <version>7.x-4.0-beta1</version>
      <tag>7.x-4.0-beta1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta1.tar.gz</download_link>
      <date>1379945522</date>
      <mdhash>ad8186178f170001d34c20afa0cc794c</mdhash>
      <filesize>178805</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ad8186178f170001d34c20afa0cc794c</md5>
          <size>178805</size>
          <filedate>1379945522</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>67339bf02bde1aaf893f870b63d3c93b</md5>
          <size>222001</size>
          <filedate>1379945522</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha10</name>
      <version>7.x-4.0-alpha10</version>
      <tag>7.x-4.0-alpha10</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha10</release_link>
      <download_link/>
      <date>1376030931</date>
      <mdhash>0dbf55ba01a9fa44eca22a25da43cf88</mdhash>
      <filesize>175015</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>0dbf55ba01a9fa44eca22a25da43cf88</md5>
          <size>175015</size>
          <filedate>1376030931</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>6a4a7c229c5a14b170cfe4c4aad675d1</md5>
          <size>216671</size>
          <filedate>1376030931</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha9</name>
      <version>7.x-4.0-alpha9</version>
      <tag>7.x-4.0-alpha9</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha9.tar.gz</download_link>
      <date>1373869569</date>
      <mdhash>2119eb8d79bb27e70f34bb5ed8d28d5f</mdhash>
      <filesize>165143</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2119eb8d79bb27e70f34bb5ed8d28d5f</md5>
          <size>165143</size>
          <filedate>1373869569</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha9.zip</url>
          <archive_type>zip</archive_type>
          <md5>05a5d3c1b0945e23c6479dc5563e081e</md5>
          <size>203723</size>
          <filedate>1373869569</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha8</name>
      <version>7.x-4.0-alpha8</version>
      <tag>7.x-4.0-alpha8</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha8.tar.gz</download_link>
      <date>1371660352</date>
      <mdhash>f137cf2c0835affeb5c9da810507a67c</mdhash>
      <filesize>160206</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f137cf2c0835affeb5c9da810507a67c</md5>
          <size>160206</size>
          <filedate>1371660352</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha8.zip</url>
          <archive_type>zip</archive_type>
          <md5>e427761490282e187111b3ee48677e3d</md5>
          <size>198374</size>
          <filedate>1371660352</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha7</name>
      <version>7.x-4.0-alpha7</version>
      <tag>7.x-4.0-alpha7</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha7.tar.gz</download_link>
      <date>1371629090</date>
      <mdhash>1639e10d3bcdccddbeed2947a93fa127</mdhash>
      <filesize>160211</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1639e10d3bcdccddbeed2947a93fa127</md5>
          <size>160211</size>
          <filedate>1371629090</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>976fd3fe4d3b0e503cf8784e71af0c7e</md5>
          <size>198370</size>
          <filedate>1371629091</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha6</name>
      <version>7.x-4.0-alpha6</version>
      <tag>7.x-4.0-alpha6</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha6.tar.gz</download_link>
      <date>1347692127</date>
      <mdhash>75227a94b7db0ef7a500af2c2ebb7ab9</mdhash>
      <filesize>155734</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>75227a94b7db0ef7a500af2c2ebb7ab9</md5>
          <size>155734</size>
          <filedate>1347692127</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>081bd1ebcee97391274fa18b9cad2f66</md5>
          <size>193637</size>
          <filedate>1347692127</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha5</name>
      <version>7.x-4.0-alpha5</version>
      <tag>7.x-4.0-alpha5</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha5.tar.gz</download_link>
      <date>1347551761</date>
      <mdhash>5d02f0e052f7c49ffba4539b8bb4aef9</mdhash>
      <filesize>152419</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d02f0e052f7c49ffba4539b8bb4aef9</md5>
          <size>152419</size>
          <filedate>1347551761</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>f282c97bffa7e47bf7e4676366120a7c</md5>
          <size>188448</size>
          <filedate>1347551761</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha4</name>
      <version>7.x-4.0-alpha4</version>
      <tag>7.x-4.0-alpha4</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha4</release_link>
      <download_link/>
      <date>1343527082</date>
      <mdhash>e33d957ef4bc8f00f00d3a60aa21ac6c</mdhash>
      <filesize>152205</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>e33d957ef4bc8f00f00d3a60aa21ac6c</md5>
          <size>152205</size>
          <filedate>1343527082</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a67c43bc1234b7980d0d938f8a003410</md5>
          <size>188165</size>
          <filedate>1343527082</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha3</name>
      <version>7.x-4.0-alpha3</version>
      <tag>7.x-4.0-alpha3</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha3</release_link>
      <download_link/>
      <date>1339117034</date>
      <mdhash>39f02ba0cee0a419a38254075f966c6a</mdhash>
      <filesize>151352</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>39f02ba0cee0a419a38254075f966c6a</md5>
          <size>151352</size>
          <filedate>1339117034</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>670de512ec5e8e682d502f6ab9d3378e</md5>
          <size>187161</size>
          <filedate>1339117034</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha2</name>
      <version>7.x-4.0-alpha2</version>
      <tag>7.x-4.0-alpha2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha2.tar.gz</download_link>
      <date>1338609133</date>
      <mdhash>7fb52e64f354fb6875da48469a0d55af</mdhash>
      <filesize>150956</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7fb52e64f354fb6875da48469a0d55af</md5>
          <size>150956</size>
          <filedate>1338609133</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>df39ad60cb90996afc1fc560bbdb264b</md5>
          <size>186735</size>
          <filedate>1338609133</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.0-alpha1</name>
      <version>7.x-4.0-alpha1</version>
      <tag>7.x-4.0-alpha1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha1.tar.gz</download_link>
      <date>1337451095</date>
      <mdhash>512260ef02e1ce57ef43ec90683c1e78</mdhash>
      <filesize>135199</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>512260ef02e1ce57ef43ec90683c1e78</md5>
          <size>135199</size>
          <filedate>1337451095</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>34c0bfa47e24026cd688c4057312ac6d</md5>
          <size>169537</size>
          <filedate>1337451095</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.31</name>
      <version>7.x-3.31</version>
      <tag>7.x-3.31</tag>
      <version_major>3</version_major>
      <version_patch>31</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.31</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.31.tar.gz</download_link>
      <date>1621619470</date>
      <mdhash>214764c7e5693b35e84434bb7601ec76</mdhash>
      <filesize>140261</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.31.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>214764c7e5693b35e84434bb7601ec76</md5>
          <size>140261</size>
          <filedate>1621619470</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.31.zip</url>
          <archive_type>zip</archive_type>
          <md5>3084e2588ba66ed24b62ec076cdf52e1</md5>
          <size>174420</size>
          <filedate>1621619470</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.31-rc1</name>
      <version>7.x-3.31-rc1</version>
      <tag>7.x-3.31-rc1</tag>
      <version_major>3</version_major>
      <version_patch>31</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.31-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.31-rc1.tar.gz</download_link>
      <date>1619802444</date>
      <mdhash>1daae85466bbf0a177ac583981367b1c</mdhash>
      <filesize>140267</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.31-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1daae85466bbf0a177ac583981367b1c</md5>
          <size>140267</size>
          <filedate>1619802444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.31-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>00416053416ea36fb53b5b813d7dea74</md5>
          <size>174422</size>
          <filedate>1619802444</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.30</name>
      <version>7.x-3.30</version>
      <tag>7.x-3.30</tag>
      <version_major>3</version_major>
      <version_patch>30</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.30</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.30.tar.gz</download_link>
      <date>1580485833</date>
      <mdhash>01ae6bf49db60a96eccce1fef3fcdf49</mdhash>
      <filesize>140197</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.30.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>01ae6bf49db60a96eccce1fef3fcdf49</md5>
          <size>140197</size>
          <filedate>1580485833</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.30.zip</url>
          <archive_type>zip</archive_type>
          <md5>0c22eb32947f6f480f55d92f7d0c969a</md5>
          <size>174342</size>
          <filedate>1580485833</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.30-rc1</name>
      <version>7.x-3.30-rc1</version>
      <tag>7.x-3.30-rc1</tag>
      <version_major>3</version_major>
      <version_patch>30</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.30-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.30-rc1.tar.gz</download_link>
      <date>1579277888</date>
      <mdhash>3407f93119339c932d5545a3889b331b</mdhash>
      <filesize>140205</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.30-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3407f93119339c932d5545a3889b331b</md5>
          <size>140205</size>
          <filedate>1579277888</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.30-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1158f7f3a5e337c8f1f53d2b9c81a920</md5>
          <size>174344</size>
          <filedate>1579277888</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.29</name>
      <version>7.x-3.29</version>
      <tag>7.x-3.29</tag>
      <version_major>3</version_major>
      <version_patch>29</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.29</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.29.tar.gz</download_link>
      <date>1576092784</date>
      <mdhash>70c27c2497f27d2dce90090f9b4a602a</mdhash>
      <filesize>139999</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.29.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>70c27c2497f27d2dce90090f9b4a602a</md5>
          <size>139999</size>
          <filedate>1576092784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.29.zip</url>
          <archive_type>zip</archive_type>
          <md5>e9c21eddda371f985e3b9f5d734ab414</md5>
          <size>174146</size>
          <filedate>1576092784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.28</name>
      <version>7.x-3.28</version>
      <tag>7.x-3.28</tag>
      <version_major>3</version_major>
      <version_patch>28</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.28</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.28.tar.gz</download_link>
      <date>1575911280</date>
      <mdhash>228da29406ca0e54bf817ce22ffc65fe</mdhash>
      <filesize>139943</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.28.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>228da29406ca0e54bf817ce22ffc65fe</md5>
          <size>139943</size>
          <filedate>1575911280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.28.zip</url>
          <archive_type>zip</archive_type>
          <md5>2111ec6f6671c57923da63a5b51332a3</md5>
          <size>174084</size>
          <filedate>1575911280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.28-rc1</name>
      <version>7.x-3.28-rc1</version>
      <tag>7.x-3.28-rc1</tag>
      <version_major>3</version_major>
      <version_patch>28</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.28-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.28-rc1.tar.gz</download_link>
      <date>1574701085</date>
      <mdhash>b8d433e538d91709f1bede0e36e8b0fe</mdhash>
      <filesize>139945</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.28-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8d433e538d91709f1bede0e36e8b0fe</md5>
          <size>139945</size>
          <filedate>1574701085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.28-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9d31538103089f5264d1b1a0e3ce2fd1</md5>
          <size>174088</size>
          <filedate>1574701085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.27</name>
      <version>7.x-3.27</version>
      <tag>7.x-3.27</tag>
      <version_major>3</version_major>
      <version_patch>27</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.27</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.27.tar.gz</download_link>
      <date>1486665483</date>
      <mdhash>17c1e11d7826c9a3554d4b6a93594c9f</mdhash>
      <filesize>139957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.27.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>17c1e11d7826c9a3554d4b6a93594c9f</md5>
          <size>139957</size>
          <filedate>1486665483</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.27.zip</url>
          <archive_type>zip</archive_type>
          <md5>814ed9b76de1a7007e60b7e69c9d807d</md5>
          <size>174013</size>
          <filedate>1486665483</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.27-rc1</name>
      <version>7.x-3.27-rc1</version>
      <tag>7.x-3.27-rc1</tag>
      <version_major>3</version_major>
      <version_patch>27</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.27-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.27-rc1.tar.gz</download_link>
      <date>1485444182</date>
      <mdhash>786145dda5a2e5f4520c2197c3ea69b1</mdhash>
      <filesize>139957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.27-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>786145dda5a2e5f4520c2197c3ea69b1</md5>
          <size>139957</size>
          <filedate>1485444182</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.27-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2c06608c16d8d38fec9f743ea364b8be</md5>
          <size>174016</size>
          <filedate>1485444182</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.26</name>
      <version>7.x-3.26</version>
      <tag>7.x-3.26</tag>
      <version_major>3</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.26.tar.gz</download_link>
      <date>1480529583</date>
      <mdhash>065cedb9c220daccccb22f907bb3d744</mdhash>
      <filesize>139944</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>065cedb9c220daccccb22f907bb3d744</md5>
          <size>139944</size>
          <filedate>1480529583</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c82d4a8677d9d3dcb756722ab0bdd43</md5>
          <size>174009</size>
          <filedate>1480529583</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.26-rc1</name>
      <version>7.x-3.26-rc1</version>
      <tag>7.x-3.26-rc1</tag>
      <version_major>3</version_major>
      <version_patch>26</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.26-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.26-rc1.tar.gz</download_link>
      <date>1479312239</date>
      <mdhash>8de44a808ec179c299df85736196b446</mdhash>
      <filesize>137812</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.26-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8de44a808ec179c299df85736196b446</md5>
          <size>137812</size>
          <filedate>1479312239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.26-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3d388f67bb554d1010478210d59736ff</md5>
          <size>174011</size>
          <filedate>1479312239</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.25</name>
      <version>7.x-3.25</version>
      <tag>7.x-3.25</tag>
      <version_major>3</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.25.tar.gz</download_link>
      <date>1476870839</date>
      <mdhash>8b039236e6e7d71ec72e0894205dae5e</mdhash>
      <filesize>137744</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b039236e6e7d71ec72e0894205dae5e</md5>
          <size>137744</size>
          <filedate>1476870839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>9a373e7ab623c13974409919d7f1213e</md5>
          <size>173960</size>
          <filedate>1476870839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.24</name>
      <version>7.x-3.24</version>
      <tag>7.x-3.24</tag>
      <version_major>3</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.24.tar.gz</download_link>
      <date>1427956545</date>
      <mdhash>4cc3efd543655c6af4356ff2f88d7ac3</mdhash>
      <filesize>137672</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4cc3efd543655c6af4356ff2f88d7ac3</md5>
          <size>137672</size>
          <filedate>1427956545</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>9a32a44b7474d4eeb82635b311e36e60</md5>
          <size>173819</size>
          <filedate>1427956545</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.23</name>
      <version>7.x-3.23</version>
      <tag>7.x-3.23</tag>
      <version_major>3</version_major>
      <version_patch>23</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.23</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.23.tar.gz</download_link>
      <date>1426611015</date>
      <mdhash>6a507412ba68d7b371fb13f18c739149</mdhash>
      <filesize>137624</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.23.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6a507412ba68d7b371fb13f18c739149</md5>
          <size>137624</size>
          <filedate>1426611015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.23.zip</url>
          <archive_type>zip</archive_type>
          <md5>983c5de791d9d7bd19844cb3bb41fae6</md5>
          <size>173773</size>
          <filedate>1426611015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.22</name>
      <version>7.x-3.22</version>
      <tag>7.x-3.22</tag>
      <version_major>3</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.22.tar.gz</download_link>
      <date>1425406681</date>
      <mdhash>af8e64222a07a850b5d9d1798a1d1bb2</mdhash>
      <filesize>136634</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af8e64222a07a850b5d9d1798a1d1bb2</md5>
          <size>136634</size>
          <filedate>1425406681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8cc8c061146b93118fb75f7db811a77</md5>
          <size>172884</size>
          <filedate>1425406681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.21</name>
      <version>7.x-3.21</version>
      <tag>7.x-3.21</tag>
      <version_major>3</version_major>
      <version_patch>21</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.21</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.21.tar.gz</download_link>
      <date>1412681928</date>
      <mdhash>0a5579986a6d8698eca05ef4cd4f0ee3</mdhash>
      <filesize>136660</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.21.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0a5579986a6d8698eca05ef4cd4f0ee3</md5>
          <size>136660</size>
          <filedate>1412681928</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.21.zip</url>
          <archive_type>zip</archive_type>
          <md5>955d3a17112012550ae56684523533c5</md5>
          <size>172891</size>
          <filedate>1412681928</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.20</name>
      <version>7.x-3.20</version>
      <tag>7.x-3.20</tag>
      <version_major>3</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.20.tar.gz</download_link>
      <date>1392182304</date>
      <mdhash>5011725f4998fbdff66ad31bc539c473</mdhash>
      <filesize>136668</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5011725f4998fbdff66ad31bc539c473</md5>
          <size>136668</size>
          <filedate>1392182304</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>b5e080cfbf06e4ef1b5318a1b7d3eb07</md5>
          <size>170557</size>
          <filedate>1392182304</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.19</name>
      <version>7.x-3.19</version>
      <tag>7.x-3.19</tag>
      <version_major>3</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.19.tar.gz</download_link>
      <date>1369860079</date>
      <mdhash>75a7796e7571b8eb8107f4a0171f2a64</mdhash>
      <filesize>135557</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>75a7796e7571b8eb8107f4a0171f2a64</md5>
          <size>135557</size>
          <filedate>1369860079</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>f065c9063c7a02b1c794ee40a44ada49</md5>
          <size>169567</size>
          <filedate>1369860079</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.18</name>
      <version>7.x-3.18</version>
      <tag>7.x-3.18</tag>
      <version_major>3</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.18.tar.gz</download_link>
      <date>1336890411</date>
      <mdhash>0c831e201bfc065207e0ddfeb8169e68</mdhash>
      <filesize>134663</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0c831e201bfc065207e0ddfeb8169e68</md5>
          <size>134663</size>
          <filedate>1336890411</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>5028c2883080e87504531aa418f8ff0a</md5>
          <size>168640</size>
          <filedate>1336890411</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.17</name>
      <version>7.x-3.17</version>
      <tag>7.x-3.17</tag>
      <version_major>3</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.17</release_link>
      <download_link/>
      <date>1331149848</date>
      <mdhash>99164a28431b75a3ee58727698c159ba</mdhash>
      <filesize>132843</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>99164a28431b75a3ee58727698c159ba</md5>
          <size>132843</size>
          <filedate>1331149848</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>8012119cab2b4a86e199f1c5b518b402</md5>
          <size>166056</size>
          <filedate>1331149848</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.16</name>
      <version>7.x-3.16</version>
      <tag>7.x-3.16</tag>
      <version_major>3</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.16.tar.gz</download_link>
      <date>1330334150</date>
      <mdhash>c0b428e7931efb4416e7b1057f37e02f</mdhash>
      <filesize>131554</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c0b428e7931efb4416e7b1057f37e02f</md5>
          <size>131554</size>
          <filedate>1330334150</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>314080196b1568e553bc004f2e731583</md5>
          <size>164622</size>
          <filedate>1330334150</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.15</name>
      <version>7.x-3.15</version>
      <tag>7.x-3.15</tag>
      <version_major>3</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.15.tar.gz</download_link>
      <date>1323326755</date>
      <mdhash>6631cf2e3cfbab71e8e48dc31c0b5f04</mdhash>
      <filesize>123969</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6631cf2e3cfbab71e8e48dc31c0b5f04</md5>
          <size>123969</size>
          <filedate>1323326755</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ce78f8cc54162ecf5fd307160316cf2</md5>
          <size>155533</size>
          <filedate>1323326755</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.13</name>
      <version>7.x-3.13</version>
      <tag>7.x-3.13</tag>
      <version_major>3</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.13.tar.gz</download_link>
      <date>1314806221</date>
      <mdhash>3608e957d4685680c755870b2264d444</mdhash>
      <filesize>121276</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3608e957d4685680c755870b2264d444</md5>
          <size>121276</size>
          <filedate>1314806221</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>6237622cef10192623cbd7a43bbf9d0b</md5>
          <size>153190</size>
          <filedate>1314806221</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.12</name>
      <version>7.x-3.12</version>
      <tag>7.x-3.12</tag>
      <version_major>3</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.12.tar.gz</download_link>
      <date>1313561826</date>
      <mdhash>9094d0cc5d3d1e746767cd6b03474be8</mdhash>
      <filesize>119057</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9094d0cc5d3d1e746767cd6b03474be8</md5>
          <size>119057</size>
          <filedate>1313561826</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>253a390c235d52ff92e6457b7d6fbdfa</md5>
          <size>150934</size>
          <filedate>1313561826</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.11</release_link>
      <download_link/>
      <date>1305758218</date>
      <mdhash>ab63b21e78594d9012c688d6f9242c1a</mdhash>
      <filesize>117272</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>ab63b21e78594d9012c688d6f9242c1a</md5>
          <size>117272</size>
          <filedate>1305758218</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a1278856ca421cc27de09abaa2532577</md5>
          <size>148875</size>
          <filedate>1305758218</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.10</release_link>
      <download_link/>
      <date>1305756717</date>
      <mdhash>a463377335ec4ea95512dabd400ac6c6</mdhash>
      <filesize>115439</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>a463377335ec4ea95512dabd400ac6c6</md5>
          <size>115439</size>
          <filedate>1305756717</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>781010e300d023ef0702c61e6f2b580d</md5>
          <size>147188</size>
          <filedate>1305756717</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.9</name>
      <version>7.x-3.9</version>
      <tag>7.x-3.9</tag>
      <version_major>3</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.9.tar.gz</download_link>
      <date>1299399369</date>
      <mdhash>4622ef7902efbbdf2c76ea1a70095350</mdhash>
      <filesize>115250</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4622ef7902efbbdf2c76ea1a70095350</md5>
          <size>115250</size>
          <filedate>1299399369</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>524350425713f735d3d85191053e0f8b</md5>
          <size>147066</size>
          <filedate>1299399369</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.8</name>
      <version>7.x-3.8</version>
      <tag>7.x-3.8</tag>
      <version_major>3</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.8.tar.gz</download_link>
      <date>1298677273</date>
      <mdhash>a55a253b21abae132f519943a6dfb1e2</mdhash>
      <filesize>115237</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a55a253b21abae132f519943a6dfb1e2</md5>
          <size>115237</size>
          <filedate>1298677273</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ba6ed324d47e97cf0eaf1a239a964ec</md5>
          <size>147054</size>
          <filedate>1298677273</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.7.tar.gz</download_link>
      <date>1298534230</date>
      <mdhash>0531afc17545bf2df8ff36fd770c9522</mdhash>
      <filesize>332813</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0531afc17545bf2df8ff36fd770c9522</md5>
          <size>332813</size>
          <filedate>1298534230</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf5b4ea77fe203af4d3d677e73a47c81</md5>
          <size>379278</size>
          <filedate>1298534230</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.6</release_link>
      <download_link/>
      <date>1294819898</date>
      <mdhash>5b5d011f368ccc3458acb816bd322368</mdhash>
      <filesize>342890</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>5b5d011f368ccc3458acb816bd322368</md5>
          <size>342890</size>
          <filedate>1294819898</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>3dd883e02e16132b65f019c0b0768fb5</md5>
          <size>389771</size>
          <filedate>1294819899</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform 7.x-3.4-beta1</name>
      <version>7.x-3.4-beta1</version>
      <tag>7.x-3.4-beta1</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.4-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.4-beta1.tar.gz</download_link>
      <date>1287598575</date>
      <mdhash>2a39a8641a3da45835309830600c78dd</mdhash>
      <filesize>342322</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.4-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a39a8641a3da45835309830600c78dd</md5>
          <size>342322</size>
          <filedate>1287598575</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.4-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>061470b91337d0315c418dccec338135</md5>
          <size>387867</size>
          <filedate>1293235866</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.3-beta1</name>
      <version>7.x-3.3-beta1</version>
      <tag>7.x-3.3-beta1</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.3-beta1</release_link>
      <download_link/>
      <date>1287390088</date>
      <mdhash>05abb33a143713e3dec72dc9b832b488</mdhash>
      <filesize>342034</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>05abb33a143713e3dec72dc9b832b488</md5>
          <size>342034</size>
          <filedate>1287390088</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>fb2c3f12e7b68f79198354e71d919896</md5>
          <size>387631</size>
          <filedate>1293235850</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta8</name>
      <version>7.x-3.0-beta8</version>
      <tag>7.x-3.0-beta8</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta8.tar.gz</download_link>
      <date>1282828912</date>
      <mdhash>da4804382f6f3ba985982bb4ef0f1821</mdhash>
      <filesize>336720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>da4804382f6f3ba985982bb4ef0f1821</md5>
          <size>336720</size>
          <filedate>1282828912</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>14521852ab3fc0dda5131ff177c04e4b</md5>
          <size>380319</size>
          <filedate>1293235842</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta7</name>
      <version>7.x-3.0-beta7</version>
      <tag>7.x-3.0-beta7</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta7.tar.gz</download_link>
      <date>1281825694</date>
      <mdhash>e6daf245f2f70b7e101864f3c086235c</mdhash>
      <filesize>336332</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e6daf245f2f70b7e101864f3c086235c</md5>
          <size>336332</size>
          <filedate>1281825694</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>b0da7f5e41c804673d9d8fa36b5061cd</md5>
          <size>379876</size>
          <filedate>1293235861</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta6</name>
      <version>7.x-3.0-beta6</version>
      <tag>7.x-3.0-beta6</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta6.tar.gz</download_link>
      <date>1277097010</date>
      <mdhash>b2e32326c4d009b9af6efb190f3623cd</mdhash>
      <filesize>336300</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b2e32326c4d009b9af6efb190f3623cd</md5>
          <size>336300</size>
          <filedate>1277097010</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>f6bf8c73556533d3a43e78bc2d304633</md5>
          <size>379842</size>
          <filedate>1293235852</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta5</name>
      <version>7.x-3.0-beta5</version>
      <tag>7.x-3.0-beta5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta5.tar.gz</download_link>
      <date>1271177414</date>
      <mdhash>8e2e4e4b5c3e7f3b05f5af0454417f8a</mdhash>
      <filesize>334969</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8e2e4e4b5c3e7f3b05f5af0454417f8a</md5>
          <size>334969</size>
          <filedate>1271177414</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>7ea26b1a35e2930858d01ff0ba4746cd</md5>
          <size>378488</size>
          <filedate>1293235843</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta4</name>
      <version>7.x-3.0-beta4</version>
      <tag>7.x-3.0-beta4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta4.tar.gz</download_link>
      <date>1270268112</date>
      <mdhash>58886079d44ff03a11e8fc7c79e457e1</mdhash>
      <filesize>333147</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>58886079d44ff03a11e8fc7c79e457e1</md5>
          <size>333147</size>
          <filedate>1270268112</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>577cbbb083d6820ee5ff6bb06c692492</md5>
          <size>375420</size>
          <filedate>1293235844</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta3</name>
      <version>7.x-3.0-beta3</version>
      <tag>7.x-3.0-beta3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta3.tar.gz</download_link>
      <date>1270265414</date>
      <mdhash>285cce90e04b2bce7fe62f7e08ec152b</mdhash>
      <filesize>333092</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>285cce90e04b2bce7fe62f7e08ec152b</md5>
          <size>333092</size>
          <filedate>1270265414</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>50fcb12c5f6130a1193bcb7bdd17b930</md5>
          <size>375370</size>
          <filedate>1293235863</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta2.tar.gz</download_link>
      <date>1268446210</date>
      <mdhash>6152ba9b80f69e4098c5fc14457e1a2e</mdhash>
      <filesize>327887</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6152ba9b80f69e4098c5fc14457e1a2e</md5>
          <size>327887</size>
          <filedate>1268446210</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>51723ce720841fdf50539086aea287c0</md5>
          <size>369898</size>
          <filedate>1293235846</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-4.x-dev.tar.gz</download_link>
      <date>1731372619</date>
      <mdhash>402dc6f542603740793079d6c7adf833</mdhash>
      <filesize>255773</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>402dc6f542603740793079d6c7adf833</md5>
          <size>255773</size>
          <filedate>1731372619</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5b6f2bc5d9e80e222cf4641f3b31d76c</md5>
          <size>307884</size>
          <filedate>1731372619</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform-7.x-3.x-dev.tar.gz</download_link>
      <date>1669234352</date>
      <mdhash>d69f3fc2337f206bd3391bca48c3ea51</mdhash>
      <filesize>140273</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d69f3fc2337f206bd3391bca48c3ea51</md5>
          <size>140273</size>
          <filedate>1669234352</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9aa005e76358b619fca68e9573aea827</md5>
          <size>174430</size>
          <filedate>1669234352</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
