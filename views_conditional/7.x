<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Conditional</title>
  <short_name>views_conditional</short_name>
  <dc:creator>MChittenden</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_conditional</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_conditional 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_conditional/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.3.tar.gz</download_link>
      <date>1416718381</date>
      <mdhash>04c2d7f3dc499457069654f8d87f3bec</mdhash>
      <filesize>10536</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>04c2d7f3dc499457069654f8d87f3bec</md5>
          <size>10536</size>
          <filedate>1416718381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>236367bd6b78847e729f7e94dc8b1896</md5>
          <size>12773</size>
          <filedate>1416718381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_conditional 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_conditional/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.2.tar.gz</download_link>
      <date>1415875380</date>
      <mdhash>7b1b01db9b93a60ac46766063c3ddca9</mdhash>
      <filesize>10412</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7b1b01db9b93a60ac46766063c3ddca9</md5>
          <size>10412</size>
          <filedate>1415875380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ec2c8263876135dd2f4e507a1f571bd4</md5>
          <size>12637</size>
          <filedate>1415875380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_conditional 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_conditional/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.1.tar.gz</download_link>
      <date>1372873258</date>
      <mdhash>7e13f4dc3501e9b4319386b1d0e75ecb</mdhash>
      <filesize>10386</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e13f4dc3501e9b4319386b1d0e75ecb</md5>
          <size>10386</size>
          <filedate>1372873258</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>da5a9e6546430dec3c0beeeb02d8aef8</md5>
          <size>12394</size>
          <filedate>1372873258</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_conditional 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_conditional/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.0.tar.gz</download_link>
      <date>1368475812</date>
      <mdhash>f47a53a6cdc80641477115654d966266</mdhash>
      <filesize>10314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f47a53a6cdc80641477115654d966266</md5>
          <size>10314</size>
          <filedate>1368475812</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b9ca7e3cd6287c1b0b04a7e09e0f5d32</md5>
          <size>12327</size>
          <filedate>1368475812</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_conditional 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_conditional/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.x-dev.tar.gz</download_link>
      <date>1416718981</date>
      <mdhash>632d7dde86a6a0a632dd7b284c650143</mdhash>
      <filesize>10555</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>632d7dde86a6a0a632dd7b284c650143</md5>
          <size>10555</size>
          <filedate>1416718981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_conditional-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>44ba4b48238fe929d1f8a1b1b8734ced</md5>
          <size>12798</size>
          <filedate>1416718981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
