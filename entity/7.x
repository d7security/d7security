<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entity API</title>
  <short_name>entity</short_name>
  <dc:creator>fago</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/entity</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entity 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/entity/-/releases/7.x-1.12</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67464550/packages/generic/entity/7.x-1.12/entity-7.x-1.12.tar.gz</download_link>
      <date>1740503765</date>
      <mdhash>1210747b1bacb9cf7702830f8ff580b3</mdhash>
      <filesize>127108</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67464550/packages/generic/entity/7.x-1.12/entity-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1210747b1bacb9cf7702830f8ff580b3</md5>
          <size>127108</size>
          <filedate>1740503765</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67464550/packages/generic/entity/7.x-1.12/entity-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7f04f4dcde76c6e0addfb25af9bc8fd</md5>
          <size>160455</size>
          <filedate>1740503765</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>entity 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.11.tar.gz</download_link>
      <date>1720924766</date>
      <mdhash>358798beb0591086462b371de952a086</mdhash>
      <filesize>126259</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>358798beb0591086462b371de952a086</md5>
          <size>126259</size>
          <filedate>1720924766</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>0090451c3ab952a238eb17e1c425a5dd</md5>
          <size>161235</size>
          <filedate>1720924766</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.10.tar.gz</download_link>
      <date>1637434456</date>
      <mdhash>3baaf984fe5c276e82cd44aac72db9e1</mdhash>
      <filesize>125515</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3baaf984fe5c276e82cd44aac72db9e1</md5>
          <size>125515</size>
          <filedate>1637434456</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>c62fe80e5a6a94f190ee12bb2218ee3e</md5>
          <size>160128</size>
          <filedate>1637434456</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.9.tar.gz</download_link>
      <date>1518620284</date>
      <mdhash>793870ebcaa31da748e165d470c0b9bb</mdhash>
      <filesize>124231</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>793870ebcaa31da748e165d470c0b9bb</md5>
          <size>124231</size>
          <filedate>1518620284</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc4e15cffb4f64718da245c0f4efe8c8</md5>
          <size>158704</size>
          <filedate>1518620284</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.8.tar.gz</download_link>
      <date>1474545839</date>
      <mdhash>3926d48c86387c3d1d5777538903a752</mdhash>
      <filesize>123957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3926d48c86387c3d1d5777538903a752</md5>
          <size>123957</size>
          <filedate>1474545839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>a1406f80c21d9b60c78415337a02c3a2</md5>
          <size>158240</size>
          <filedate>1474545839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.7.tar.gz</download_link>
      <date>1458222242</date>
      <mdhash>f4787da0230869cc7420b6cea78212e5</mdhash>
      <filesize>121426</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f4787da0230869cc7420b6cea78212e5</md5>
          <size>121426</size>
          <filedate>1458222242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>0e50456b74c59dacfce61ab00fa115cf</md5>
          <size>154980</size>
          <filedate>1458222242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.6.tar.gz</download_link>
      <date>1424876581</date>
      <mdhash>a778a5c82eaddac50e40a3dd0a3ebac6</mdhash>
      <filesize>120850</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a778a5c82eaddac50e40a3dd0a3ebac6</md5>
          <size>120850</size>
          <filedate>1424876581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f96b996c8215a51ebc3e6c1de5c20f6</md5>
          <size>154391</size>
          <filedate>1424876581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.5.tar.gz</download_link>
      <date>1396975451</date>
      <mdhash>04b90c27b2f1627d0a218006291afa9f</mdhash>
      <filesize>121011</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>04b90c27b2f1627d0a218006291afa9f</md5>
          <size>121011</size>
          <filedate>1396975451</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>49b5007beeaa400019ecc464ac80b799</md5>
          <size>152863</size>
          <filedate>1396975451</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.4.tar.gz</download_link>
      <date>1395335056</date>
      <mdhash>d026208f7b433b9b2dd5fdc705396d0e</mdhash>
      <filesize>120371</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d026208f7b433b9b2dd5fdc705396d0e</md5>
          <size>120371</size>
          <filedate>1395335056</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>b7f712ce28d82bb9831de5b80fced07e</md5>
          <size>152256</size>
          <filedate>1395335056</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.3.tar.gz</download_link>
      <date>1389210804</date>
      <mdhash>92bb78da6928049c0d9bdea76629416b</mdhash>
      <filesize>119279</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92bb78da6928049c0d9bdea76629416b</md5>
          <size>119279</size>
          <filedate>1389210804</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f05f17615667d0f34391f0787513f051</md5>
          <size>150951</size>
          <filedate>1389210804</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.2</release_link>
      <download_link/>
      <date>1376493705</date>
      <mdhash>c87e0ce3708a26a3045c36b8ad15f61c</mdhash>
      <filesize>115225</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c87e0ce3708a26a3045c36b8ad15f61c</md5>
          <size>115225</size>
          <filedate>1376493705</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a53171d320ba59c7c74cd24bf0b750ad</md5>
          <size>146392</size>
          <filedate>1376493705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.1.tar.gz</download_link>
      <date>1367338217</date>
      <mdhash>1f23d1bee48287f1c53f8d3f1391187a</mdhash>
      <filesize>110514</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1f23d1bee48287f1c53f8d3f1391187a</md5>
          <size>110514</size>
          <filedate>1367338217</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0102720b7eafa2aaf1275a5d905e324f</md5>
          <size>140635</size>
          <filedate>1367338217</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0.tar.gz</download_link>
      <date>1356471145</date>
      <mdhash>f072eda34e284561bb55cfae0dbe30db</mdhash>
      <filesize>110082</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f072eda34e284561bb55cfae0dbe30db</md5>
          <size>110082</size>
          <filedate>1356471145</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>32d84f8bcc903600822d60d6a9bd2dca</md5>
          <size>140212</size>
          <filedate>1356471145</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc3.tar.gz</download_link>
      <date>1337981155</date>
      <mdhash>267c552757d1fa8bbe17c624a7ec4c3f</mdhash>
      <filesize>106628</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>267c552757d1fa8bbe17c624a7ec4c3f</md5>
          <size>106628</size>
          <filedate>1337981155</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d2c965ad93c03a5edd3b269925d9ad11</md5>
          <size>136862</size>
          <filedate>1337981155</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-rc2</release_link>
      <download_link/>
      <date>1335099086</date>
      <mdhash>587d7b6a088a195722fe7d11b1c9c8fa</mdhash>
      <filesize>105993</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>587d7b6a088a195722fe7d11b1c9c8fa</md5>
          <size>105993</size>
          <filedate>1335099086</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>5459aa32330c57b1f817f57d23bdf7e6</md5>
          <size>136208</size>
          <filedate>1335099086</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc1.tar.gz</download_link>
      <date>1320914735</date>
      <mdhash>daa6dff009d00756f86b80b31af32c77</mdhash>
      <filesize>95227</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>daa6dff009d00756f86b80b31af32c77</md5>
          <size>95227</size>
          <filedate>1320914735</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>99d53976848240c2821396976471cf8b</md5>
          <size>122557</size>
          <filedate>1320914735</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta11</name>
      <version>7.x-1.0-beta11</version>
      <tag>7.x-1.0-beta11</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta11</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta11.tar.gz</download_link>
      <date>1319558131</date>
      <mdhash>0d98c2c361e5936593b8365614fa04e3</mdhash>
      <filesize>83206</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d98c2c361e5936593b8365614fa04e3</md5>
          <size>83206</size>
          <filedate>1319558131</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta11.zip</url>
          <archive_type>zip</archive_type>
          <md5>b16557aaf793d63ba62ba68f49c4e3af</md5>
          <size>100368</size>
          <filedate>1319558131</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta10</name>
      <version>7.x-1.0-beta10</version>
      <tag>7.x-1.0-beta10</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta10.tar.gz</download_link>
      <date>1310486817</date>
      <mdhash>016f48d7ecee0da62ae1a8f6cf26a656</mdhash>
      <filesize>77197</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>016f48d7ecee0da62ae1a8f6cf26a656</md5>
          <size>77197</size>
          <filedate>1310486817</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta10.zip</url>
          <archive_type>zip</archive_type>
          <md5>d780466334235ecf9471a6277d1c6e1a</md5>
          <size>93599</size>
          <filedate>1310486817</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta9</name>
      <version>7.x-1.0-beta9</version>
      <tag>7.x-1.0-beta9</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta9</release_link>
      <download_link/>
      <date>1308239815</date>
      <mdhash>a965dc47b0b8afc2f75b69be31e767d0</mdhash>
      <filesize>75840</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>a965dc47b0b8afc2f75b69be31e767d0</md5>
          <size>75840</size>
          <filedate>1308239815</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>57ab6f16c8d15c09b5dae9510ad83f3a</md5>
          <size>92153</size>
          <filedate>1308239815</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta8</name>
      <version>7.x-1.0-beta8</version>
      <tag>7.x-1.0-beta8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta8.tar.gz</download_link>
      <date>1300731067</date>
      <mdhash>317ec15a5f9d90e7613b52f20a7e9a82</mdhash>
      <filesize>71258</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>317ec15a5f9d90e7613b52f20a7e9a82</md5>
          <size>71258</size>
          <filedate>1300731067</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>bdda06d40b3c07171c1ebd4382eba58e</md5>
          <size>86714</size>
          <filedate>1300731067</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta7.tar.gz</download_link>
      <date>1298285230</date>
      <mdhash>9598452892a90a3b098d3152334aeb85</mdhash>
      <filesize>72722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9598452892a90a3b098d3152334aeb85</md5>
          <size>72722</size>
          <filedate>1298285230</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>7dc9b04be79bf57c93ffec81ab37f7dd</md5>
          <size>91780</size>
          <filedate>1298285230</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta6.tar.gz</download_link>
      <date>1294170388</date>
      <mdhash>64588b70e6773d146e0bf4267a90a270</mdhash>
      <filesize>68465</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>64588b70e6773d146e0bf4267a90a270</md5>
          <size>68465</size>
          <filedate>1294170388</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>cce03b3f1c7e6855a4ff0bee3883dd0a</md5>
          <size>86988</size>
          <filedate>1294170388</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta5.tar.gz</download_link>
      <date>1293135097</date>
      <mdhash>53325d82ab40c0dc325ccb59d0bca3e6</mdhash>
      <filesize>65300</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>53325d82ab40c0dc325ccb59d0bca3e6</md5>
          <size>65300</size>
          <filedate>1293135097</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>19f1479eadabec7cd4b506e03dc52243</md5>
          <size>83381</size>
          <filedate>1293231243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta4.tar.gz</download_link>
      <date>1292941950</date>
      <mdhash>93210f6c3b3b19258f217a64e6bd56a8</mdhash>
      <filesize>65305</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>93210f6c3b3b19258f217a64e6bd56a8</md5>
          <size>65305</size>
          <filedate>1292941950</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>636df968af15c179ee9f541143978c5e</md5>
          <size>83383</size>
          <filedate>1293231244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta3.tar.gz</download_link>
      <date>1291297866</date>
      <mdhash>cb05413449da98da415d40e3520f0f6d</mdhash>
      <filesize>61957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb05413449da98da415d40e3520f0f6d</md5>
          <size>61957</size>
          <filedate>1291297866</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>97468de033a396af051f066ca8b24ba0</md5>
          <size>80526</size>
          <filedate>1293231244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta2</release_link>
      <download_link/>
      <date>1290197444</date>
      <mdhash>a1646f5c1933772feb76e2dd31d6324b</mdhash>
      <filesize>60670</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>a1646f5c1933772feb76e2dd31d6324b</md5>
          <size>60670</size>
          <filedate>1290197444</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>5702ddf6e544dbb6ab3a82bd4380e3f5</md5>
          <size>79176</size>
          <filedate>1293231243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta1.tar.gz</download_link>
      <date>1286786762</date>
      <mdhash>2e53243fdf0fae60217dd7916d982e47</mdhash>
      <filesize>54660</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2e53243fdf0fae60217dd7916d982e47</md5>
          <size>54660</size>
          <filedate>1286786762</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>04c8313cb68456f275f5aabca07a1e74</md5>
          <size>72356</size>
          <filedate>1293231244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha6</name>
      <version>7.x-1.0-alpha6</version>
      <tag>7.x-1.0-alpha6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha6</release_link>
      <download_link/>
      <date>1280228111</date>
      <mdhash>5d88abea4508ea3561332f466746ab3d</mdhash>
      <filesize>47315</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>5d88abea4508ea3561332f466746ab3d</md5>
          <size>47315</size>
          <filedate>1280228111</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>67bbd091c22ea67d0df387c32c110216</md5>
          <size>62395</size>
          <filedate>1293231245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1273607706</date>
      <mdhash>13348a08070605b3742d4fad7856d76a</mdhash>
      <filesize>45331</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>13348a08070605b3742d4fad7856d76a</md5>
          <size>45331</size>
          <filedate>1273607706</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>e01b31f8d1bdce97dcbf61c57b76a782</md5>
          <size>60020</size>
          <filedate>1293231243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha4</release_link>
      <download_link/>
      <date>1268336409</date>
      <mdhash>0e093c0ded92b23a4e628a7953a198dc</mdhash>
      <filesize>41614</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>0e093c0ded92b23a4e628a7953a198dc</md5>
          <size>41614</size>
          <filedate>1268336409</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>fd57cb018430af65e11c28757fd212e2</md5>
          <size>55898</size>
          <filedate>1293231244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1266851413</date>
      <mdhash>d347ca5528b9ed9d132b492cb4b7b2af</mdhash>
      <filesize>40746</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d347ca5528b9ed9d132b492cb4b7b2af</md5>
          <size>40746</size>
          <filedate>1266851413</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>29d6706a1d6430840df050095bdc0f31</md5>
          <size>53781</size>
          <filedate>1293231245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1263231309</date>
      <mdhash>be9a91bd68b59480bdca9e8a6d6ad932</mdhash>
      <filesize>31954</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>be9a91bd68b59480bdca9e8a6d6ad932</md5>
          <size>31954</size>
          <filedate>1263231309</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c566eaadd7fae51905dc36d3ae2ee3f1</md5>
          <size>44379</size>
          <filedate>1293231243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1260807922</date>
      <mdhash>858c5a1b255f5316737dbbd3b562d825</mdhash>
      <filesize>29154</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>858c5a1b255f5316737dbbd3b562d825</md5>
          <size>29154</size>
          <filedate>1260807922</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a862618e1ae4fe58e5d164479e29c071</md5>
          <size>41126</size>
          <filedate>1293231244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity-7.x-1.x-dev.tar.gz</download_link>
      <date>1720923794</date>
      <mdhash>d4eb0bea15182287953a6bf360a26f3e</mdhash>
      <filesize>126269</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d4eb0bea15182287953a6bf360a26f3e</md5>
          <size>126269</size>
          <filedate>1720923794</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5958b885755c061b7c25b2d98a73524e</md5>
          <size>161264</size>
          <filedate>1720923794</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
