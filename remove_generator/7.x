<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Remove Generator META tag</title>
  <short_name>remove_generator</short_name>
  <dc:creator>seandunaway</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/remove_generator</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>remove_generator 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.4.tar.gz</download_link>
      <date>1310360821</date>
      <mdhash>da8f341a2082b5bc814b7877c0407ecc</mdhash>
      <filesize>6791</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>da8f341a2082b5bc814b7877c0407ecc</md5>
          <size>6791</size>
          <filedate>1310360821</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba2e0acfdaee40cb78a7e8d7322f33ba</md5>
          <size>7766</size>
          <filedate>1310360821</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>remove_generator 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.3.tar.gz</download_link>
      <date>1310339821</date>
      <mdhash>13ebe72647c47fe7879f44ce95c45001</mdhash>
      <filesize>6788</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>13ebe72647c47fe7879f44ce95c45001</md5>
          <size>6788</size>
          <filedate>1310339821</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4776292c54da6d5b0b5092bf6aa72e77</md5>
          <size>7761</size>
          <filedate>1310339821</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>remove_generator 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.2.tar.gz</download_link>
      <date>1310011019</date>
      <mdhash>deee8d63509704da79467b6d3e150a55</mdhash>
      <filesize>6419</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>deee8d63509704da79467b6d3e150a55</md5>
          <size>6419</size>
          <filedate>1310011019</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e8cd56d18421c2b529166fc96a5a447e</md5>
          <size>7130</size>
          <filedate>1310011019</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>remove_generator 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.1.tar.gz</download_link>
      <date>1309736219</date>
      <mdhash>fa22fe62dc7ecd74bf0324fa0899a527</mdhash>
      <filesize>6818</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa22fe62dc7ecd74bf0324fa0899a527</md5>
          <size>6818</size>
          <filedate>1309736219</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e0eae31216d32828559fbcfb697ddb3</md5>
          <size>7763</size>
          <filedate>1309736219</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>remove_generator 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.0.tar.gz</download_link>
      <date>1309734120</date>
      <mdhash>d1d235df238a108ddae08ec70990e025</mdhash>
      <filesize>6813</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d1d235df238a108ddae08ec70990e025</md5>
          <size>6813</size>
          <filedate>1309734120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>67d13285964ce4d94ac2ce04961b0066</md5>
          <size>7763</size>
          <filedate>1309734120</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>remove_generator 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/remove_generator/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.x-dev.tar.gz</download_link>
      <date>1380625596</date>
      <mdhash>42f1ac9fbefb984152721bd00b17de44</mdhash>
      <filesize>7907</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42f1ac9fbefb984152721bd00b17de44</md5>
          <size>7907</size>
          <filedate>1380625596</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/remove_generator-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5710516ad9bb22b05dfcb424173dcb66</md5>
          <size>8875</size>
          <filedate>1380625596</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
