<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>IMCE</title>
  <short_name>imce</short_name>
  <dc:creator>ufku</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/imce</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>imce 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-2.0.tar.gz</download_link>
      <date>1672242659</date>
      <mdhash>aa0317bac1e886e5dbecf9a687b040e5</mdhash>
      <filesize>64199</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aa0317bac1e886e5dbecf9a687b040e5</md5>
          <size>64199</size>
          <filedate>1672242659</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>14bb455c71398a0a826e9127185b5790</md5>
          <size>76764</size>
          <filedate>1672242659</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.11.tar.gz</download_link>
      <date>1495890784</date>
      <mdhash>57c956347a4511727b7e03f122b909fc</mdhash>
      <filesize>62452</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57c956347a4511727b7e03f122b909fc</md5>
          <size>62452</size>
          <filedate>1495890784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>8ff0ee5acd11b146182b0fd42e52b67c</md5>
          <size>74792</size>
          <filedate>1495890784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.10.tar.gz</download_link>
      <date>1459346866</date>
      <mdhash>62aad4be77c4a9b1c5ca5588c2eb35da</mdhash>
      <filesize>63986</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>62aad4be77c4a9b1c5ca5588c2eb35da</md5>
          <size>63986</size>
          <filedate>1459346866</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d51d972be5aa3ec96a0fecd6ffe78c6</md5>
          <size>74566</size>
          <filedate>1459346866</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.9.tar.gz</download_link>
      <date>1400275427</date>
      <mdhash>bcc791ae6fac1c501837e4679f9002b8</mdhash>
      <filesize>63448</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bcc791ae6fac1c501837e4679f9002b8</md5>
          <size>63448</size>
          <filedate>1400275427</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>7064aeb43ab162b9647781fc72f2070c</md5>
          <size>73471</size>
          <filedate>1400275427</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.8.tar.gz</download_link>
      <date>1387302358</date>
      <mdhash>edba7f3567eb36ca040f63699b23603e</mdhash>
      <filesize>61452</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>edba7f3567eb36ca040f63699b23603e</md5>
          <size>61452</size>
          <filedate>1387302358</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>f219ffdda595128725b2898e243f8de5</md5>
          <size>71627</size>
          <filedate>1387302358</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.7.tar.gz</download_link>
      <date>1359476607</date>
      <mdhash>c352bf9da8433628bed0b79c4acdcdb8</mdhash>
      <filesize>61286</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c352bf9da8433628bed0b79c4acdcdb8</md5>
          <size>61286</size>
          <filedate>1359476607</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>546551d504efd6bc01ca4b5d53dec0f1</md5>
          <size>71606</size>
          <filedate>1359476607</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.6.tar.gz</download_link>
      <date>1352537820</date>
      <mdhash>e1300316ea4245063b79bd91f9fd0bcf</mdhash>
      <filesize>61316</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e1300316ea4245063b79bd91f9fd0bcf</md5>
          <size>61316</size>
          <filedate>1352537820</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e644d9a75ddd60d549bb1fccfa199d2</md5>
          <size>71649</size>
          <filedate>1352537820</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.5.tar.gz</download_link>
      <date>1319104232</date>
      <mdhash>43c36da627ed770bec0e21a442eec712</mdhash>
      <filesize>61276</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>43c36da627ed770bec0e21a442eec712</md5>
          <size>61276</size>
          <filedate>1319104232</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d84703df42950d6382c5824ad52cdd50</md5>
          <size>71635</size>
          <filedate>1319104232</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.4.tar.gz</download_link>
      <date>1307386616</date>
      <mdhash>6d11af3cd365c143317a0dc37599505c</mdhash>
      <filesize>60190</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6d11af3cd365c143317a0dc37599505c</md5>
          <size>60190</size>
          <filedate>1307386616</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2dc678b047a64ff1e28ee7938dc05c79</md5>
          <size>70427</size>
          <filedate>1307386616</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.3.tar.gz</download_link>
      <date>1300384567</date>
      <mdhash>9a3b27ca2b5e2aceb3f1ee30841fa8b3</mdhash>
      <filesize>59939</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a3b27ca2b5e2aceb3f1ee30841fa8b3</md5>
          <size>59939</size>
          <filedate>1300384567</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>0be26cbf0cbed18be8b31173fcab4d37</md5>
          <size>70157</size>
          <filedate>1300384567</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.2.tar.gz</download_link>
      <date>1296601518</date>
      <mdhash>c6bb7fea90545f71dcf32134b8cf8157</mdhash>
      <filesize>58810</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c6bb7fea90545f71dcf32134b8cf8157</md5>
          <size>58810</size>
          <filedate>1296601518</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5e242af626c7d68ff8fa189a26d2009</md5>
          <size>71430</size>
          <filedate>1296601518</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.1.tar.gz</download_link>
      <date>1293481279</date>
      <mdhash>cef3122364a21b587448b659c34d7b06</mdhash>
      <filesize>58517</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cef3122364a21b587448b659c34d7b06</md5>
          <size>58517</size>
          <filedate>1293481279</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc0eac5255262713a6041c878d6aec74</md5>
          <size>71084</size>
          <filedate>1293481279</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.0.tar.gz</download_link>
      <date>1293289898</date>
      <mdhash>63947e1f2fd23888c014df5bc618b636</mdhash>
      <filesize>58495</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>63947e1f2fd23888c014df5bc618b636</md5>
          <size>58495</size>
          <filedate>1293289898</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6014392b58fbe0934a3ac0c5e715d73</md5>
          <size>71091</size>
          <filedate>1293289899</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.0-beta1.tar.gz</download_link>
      <date>1286606463</date>
      <mdhash>f0a6cd298c8fd85e96b225d4b0e6a38d</mdhash>
      <filesize>58423</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0a6cd298c8fd85e96b225d4b0e6a38d</md5>
          <size>58423</size>
          <filedate>1286606463</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>718d39b16a745cd72672d8ae63521b50</md5>
          <size>70180</size>
          <filedate>1293232302</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1277145015</date>
      <mdhash>161f69f59d6f56a8529446fb147992d3</mdhash>
      <filesize>57825</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>161f69f59d6f56a8529446fb147992d3</md5>
          <size>57825</size>
          <filedate>1277145015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>924966c89f5a6e94774ef65eab23f8e9</md5>
          <size>69740</size>
          <filedate>1293232301</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1275125108</date>
      <mdhash>5692e073e353d7451c47c63b381fae9c</mdhash>
      <filesize>57483</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5692e073e353d7451c47c63b381fae9c</md5>
          <size>57483</size>
          <filedate>1275125108</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>72ca7e43b3441569ecbbeb5ab0926e21</md5>
          <size>69366</size>
          <filedate>1293232303</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1268868307</date>
      <mdhash>3aeebc6a28e99abbd5260c28e499d646</mdhash>
      <filesize>55587</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3aeebc6a28e99abbd5260c28e499d646</md5>
          <size>55587</size>
          <filedate>1268868307</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b2f2d82b81f49ae597804e27921d3e40</md5>
          <size>67555</size>
          <filedate>1293232301</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce HEAD</name>
      <version>master</version>
      <tag>master</tag>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/master</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-HEAD.tar.gz</download_link>
      <date>1286626342</date>
      <mdhash>94c619d7b66e55cd6c810baf0c13211d</mdhash>
      <filesize>58416</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-HEAD.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94c619d7b66e55cd6c810baf0c13211d</md5>
          <size>58416</size>
          <filedate>1286626342</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-master.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>abfd6695c3b3b6c00c712cf55eed3113</md5>
          <size>60150</size>
          <filedate>1380583028</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-HEAD.zip</url>
          <archive_type>zip</archive_type>
          <md5>8081bcef61571bd7139caee81a09c5fb</md5>
          <size>70173</size>
          <filedate>1293250641</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-master.zip</url>
          <archive_type>zip</archive_type>
          <md5>c03eede532cd9cc4f76f28a84f9b7bf4</md5>
          <size>70444</size>
          <filedate>1380583028</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-2.x-dev.tar.gz</download_link>
      <date>1712258077</date>
      <mdhash>61cd22b39a470bbf1a6341fa46f61281</mdhash>
      <filesize>64165</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61cd22b39a470bbf1a6341fa46f61281</md5>
          <size>64165</size>
          <filedate>1712258077</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b89725b321e6c95ac8faec52d7ad68a</md5>
          <size>76737</size>
          <filedate>1712258077</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imce 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce-7.x-1.x-dev.tar.gz</download_link>
      <date>1664301915</date>
      <mdhash>89e7fd352e26e6f32f2995f63b57ce6b</mdhash>
      <filesize>64771</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89e7fd352e26e6f32f2995f63b57ce6b</md5>
          <size>64771</size>
          <filedate>1664301915</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d067526e9d0f2dffdf52ba4dbd76a456</md5>
          <size>77365</size>
          <filedate>1664301915</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
