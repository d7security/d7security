<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>TFA Basic plugins</title>
  <short_name>tfa_basic</short_name>
  <dc:creator>coltrane</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/tfa_basic</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>tfa_basic 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/tfa_basic/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67503847/packages/generic/tfa_basic/7.x-1.3/tfa_basic-7.x-1.3.tar.gz</download_link>
      <date>1740592746</date>
      <mdhash>db37d7a75690f85d0086123cd02e2e2f</mdhash>
      <filesize>36246</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67503847/packages/generic/tfa_basic/7.x-1.3/tfa_basic-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>db37d7a75690f85d0086123cd02e2e2f</md5>
          <size>36246</size>
          <filedate>1740592746</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67503847/packages/generic/tfa_basic/7.x-1.3/tfa_basic-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>53837d6b760a5a5437e296200b713a00</md5>
          <size>45420</size>
          <filedate>1740592746</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.2.tar.gz</download_link>
      <date>1689537944</date>
      <mdhash>9ae4dc5b2db7a2e7e2c5ac7317600a04</mdhash>
      <filesize>33760</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9ae4dc5b2db7a2e7e2c5ac7317600a04</md5>
          <size>33760</size>
          <filedate>1689537944</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2d4c05458d6728e4cd297dc1184d6dba</md5>
          <size>41897</size>
          <filedate>1689537944</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.1.tar.gz</download_link>
      <date>1530072225</date>
      <mdhash>eefef5c976df1a99a8b489e1077500d9</mdhash>
      <filesize>33691</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eefef5c976df1a99a8b489e1077500d9</md5>
          <size>33691</size>
          <filedate>1530072225</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d43b704fba3574be5baa49924748d55</md5>
          <size>41795</size>
          <filedate>1530072225</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0.tar.gz</download_link>
      <date>1477519741</date>
      <mdhash>81e7efc2f4737c785fefc7eebb81cfd1</mdhash>
      <filesize>33807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81e7efc2f4737c785fefc7eebb81cfd1</md5>
          <size>33807</size>
          <filedate>1477519741</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>71fdf5b5007f752bf6cb6afb00f535bb</md5>
          <size>41598</size>
          <filedate>1477519741</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta3.tar.gz</download_link>
      <date>1463174053</date>
      <mdhash>d27a497c7bdd764bc6ece59133bb09b8</mdhash>
      <filesize>33382</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d27a497c7bdd764bc6ece59133bb09b8</md5>
          <size>33382</size>
          <filedate>1463174053</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>18114ea2487e27cd599a6fe5d9bee326</md5>
          <size>41116</size>
          <filedate>1463174053</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta2.tar.gz</download_link>
      <date>1430776981</date>
      <mdhash>d72e920db2778e3a2f187e0b891e60a2</mdhash>
      <filesize>32606</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d72e920db2778e3a2f187e0b891e60a2</md5>
          <size>32606</size>
          <filedate>1430776981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>153d3fc84febbc20e5662d6b3124dac1</md5>
          <size>40317</size>
          <filedate>1430776981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta1.tar.gz</download_link>
      <date>1410905328</date>
      <mdhash>42609b0b33a971c3ffe2410dfc9a1507</mdhash>
      <filesize>29603</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42609b0b33a971c3ffe2410dfc9a1507</md5>
          <size>29603</size>
          <filedate>1410905328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a729bea6d5557241440daac6b81f6713</md5>
          <size>36904</size>
          <filedate>1410905328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa_basic 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa_basic/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.x-dev.tar.gz</download_link>
      <date>1711666782</date>
      <mdhash>57c8d293da919980a7fa50a548342d63</mdhash>
      <filesize>35018</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57c8d293da919980a7fa50a548342d63</md5>
          <size>35018</size>
          <filedate>1711666782</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa_basic-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ccb59ba5f76a48e2fc4be4ea196b414b</md5>
          <size>43484</size>
          <filedate>1711666782</filedate>
        </file>
      </files>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
  </releases>
</project>
