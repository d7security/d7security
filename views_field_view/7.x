<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Field View</title>
  <short_name>views_field_view</short_name>
  <dc:creator>dawehner</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/views_field_view</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_field_view 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/views_field_view/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67458827/packages/generic/views_field_view/7.x-1.3/views_field_view-7.x-1.3.tar.gz</download_link>
      <date>1740494572</date>
      <mdhash>34c42fec46da2e53d398114b53cf8ad7</mdhash>
      <filesize>18306</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67458827/packages/generic/views_field_view/7.x-1.3/views_field_view-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>34c42fec46da2e53d398114b53cf8ad7</md5>
          <size>18306</size>
          <filedate>1740494572</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67458827/packages/generic/views_field_view/7.x-1.3/views_field_view-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6fbae4bb2ed16b22bd8dee072b281c79</md5>
          <size>20926</size>
          <filedate>1740494572</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.2.tar.gz</download_link>
      <date>1442561051</date>
      <mdhash>770f9dd87dfd671ca694e710ea857037</mdhash>
      <filesize>18128</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>770f9dd87dfd671ca694e710ea857037</md5>
          <size>18128</size>
          <filedate>1442561051</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9678f244cfeaf3881e7529775b8e7ff</md5>
          <size>20607</size>
          <filedate>1442561051</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.1.tar.gz</download_link>
      <date>1369511162</date>
      <mdhash>b8b60b9b01a83458ade30684150c68ff</mdhash>
      <filesize>17114</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8b60b9b01a83458ade30684150c68ff</md5>
          <size>17114</size>
          <filedate>1369511162</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c16405e467915f218a49bc81f7dde74</md5>
          <size>19550</size>
          <filedate>1369511162</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0.tar.gz</download_link>
      <date>1347902395</date>
      <mdhash>9571ece5309db2adb70a65a346981341</mdhash>
      <filesize>16947</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9571ece5309db2adb70a65a346981341</md5>
          <size>16947</size>
          <filedate>1347902395</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>84d5a05b154fe35cef6c6bf67fb8b4f7</md5>
          <size>19384</size>
          <filedate>1347902395</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc3.tar.gz</download_link>
      <date>1337181098</date>
      <mdhash>50c38f9673d1f5248cf04d9f9483d6a9</mdhash>
      <filesize>15625</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>50c38f9673d1f5248cf04d9f9483d6a9</md5>
          <size>15625</size>
          <filedate>1337181098</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>75948b206cea86d579aed1a4ea8a5ef1</md5>
          <size>17571</size>
          <filedate>1337181098</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc2.tar.gz</download_link>
      <date>1333560973</date>
      <mdhash>60479557964c5725d857e3fc7e1c2514</mdhash>
      <filesize>15221</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>60479557964c5725d857e3fc7e1c2514</md5>
          <size>15221</size>
          <filedate>1333560973</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f94e4d5067eebaa73b18a6cc43e73d82</md5>
          <size>17141</size>
          <filedate>1333560973</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc1.tar.gz</download_link>
      <date>1324652756</date>
      <mdhash>d8960a723255c8510bc6fcea36ab2332</mdhash>
      <filesize>12031</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d8960a723255c8510bc6fcea36ab2332</md5>
          <size>12031</size>
          <filedate>1324652756</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b88b6bbb6b80c7e043ced4726e730b42</md5>
          <size>13229</size>
          <filedate>1324652756</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_field_view 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_field_view/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.x-dev.tar.gz</download_link>
      <date>1440827640</date>
      <mdhash>011e10be394f912355511b8eeec3d5e5</mdhash>
      <filesize>18126</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>011e10be394f912355511b8eeec3d5e5</md5>
          <size>18126</size>
          <filedate>1440827640</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_field_view-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b85d8c4fc46d2a9d1a3b3c8486668f94</md5>
          <size>20613</size>
          <filedate>1440827640</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
