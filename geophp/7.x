<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>geoPHP</title>
  <short_name>geophp</short_name>
  <dc:creator>neslee canil pinto</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/geophp</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>geophp 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.8.tar.gz</download_link>
      <date>1599928820</date>
      <mdhash>0d1dc46748c08201697d79f7c1579b4b</mdhash>
      <filesize>199129</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d1dc46748c08201697d79f7c1579b4b</md5>
          <size>199129</size>
          <filedate>1599928820</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>034469853ce9733637ded75e08efa17b</md5>
          <size>221790</size>
          <filedate>1599928820</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.7.tar.gz</download_link>
      <date>1352084822</date>
      <mdhash>66d43e0f91da47fcc39633c211328d90</mdhash>
      <filesize>196513</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66d43e0f91da47fcc39633c211328d90</md5>
          <size>196513</size>
          <filedate>1352084822</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>3655df80d3b7277b752d5fcedc4e6acf</md5>
          <size>217372</size>
          <filedate>1352084822</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.6.tar.gz</download_link>
      <date>1346278925</date>
      <mdhash>5c7e9cd282ce44b1a1dbb496d40aa4db</mdhash>
      <filesize>112597</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5c7e9cd282ce44b1a1dbb496d40aa4db</md5>
          <size>112597</size>
          <filedate>1346278925</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>46197ebaac5980eaf61346fe91a1d743</md5>
          <size>128998</size>
          <filedate>1346278925</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.5.tar.gz</download_link>
      <date>1346104041</date>
      <mdhash>137a014a1c4ed0e2c33db2efbf1f14f4</mdhash>
      <filesize>194938</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>137a014a1c4ed0e2c33db2efbf1f14f4</md5>
          <size>194938</size>
          <filedate>1346104041</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>1b8da3245f7b3c34cc2717f50c2d2da1</md5>
          <size>214912</size>
          <filedate>1346104042</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.4.tar.gz</download_link>
      <date>1336257955</date>
      <mdhash>0cc13648722d43070d641f1cda208102</mdhash>
      <filesize>112597</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0cc13648722d43070d641f1cda208102</md5>
          <size>112597</size>
          <filedate>1336257955</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>df02e221e4a6894dcfe046a0360fdba1</md5>
          <size>128996</size>
          <filedate>1336257955</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.3.tar.gz</download_link>
      <date>1333313747</date>
      <mdhash>165009977dc26faeab19f63c8f071e1d</mdhash>
      <filesize>86754</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>165009977dc26faeab19f63c8f071e1d</md5>
          <size>86754</size>
          <filedate>1333313747</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>15f037d164aaf96894b32f7656f7cf82</md5>
          <size>100899</size>
          <filedate>1333313747</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.2.tar.gz</download_link>
      <date>1333312549</date>
      <mdhash>1ab5bd0e3cf6e74de0357102f99464a4</mdhash>
      <filesize>86756</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1ab5bd0e3cf6e74de0357102f99464a4</md5>
          <size>86756</size>
          <filedate>1333312549</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f2fbf87341abe37127d4d6d460df0aa</md5>
          <size>100895</size>
          <filedate>1333312549</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.1.tar.gz</download_link>
      <date>1331694350</date>
      <mdhash>5cd5d241b647052a03e9eed3c45e27aa</mdhash>
      <filesize>86564</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5cd5d241b647052a03e9eed3c45e27aa</md5>
          <size>86564</size>
          <filedate>1331694350</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0609e165807d6906fd1eb44e2f7f83e</md5>
          <size>100366</size>
          <filedate>1331694350</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.0.tar.gz</download_link>
      <date>1330836048</date>
      <mdhash>a39fdc11914c9b824813b2fa5e9a3d21</mdhash>
      <filesize>7839</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a39fdc11914c9b824813b2fa5e9a3d21</md5>
          <size>7839</size>
          <filedate>1330836048</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>4bc36cfeb28b1e2684ec847d686e672b</md5>
          <size>8352</size>
          <filedate>1330836048</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geophp 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geophp/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geophp-7.x-1.x-dev.tar.gz</download_link>
      <date>1646316168</date>
      <mdhash>e0143d393b9f2636e79d144eb63e1709</mdhash>
      <filesize>199142</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e0143d393b9f2636e79d144eb63e1709</md5>
          <size>199142</size>
          <filedate>1646316168</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geophp-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>212782697f731ec218d03d400e5c8975</md5>
          <size>221795</size>
          <filedate>1646316168</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
