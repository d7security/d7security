<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Firstchild</title>
  <short_name>menu_firstchild</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/menu_firstchild</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_firstchild 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_firstchild/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.1.tar.gz</download_link>
      <date>1320754836</date>
      <mdhash>1ade2e28384022f2b9c9c55ed414b0e5</mdhash>
      <filesize>8632</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1ade2e28384022f2b9c9c55ed414b0e5</md5>
          <size>8632</size>
          <filedate>1320754836</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cbc6b4f978bb567efc87b87563fc23c7</md5>
          <size>9833</size>
          <filedate>1320754836</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_firstchild 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_firstchild/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0.tar.gz</download_link>
      <date>1295607105</date>
      <mdhash>f11fa4a47998c53353b5d364f6261028</mdhash>
      <filesize>8312</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f11fa4a47998c53353b5d364f6261028</md5>
          <size>8312</size>
          <filedate>1295607105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc48c8df72fc57fb054905f4c70dde27</md5>
          <size>10833</size>
          <filedate>1295607105</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_firstchild 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_firstchild/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0-beta1.tar.gz</download_link>
      <date>1288104038</date>
      <mdhash>61861c81d8b29a3274e7d5ee252843d3</mdhash>
      <filesize>8272</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61861c81d8b29a3274e7d5ee252843d3</md5>
          <size>8272</size>
          <filedate>1288104038</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7bd1688b389fa925a8b2f370b18235a8</md5>
          <size>10678</size>
          <filedate>1293232932</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_firstchild 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_firstchild/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.x-dev.tar.gz</download_link>
      <date>1665495157</date>
      <mdhash>96f3bc2bd4048efd74019b56fbccd90b</mdhash>
      <filesize>8965</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>96f3bc2bd4048efd74019b56fbccd90b</md5>
          <size>8965</size>
          <filedate>1665495157</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_firstchild-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8fc758ca64a01187b687a9946a1d2b25</md5>
          <size>10300</size>
          <filedate>1665495157</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
