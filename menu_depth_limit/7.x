<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Depth Limit</title>
  <short_name>menu_depth_limit</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/menu_depth_limit</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_depth_limit 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_depth_limit/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.0.tar.gz</download_link>
      <date>1341260809</date>
      <mdhash>7d14789985425183b5fd58c81e45e754</mdhash>
      <filesize>8858</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d14789985425183b5fd58c81e45e754</md5>
          <size>8858</size>
          <filedate>1341260809</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>f16b39154703632dbdf27148eb6032f0</md5>
          <size>9722</size>
          <filedate>1341260809</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_depth_limit 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_depth_limit/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.x-dev.tar.gz</download_link>
      <date>1380586815</date>
      <mdhash>f4c40814ae7f059fa883ae68a3223466</mdhash>
      <filesize>8874</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f4c40814ae7f059fa883ae68a3223466</md5>
          <size>8874</size>
          <filedate>1380586815</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_depth_limit-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e2b129783f0de0372e1279e0e1f7318</md5>
          <size>9736</size>
          <filedate>1380586816</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
