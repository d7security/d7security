<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>FileField Sources</title>
  <short_name>filefield_sources</short_name>
  <dc:creator>quicksketch</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/filefield_sources</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>filefield_sources 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.11.tar.gz</download_link>
      <date>1517969883</date>
      <mdhash>5718b0dfc31103ca20604f01e3ca30fa</mdhash>
      <filesize>29986</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5718b0dfc31103ca20604f01e3ca30fa</md5>
          <size>29986</size>
          <filedate>1517969883</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>e48ad8ff620acf3d0dfc48188f9e6b61</md5>
          <size>39917</size>
          <filedate>1517969883</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.10.tar.gz</download_link>
      <date>1439892839</date>
      <mdhash>129a4ed883f879952b89b3ba95150917</mdhash>
      <filesize>29658</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>129a4ed883f879952b89b3ba95150917</md5>
          <size>29658</size>
          <filedate>1439892839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>787abc3bc3b4dbb40fa3bcd4d98c6580</md5>
          <size>39376</size>
          <filedate>1439892839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.9.tar.gz</download_link>
      <date>1383155428</date>
      <mdhash>32f554452ce2d8d74511f9d62dde89b4</mdhash>
      <filesize>28973</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>32f554452ce2d8d74511f9d62dde89b4</md5>
          <size>28973</size>
          <filedate>1383155428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>950f8dc1f3e4a99c831ae2f5b9199c0f</md5>
          <size>38399</size>
          <filedate>1383155428</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.8.tar.gz</download_link>
      <date>1366782913</date>
      <mdhash>df496a7eef200aed130c70cf6f21731a</mdhash>
      <filesize>28648</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df496a7eef200aed130c70cf6f21731a</md5>
          <size>28648</size>
          <filedate>1366782913</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>a53b228074fae97e98addc461283a35f</md5>
          <size>38102</size>
          <filedate>1366782913</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.7.tar.gz</download_link>
      <date>1352060513</date>
      <mdhash>8fc5ac87404d82d3b4d15a2ac94c92dc</mdhash>
      <filesize>28045</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8fc5ac87404d82d3b4d15a2ac94c92dc</md5>
          <size>28045</size>
          <filedate>1352060513</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>d4ddf76d2dca1688922e9da31a273c54</md5>
          <size>37448</size>
          <filedate>1352060513</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.6.tar.gz</download_link>
      <date>1348074830</date>
      <mdhash>a04b4daa3f8dfb5a84a0e6fca8dcf257</mdhash>
      <filesize>25785</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a04b4daa3f8dfb5a84a0e6fca8dcf257</md5>
          <size>25785</size>
          <filedate>1348074830</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>75edf8d3a881b1470df46c8bac576f73</md5>
          <size>33326</size>
          <filedate>1348074830</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.5.tar.gz</download_link>
      <date>1346908024</date>
      <mdhash>30562a5e512b1090866d8ca691a68ca5</mdhash>
      <filesize>25778</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>30562a5e512b1090866d8ca691a68ca5</md5>
          <size>25778</size>
          <filedate>1346908024</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c2dd057aba85d2cb199ea79338e3645</md5>
          <size>33318</size>
          <filedate>1346908024</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.4.tar.gz</download_link>
      <date>1304045215</date>
      <mdhash>a2c3d6cae744d1c61cc302c43409b1cd</mdhash>
      <filesize>22974</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a2c3d6cae744d1c61cc302c43409b1cd</md5>
          <size>22974</size>
          <filedate>1304045215</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9fd1b7bcc81df115db7eda5a875ee469</md5>
          <size>30170</size>
          <filedate>1304045215</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.3.tar.gz</download_link>
      <date>1303711916</date>
      <mdhash>dc0dbe78ca89028651ab714aeaaa79e7</mdhash>
      <filesize>22972</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc0dbe78ca89028651ab714aeaaa79e7</md5>
          <size>22972</size>
          <filedate>1303711916</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd288ec5ae43dc76eb3c64f4afef2534</md5>
          <size>30173</size>
          <filedate>1303711916</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.2-beta1</name>
      <version>7.x-1.2-beta1</version>
      <tag>7.x-1.2-beta1</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.2-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.2-beta1.tar.gz</download_link>
      <date>1303537015</date>
      <mdhash>9e56129f25e14c19356b539e6cc6810b</mdhash>
      <filesize>20909</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.2-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9e56129f25e14c19356b539e6cc6810b</md5>
          <size>20909</size>
          <filedate>1303537015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.2-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d4cdc5fcb4bb0b495fb3aac5e8e6f2ae</md5>
          <size>27324</size>
          <filedate>1303537015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_sources 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_sources/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.x-dev.tar.gz</download_link>
      <date>1517967784</date>
      <mdhash>7837ea966c971cf4f8d00948e4275775</mdhash>
      <filesize>29991</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7837ea966c971cf4f8d00948e4275775</md5>
          <size>29991</size>
          <filedate>1517967784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_sources-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b498d98ee59286b62dc40a9155796a2a</md5>
          <size>39922</size>
          <filedate>1517967784</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
