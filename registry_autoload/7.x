<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Registry Autoload</title>
  <short_name>registry_autoload</short_name>
  <dc:creator>fabianx</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/registry_autoload</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>registry_autoload 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/registry_autoload/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.3.tar.gz</download_link>
      <date>1441286339</date>
      <mdhash>f7d4b0ede263c927aa8e26a71444b4d9</mdhash>
      <filesize>14700</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f7d4b0ede263c927aa8e26a71444b4d9</md5>
          <size>14700</size>
          <filedate>1441286339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f07e066de7f840218b889841e07330bf</md5>
          <size>28104</size>
          <filedate>1441286339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>registry_autoload 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/registry_autoload/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.2.tar.gz</download_link>
      <date>1417618980</date>
      <mdhash>4cfe567254694b199e12b8f6c04f20c7</mdhash>
      <filesize>14634</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4cfe567254694b199e12b8f6c04f20c7</md5>
          <size>14634</size>
          <filedate>1417618980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ec5fed186ec163a963afb83c22be0fe</md5>
          <size>28015</size>
          <filedate>1417618980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>registry_autoload 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/registry_autoload/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.1.tar.gz</download_link>
      <date>1417615680</date>
      <mdhash>c09b254f64fe68d4c5bb49cbd0602e28</mdhash>
      <filesize>14403</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c09b254f64fe68d4c5bb49cbd0602e28</md5>
          <size>14403</size>
          <filedate>1417615680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8e6023ca7fab5548e39e4e6394f80cfe</md5>
          <size>26246</size>
          <filedate>1417615680</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>registry_autoload 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/registry_autoload/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.0.tar.gz</download_link>
      <date>1415961481</date>
      <mdhash>9ec43f622b4c631f19a8341f6bdd2f91</mdhash>
      <filesize>11842</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9ec43f622b4c631f19a8341f6bdd2f91</md5>
          <size>11842</size>
          <filedate>1415961481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6a30edefb7c06f8fd46664569e6e19e1</md5>
          <size>19936</size>
          <filedate>1415961481</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>registry_autoload 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/registry_autoload/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.x-dev.tar.gz</download_link>
      <date>1677620886</date>
      <mdhash>c7262d72604574d1ee6e7da8b3fa92d3</mdhash>
      <filesize>14694</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c7262d72604574d1ee6e7da8b3fa92d3</md5>
          <size>14694</size>
          <filedate>1677620886</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/registry_autoload-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9267d12ad618537e99696912adc1e4d5</md5>
          <size>28245</size>
          <filedate>1677620886</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
