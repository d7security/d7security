<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Hacked!</title>
  <short_name>hacked</short_name>
  <dc:creator>steven jones</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/hacked</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>hacked 7.x-2.0-beta8</name>
      <version>7.x-2.0-beta8</version>
      <tag>7.x-2.0-beta8</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta8.tar.gz</download_link>
      <date>1549435380</date>
      <mdhash>0c80da5bda19f03e3e3677e417885833</mdhash>
      <filesize>26384</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0c80da5bda19f03e3e3677e417885833</md5>
          <size>26384</size>
          <filedate>1549435380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>905523937fd56b6e8b4767361d46f91e</md5>
          <size>35826</size>
          <filedate>1549435380</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta7</name>
      <version>7.x-2.0-beta7</version>
      <tag>7.x-2.0-beta7</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta7.tar.gz</download_link>
      <date>1544477580</date>
      <mdhash>1352bbed1bf1082a5c7ec7682220fd1d</mdhash>
      <filesize>26238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1352bbed1bf1082a5c7ec7682220fd1d</md5>
          <size>26238</size>
          <filedate>1544477580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>cee72ffc8718fb3f0cf41400fb85901b</md5>
          <size>35693</size>
          <filedate>1544477580</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta6</name>
      <version>7.x-2.0-beta6</version>
      <tag>7.x-2.0-beta6</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta6.tar.gz</download_link>
      <date>1469219639</date>
      <mdhash>132b01b0d423a5aa18c5452ef898b75e</mdhash>
      <filesize>22675</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>132b01b0d423a5aa18c5452ef898b75e</md5>
          <size>22675</size>
          <filedate>1469219639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>8b2563e2e49a233fcfd494b83c3062d8</md5>
          <size>30380</size>
          <filedate>1469219639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta5</name>
      <version>7.x-2.0-beta5</version>
      <tag>7.x-2.0-beta5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta5.tar.gz</download_link>
      <date>1367823012</date>
      <mdhash>70ff1ea41cfad1e75b2eaff37b7c5029</mdhash>
      <filesize>22402</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>70ff1ea41cfad1e75b2eaff37b7c5029</md5>
          <size>22402</size>
          <filedate>1367823012</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d1c962e6ac37605317456aaad0208ca3</md5>
          <size>26167</size>
          <filedate>1367823012</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta4.tar.gz</download_link>
      <date>1330848043</date>
      <mdhash>eb73e46cda141a36197df1fbc3d579bf</mdhash>
      <filesize>22488</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb73e46cda141a36197df1fbc3d579bf</md5>
          <size>22488</size>
          <filedate>1330848043</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>98b5fb752ce60d39c49d9db311f81cc9</md5>
          <size>26274</size>
          <filedate>1330848043</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta3.tar.gz</download_link>
      <date>1300368968</date>
      <mdhash>94b2e59bd00fa8d5f33344d6905f56d6</mdhash>
      <filesize>20779</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94b2e59bd00fa8d5f33344d6905f56d6</md5>
          <size>20779</size>
          <filedate>1300368968</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a741181b31c35ea5783eb3474270795a</md5>
          <size>24598</size>
          <filedate>1300368968</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta2.tar.gz</download_link>
      <date>1299537971</date>
      <mdhash>993e512fc4cc50470acea5c253039883</mdhash>
      <filesize>19361</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>993e512fc4cc50470acea5c253039883</md5>
          <size>19361</size>
          <filedate>1299537971</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f218d76a589cce94d1efa7fc354ec867</md5>
          <size>23111</size>
          <filedate>1299537971</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta1.tar.gz</download_link>
      <date>1299423967</date>
      <mdhash>7dc1a721ad28318e01eb2dba70f4c16e</mdhash>
      <filesize>19303</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7dc1a721ad28318e01eb2dba70f4c16e</md5>
          <size>19303</size>
          <filedate>1299423967</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1777403585b57434fe85dfa7d55be90e</md5>
          <size>23056</size>
          <filedate>1299423967</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-3.x-dev.tar.gz</download_link>
      <date>1581683891</date>
      <mdhash>4518d47950e75bd2537f0eb34e6c9585</mdhash>
      <filesize>10761</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4518d47950e75bd2537f0eb34e6c9585</md5>
          <size>10761</size>
          <filedate>1581683891</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ebd7fbfff3fecd24793fd36919e604f8</md5>
          <size>12476</size>
          <filedate>1581683891</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>hacked 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/hacked/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/hacked-7.x-2.x-dev.tar.gz</download_link>
      <date>1549437484</date>
      <mdhash>c876d1fc24d13402ef516b99db22c7e3</mdhash>
      <filesize>26335</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c876d1fc24d13402ef516b99db22c7e3</md5>
          <size>26335</size>
          <filedate>1549437484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/hacked-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>95650eb42b14f272d49deaf9cf9e6541</md5>
          <size>35775</size>
          <filedate>1549437484</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
