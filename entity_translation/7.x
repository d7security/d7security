<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entity Translation</title>
  <short_name>entity_translation</short_name>
  <dc:creator>plach</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/entity_translation</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entity_translation 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.0.tar.gz</download_link>
      <date>1671618997</date>
      <mdhash>ed4e02b81907d553766a0134f87b3d39</mdhash>
      <filesize>98695</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ed4e02b81907d553766a0134f87b3d39</md5>
          <size>98695</size>
          <filedate>1671618997</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>569ecee7b3cfe099bdf49949ed873899</md5>
          <size>123079</size>
          <filedate>1671618997</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity_translation 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0.tar.gz</download_link>
      <date>1639006133</date>
      <mdhash>11db2af7a637dab99eca576e2a25ee92</mdhash>
      <filesize>98636</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11db2af7a637dab99eca576e2a25ee92</md5>
          <size>98636</size>
          <filedate>1639006133</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>0dd1744dba83a4394cf990159842bcc2</md5>
          <size>123044</size>
          <filedate>1639006133</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity_translation 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0-rc1.tar.gz</download_link>
      <date>1631081245</date>
      <mdhash>24d713847e1ccaf384828038b75ddfb9</mdhash>
      <filesize>98646</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24d713847e1ccaf384828038b75ddfb9</md5>
          <size>98646</size>
          <filedate>1631081245</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b90544289357c5e2d0abed9d0b6c093b</md5>
          <size>123029</size>
          <filedate>1631081245</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.1.tar.gz</download_link>
      <date>1574630884</date>
      <mdhash>f915b3a63b2acdc320a6115462687012</mdhash>
      <filesize>98273</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f915b3a63b2acdc320a6115462687012</md5>
          <size>98273</size>
          <filedate>1574630884</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6bf62aa13abdd159301f11ab5675f9ee</md5>
          <size>122433</size>
          <filedate>1574630884</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0.tar.gz</download_link>
      <date>1522600680</date>
      <mdhash>7933858666e7781c07c20c05be30d3d5</mdhash>
      <filesize>94878</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7933858666e7781c07c20c05be30d3d5</md5>
          <size>94878</size>
          <filedate>1522600680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>febfafa0c1ccf50f80a40acaa83492fd</md5>
          <size>118965</size>
          <filedate>1522600680</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-rc1.tar.gz</download_link>
      <date>1521631084</date>
      <mdhash>86f2b10c59a2ee5ce948b681560d0001</mdhash>
      <filesize>94876</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>86f2b10c59a2ee5ce948b681560d0001</md5>
          <size>94876</size>
          <filedate>1521631084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2923c89a8113448b0493d091085d7d10</md5>
          <size>118959</size>
          <filedate>1521631084</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta7.tar.gz</download_link>
      <date>1503350643</date>
      <mdhash>5d4ee3aea352273640ccb2f5145dde50</mdhash>
      <filesize>88359</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d4ee3aea352273640ccb2f5145dde50</md5>
          <size>88359</size>
          <filedate>1503350643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>7854ce446c15b17f0fd875a8021a59da</md5>
          <size>111990</size>
          <filedate>1503350643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta6.tar.gz</download_link>
      <date>1488532384</date>
      <mdhash>8ea8fb21603edcca0cf2a6cdb4b852bf</mdhash>
      <filesize>82264</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8ea8fb21603edcca0cf2a6cdb4b852bf</md5>
          <size>82264</size>
          <filedate>1488532384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>6abbbb061b1b64cad8ceee208d308624</md5>
          <size>104477</size>
          <filedate>1488532384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta5.tar.gz</download_link>
      <date>1462661040</date>
      <mdhash>144c17796dcbb817515ef1a8bb5c7711</mdhash>
      <filesize>80198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>144c17796dcbb817515ef1a8bb5c7711</md5>
          <size>80198</size>
          <filedate>1462661040</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>40a41a604d336ec513c2d3926a37f5ba</md5>
          <size>100809</size>
          <filedate>1462661040</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta4.tar.gz</download_link>
      <date>1421971080</date>
      <mdhash>81fdbd18abb8dc80e81c09deda8e320d</mdhash>
      <filesize>77355</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81fdbd18abb8dc80e81c09deda8e320d</md5>
          <size>77355</size>
          <filedate>1421971080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c0b5310de9c6fe9483a0fc1b5145e56</md5>
          <size>97010</size>
          <filedate>1421971080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta3</release_link>
      <download_link/>
      <date>1374601568</date>
      <mdhash>0ac4a62b82f9af44acb1950b9dd1ce2d</mdhash>
      <filesize>70426</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>0ac4a62b82f9af44acb1950b9dd1ce2d</md5>
          <size>70426</size>
          <filedate>1374601568</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>3a45a64a49d91f6b2db1c7cd53600020</md5>
          <size>88376</size>
          <filedate>1374601568</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta2</release_link>
      <download_link/>
      <date>1353501457</date>
      <mdhash>b9066a9a691dc1f5fb3e6ad9b56b5e93</mdhash>
      <filesize>67297</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>b9066a9a691dc1f5fb3e6ad9b56b5e93</md5>
          <size>67297</size>
          <filedate>1353501457</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>2822e1f283f3aa27b4ee90f46f7f5e59</md5>
          <size>85057</size>
          <filedate>1353501457</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-beta1</release_link>
      <download_link/>
      <date>1351833713</date>
      <mdhash>b746a39ae04bd326fe372d2db2ff6539</mdhash>
      <filesize>66578</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>b746a39ae04bd326fe372d2db2ff6539</md5>
          <size>66578</size>
          <filedate>1351833713</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>d265defafbb16cfde3e47368afa0b714</md5>
          <size>84301</size>
          <filedate>1351833713</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-alpha2</release_link>
      <download_link/>
      <date>1339852879</date>
      <mdhash>d2347a128b2d1e608048528e89a904f9</mdhash>
      <filesize>38446</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>d2347a128b2d1e608048528e89a904f9</md5>
          <size>38446</size>
          <filedate>1339852879</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a1b5b8e55c32b7cd28cf1ed39e51ffc0</md5>
          <size>51126</size>
          <filedate>1339852879</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1315386420</date>
      <mdhash>125baf46de4fd9f82df72e24918b5f7b</mdhash>
      <filesize>27442</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>125baf46de4fd9f82df72e24918b5f7b</md5>
          <size>27442</size>
          <filedate>1315386420</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4ee0c9618e49140b7d4a446c511d0a6</md5>
          <size>35883</size>
          <filedate>1315386420</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.x-dev.tar.gz</download_link>
      <date>1671619367</date>
      <mdhash>7f64293f15830b4d76b47d77829f8e85</mdhash>
      <filesize>98703</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7f64293f15830b4d76b47d77829f8e85</md5>
          <size>98703</size>
          <filedate>1671619367</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>3cbf6423de994bc7419e0cd9eca258a8</md5>
          <size>123098</size>
          <filedate>1671619367</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.x-dev.tar.gz</download_link>
      <date>1639007217</date>
      <mdhash>bf266b325ad0275290d5b9389266f93c</mdhash>
      <filesize>98671</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf266b325ad0275290d5b9389266f93c</md5>
          <size>98671</size>
          <filedate>1639007217</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>fdce7aa271eebb2f1194a4494dc7b25c</md5>
          <size>123063</size>
          <filedate>1639007217</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entity_translation 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entity_translation/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.x-dev.tar.gz</download_link>
      <date>1639005351</date>
      <mdhash>51ae72759d28cdd09b460f7b10ea6b1f</mdhash>
      <filesize>98635</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51ae72759d28cdd09b460f7b10ea6b1f</md5>
          <size>98635</size>
          <filedate>1639005351</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entity_translation-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b19d0bdd45b8bd71a131cdde884592b6</md5>
          <size>123046</size>
          <filedate>1639005351</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
