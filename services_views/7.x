<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Services Views</title>
  <short_name>services_views</short_name>
  <dc:creator>generalredneck</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/services_views</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>services_views 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/services_views/-/releases/7.x-1.5</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67462550/packages/generic/services_views/7.x-1.5/services_views-7.x-1.5.tar.gz</download_link>
      <date>1740499755</date>
      <mdhash>9a2bad5141350d2a46d56996fa278c00</mdhash>
      <filesize>22726</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67462550/packages/generic/services_views/7.x-1.5/services_views-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a2bad5141350d2a46d56996fa278c00</md5>
          <size>22726</size>
          <filedate>1740499755</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67462550/packages/generic/services_views/7.x-1.5/services_views-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>f5178b576f3305f37e51a50ca65d8ca7</md5>
          <size>28680</size>
          <filedate>1740499755</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.4.tar.gz</download_link>
      <date>1573747386</date>
      <mdhash>803f515ae1005daed98380b4b0f8e217</mdhash>
      <filesize>21330</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>803f515ae1005daed98380b4b0f8e217</md5>
          <size>21330</size>
          <filedate>1573747386</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>0aa08f81be02d3e42cda17c25c6c3d51</md5>
          <size>27244</size>
          <filedate>1573747386</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.3.tar.gz</download_link>
      <date>1512141185</date>
      <mdhash>5a15ee959e6aa69e943527432168b916</mdhash>
      <filesize>21324</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5a15ee959e6aa69e943527432168b916</md5>
          <size>21324</size>
          <filedate>1512141185</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f5918a016ea4e5c7ff36167e1c57b32</md5>
          <size>27225</size>
          <filedate>1512141185</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.3-rc1</name>
      <version>7.x-1.3-rc1</version>
      <tag>7.x-1.3-rc1</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.3-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.3-rc1.tar.gz</download_link>
      <date>1504484044</date>
      <mdhash>21986b0e4b6d77294383c070e242c68c</mdhash>
      <filesize>21326</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.3-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>21986b0e4b6d77294383c070e242c68c</md5>
          <size>21326</size>
          <filedate>1504484044</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.3-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c2b773551f27d3d535b330c003ed671a</md5>
          <size>27231</size>
          <filedate>1504484044</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services_views 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.2.tar.gz</download_link>
      <date>1502128443</date>
      <mdhash>afff378b8dd258452c9c2bc06a63ffc7</mdhash>
      <filesize>16344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>afff378b8dd258452c9c2bc06a63ffc7</md5>
          <size>16344</size>
          <filedate>1502128443</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4446e81ce779bc492d3165881f3eb79d</md5>
          <size>19781</size>
          <filedate>1502128443</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.2-beta1</name>
      <version>7.x-1.2-beta1</version>
      <tag>7.x-1.2-beta1</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.2-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.2-beta1.tar.gz</download_link>
      <date>1472160539</date>
      <mdhash>60b7c78bf3eb557ade91e7329591ed0b</mdhash>
      <filesize>14331</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.2-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>60b7c78bf3eb557ade91e7329591ed0b</md5>
          <size>14331</size>
          <filedate>1472160539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.2-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d6a3ba708346f126d5170805cc066b5e</md5>
          <size>17098</size>
          <filedate>1472160539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services_views 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.1.tar.gz</download_link>
      <date>1440452639</date>
      <mdhash>f5638b9d48f0b41649564d7063cd9600</mdhash>
      <filesize>14154</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f5638b9d48f0b41649564d7063cd9600</md5>
          <size>14154</size>
          <filedate>1440452639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>95869bdcf10b79bbbbf34d3eeb72759c</md5>
          <size>16861</size>
          <filedate>1440452639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.0.tar.gz</download_link>
      <date>1388679504</date>
      <mdhash>c7e7715dadeb06129c48f76215cf2e53</mdhash>
      <filesize>12764</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c7e7715dadeb06129c48f76215cf2e53</md5>
          <size>12764</size>
          <filedate>1388679504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7c432813abf4fc95a6a62f90823291b8</md5>
          <size>15244</size>
          <filedate>1388679504</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services_views 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta2.tar.gz</download_link>
      <date>1339563415</date>
      <mdhash>0dad018961dc1e169f104ad06f8426d5</mdhash>
      <filesize>12304</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0dad018961dc1e169f104ad06f8426d5</md5>
          <size>12304</size>
          <filedate>1339563415</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>24c6109646d810eb0283a79d895ceab9</md5>
          <size>14309</size>
          <filedate>1339563415</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services_views 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta1.tar.gz</download_link>
      <date>1338541014</date>
      <mdhash>71829b939094e1c02f4283dfe6349c68</mdhash>
      <filesize>11418</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>71829b939094e1c02f4283dfe6349c68</md5>
          <size>11418</size>
          <filedate>1338541014</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6a4ee0b7570176571a48d32096a16ded</md5>
          <size>13479</size>
          <filedate>1338541014</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services_views 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services_views/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services_views-7.x-1.x-dev.tar.gz</download_link>
      <date>1597633219</date>
      <mdhash>cf29c5b192afab7b738aeb0f7b9eb028</mdhash>
      <filesize>22405</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cf29c5b192afab7b738aeb0f7b9eb028</md5>
          <size>22405</size>
          <filedate>1597633219</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services_views-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f82c0b60d44353f55c6dc10fc595c2b6</md5>
          <size>28436</size>
          <filedate>1597633219</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
