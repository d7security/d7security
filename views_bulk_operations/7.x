<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Bulk Operations (VBO)</title>
  <short_name>views_bulk_operations</short_name>
  <dc:creator>bojanz</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_bulk_operations</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_bulk_operations 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.7.tar.gz</download_link>
      <date>1666149178</date>
      <mdhash>442c7e81f563d2d310ae25d345ca6a81</mdhash>
      <filesize>46329</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>442c7e81f563d2d310ae25d345ca6a81</md5>
          <size>46329</size>
          <filedate>1666149178</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>e626843ffb811b4a3de850c8d917fa11</md5>
          <size>62589</size>
          <filedate>1666149178</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.6.tar.gz</download_link>
      <date>1591196777</date>
      <mdhash>b6ff09d19230da77d90c2675061ae69d</mdhash>
      <filesize>46237</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b6ff09d19230da77d90c2675061ae69d</md5>
          <size>46237</size>
          <filedate>1591196777</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>acb77d336c37c95d5cd75d2f4fcb0d9a</md5>
          <size>62491</size>
          <filedate>1591196777</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.5.tar.gz</download_link>
      <date>1525821480</date>
      <mdhash>e411e705507d4cf4cbcece29ecfcbf38</mdhash>
      <filesize>45974</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e411e705507d4cf4cbcece29ecfcbf38</md5>
          <size>45974</size>
          <filedate>1525821480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>6698a57f702f25db6b77c73870290ef7</md5>
          <size>62207</size>
          <filedate>1525821480</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.4.tar.gz</download_link>
      <date>1487698683</date>
      <mdhash>0009517dea9b41bccfdd6ded6881e21a</mdhash>
      <filesize>45529</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0009517dea9b41bccfdd6ded6881e21a</md5>
          <size>45529</size>
          <filedate>1487698683</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>04643e823eb39265beb519bb18625fa6</md5>
          <size>61718</size>
          <filedate>1487698683</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.3.tar.gz</download_link>
      <date>1435764539</date>
      <mdhash>b9583bb0a2e9b89926adc3ecb2bda93b</mdhash>
      <filesize>44411</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9583bb0a2e9b89926adc3ecb2bda93b</md5>
          <size>44411</size>
          <filedate>1435764539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d9472296ed59a2f2b79a92d2e04562cd</md5>
          <size>59432</size>
          <filedate>1435764539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.2.tar.gz</download_link>
      <date>1387798104</date>
      <mdhash>6e504288c3cde5dc71ef536764c14650</mdhash>
      <filesize>44149</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6e504288c3cde5dc71ef536764c14650</md5>
          <size>44149</size>
          <filedate>1387798104</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a346e970c455bf20065632b45e76535d</md5>
          <size>58383</size>
          <filedate>1387798104</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.1.tar.gz</download_link>
      <date>1354500015</date>
      <mdhash>0650fc653870b5c12b2e7772ceaf6761</mdhash>
      <filesize>41247</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0650fc653870b5c12b2e7772ceaf6761</md5>
          <size>41247</size>
          <filedate>1354500015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e94fdb5565fcac4d25c256ed07ea7fde</md5>
          <size>54744</size>
          <filedate>1354500015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0.tar.gz</download_link>
      <date>1347654564</date>
      <mdhash>9c80c378576571607af80f578d0a732d</mdhash>
      <filesize>43783</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9c80c378576571607af80f578d0a732d</md5>
          <size>43783</size>
          <filedate>1347654564</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>df8f71b3849c249c1b624529b6f589d3</md5>
          <size>56703</size>
          <filedate>1347654564</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0-rc1.tar.gz</download_link>
      <date>1328576162</date>
      <mdhash>801679e983d2f7021584b40328e828ee</mdhash>
      <filesize>45073</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>801679e983d2f7021584b40328e828ee</md5>
          <size>45073</size>
          <filedate>1328576162</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fcb344537f031ab7485257a1d7f1cb7c</md5>
          <size>57893</size>
          <filedate>1328576162</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-beta3</name>
      <version>7.x-3.0-beta3</version>
      <tag>7.x-3.0-beta3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-beta3</release_link>
      <download_link/>
      <date>1318586502</date>
      <mdhash>6533e4877c8fb6cce9b30ef21368155e</mdhash>
      <filesize>37993</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>6533e4877c8fb6cce9b30ef21368155e</md5>
          <size>37993</size>
          <filedate>1318586502</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>184c4dec5f3e3329609c371c6763f788</md5>
          <size>48944</size>
          <filedate>1318586502</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-beta2</release_link>
      <download_link/>
      <date>1313422921</date>
      <mdhash>1c66f47594adc4a1e902ce248a0b3e44</mdhash>
      <filesize>34077</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>1c66f47594adc4a1e902ce248a0b3e44</md5>
          <size>34077</size>
          <filedate>1313422921</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>e80c96d9a8e9e09a101021989e8a65b5</md5>
          <size>44344</size>
          <filedate>1313422921</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-beta1</release_link>
      <download_link/>
      <date>1310769122</date>
      <mdhash>69852b6f084949ea312a016c0ede58d0</mdhash>
      <filesize>33185</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>69852b6f084949ea312a016c0ede58d0</md5>
          <size>33185</size>
          <filedate>1310769122</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>6f80d68b71f98e4466e7665f015becfd</md5>
          <size>43333</size>
          <filedate>1310769122</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-alpha3</release_link>
      <download_link/>
      <date>1308505319</date>
      <mdhash>53ac21f79173a955ea298250ce5e73e6</mdhash>
      <filesize>30612</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>53ac21f79173a955ea298250ce5e73e6</md5>
          <size>30612</size>
          <filedate>1308505319</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>594507b7845aced572768879f3f622a4</md5>
          <size>38280</size>
          <filedate>1308505319</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-alpha2</release_link>
      <download_link/>
      <date>1306677417</date>
      <mdhash>705895d000a7919df2afcf8773017692</mdhash>
      <filesize>31069</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>705895d000a7919df2afcf8773017692</md5>
          <size>31069</size>
          <filedate>1306677417</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>dd5118cba1f1ebc4f2fe18c701694273</md5>
          <size>38554</size>
          <filedate>1306677417</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.0-alpha1</release_link>
      <download_link/>
      <date>1306621617</date>
      <mdhash>c0e74e29388a67d12af24d5db4dcdda8</mdhash>
      <filesize>30918</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c0e74e29388a67d12af24d5db4dcdda8</md5>
          <size>30918</size>
          <filedate>1306621617</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>37880331f0c516c28f30c6d65577229e</md5>
          <size>38376</size>
          <filedate>1306621617</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_bulk_operations 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_bulk_operations/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.x-dev.tar.gz</download_link>
      <date>1666131152</date>
      <mdhash>241b0e3182bb81f959f14b6a4d6c36d8</mdhash>
      <filesize>46329</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>241b0e3182bb81f959f14b6a4d6c36d8</md5>
          <size>46329</size>
          <filedate>1666131152</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_bulk_operations-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>92dc4e974fe762c2d72d93cd7d9aeb45</md5>
          <size>62597</size>
          <filedate>1666131152</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
