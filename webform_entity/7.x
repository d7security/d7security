<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Webform Entity</title>
  <short_name>webform_entity</short_name>
  <dc:creator>tizzo</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/webform_entity</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>webform_entity 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/webform_entity/-/releases/7.x-1.1</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.1/webform_entity-7.x-1.1.tar.gz</download_link>
      <date>1737749900</date>
      <mdhash>1ec4c2af5947be787d71a94f97ecbfb9</mdhash>
      <filesize>27608</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.1/webform_entity-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1ec4c2af5947be787d71a94f97ecbfb9</md5>
          <size>27608</size>
          <filedate>1737749900</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.1/webform_entity-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>81ca5317736c9f2c25546a2e0cab07ec</md5>
          <size>35829</size>
          <filedate>1737749900</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>webform_entity 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/webform_entity/-/releases/7.x-1.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.0/webform_entity-7.x-1.0.tar.gz</download_link>
      <date>1737748258</date>
      <mdhash>0a9120d450cd8decdc4b61a62eca6583</mdhash>
      <filesize>27609</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.0/webform_entity-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0a9120d450cd8decdc4b61a62eca6583</md5>
          <size>27609</size>
          <filedate>1737748258</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66504678/packages/generic/webform_entity/7.x-1.0/webform_entity-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0d1ba450af71d2ee08cafb546b6cfb2</md5>
          <size>35828</size>
          <filedate>1737748258</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>webform_entity 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_entity/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_entity-7.x-1.x-dev.tar.gz</download_link>
      <date>1446479340</date>
      <mdhash>546f081e8ae2f55e8b5e4caaed00fe9d</mdhash>
      <filesize>27487</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_entity-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>546f081e8ae2f55e8b5e4caaed00fe9d</md5>
          <size>27487</size>
          <filedate>1446479340</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_entity-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8e854672a7d6ad8e6552183ab4a0eef2</md5>
          <size>35601</size>
          <filedate>1446479340</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
