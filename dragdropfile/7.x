<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>HTML5 Drag &amp;amp; Drop File</title>
  <short_name>dragdropfile</short_name>
  <dc:creator>rudiedirkx</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/dragdropfile</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking new maintainer</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>dragdropfile 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.7.tar.gz</download_link>
      <date>1471433939</date>
      <mdhash>482b3d92ddfc125e61edcdb89033ad9f</mdhash>
      <filesize>15105</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>482b3d92ddfc125e61edcdb89033ad9f</md5>
          <size>15105</size>
          <filedate>1471433939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>99f391ec22c72a340655ddeba1dfb58b</md5>
          <size>17422</size>
          <filedate>1471433939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.6.tar.gz</download_link>
      <date>1467407339</date>
      <mdhash>60b28116d0262591a39638aef188056f</mdhash>
      <filesize>14912</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>60b28116d0262591a39638aef188056f</md5>
          <size>14912</size>
          <filedate>1467407339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>697bfaba1f76e3134337ac8e5b0739ae</md5>
          <size>17229</size>
          <filedate>1467407339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.5.tar.gz</download_link>
      <date>1445534339</date>
      <mdhash>edfea2fd9c95ea496c671f41b01c70fc</mdhash>
      <filesize>15672</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>edfea2fd9c95ea496c671f41b01c70fc</md5>
          <size>15672</size>
          <filedate>1445534339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>6ca9c1082ac9f72750de0a254ea21def</md5>
          <size>18342</size>
          <filedate>1445534339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.4.tar.gz</download_link>
      <date>1441385339</date>
      <mdhash>78508fe2fe22a00f130ac2bcff74e17b</mdhash>
      <filesize>15081</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>78508fe2fe22a00f130ac2bcff74e17b</md5>
          <size>15081</size>
          <filedate>1441385339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>5b0a5d1ca3863b2a1278be090f31489e</md5>
          <size>17657</size>
          <filedate>1441385339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.3.tar.gz</download_link>
      <date>1400246627</date>
      <mdhash>1b11b1d0b0fda427e6435ce4612ce4a1</mdhash>
      <filesize>13691</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b11b1d0b0fda427e6435ce4612ce4a1</md5>
          <size>13691</size>
          <filedate>1400246627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>183577ea85264d3299e6366b13582ac6</md5>
          <size>17064</size>
          <filedate>1400246627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.2.tar.gz</download_link>
      <date>1389367105</date>
      <mdhash>e65a26cf5233f68eb6ca40f28d2533b7</mdhash>
      <filesize>13578</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e65a26cf5233f68eb6ca40f28d2533b7</md5>
          <size>13578</size>
          <filedate>1389367105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>53444e0486955d034f044f66e8ec6c40</md5>
          <size>16964</size>
          <filedate>1389367105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.1.tar.gz</download_link>
      <date>1389364405</date>
      <mdhash>a25ec0ffcd5d94c9b356c28838292eba</mdhash>
      <filesize>13494</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a25ec0ffcd5d94c9b356c28838292eba</md5>
          <size>13494</size>
          <filedate>1389364405</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e1b863e0a020c082d1554bb09dd646ae</md5>
          <size>16854</size>
          <filedate>1389364405</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0.tar.gz</download_link>
      <date>1386365624</date>
      <mdhash>f957a36bba08a1e62476c5d57611f27f</mdhash>
      <filesize>12724</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f957a36bba08a1e62476c5d57611f27f</md5>
          <size>12724</size>
          <filedate>1386365624</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>57fa7043fae20cd01c277fe9ef184f96</md5>
          <size>15635</size>
          <filedate>1386365624</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1378842136</date>
      <mdhash>19faced9f44ce86320823b8fa25fd28b</mdhash>
      <filesize>12766</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>19faced9f44ce86320823b8fa25fd28b</md5>
          <size>12766</size>
          <filedate>1378842136</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>769ac04815a1ea5659ede9804ce6ebf7</md5>
          <size>15610</size>
          <filedate>1378842136</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1378651308</date>
      <mdhash>e6e10e2c9672703ee788cca80925da62</mdhash>
      <filesize>12720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e6e10e2c9672703ee788cca80925da62</md5>
          <size>12720</size>
          <filedate>1378651308</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e79859c029cc751835909e24495048f</md5>
          <size>15504</size>
          <filedate>1378651308</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>dragdropfile 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/dragdropfile/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.x-dev.tar.gz</download_link>
      <date>1494613085</date>
      <mdhash>f19a52d40fe6be363f9bc1dd10d615cd</mdhash>
      <filesize>15446</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f19a52d40fe6be363f9bc1dd10d615cd</md5>
          <size>15446</size>
          <filedate>1494613085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/dragdropfile-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ad2511fb6c7d885aec4dec3520a0a002</md5>
          <size>17789</size>
          <filedate>1494613085</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
