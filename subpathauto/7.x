<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Sub-pathauto (Sub-path URL Aliases)</title>
  <short_name>subpathauto</short_name>
  <dc:creator>dave reid</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/subpathauto</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>subpathauto 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/subpathauto/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.3.tar.gz</download_link>
      <date>1347990612</date>
      <mdhash>a08f27deb146702c89baf10eae7cd716</mdhash>
      <filesize>10591</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a08f27deb146702c89baf10eae7cd716</md5>
          <size>10591</size>
          <filedate>1347990612</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>10531d8b7343261e58ab6b2713402e9c</md5>
          <size>12058</size>
          <filedate>1347990612</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>subpathauto 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/subpathauto/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.2.tar.gz</download_link>
      <date>1327514759</date>
      <mdhash>2e35a7252a0da530247f6f412fecbd6b</mdhash>
      <filesize>10471</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2e35a7252a0da530247f6f412fecbd6b</md5>
          <size>10471</size>
          <filedate>1327514759</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f51447b0ea8970459edbb6b62f24f0cd</md5>
          <size>11938</size>
          <filedate>1327514759</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>subpathauto 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/subpathauto/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.1.tar.gz</download_link>
      <date>1313276821</date>
      <mdhash>1d479122d4a1c521e911daa0788e1b3d</mdhash>
      <filesize>9169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1d479122d4a1c521e911daa0788e1b3d</md5>
          <size>9169</size>
          <filedate>1313276821</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c06bb0706ed0edcf7f58a62e2568c148</md5>
          <size>10555</size>
          <filedate>1313276821</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>subpathauto 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/subpathauto/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.0.tar.gz</download_link>
      <date>1313027819</date>
      <mdhash>d5b05274e7cc1795ed4a8fedcbcb8b80</mdhash>
      <filesize>8028</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d5b05274e7cc1795ed4a8fedcbcb8b80</md5>
          <size>8028</size>
          <filedate>1313027819</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc9a21b08eb8495efecd0520b6935c95</md5>
          <size>9013</size>
          <filedate>1313027819</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>subpathauto 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/subpathauto/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.x-dev.tar.gz</download_link>
      <date>1380638224</date>
      <mdhash>863ca3913ce63e3bddc8d7b9a433c67a</mdhash>
      <filesize>10600</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>863ca3913ce63e3bddc8d7b9a433c67a</md5>
          <size>10600</size>
          <filedate>1380638224</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/subpathauto-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a1cda6a20d7d5382f9e4b265a51fbf65</md5>
          <size>12062</size>
          <filedate>1380638224</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
