<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>OAuth2 Server</title>
  <short_name>oauth2_server</short_name>
  <dc:creator>bojanz</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/oauth2_server</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>oauth2_server 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/oauth2_server/-/releases/7.x-1.8</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66208021/packages/generic/oauth2_server/7.x-1.8/oauth2_server-7.x-1.8.tar.gz</download_link>
      <date>1737025263</date>
      <mdhash>acc5bf8e667573cc63753dac9322ddc1</mdhash>
      <filesize>48066</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66208021/packages/generic/oauth2_server/7.x-1.8/oauth2_server-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>acc5bf8e667573cc63753dac9322ddc1</md5>
          <size>48066</size>
          <filedate>1737025263</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66208021/packages/generic/oauth2_server/7.x-1.8/oauth2_server-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>71f6557ad2de44231bde738bd79a6524</md5>
          <size>64021</size>
          <filedate>1737025263</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.7.tar.gz</download_link>
      <date>1477993740</date>
      <mdhash>c3d401446215c043147acf638f493d86</mdhash>
      <filesize>46193</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c3d401446215c043147acf638f493d86</md5>
          <size>46193</size>
          <filedate>1477993740</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc4d9b2a57338fca916f1732bc2caab4</md5>
          <size>62396</size>
          <filedate>1477993740</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.6.tar.gz</download_link>
      <date>1456440839</date>
      <mdhash>c7d042867339e4740eafe1355714a04c</mdhash>
      <filesize>45945</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c7d042867339e4740eafe1355714a04c</md5>
          <size>45945</size>
          <filedate>1456440839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>057aa38d8eae6f79fe4702f60d654667</md5>
          <size>62254</size>
          <filedate>1456440839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.5.tar.gz</download_link>
      <date>1454767439</date>
      <mdhash>1b490b13c71838b77312c512c6b96173</mdhash>
      <filesize>45934</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b490b13c71838b77312c512c6b96173</md5>
          <size>45934</size>
          <filedate>1454767439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3f4a35413c7c5bc5d9cdeed28b765262</md5>
          <size>62229</size>
          <filedate>1454767439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.4.tar.gz</download_link>
      <date>1451471339</date>
      <mdhash>6f0e0122c2a630cf8d96c49823c15050</mdhash>
      <filesize>45453</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6f0e0122c2a630cf8d96c49823c15050</md5>
          <size>45453</size>
          <filedate>1451471339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>e18c58492ccd6da81b6cf2b0b0250af7</md5>
          <size>61666</size>
          <filedate>1451471339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.3.tar.gz</download_link>
      <date>1430995681</date>
      <mdhash>5366fb7e119a91c16ee04f1e11207d07</mdhash>
      <filesize>44659</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5366fb7e119a91c16ee04f1e11207d07</md5>
          <size>44659</size>
          <filedate>1430995681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6aeeb61485297bf2995acfd431f1edda</md5>
          <size>60251</size>
          <filedate>1430995681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.2.tar.gz</download_link>
      <date>1429779781</date>
      <mdhash>cac639db834e79179cca1e099d7428e5</mdhash>
      <filesize>42999</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cac639db834e79179cca1e099d7428e5</md5>
          <size>42999</size>
          <filedate>1429779781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>aab5b432c60a7af4fda01d65c68fd75f</md5>
          <size>57271</size>
          <filedate>1429779781</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.1.tar.gz</download_link>
      <date>1403009027</date>
      <mdhash>7214e23d50a0e378c36eb6b442b9f22e</mdhash>
      <filesize>38883</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7214e23d50a0e378c36eb6b442b9f22e</md5>
          <size>38883</size>
          <filedate>1403009027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1b1295af003f0d49fcd967aa9500bc20</md5>
          <size>52616</size>
          <filedate>1403009027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0.tar.gz</download_link>
      <date>1387820904</date>
      <mdhash>80ac3eca0ecee635833f183a76cd1949</mdhash>
      <filesize>30750</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>80ac3eca0ecee635833f183a76cd1949</md5>
          <size>30750</size>
          <filedate>1387820904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>509e2d6998b760d4fbbaa3edcec93d61</md5>
          <size>43629</size>
          <filedate>1387820904</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc3.tar.gz</download_link>
      <date>1378818408</date>
      <mdhash>163ddf244340257f7ea77c83f548ed6b</mdhash>
      <filesize>30879</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>163ddf244340257f7ea77c83f548ed6b</md5>
          <size>30879</size>
          <filedate>1378818408</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9c1bc4e5fe081a9dc8f61a855ea113bd</md5>
          <size>43427</size>
          <filedate>1378818408</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc2.tar.gz</download_link>
      <date>1378216631</date>
      <mdhash>c9c979b548c91db6bf63c75045abcd8c</mdhash>
      <filesize>30403</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c9c979b548c91db6bf63c75045abcd8c</md5>
          <size>30403</size>
          <filedate>1378216631</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>acee55c2824622d0e003506fb10f3ca0</md5>
          <size>42489</size>
          <filedate>1378216631</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc1.tar.gz</download_link>
      <date>1369729861</date>
      <mdhash>123d25c9718bbe0840cffaccd17bfcd5</mdhash>
      <filesize>30123</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>123d25c9718bbe0840cffaccd17bfcd5</md5>
          <size>30123</size>
          <filedate>1369729861</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>deaaaf2f2719b5d8208540b703d5e65c</md5>
          <size>42197</size>
          <filedate>1369729861</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta2.tar.gz</download_link>
      <date>1366069211</date>
      <mdhash>6a68a559f6783b7b1a74133ece9818b5</mdhash>
      <filesize>29780</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6a68a559f6783b7b1a74133ece9818b5</md5>
          <size>29780</size>
          <filedate>1366069211</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c417d458c0d67a7696a2afc3ee341818</md5>
          <size>41872</size>
          <filedate>1366069211</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta1.tar.gz</download_link>
      <date>1364901612</date>
      <mdhash>ef1c78202607fda7439f804454e3c97c</mdhash>
      <filesize>29297</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef1c78202607fda7439f804454e3c97c</md5>
          <size>29297</size>
          <filedate>1364901612</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5689f93fdf891115ddbc59fd9f48596c</md5>
          <size>41240</size>
          <filedate>1364901612</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>oauth2_server 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_server/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.x-dev.tar.gz</download_link>
      <date>1728265416</date>
      <mdhash>bce9a97f39976a9a6472404f4277a218</mdhash>
      <filesize>46635</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bce9a97f39976a9a6472404f4277a218</md5>
          <size>46635</size>
          <filedate>1728265416</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_server-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5489728ad1c2ec4ba47cd8dfc482aca9</md5>
          <size>63612</size>
          <filedate>1728265416</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
