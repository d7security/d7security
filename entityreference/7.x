<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entity reference</title>
  <short_name>entityreference</short_name>
  <dc:creator>Damien Tournoud</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/entityreference</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entityreference 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/entityreference/-/releases/7.x-1.10</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67502269/packages/generic/entityreference/7.x-1.10/entityreference-7.x-1.10.tar.gz</download_link>
      <date>1740589940</date>
      <mdhash>d3224b0a66ab37bfe702efeca4ba58f7</mdhash>
      <filesize>50823</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67502269/packages/generic/entityreference/7.x-1.10/entityreference-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d3224b0a66ab37bfe702efeca4ba58f7</md5>
          <size>50823</size>
          <filedate>1740589940</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67502269/packages/generic/entityreference/7.x-1.10/entityreference-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab24d7dfa0b194d0aed3c46b4aac3463</md5>
          <size>73869</size>
          <filedate>1740589940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.9.tar.gz</download_link>
      <date>1681490517</date>
      <mdhash>a262d90377672af0f959928453f77592</mdhash>
      <filesize>50667</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a262d90377672af0f959928453f77592</md5>
          <size>50667</size>
          <filedate>1681490517</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>f8292b0a409b0735ec1b0fa3ee80dc99</md5>
          <size>73391</size>
          <filedate>1681490517</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.8.tar.gz</download_link>
      <date>1679585046</date>
      <mdhash>8965378f63177a294f67fe0c10f17e00</mdhash>
      <filesize>50420</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8965378f63177a294f67fe0c10f17e00</md5>
          <size>50420</size>
          <filedate>1679585046</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>958ad865bd3d9226eff931cdee92a8aa</md5>
          <size>73122</size>
          <filedate>1679585046</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.7.tar.gz</download_link>
      <date>1678809340</date>
      <mdhash>ccf8c8a961e34b2c996add7c2b52e6e0</mdhash>
      <filesize>47482</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ccf8c8a961e34b2c996add7c2b52e6e0</md5>
          <size>47482</size>
          <filedate>1678809340</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>b8d5a61ca8846f71366bee91d8b2200e</md5>
          <size>67815</size>
          <filedate>1678809340</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6.tar.gz</download_link>
      <date>1676069718</date>
      <mdhash>40abf11ac4d4ff51ffa52374fac7fb6d</mdhash>
      <filesize>47323</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>40abf11ac4d4ff51ffa52374fac7fb6d</md5>
          <size>47323</size>
          <filedate>1676069718</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d39f1c596a1848b3c88b19dd7fd89ab</md5>
          <size>67601</size>
          <filedate>1676069718</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.6-rc3</name>
      <version>7.x-1.6-rc3</version>
      <tag>7.x-1.6-rc3</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.6-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc3.tar.gz</download_link>
      <date>1674798965</date>
      <mdhash>d0a4c5bd8089689cbefd956e9cc431cd</mdhash>
      <filesize>47328</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d0a4c5bd8089689cbefd956e9cc431cd</md5>
          <size>47328</size>
          <filedate>1674798965</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9831096730e455c537591ac2270f82c7</md5>
          <size>67613</size>
          <filedate>1674798965</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.6-rc2</name>
      <version>7.x-1.6-rc2</version>
      <tag>7.x-1.6-rc2</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.6-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc2.tar.gz</download_link>
      <date>1674165713</date>
      <mdhash>7c6c3b094bb6af9467eb4dac48f615d6</mdhash>
      <filesize>47223</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7c6c3b094bb6af9467eb4dac48f615d6</md5>
          <size>47223</size>
          <filedate>1674165713</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6e0b53501903ed12778824cd5d4b6f2</md5>
          <size>67480</size>
          <filedate>1674165713</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.6-rc1</name>
      <version>7.x-1.6-rc1</version>
      <tag>7.x-1.6-rc1</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.6-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc1.tar.gz</download_link>
      <date>1552335486</date>
      <mdhash>8c58d2dc008a17663996ad535cc15aa9</mdhash>
      <filesize>44298</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8c58d2dc008a17663996ad535cc15aa9</md5>
          <size>44298</size>
          <filedate>1552335486</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.6-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>208017c732e0479b271f6db9fe21480a</md5>
          <size>62654</size>
          <filedate>1552335486</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.5.tar.gz</download_link>
      <date>1502895843</date>
      <mdhash>56f6198ead378b93eb978444c3cdd948</mdhash>
      <filesize>43330</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>56f6198ead378b93eb978444c3cdd948</md5>
          <size>43330</size>
          <filedate>1502895843</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>774125279e067db5678cb592cf314977</md5>
          <size>61582</size>
          <filedate>1502895843</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.4.tar.gz</download_link>
      <date>1495557183</date>
      <mdhash>ce8bde18b2b0269f218a1e2dc17579ee</mdhash>
      <filesize>43230</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce8bde18b2b0269f218a1e2dc17579ee</md5>
          <size>43230</size>
          <filedate>1495557183</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>342c5e3a058a49dadfe6d440d87e63bd</md5>
          <size>61445</size>
          <filedate>1495557183</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.3.tar.gz</download_link>
      <date>1494450485</date>
      <mdhash>e800309d10d254bd8e80770beb17f4f3</mdhash>
      <filesize>43171</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e800309d10d254bd8e80770beb17f4f3</md5>
          <size>43171</size>
          <filedate>1494450485</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>06de121c2443eb1336ddda2964b1be09</md5>
          <size>61382</size>
          <filedate>1494450485</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.2.tar.gz</download_link>
      <date>1474306739</date>
      <mdhash>ef8f161721709dedcd1babc18b033006</mdhash>
      <filesize>40840</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef8f161721709dedcd1babc18b033006</md5>
          <size>40840</size>
          <filedate>1474306739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c341d8926551c4cbda6ab26e204eaca6</md5>
          <size>59065</size>
          <filedate>1474306739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.1.tar.gz</download_link>
      <date>1384973004</date>
      <mdhash>604843bda17acbd927f71f8cf8a74a48</mdhash>
      <filesize>40371</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>604843bda17acbd927f71f8cf8a74a48</md5>
          <size>40371</size>
          <filedate>1384973004</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>dab804823b45c67ad0ffde79283963a3</md5>
          <size>57857</size>
          <filedate>1384973004</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0.tar.gz</download_link>
      <date>1353230808</date>
      <mdhash>81f458c4b41b61f405af905d2fd8a5ec</mdhash>
      <filesize>36787</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81f458c4b41b61f405af905d2fd8a5ec</md5>
          <size>36787</size>
          <filedate>1353230808</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>37b9e9979928e4df8e4257dfd9fee28b</md5>
          <size>52376</size>
          <filedate>1353230808</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-rc5</name>
      <version>7.x-1.0-rc5</version>
      <tag>7.x-1.0-rc5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc5.tar.gz</download_link>
      <date>1348565045</date>
      <mdhash>648a462e0bc442533debbd0b1b98e665</mdhash>
      <filesize>32328</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>648a462e0bc442533debbd0b1b98e665</md5>
          <size>32328</size>
          <filedate>1348565045</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>53ec0e684495ec514c452c29af48c8f0</md5>
          <size>45935</size>
          <filedate>1348565045</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-rc4</name>
      <version>7.x-1.0-rc4</version>
      <tag>7.x-1.0-rc4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc4.tar.gz</download_link>
      <date>1348486666</date>
      <mdhash>b1bdad67159df7cfa1edbc71ca1f15be</mdhash>
      <filesize>31223</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b1bdad67159df7cfa1edbc71ca1f15be</md5>
          <size>31223</size>
          <filedate>1348486666</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>b0226cbdbd135a5859bc8d2567c9398a</md5>
          <size>44029</size>
          <filedate>1348486666</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc3.tar.gz</download_link>
      <date>1338411956</date>
      <mdhash>6f07ffafd42ec5fa9b76d398831c9956</mdhash>
      <filesize>30501</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6f07ffafd42ec5fa9b76d398831c9956</md5>
          <size>30501</size>
          <filedate>1338411956</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>89d0ee07114babdad4531c6d1fa69c8b</md5>
          <size>43242</size>
          <filedate>1338411956</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc2.tar.gz</download_link>
      <date>1338371757</date>
      <mdhash>35336e7b27b1755db8464d786167081b</mdhash>
      <filesize>30494</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35336e7b27b1755db8464d786167081b</md5>
          <size>30494</size>
          <filedate>1338371757</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0646705363588f765c23cb52cc748d32</md5>
          <size>43236</size>
          <filedate>1338371757</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc1.tar.gz</download_link>
      <date>1332279946</date>
      <mdhash>83f006ab5235890efc2b3986f8dcdfe3</mdhash>
      <filesize>24495</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83f006ab5235890efc2b3986f8dcdfe3</md5>
          <size>24495</size>
          <filedate>1332279946</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc798d28aef7a36246e8a5108f7fc909</md5>
          <size>33888</size>
          <filedate>1332279946</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta5.tar.gz</download_link>
      <date>1330297849</date>
      <mdhash>1508867497f54a68acb9f92ca4cddcfe</mdhash>
      <filesize>24228</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1508867497f54a68acb9f92ca4cddcfe</md5>
          <size>24228</size>
          <filedate>1330297849</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>9b0b20590623cf1024c2f2eaa32b8633</md5>
          <size>33597</size>
          <filedate>1330297850</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta4.tar.gz</download_link>
      <date>1329149138</date>
      <mdhash>7d5566cd6425d2dca4d0597984726eb8</mdhash>
      <filesize>24005</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d5566cd6425d2dca4d0597984726eb8</md5>
          <size>24005</size>
          <filedate>1329149138</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>79682a79c6bb7b0bd5fd68ed41328364</md5>
          <size>32644</size>
          <filedate>1329149138</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-beta3</release_link>
      <download_link/>
      <date>1324748139</date>
      <mdhash>397fa7a810501fbb22390c8fb8ac10ef</mdhash>
      <filesize>19799</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>397fa7a810501fbb22390c8fb8ac10ef</md5>
          <size>19799</size>
          <filedate>1324748139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>38abec738f159be6e8786d3461d70dd8</md5>
          <size>23979</size>
          <filedate>1324748139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta2.tar.gz</download_link>
      <date>1322214940</date>
      <mdhash>41b5dd4ed7115763e5b30c41a9a07c3d</mdhash>
      <filesize>19792</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>41b5dd4ed7115763e5b30c41a9a07c3d</md5>
          <size>19792</size>
          <filedate>1322214940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b5778d2a6c0fb2a458fb8f49b320072</md5>
          <size>23973</size>
          <filedate>1322214940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta1.tar.gz</download_link>
      <date>1316702804</date>
      <mdhash>a056dc3e075e9b0783cb1addaad5086e</mdhash>
      <filesize>18948</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a056dc3e075e9b0783cb1addaad5086e</md5>
          <size>18948</size>
          <filedate>1316702804</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2783f443d6bb0203e691c4f644179750</md5>
          <size>22869</size>
          <filedate>1316702804</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1316171203</date>
      <mdhash>0f5a3e9f3f998ed12d7c6bf1fd864274</mdhash>
      <filesize>17761</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0f5a3e9f3f998ed12d7c6bf1fd864274</md5>
          <size>17761</size>
          <filedate>1316171203</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2dea8a686729cac30706a72f761052c2</md5>
          <size>21668</size>
          <filedate>1316171203</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1313085717</date>
      <mdhash>d17373159f2c745be04bf3ea2df7632d</mdhash>
      <filesize>10847</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d17373159f2c745be04bf3ea2df7632d</md5>
          <size>10847</size>
          <filedate>1313085717</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>56a82158004bd34690587a6c593a8e15</md5>
          <size>11613</size>
          <filedate>1313085717</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entityreference 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference-7.x-1.x-dev.tar.gz</download_link>
      <date>1691643813</date>
      <mdhash>10c70f1db5caab5a87bc72c42ae7a6c1</mdhash>
      <filesize>50697</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10c70f1db5caab5a87bc72c42ae7a6c1</md5>
          <size>50697</size>
          <filedate>1691643813</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c6bdbda345079bb40082ec7fb0b44ac3</md5>
          <size>73412</size>
          <filedate>1691643813</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
