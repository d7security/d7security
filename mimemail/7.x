<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Mime Mail</title>
  <short_name>mimemail</short_name>
  <dc:creator>tr</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/mimemail</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>mimemail 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.2.tar.gz</download_link>
      <date>1638772652</date>
      <mdhash>f61cf04628bf376e96a1a3ed9c5f6db9</mdhash>
      <filesize>42382</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f61cf04628bf376e96a1a3ed9c5f6db9</md5>
          <size>42382</size>
          <filedate>1638772652</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8d064426b3087410752d0d30348a03e</md5>
          <size>54789</size>
          <filedate>1638772652</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>mimemail 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.1.tar.gz</download_link>
      <date>1539793381</date>
      <mdhash>69f7ee8de5522fec5e744a2ae1780cac</mdhash>
      <filesize>40924</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69f7ee8de5522fec5e744a2ae1780cac</md5>
          <size>40924</size>
          <filedate>1539793381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4dfecf2d44b34dc1b66963347893e931</md5>
          <size>52907</size>
          <filedate>1539793381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0.tar.gz</download_link>
      <date>1494775685</date>
      <mdhash>3ad7c008bf863ce7adaa2943c7b229e3</mdhash>
      <filesize>40600</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ad7c008bf863ce7adaa2943c7b229e3</md5>
          <size>40600</size>
          <filedate>1494775685</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>bcfd6629264a09ac55cf5bd1532a46d8</md5>
          <size>52567</size>
          <filedate>1494775685</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta4.tar.gz</download_link>
      <date>1438530539</date>
      <mdhash>ca2b9095c9f8d1fa5e09d7a3a9d99eea</mdhash>
      <filesize>40154</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ca2b9095c9f8d1fa5e09d7a3a9d99eea</md5>
          <size>40154</size>
          <filedate>1438530539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>c562cba69f6ab29eaa6f1ed61a34fd01</md5>
          <size>51943</size>
          <filedate>1438530539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta3.tar.gz</download_link>
      <date>1394018006</date>
      <mdhash>7664d1decac54f9ba1349be30cb5e53f</mdhash>
      <filesize>38122</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7664d1decac54f9ba1349be30cb5e53f</md5>
          <size>38122</size>
          <filedate>1394018006</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5e64cc2e8a63a0edcde2574128c914d</md5>
          <size>47879</size>
          <filedate>1394018006</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta2.tar.gz</download_link>
      <date>1393439605</date>
      <mdhash>38541e29bd718c846ca0e4e7a56f7bcc</mdhash>
      <filesize>38103</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38541e29bd718c846ca0e4e7a56f7bcc</md5>
          <size>38103</size>
          <filedate>1393439605</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>42d39a1d42f7ab13a41b6c5e9a331f82</md5>
          <size>47839</size>
          <filedate>1393439605</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta1.tar.gz</download_link>
      <date>1381150946</date>
      <mdhash>637719a0b1f9536e2a132f1c08bf4e14</mdhash>
      <filesize>38224</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>637719a0b1f9536e2a132f1c08bf4e14</md5>
          <size>38224</size>
          <filedate>1381150946</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6cdfd9a06e050bc3dcc97fb3bc8e6da8</md5>
          <size>47375</size>
          <filedate>1381150946</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1345634537</date>
      <mdhash>2ea8c8836565f52695c96a7b550dc2f8</mdhash>
      <filesize>34175</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2ea8c8836565f52695c96a7b550dc2f8</md5>
          <size>34175</size>
          <filedate>1345634537</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>37fecc5041bc016b5b032ac4b31dde34</md5>
          <size>42207</size>
          <filedate>1345634537</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>mimemail 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/mimemail/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1324234543</date>
      <mdhash>65c85659488f689c4c14a25c34f71c2c</mdhash>
      <filesize>30515</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>65c85659488f689c4c14a25c34f71c2c</md5>
          <size>30515</size>
          <filedate>1324234543</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/mimemail-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7079fc0884f6258a61b46dd9c3dfad2e</md5>
          <size>38057</size>
          <filedate>1324234543</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
