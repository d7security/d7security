<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Svg Image</title>
  <short_name>svg_image</short_name>
  <dc:creator>imyaro</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/svg_image</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>svg_image 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/svg_image/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67454093/packages/generic/svg_image/7.x-1.3/svg_image-7.x-1.3.tar.gz</download_link>
      <date>1740487292</date>
      <mdhash>29cc8407d82a1b3fe007cfb8a3d6c3fb</mdhash>
      <filesize>11309</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67454093/packages/generic/svg_image/7.x-1.3/svg_image-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>29cc8407d82a1b3fe007cfb8a3d6c3fb</md5>
          <size>11309</size>
          <filedate>1740487292</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67454093/packages/generic/svg_image/7.x-1.3/svg_image-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c07f3798aa0439d19452b92036880577</md5>
          <size>12880</size>
          <filedate>1740487292</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>svg_image 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/svg_image/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/svg_image-7.x-1.2.tar.gz</download_link>
      <date>1539583681</date>
      <mdhash>d05533d17744fc4cadfe91425ac7260a</mdhash>
      <filesize>10966</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d05533d17744fc4cadfe91425ac7260a</md5>
          <size>10966</size>
          <filedate>1539583681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>48ed7cd09443f313a1c659834eeb029d</md5>
          <size>12428</size>
          <filedate>1539583681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>svg_image 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/svg_image/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/svg_image-7.x-1.1.tar.gz</download_link>
      <date>1532664484</date>
      <mdhash>5374fcd8a7b743ebafd773131c3ecafd</mdhash>
      <filesize>10880</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5374fcd8a7b743ebafd773131c3ecafd</md5>
          <size>10880</size>
          <filedate>1532664484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>88a68c25aebd0de21bc04a92d753ee22</md5>
          <size>12336</size>
          <filedate>1532664484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>svg_image 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/svg_image/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/svg_image-7.x-1.0.tar.gz</download_link>
      <date>1532664184</date>
      <mdhash>1b6abefc4d7193b474e3ba9dfdd20cc7</mdhash>
      <filesize>10832</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b6abefc4d7193b474e3ba9dfdd20cc7</md5>
          <size>10832</size>
          <filedate>1532664184</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a3cbce8b12134f3195626fc71267a74e</md5>
          <size>12271</size>
          <filedate>1532664184</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>svg_image 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/svg_image/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/svg_image-7.x-1.x-dev.tar.gz</download_link>
      <date>1548749580</date>
      <mdhash>f04c39f2ad7134b610868a69dee07695</mdhash>
      <filesize>11152</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f04c39f2ad7134b610868a69dee07695</md5>
          <size>11152</size>
          <filedate>1548749580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/svg_image-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>09a556d69a3e2935cef81710a77f76ef</md5>
          <size>12640</size>
          <filedate>1548749580</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
