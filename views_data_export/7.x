<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views data export</title>
  <short_name>views_data_export</short_name>
  <dc:creator>steven jones</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/views_data_export</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Import and export</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_data_export 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/views_data_export/-/releases/7.x-3.4</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66137958/packages/generic/views_data_export/7.x-3.4/views_data_export-7.x-3.4.tar.gz</download_link>
      <date>1736941426</date>
      <mdhash>c4ad114d06944b073fd6683d9544a443</mdhash>
      <filesize>46125</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66137958/packages/generic/views_data_export/7.x-3.4/views_data_export-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c4ad114d06944b073fd6683d9544a443</md5>
          <size>46125</size>
          <filedate>1736941426</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66137958/packages/generic/views_data_export/7.x-3.4/views_data_export-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2daa581ff68b217e6bd87c6b37590798</md5>
          <size>64168</size>
          <filedate>1736941426</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.3.tar.gz</download_link>
      <date>1704889526</date>
      <mdhash>0d00bb481f1026f7ab29db31593a7ac7</mdhash>
      <filesize>45035</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d00bb481f1026f7ab29db31593a7ac7</md5>
          <size>45035</size>
          <filedate>1704889526</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>60b9776f554fc58214fbbe8da042c4ec</md5>
          <size>63819</size>
          <filedate>1704889526</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.2.tar.gz</download_link>
      <date>1491379384</date>
      <mdhash>8125549c44330a15f5817bd8710fe8ed</mdhash>
      <filesize>45008</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8125549c44330a15f5817bd8710fe8ed</md5>
          <size>45008</size>
          <filedate>1491379384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fecaf0df8cd552ab47cb5806de9d05c7</md5>
          <size>63795</size>
          <filedate>1491379384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.1.tar.gz</download_link>
      <date>1474360139</date>
      <mdhash>e7ece08bc28dff3eee69838da49cf229</mdhash>
      <filesize>46755</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e7ece08bc28dff3eee69838da49cf229</md5>
          <size>46755</size>
          <filedate>1474360139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>73e6ee00c707214130a1d251f886ffd2</md5>
          <size>64558</size>
          <filedate>1474360139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0.tar.gz</download_link>
      <date>1473932339</date>
      <mdhash>ea974c8a5959ab2666102021cfdf9dab</mdhash>
      <filesize>46016</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ea974c8a5959ab2666102021cfdf9dab</md5>
          <size>46016</size>
          <filedate>1473932339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6c1aa042de986a109ef2ebd6cd1487e1</md5>
          <size>62654</size>
          <filedate>1473932339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta9</name>
      <version>7.x-3.0-beta9</version>
      <tag>7.x-3.0-beta9</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta9.tar.gz</download_link>
      <date>1445842740</date>
      <mdhash>c4f294647dcdae75bcfed27b894c2647</mdhash>
      <filesize>44829</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c4f294647dcdae75bcfed27b894c2647</md5>
          <size>44829</size>
          <filedate>1445842740</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta9.zip</url>
          <archive_type>zip</archive_type>
          <md5>8db9e360e64e3ee8efbb1a120405a49f</md5>
          <size>60358</size>
          <filedate>1445842740</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta8</name>
      <version>7.x-3.0-beta8</version>
      <tag>7.x-3.0-beta8</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta8.tar.gz</download_link>
      <date>1409118829</date>
      <mdhash>a9c0c25c7c20ad39b310f9467eb2ffa2</mdhash>
      <filesize>44369</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a9c0c25c7c20ad39b310f9467eb2ffa2</md5>
          <size>44369</size>
          <filedate>1409118829</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>e5329b7b0593acc4ee05593cafa36466</md5>
          <size>59974</size>
          <filedate>1409118829</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta7</name>
      <version>7.x-3.0-beta7</version>
      <tag>7.x-3.0-beta7</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta7.tar.gz</download_link>
      <date>1386882504</date>
      <mdhash>ef2161906615c0be854b51f2e6048a4d</mdhash>
      <filesize>43169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef2161906615c0be854b51f2e6048a4d</md5>
          <size>43169</size>
          <filedate>1386882504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>578bcfc8c76de817d41a69415311523d</md5>
          <size>58903</size>
          <filedate>1386882504</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta6</name>
      <version>7.x-3.0-beta6</version>
      <tag>7.x-3.0-beta6</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta6.tar.gz</download_link>
      <date>1336632688</date>
      <mdhash>2de9b43ab38e0ab79b6731d4669420b1</mdhash>
      <filesize>35527</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2de9b43ab38e0ab79b6731d4669420b1</md5>
          <size>35527</size>
          <filedate>1336632688</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>e4df3fded11deb002e5c30bdbb63c7ed</md5>
          <size>46562</size>
          <filedate>1336632688</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta5</name>
      <version>7.x-3.0-beta5</version>
      <tag>7.x-3.0-beta5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta5.tar.gz</download_link>
      <date>1309549920</date>
      <mdhash>746f20ded1cee71308eb522a210cf2eb</mdhash>
      <filesize>33826</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>746f20ded1cee71308eb522a210cf2eb</md5>
          <size>33826</size>
          <filedate>1309549920</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>e557fd33f54b033697aaa257c88c6ce2</md5>
          <size>44652</size>
          <filedate>1309549920</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta4</name>
      <version>7.x-3.0-beta4</version>
      <tag>7.x-3.0-beta4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta4.tar.gz</download_link>
      <date>1302280017</date>
      <mdhash>e4df8a7f81cd262ae6b6548f5e69c8e8</mdhash>
      <filesize>33268</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4df8a7f81cd262ae6b6548f5e69c8e8</md5>
          <size>33268</size>
          <filedate>1302280017</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f73077925f5713472a5a1e6328cdd7cb</md5>
          <size>43979</size>
          <filedate>1302280017</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta2.tar.gz</download_link>
      <date>1291490743</date>
      <mdhash>216add73f29b2116e6afd80b64c0477e</mdhash>
      <filesize>30954</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>216add73f29b2116e6afd80b64c0477e</md5>
          <size>30954</size>
          <filedate>1291490743</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd44442c5e716f8b14850f8e0d4b2634</md5>
          <size>42854</size>
          <filedate>1293235689</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1291204558</date>
      <mdhash>b0be3efa13d5516ade9851f21058b0d2</mdhash>
      <filesize>30950</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b0be3efa13d5516ade9851f21058b0d2</md5>
          <size>30950</size>
          <filedate>1291204558</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>da5953e4ac084fe86519075e3232f245</md5>
          <size>42852</size>
          <filedate>1293235689</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-4.x-dev.tar.gz</download_link>
      <date>1410975725</date>
      <mdhash>079f0d737c4f5023077e34ac4ffb3b97</mdhash>
      <filesize>56756</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>079f0d737c4f5023077e34ac4ffb3b97</md5>
          <size>56756</size>
          <filedate>1410975725</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>46eb0904449647567d53efc6a26fb3d8</md5>
          <size>81438</size>
          <filedate>1410975725</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.x-dev.tar.gz</download_link>
      <date>1704889286</date>
      <mdhash>ddff7c88397c4810e722e215221ba1ce</mdhash>
      <filesize>45041</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ddff7c88397c4810e722e215221ba1ce</md5>
          <size>45041</size>
          <filedate>1704889286</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4aaf9cc7425d0c0008dc019c62a5e36f</md5>
          <size>63825</size>
          <filedate>1704889286</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_data_export 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_data_export/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.x-dev.tar.gz</download_link>
      <date>1382164716</date>
      <mdhash>0002e4a344eef3192b6af07686d18cb2</mdhash>
      <filesize>32551</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0002e4a344eef3192b6af07686d18cb2</md5>
          <size>32551</size>
          <filedate>1382164716</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_data_export-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>64eb7d8ccbe5c30c5d4fad9ac2b202fe</md5>
          <size>43293</size>
          <filedate>1382164716</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
