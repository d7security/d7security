<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entity cache</title>
  <short_name>entitycache</short_name>
  <dc:creator>catch</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/entitycache</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entitycache 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.7.tar.gz</download_link>
      <date>1695818411</date>
      <mdhash>49290dc181ad7e51eb4f97c28772c44e</mdhash>
      <filesize>17542</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>49290dc181ad7e51eb4f97c28772c44e</md5>
          <size>17542</size>
          <filedate>1695818411</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>9503c5bf2429068091e47c7c022fdc5b</md5>
          <size>22661</size>
          <filedate>1695818411</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6.tar.gz</download_link>
      <date>1691488459</date>
      <mdhash>986f9231f9b075400d0d9ae7635c331c</mdhash>
      <filesize>17571</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>986f9231f9b075400d0d9ae7635c331c</md5>
          <size>17571</size>
          <filedate>1691488459</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>560c0208d00c89802ca474a3a85ca3be</md5>
          <size>22672</size>
          <filedate>1691488459</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.6-rc1</name>
      <version>7.x-1.6-rc1</version>
      <tag>7.x-1.6-rc1</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.6-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6-rc1.tar.gz</download_link>
      <date>1690302470</date>
      <mdhash>b9635df0590e110c5538a1161b51a1a4</mdhash>
      <filesize>17573</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9635df0590e110c5538a1161b51a1a4</md5>
          <size>17573</size>
          <filedate>1690302470</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.6-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>19b4a6c9625cb71eab80f3c17dfe64a5</md5>
          <size>22675</size>
          <filedate>1690302470</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entitycache 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.5.tar.gz</download_link>
      <date>1445943839</date>
      <mdhash>cde613de126f81ca225fbfb8459ec7ab</mdhash>
      <filesize>16218</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cde613de126f81ca225fbfb8459ec7ab</md5>
          <size>16218</size>
          <filedate>1445943839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>31fa1cb2af09186c92bd12e623b4adbc</md5>
          <size>20734</size>
          <filedate>1445943839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.4.tar.gz</download_link>
      <date>1445894639</date>
      <mdhash>8e28cae13416171e259bc21aeea0d5f1</mdhash>
      <filesize>16210</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8e28cae13416171e259bc21aeea0d5f1</md5>
          <size>16210</size>
          <filedate>1445894639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>d94a4c361f2476cdc1e5b3134e6f428a</md5>
          <size>20726</size>
          <filedate>1445894639</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.3.tar.gz</download_link>
      <date>1445894339</date>
      <mdhash>df7dfdaa22ac39ae4db14133ce3163f6</mdhash>
      <filesize>15849</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df7dfdaa22ac39ae4db14133ce3163f6</md5>
          <size>15849</size>
          <filedate>1445894339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>cd27e284f0d011ab04ff3a6edb4633df</md5>
          <size>20326</size>
          <filedate>1445894339</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.2.tar.gz</download_link>
      <date>1383216926</date>
      <mdhash>6690a64a90a057a5233121d998edd977</mdhash>
      <filesize>14559</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6690a64a90a057a5233121d998edd977</md5>
          <size>14559</size>
          <filedate>1383216926</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d2df60de02d3f332a014cec71e5e23b8</md5>
          <size>16571</size>
          <filedate>1383216926</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.1.tar.gz</download_link>
      <date>1315901203</date>
      <mdhash>94a763c52768da1216f44aac0b92cf4e</mdhash>
      <filesize>12887</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94a763c52768da1216f44aac0b92cf4e</md5>
          <size>12887</size>
          <filedate>1315901203</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8ebf7f0a09f81f45625761e15ae3e63c</md5>
          <size>14727</size>
          <filedate>1315901203</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0.tar.gz</download_link>
      <date>1304944915</date>
      <mdhash>08d0cb43c6012586aa16e129ce02240d</mdhash>
      <filesize>12677</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>08d0cb43c6012586aa16e129ce02240d</md5>
          <size>12677</size>
          <filedate>1304944915</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>5518cb5857f28e95851a0fd9e6d4bf36</md5>
          <size>14451</size>
          <filedate>1304944915</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entitycache 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc2.tar.gz</download_link>
      <date>1303485415</date>
      <mdhash>79da951b85f2857c4e9c498f89c7c121</mdhash>
      <filesize>12673</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>79da951b85f2857c4e9c498f89c7c121</md5>
          <size>12673</size>
          <filedate>1303485415</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ca21f5dc81fe192195c8267a2382586f</md5>
          <size>14455</size>
          <filedate>1303485415</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entitycache 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc1.tar.gz</download_link>
      <date>1303402615</date>
      <mdhash>5789c081ad9fcf640282b61459de681a</mdhash>
      <filesize>12652</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5789c081ad9fcf640282b61459de681a</md5>
          <size>12652</size>
          <filedate>1303402615</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>88b1caa3ea2e259a2beae653797a1691</md5>
          <size>14425</size>
          <filedate>1303402615</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entitycache 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta2.tar.gz</download_link>
      <date>1296832333</date>
      <mdhash>ab1e8c17f1424e671cb6a8819fa55314</mdhash>
      <filesize>12284</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ab1e8c17f1424e671cb6a8819fa55314</md5>
          <size>12284</size>
          <filedate>1296832333</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e37f166795e404bf314a5c7bf068ebdf</md5>
          <size>13986</size>
          <filedate>1296832333</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entitycache 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta1.tar.gz</download_link>
      <date>1293466296</date>
      <mdhash>7d7c96b78b7b52f3101427672394f4dd</mdhash>
      <filesize>9582</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d7c96b78b7b52f3101427672394f4dd</md5>
          <size>9582</size>
          <filedate>1293466296</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d052ecbdba4aae8edefda99d9260f762</md5>
          <size>10829</size>
          <filedate>1293466296</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>entitycache 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entitycache/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entitycache-7.x-1.x-dev.tar.gz</download_link>
      <date>1695818185</date>
      <mdhash>fcddfae412b8626a0041c972e2830520</mdhash>
      <filesize>17546</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fcddfae412b8626a0041c972e2830520</md5>
          <size>17546</size>
          <filedate>1695818185</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entitycache-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5f7cee13d26446d4470bc8d7b65e463</md5>
          <size>22666</size>
          <filedate>1695818185</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
