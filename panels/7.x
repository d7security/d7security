<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Panels</title>
  <short_name>panels</short_name>
  <dc:creator>merlinofchaos</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/panels</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>panels 7.x-3.13</name>
      <version>7.x-3.13</version>
      <tag>7.x-3.13</tag>
      <version_major>3</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/panels/-/releases/7.x-3.13</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66138503/packages/generic/panels/7.x-3.13/panels-7.x-3.13.tar.gz</download_link>
      <date>1736941452</date>
      <mdhash>2d9519ac9089470b0f263e8602c5aed5</mdhash>
      <filesize>367901</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66138503/packages/generic/panels/7.x-3.13/panels-7.x-3.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2d9519ac9089470b0f263e8602c5aed5</md5>
          <size>367901</size>
          <filedate>1736941452</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66138503/packages/generic/panels/7.x-3.13/panels-7.x-3.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>153022a4f8f3422131fb8ff2e857ec57</md5>
          <size>450041</size>
          <filedate>1736941452</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>panels 7.x-3.12</name>
      <version>7.x-3.12</version>
      <tag>7.x-3.12</tag>
      <version_major>3</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.12.tar.gz</download_link>
      <date>1692843040</date>
      <mdhash>b9b27fb0cb51f7941ac88b38db0167c4</mdhash>
      <filesize>363274</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9b27fb0cb51f7941ac88b38db0167c4</md5>
          <size>363274</size>
          <filedate>1692843040</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc14de022fd85b4784a2bad9ec46025b</md5>
          <size>449774</size>
          <filedate>1692843040</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.11.tar.gz</download_link>
      <date>1668588138</date>
      <mdhash>dfd09fae2e9f9395d2466e605c6d9171</mdhash>
      <filesize>363166</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dfd09fae2e9f9395d2466e605c6d9171</md5>
          <size>363166</size>
          <filedate>1668588138</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>eedced284518379d752469034d9a3599</md5>
          <size>449652</size>
          <filedate>1668588138</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.10.tar.gz</download_link>
      <date>1593990785</date>
      <mdhash>5f181fccbe56d5832f4ced257f1c6572</mdhash>
      <filesize>362771</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5f181fccbe56d5832f4ced257f1c6572</md5>
          <size>362771</size>
          <filedate>1593990785</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>005a48f256a49409f840d2e55182fd6f</md5>
          <size>449322</size>
          <filedate>1593990785</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.9</name>
      <version>7.x-3.9</version>
      <tag>7.x-3.9</tag>
      <version_major>3</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.9.tar.gz</download_link>
      <date>1486394283</date>
      <mdhash>b44a927da8ec8863192f3d806a127c6b</mdhash>
      <filesize>363262</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b44a927da8ec8863192f3d806a127c6b</md5>
          <size>363262</size>
          <filedate>1486394283</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>b2239714bf8534408f8d29d5c443bb6e</md5>
          <size>448891</size>
          <filedate>1486394283</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.8</name>
      <version>7.x-3.8</version>
      <tag>7.x-3.8</tag>
      <version_major>3</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.8.tar.gz</download_link>
      <date>1476582270</date>
      <mdhash>dddff691fe417c4d82d29388972b6ece</mdhash>
      <filesize>363078</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dddff691fe417c4d82d29388972b6ece</md5>
          <size>363078</size>
          <filedate>1476582270</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>b50b24dd8af0f416a22bfdf1b88f7ec7</md5>
          <size>447586</size>
          <filedate>1476582270</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.7.tar.gz</download_link>
      <date>1471704239</date>
      <mdhash>fd8c986bf2bcdcd6581a6a7921df6661</mdhash>
      <filesize>361717</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd8c986bf2bcdcd6581a6a7921df6661</md5>
          <size>361717</size>
          <filedate>1471704239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>ef021a90d6498b739c6c16b27100ab53</md5>
          <size>444470</size>
          <filedate>1471704239</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.6.tar.gz</download_link>
      <date>1471447739</date>
      <mdhash>e630a2290ee81462e6e14680520e3193</mdhash>
      <filesize>360101</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e630a2290ee81462e6e14680520e3193</md5>
          <size>360101</size>
          <filedate>1471447739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>b559daf0ccc7301372aab3e540c8ebcf</md5>
          <size>443778</size>
          <filedate>1471447739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.5.tar.gz</download_link>
      <date>1422472981</date>
      <mdhash>6320feeafda924c97c026acf87a3f44f</mdhash>
      <filesize>354162</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6320feeafda924c97c026acf87a3f44f</md5>
          <size>354162</size>
          <filedate>1422472981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b00a332828b83e58239b559d7139a37</md5>
          <size>435322</size>
          <filedate>1422472981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.4.tar.gz</download_link>
      <date>1392221606</date>
      <mdhash>7bb3c23efa8ad70811641a026dd43e42</mdhash>
      <filesize>351866</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7bb3c23efa8ad70811641a026dd43e42</md5>
          <size>351866</size>
          <filedate>1392221606</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7741238f362cae40d22919b0fd269e6c</md5>
          <size>428461</size>
          <filedate>1392221606</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.3.tar.gz</download_link>
      <date>1345319572</date>
      <mdhash>41d55ac3806a254d0e16cf865f74a1ee</mdhash>
      <filesize>339088</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>41d55ac3806a254d0e16cf865f74a1ee</md5>
          <size>339088</size>
          <filedate>1345319572</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5784bdab43294ebb2a6c7fb39ef102bf</md5>
          <size>415354</size>
          <filedate>1345319572</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.2.tar.gz</download_link>
      <date>1332079243</date>
      <mdhash>ce1bab0d151e3717668e5f656f7baafe</mdhash>
      <filesize>338032</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce1bab0d151e3717668e5f656f7baafe</md5>
          <size>338032</size>
          <filedate>1332079243</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>012dadc91fe08e36b25d26e9fd695d3d</md5>
          <size>413536</size>
          <filedate>1332079243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.1.tar.gz</download_link>
      <date>1332023443</date>
      <mdhash>461e61c41a279c039da2bfbe2cfa41a9</mdhash>
      <filesize>338032</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>461e61c41a279c039da2bfbe2cfa41a9</md5>
          <size>338032</size>
          <filedate>1332023443</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf6b2038e0d123c381be10892979d84e</md5>
          <size>413527</size>
          <filedate>1332023443</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.0</release_link>
      <download_link/>
      <date>1326917446</date>
      <mdhash>ff701b5e4bcea2d85a4bf9ea3a1ebc42</mdhash>
      <filesize>326150</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>ff701b5e4bcea2d85a4bf9ea3a1ebc42</md5>
          <size>326150</size>
          <filedate>1326917446</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>73b1fee9aefd5fbe2f46998d5a843c77</md5>
          <size>401726</size>
          <filedate>1326917446</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panels 7.x-3.0-alpha10</name>
      <version>7.x-3.0-alpha10</version>
      <tag>7.x-3.0-alpha10</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.0-alpha10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha10.tar.gz</download_link>
      <date>1430859182</date>
      <mdhash>df35140f0f820bc2fb1776e787f2424e</mdhash>
      <filesize>354183</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df35140f0f820bc2fb1776e787f2424e</md5>
          <size>354183</size>
          <filedate>1430859182</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha10.zip</url>
          <archive_type>zip</archive_type>
          <md5>b16b618c5503eee8afde2292f4ef8bc6</md5>
          <size>435596</size>
          <filedate>1430859182</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panels 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha3.tar.gz</download_link>
      <date>1300396268</date>
      <mdhash>278d49187d64b3bc431d67fb8f88278b</mdhash>
      <filesize>324343</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>278d49187d64b3bc431d67fb8f88278b</md5>
          <size>324343</size>
          <filedate>1300396268</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc05475325617ed140ddc7225fd9d4c2</md5>
          <size>400204</size>
          <filedate>1300396268</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panels 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1294697038</date>
      <mdhash>47b16bea57350c30e9c8418cdab2ce83</mdhash>
      <filesize>380479</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>47b16bea57350c30e9c8418cdab2ce83</md5>
          <size>380479</size>
          <filedate>1294697038</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>36f7367eae09a1501fdfb219814bdefa</md5>
          <size>465819</size>
          <filedate>1294697039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panels 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1286839262</date>
      <mdhash>948e822378926bddec3959c2f611a26c</mdhash>
      <filesize>386977</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>948e822378926bddec3959c2f611a26c</md5>
          <size>386977</size>
          <filedate>1286839262</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panels-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6b7fc6b94ceb1ec3d0cb013e241698b</md5>
          <size>468811</size>
          <filedate>1293233714</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panels 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panels/releases/7.x-3.x-dev</release_link>
      <download_link/>
      <date>1599524747</date>
      <mdhash>52bb1c2a6d018c8c93af9b1eb826ee4e</mdhash>
      <filesize>363160</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>52bb1c2a6d018c8c93af9b1eb826ee4e</md5>
          <size>363160</size>
          <filedate>1599524747</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>0edf8bce7d001479d01b6aa4654e7eb4</md5>
          <size>449676</size>
          <filedate>1599524747</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
