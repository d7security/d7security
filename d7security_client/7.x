<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>D7Security client</title>
  <short_name>d7security_client</short_name>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <recommended_major>1</recommended_major>
  <supported_majors>1</supported_majors>
  <default_major>1</default_major>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/d7security_client</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>d7security_client 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/d7security_client/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.3/d7security_client-7.x-1.3.tar.gz</download_link>
      <date>1727433789</date>
      <mdhash>6e279cbb49c870367d10b209b2483202</mdhash>
      <filesize>186942</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.3/d7security_client-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6e279cbb49c870367d10b209b2483202</md5>
          <size>186942</size>
          <filedate>1727433789</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.3/d7security_client-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>2cc379bd3f06bd0384040756278eefea</md5>
          <size>188586</size>
          <filedate>1727433789</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>d7security_client 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/d7security_client/-/releases/7.x-1.2</release_link>
      <download_link>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.2/d7security_client-7.x-1.2.tar.gz</download_link>
      <date>1713507777</date>
      <mdhash>d399eb886e94ca659aa5c80710860dca</mdhash>
      <filesize>179616</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.2/d7security_client-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d399eb886e94ca659aa5c80710860dca</md5>
          <size>179616</size>
          <filedate>1713507777</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.2/d7security_client-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c9a0c767a6c3263eb4fc586752d49a97</md5>
          <size>181191</size>
          <filedate>1713507777</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>d7security_client 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/d7security_client/-/releases/7.x-1.1</release_link>
      <download_link>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.1/d7security_client-7.x-1.1.tar.gz</download_link>
      <date>1705590524</date>
      <mdhash>957b3dc39df3657e4d511b5a7626623d</mdhash>
      <filesize>179623</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.1/d7security_client-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>957b3dc39df3657e4d511b5a7626623d</md5>
          <size>179623</size>
          <filedate>1705590524</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.1/d7security_client-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>58691a80220eaaa5bb830cd6ada28fdd</md5>
          <size>181212</size>
          <filedate>1705590524</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>d7security_client 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/d7security_client/-/releases/7.x-1.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.0/d7security_client-7.x-1.0.tar.gz</download_link>
      <date>1702829722</date>
      <mdhash>5aa698d779334a9dc23f87864acb0b66</mdhash>
      <filesize>8105</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.0/d7security_client-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5aa698d779334a9dc23f87864acb0b66</md5>
          <size>8105</size>
          <filedate>1702829722</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/53090644/packages/generic/d7security_client/7.x-1.0/d7security_client-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>225cf88786867614653da6126c6ce75e</md5>
          <size>9450</size>
          <filedate>1702829722</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
  </releases>
</project>
