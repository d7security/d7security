<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Watchdog</title>
  <short_name>views_watchdog</short_name>
  <dc:creator>rstamm</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_watchdog</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_watchdog 7.x-4.0-rc1</name>
      <version>7.x-4.0-rc1</version>
      <tag>7.x-4.0-rc1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_watchdog/releases/7.x-4.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.0-rc1.tar.gz</download_link>
      <date>1458605632</date>
      <mdhash>ee7f2c114e57744dc5a17f7522d78338</mdhash>
      <filesize>9359</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee7f2c114e57744dc5a17f7522d78338</md5>
          <size>9359</size>
          <filedate>1458605632</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4dfd2e3e2bc8e3117043a7aa5a259723</md5>
          <size>11074</size>
          <filedate>1458605632</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_watchdog 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_watchdog/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.3.tar.gz</download_link>
      <date>1322311852</date>
      <mdhash>5c0abbbaa18a0e7e885d40592a2199ab</mdhash>
      <filesize>15415</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5c0abbbaa18a0e7e885d40592a2199ab</md5>
          <size>15415</size>
          <filedate>1322311852</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>05410628f0117bf0cae7e015717ba20d</md5>
          <size>21639</size>
          <filedate>1322311852</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_watchdog 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_watchdog/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.2.tar.gz</download_link>
      <date>1311673320</date>
      <mdhash>d4e9dc927b61883b6f039a279fa09aca</mdhash>
      <filesize>13834</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d4e9dc927b61883b6f039a279fa09aca</md5>
          <size>13834</size>
          <filedate>1311673320</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>312173add90b728b74aa81e8365da54b</md5>
          <size>19517</size>
          <filedate>1311673320</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_watchdog 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_watchdog/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.x-dev.tar.gz</download_link>
      <date>1458605632</date>
      <mdhash>b970e17da662bb0feb3e1fd54b96c6b8</mdhash>
      <filesize>9369</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b970e17da662bb0feb3e1fd54b96c6b8</md5>
          <size>9369</size>
          <filedate>1458605632</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6c7c5eb4359d61542ddee9042bb09123</md5>
          <size>11078</size>
          <filedate>1458605632</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_watchdog 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_watchdog/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.x-dev.tar.gz</download_link>
      <date>1382165277</date>
      <mdhash>0cf8b92079cdfa203aaea8e3e200bf0b</mdhash>
      <filesize>15884</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0cf8b92079cdfa203aaea8e3e200bf0b</md5>
          <size>15884</size>
          <filedate>1382165277</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_watchdog-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ec171619cac48b1a3caa32859dcf6f01</md5>
          <size>23108</size>
          <filedate>1382165277</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
