<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Front Page</title>
  <short_name>front</short_name>
  <dc:creator>Dublin Drupaller</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/front</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>front 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.4.tar.gz</download_link>
      <date>1370619956</date>
      <mdhash>ebfc3282367320ee8ac6b4e16e6319de</mdhash>
      <filesize>14629</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ebfc3282367320ee8ac6b4e16e6319de</md5>
          <size>14629</size>
          <filedate>1370619956</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>55267a72c9f021dde44aec18d16139c3</md5>
          <size>16648</size>
          <filedate>1370619956</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>front 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.3.tar.gz</download_link>
      <date>1361648769</date>
      <mdhash>486329ee3bc4152066027f2e6312b045</mdhash>
      <filesize>14382</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>486329ee3bc4152066027f2e6312b045</md5>
          <size>14382</size>
          <filedate>1361648769</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d21f039b15f851e38fe07c51fbfed6cb</md5>
          <size>16390</size>
          <filedate>1361648769</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>front 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.2.tar.gz</download_link>
      <date>1360785925</date>
      <mdhash>0656563c6f9ce1796f7867e77f034f40</mdhash>
      <filesize>14346</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0656563c6f9ce1796f7867e77f034f40</md5>
          <size>14346</size>
          <filedate>1360785925</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ff97f90cb12208f6e8cc6eaa2574e17c</md5>
          <size>16347</size>
          <filedate>1360785925</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>front 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.1.tar.gz</download_link>
      <date>1319746532</date>
      <mdhash>220312ffd9afb6e4c68e560cbee37028</mdhash>
      <filesize>14161</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>220312ffd9afb6e4c68e560cbee37028</md5>
          <size>14161</size>
          <filedate>1319746532</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f6f3b3e3cba8ec5bdabe9891b20aa33</md5>
          <size>16165</size>
          <filedate>1319746532</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>front 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.0.tar.gz</download_link>
      <date>1313268716</date>
      <mdhash>dfd8ec112186d5fa30840ec76dd4ead9</mdhash>
      <filesize>13008</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dfd8ec112186d5fa30840ec76dd4ead9</md5>
          <size>13008</size>
          <filedate>1313268716</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>82a66aeeeb5bf6ae4bb86fa253f0ad06</md5>
          <size>15016</size>
          <filedate>1313268716</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>front 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.0-rc1.tar.gz</download_link>
      <date>1305072482</date>
      <mdhash>5de258c9552655fccdbd3c9f8d9109f0</mdhash>
      <filesize>12999</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5de258c9552655fccdbd3c9f8d9109f0</md5>
          <size>12999</size>
          <filedate>1305072482</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b1afae823d8199aa8f1275d12648f26f</md5>
          <size>15000</size>
          <filedate>1305072482</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>front 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1301863267</date>
      <mdhash>0910702406605fb6d18a08de7236c300</mdhash>
      <filesize>13161</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0910702406605fb6d18a08de7236c300</md5>
          <size>13161</size>
          <filedate>1301863267</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>15c47f79217254d92f0926e500f726cf</md5>
          <size>15176</size>
          <filedate>1301863267</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>front 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1301829368</date>
      <mdhash>63a61a003632134fe0583d96a8bb4217</mdhash>
      <filesize>13671</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>63a61a003632134fe0583d96a8bb4217</md5>
          <size>13671</size>
          <filedate>1301829368</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>546e1940272d1c4be87730fb1d94f2b1</md5>
          <size>15844</size>
          <filedate>1301829368</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>front 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1301829067</date>
      <mdhash>377e16dbed44907f25ccc197b665f118</mdhash>
      <filesize>12067</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>377e16dbed44907f25ccc197b665f118</md5>
          <size>12067</size>
          <filedate>1301829067</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a37ec206850f6f79d46b15c1ef8d484</md5>
          <size>13785</size>
          <filedate>1301829067</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>front 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-2.x-dev.tar.gz</download_link>
      <date>1558094586</date>
      <mdhash>05c3578c478295378a6fc4d376d8cd2d</mdhash>
      <filesize>14935</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>05c3578c478295378a6fc4d376d8cd2d</md5>
          <size>14935</size>
          <filedate>1558094586</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e4d7fb1fe8c7dbbb202e5b5439fd8872</md5>
          <size>17389</size>
          <filedate>1558094586</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>front 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/front/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/front-7.x-1.x-dev.tar.gz</download_link>
      <date>1380580254</date>
      <mdhash>08e0cb0276e0e9f03af97a6618e4bb1e</mdhash>
      <filesize>13135</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>08e0cb0276e0e9f03af97a6618e4bb1e</md5>
          <size>13135</size>
          <filedate>1380580254</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/front-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a6724227f9bf209f1b150111d3decdc</md5>
          <size>14896</size>
          <filedate>1380580254</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
