<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Reference Filter</title>
  <short_name>entityreference_filter</short_name>
  <dc:creator>maximpodorov</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/entityreference_filter</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entityreference_filter 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.7.tar.gz</download_link>
      <date>1471604814</date>
      <mdhash>e5468610cde37e66c5bd412edf31b1a1</mdhash>
      <filesize>13991</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e5468610cde37e66c5bd412edf31b1a1</md5>
          <size>13991</size>
          <filedate>1471604814</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>1ad39c6a612a3692b86db5ef85e5f9ae</md5>
          <size>16318</size>
          <filedate>1471604814</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.6.tar.gz</download_link>
      <date>1471364115</date>
      <mdhash>14bff982692348ab1eff28e948d15f8a</mdhash>
      <filesize>13861</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14bff982692348ab1eff28e948d15f8a</md5>
          <size>13861</size>
          <filedate>1471364115</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>2479b223b0ea1f78728c5ef6f4f1cb3c</md5>
          <size>16147</size>
          <filedate>1471364115</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.5.tar.gz</download_link>
      <date>1425989680</date>
      <mdhash>bc5888000bff6ac706739cdc86721345</mdhash>
      <filesize>13807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bc5888000bff6ac706739cdc86721345</md5>
          <size>13807</size>
          <filedate>1425989680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>967b668d4d61c9070e8fcda2e02d5450</md5>
          <size>16075</size>
          <filedate>1425989680</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.4.tar.gz</download_link>
      <date>1424350981</date>
      <mdhash>b6edae4d92b78ca9c462dec6193653cd</mdhash>
      <filesize>13802</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b6edae4d92b78ca9c462dec6193653cd</md5>
          <size>13802</size>
          <filedate>1424350981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>5c53ef62b658487d0e7a5ceb93821f36</md5>
          <size>16062</size>
          <filedate>1424350981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.3.tar.gz</download_link>
      <date>1414412004</date>
      <mdhash>c24f6b42bde781015e9bcabe670415d4</mdhash>
      <filesize>12917</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c24f6b42bde781015e9bcabe670415d4</md5>
          <size>12917</size>
          <filedate>1414412004</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>00a05181e6a12c5a4831b9933a4b2131</md5>
          <size>15216</size>
          <filedate>1414412004</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.2.tar.gz</download_link>
      <date>1401812327</date>
      <mdhash>359299792923a95090aa411e2ed15eef</mdhash>
      <filesize>12786</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>359299792923a95090aa411e2ed15eef</md5>
          <size>12786</size>
          <filedate>1401812327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1cb4dc6256c293aeb04dcc1930df7276</md5>
          <size>14995</size>
          <filedate>1401812327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.1.tar.gz</download_link>
      <date>1386665605</date>
      <mdhash>d81d7044878be72f9a3d0ead9ced9005</mdhash>
      <filesize>8947</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d81d7044878be72f9a3d0ead9ced9005</md5>
          <size>8947</size>
          <filedate>1386665605</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6e0360117658dc9c278a2b7c9c56700f</md5>
          <size>10350</size>
          <filedate>1386665605</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.0.tar.gz</download_link>
      <date>1371458152</date>
      <mdhash>cce98cf6563b46434285b0b7fd603bde</mdhash>
      <filesize>8926</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cce98cf6563b46434285b0b7fd603bde</md5>
          <size>8926</size>
          <filedate>1371458152</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>ada379f9e2829bd04d7cc09e4836e11a</md5>
          <size>10331</size>
          <filedate>1371458152</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_filter 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_filter/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.x-dev.tar.gz</download_link>
      <date>1471604039</date>
      <mdhash>06805272b76f64d50b9ecff0ff31e9c8</mdhash>
      <filesize>13998</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>06805272b76f64d50b9ecff0ff31e9c8</md5>
          <size>13998</size>
          <filedate>1471604039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_filter-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ce619354799817560e6bcbf5ac38a9fe</md5>
          <size>16323</size>
          <filedate>1471604039</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
