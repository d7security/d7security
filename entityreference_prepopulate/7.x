<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entityreference prepopulate [D7]</title>
  <short_name>entityreference_prepopulate</short_name>
  <dc:creator>amitaibu</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/entityreference_prepopulate</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entityreference_prepopulate 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.8.tar.gz</download_link>
      <date>1652960230</date>
      <mdhash>597385856c40ada7a7af303a117830d8</mdhash>
      <filesize>16470</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>597385856c40ada7a7af303a117830d8</md5>
          <size>16470</size>
          <filedate>1652960230</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>12a77926220396016ad9cdf98a99ebbb</md5>
          <size>21585</size>
          <filedate>1652960230</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.7.tar.gz</download_link>
      <date>1485445388</date>
      <mdhash>66f35f8dd0d6141b13a2b03b2076f6ee</mdhash>
      <filesize>16350</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66f35f8dd0d6141b13a2b03b2076f6ee</md5>
          <size>16350</size>
          <filedate>1485445388</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>61c7c086dc8b2ebf7eecb61f36b91979</md5>
          <size>21434</size>
          <filedate>1485445388</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.6.tar.gz</download_link>
      <date>1454176139</date>
      <mdhash>7ece0db3ab2066bdb62ca34ac51c3080</mdhash>
      <filesize>16404</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ece0db3ab2066bdb62ca34ac51c3080</md5>
          <size>16404</size>
          <filedate>1454176139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>a274835db029aef568f4187272bc4c9c</md5>
          <size>21401</size>
          <filedate>1454176139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.5.tar.gz</download_link>
      <date>1392845305</date>
      <mdhash>c0b0cf3f5d0c092f3fe02a2b48e459e4</mdhash>
      <filesize>16254</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c0b0cf3f5d0c092f3fe02a2b48e459e4</md5>
          <size>16254</size>
          <filedate>1392845305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>f8aa653e6c98413d0cbeb7a20e608b15</md5>
          <size>21110</size>
          <filedate>1392845305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.4.tar.gz</download_link>
      <date>1387912105</date>
      <mdhash>7e1371524d96e40ed30e134df49e0b74</mdhash>
      <filesize>14443</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e1371524d96e40ed30e134df49e0b74</md5>
          <size>14443</size>
          <filedate>1387912105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>551178c454c7376dec5c370397195600</md5>
          <size>18786</size>
          <filedate>1387912105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.3.tar.gz</download_link>
      <date>1366630855</date>
      <mdhash>e56fe3492632f9d81f2e27d1d12a7fed</mdhash>
      <filesize>13306</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e56fe3492632f9d81f2e27d1d12a7fed</md5>
          <size>13306</size>
          <filedate>1366630855</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>18d994bbe7df12fb3186522435c7871d</md5>
          <size>17599</size>
          <filedate>1366630855</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.2.tar.gz</download_link>
      <date>1358362311</date>
      <mdhash>295cdb836246467f5119b9b88590fbd0</mdhash>
      <filesize>12916</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>295cdb836246467f5119b9b88590fbd0</md5>
          <size>12916</size>
          <filedate>1358362311</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2589022958d094650e690fc080f6c976</md5>
          <size>17230</size>
          <filedate>1358362311</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.1.tar.gz</download_link>
      <date>1348075166</date>
      <mdhash>115aaba7418303358666a8f6d7c061cd</mdhash>
      <filesize>11680</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>115aaba7418303358666a8f6d7c061cd</md5>
          <size>11680</size>
          <filedate>1348075166</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>06517ce239e2231b6558191883d3b027</md5>
          <size>14465</size>
          <filedate>1348075166</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.0.tar.gz</download_link>
      <date>1331326542</date>
      <mdhash>998ae9697928cc691cf95b07d1aca3cd</mdhash>
      <filesize>11406</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>998ae9697928cc691cf95b07d1aca3cd</md5>
          <size>11406</size>
          <filedate>1331326542</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>f8890522f26d775703cf64817713d4c3</md5>
          <size>14205</size>
          <filedate>1331326542</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>entityreference_prepopulate 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_prepopulate/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.x-dev.tar.gz</download_link>
      <date>1652960768</date>
      <mdhash>2d2aff9cd3e4a2004b58d999dfed30e0</mdhash>
      <filesize>17010</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2d2aff9cd3e4a2004b58d999dfed30e0</md5>
          <size>17010</size>
          <filedate>1652960768</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_prepopulate-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>93f614108381d86933160622633fa16e</md5>
          <size>22088</size>
          <filedate>1652960768</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
