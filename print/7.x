<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Printer, email and PDF versions</title>
  <short_name>print</short_name>
  <dc:creator>jcnventura</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/print</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Import and export</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>print 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.5.tar.gz</download_link>
      <date>1717357718</date>
      <mdhash>cbc5e74a0a6c8bc279de1d0af0d86df6</mdhash>
      <filesize>87726</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cbc5e74a0a6c8bc279de1d0af0d86df6</md5>
          <size>87726</size>
          <filedate>1717357718</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>38e229948dc19c98db37c79b7a73d085</md5>
          <size>133407</size>
          <filedate>1717357718</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.4.tar.gz</download_link>
      <date>1653980174</date>
      <mdhash>6c36ba017cf211eb1f8bf8215253662d</mdhash>
      <filesize>87880</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6c36ba017cf211eb1f8bf8215253662d</md5>
          <size>87880</size>
          <filedate>1653980174</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>31b9efa08861222a6867abeb2b690f2e</md5>
          <size>133152</size>
          <filedate>1653980174</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.3.tar.gz</download_link>
      <date>1653979935</date>
      <mdhash>29b98b58c67e905f052eca7e4ca41e9a</mdhash>
      <filesize>85820</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>29b98b58c67e905f052eca7e4ca41e9a</md5>
          <size>85820</size>
          <filedate>1653979935</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a8a45a505b44a2dc928729e1a6a5fb41</md5>
          <size>130959</size>
          <filedate>1653979935</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.2.tar.gz</download_link>
      <date>1538760180</date>
      <mdhash>07052affba558935caec559d32a723dc</mdhash>
      <filesize>85375</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>07052affba558935caec559d32a723dc</md5>
          <size>85375</size>
          <filedate>1538760180</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>bef5b36f5f75648cf9f15eed8bb07803</md5>
          <size>130487</size>
          <filedate>1538760180</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.1.tar.gz</download_link>
      <date>1538588880</date>
      <mdhash>1dd98434ccd49814aea0edf0d10fd65b</mdhash>
      <filesize>84484</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1dd98434ccd49814aea0edf0d10fd65b</md5>
          <size>84484</size>
          <filedate>1538588880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0ed2b722e635061b15158a5e66134723</md5>
          <size>129336</size>
          <filedate>1538588880</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.0.tar.gz</download_link>
      <date>1396426745</date>
      <mdhash>5e5cef97acc0135bf0debefa82f3c4f8</mdhash>
      <filesize>96755</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5e5cef97acc0135bf0debefa82f3c4f8</md5>
          <size>96755</size>
          <filedate>1396426745</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>32ef759ea1171b6dcb80acb9dd1c68a0</md5>
          <size>137411</size>
          <filedate>1396426745</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-2.0-beta2.tar.gz</download_link>
      <date>1346768904</date>
      <mdhash>cef30f08f4f592a227830d1e4cbbe15c</mdhash>
      <filesize>95722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cef30f08f4f592a227830d1e4cbbe15c</md5>
          <size>95722</size>
          <filedate>1346768904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>63d1fcb2070a23eece6b3deb4e23e14e</md5>
          <size>136352</size>
          <filedate>1346768904</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.0-beta1</release_link>
      <download_link/>
      <date>1337900192</date>
      <mdhash>6f9f19c3c539a6f1fd79568a99b3ef08</mdhash>
      <filesize>90574</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>6f9f19c3c539a6f1fd79568a99b3ef08</md5>
          <size>90574</size>
          <filedate>1337900192</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>bb516c82a2e0db8186d812874cea3cbc</md5>
          <size>130564</size>
          <filedate>1337900192</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.3.tar.gz</download_link>
      <date>1396426745</date>
      <mdhash>d7847f6847aadb09e1ab7c46f06eaffc</mdhash>
      <filesize>81336</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d7847f6847aadb09e1ab7c46f06eaffc</md5>
          <size>81336</size>
          <filedate>1396426745</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>48127b3e317ea2cd18327b74ea2f3667</md5>
          <size>97015</size>
          <filedate>1396426745</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.2.tar.gz</download_link>
      <date>1346768900</date>
      <mdhash>1fcbbee6ec7ae405f0492f124058648f</mdhash>
      <filesize>80519</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1fcbbee6ec7ae405f0492f124058648f</md5>
          <size>80519</size>
          <filedate>1346768900</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ddfec4d838eaaf6d08bc46adf03ee34</md5>
          <size>96440</size>
          <filedate>1346768900</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.1.tar.gz</download_link>
      <date>1337901690</date>
      <mdhash>cb3892edeba89bbdf25a0429b818be8e</mdhash>
      <filesize>79017</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb3892edeba89bbdf25a0429b818be8e</md5>
          <size>79017</size>
          <filedate>1337901690</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d6095d426c00afd8a9f88f08164a2ed1</md5>
          <size>94742</size>
          <filedate>1337901690</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.0.tar.gz</download_link>
      <date>1333551370</date>
      <mdhash>e708f192abfbc62daae76f90b90b23f9</mdhash>
      <filesize>76324</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e708f192abfbc62daae76f90b90b23f9</md5>
          <size>76324</size>
          <filedate>1333551370</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>e0781273026af9153e9de844ee7f30bc</md5>
          <size>90687</size>
          <filedate>1333551370</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>print 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.0-beta2.tar.gz</download_link>
      <date>1332520547</date>
      <mdhash>920e59e8d0ff7238cc422a8fecb745dd</mdhash>
      <filesize>76138</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>920e59e8d0ff7238cc422a8fecb745dd</md5>
          <size>76138</size>
          <filedate>1332520547</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d0321335cdd9668585c3030ae4aef276</md5>
          <size>90345</size>
          <filedate>1332520547</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.0-beta1</release_link>
      <download_link/>
      <date>1318810900</date>
      <mdhash>c503b0115f847279a4184874235e5829</mdhash>
      <filesize>75919</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c503b0115f847279a4184874235e5829</md5>
          <size>75919</size>
          <filedate>1318810900</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>92a5d7004404cd0e09fd258f118d8f42</md5>
          <size>90057</size>
          <filedate>1318810900</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1313510220</date>
      <mdhash>01571132afd96ff0793b7c4150132e60</mdhash>
      <filesize>70995</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>01571132afd96ff0793b7c4150132e60</md5>
          <size>70995</size>
          <filedate>1313510220</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e1f3c0e56bb1c589e9ffdb6b1b3cb4c9</md5>
          <size>84593</size>
          <filedate>1313510220</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1311207167</date>
      <mdhash>a5bfaf952509d0766ad58fb7bcd1a8c4</mdhash>
      <filesize>70406</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a5bfaf952509d0766ad58fb7bcd1a8c4</md5>
          <size>70406</size>
          <filedate>1311207167</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c74231b21256654bcb13c9329cbca50</md5>
          <size>83857</size>
          <filedate>1311207167</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-2.x-dev</release_link>
      <download_link/>
      <date>1626463326</date>
      <mdhash>c114749f31c11b6cef7a66d13f30ef82</mdhash>
      <filesize>87628</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c114749f31c11b6cef7a66d13f30ef82</md5>
          <size>87628</size>
          <filedate>1626463326</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>bb54343845c4c14e27f76790dea5263e</md5>
          <size>132854</size>
          <filedate>1626463326</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>print 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/print/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/print-7.x-1.x-dev.tar.gz</download_link>
      <date>1626130782</date>
      <mdhash>af5bbe97f3122be334c0c5032930ffa1</mdhash>
      <filesize>81240</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af5bbe97f3122be334c0c5032930ffa1</md5>
          <size>81240</size>
          <filedate>1626130782</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/print-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>cbe13408574313f0e0d6268ab95b8dd2</md5>
          <size>97868</size>
          <filedate>1626130782</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
