<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Asynchronous Prefetch Database Query Cache</title>
  <short_name>apdqc</short_name>
  <dc:creator>mikeytown2</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/apdqc</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>apdqc 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/apdqc/-/releases/7.x-1.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66660013/packages/generic/apdqc/7.x-1.0/apdqc-7.x-1.0.tar.gz</download_link>
      <date>1738241483</date>
      <mdhash>74807b6cf8d11e8b516adaf905e41d71</mdhash>
      <filesize>85914</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66660013/packages/generic/apdqc/7.x-1.0/apdqc-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>74807b6cf8d11e8b516adaf905e41d71</md5>
          <size>85914</size>
          <filedate>1738241483</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66660013/packages/generic/apdqc/7.x-1.0/apdqc-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>c6758a2386a3d0971e18aac009ad5c44</md5>
          <size>95945</size>
          <filedate>1738241483</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc3.tar.gz</download_link>
      <date>1468977244</date>
      <mdhash>61cb9d888de3c8b25df58e59e32af6a3</mdhash>
      <filesize>84410</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61cb9d888de3c8b25df58e59e32af6a3</md5>
          <size>84410</size>
          <filedate>1468977244</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a6f35c0144fcf0ab04a633ef2baf8890</md5>
          <size>93441</size>
          <filedate>1468977244</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc2.tar.gz</download_link>
      <date>1462584240</date>
      <mdhash>c075f325151b80ea6f571e4c870d439f</mdhash>
      <filesize>74032</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c075f325151b80ea6f571e4c870d439f</md5>
          <size>74032</size>
          <filedate>1462584240</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b190d9963e40f96201853b44b04a2804</md5>
          <size>83797</size>
          <filedate>1462584240</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc1.tar.gz</download_link>
      <date>1446084240</date>
      <mdhash>f8002236fe05e82bcdaf5339eb257479</mdhash>
      <filesize>59562</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f8002236fe05e82bcdaf5339eb257479</md5>
          <size>59562</size>
          <filedate>1446084240</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cebf06c3cd202d474150618333bf36d2</md5>
          <size>69535</size>
          <filedate>1446084240</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta4.tar.gz</download_link>
      <date>1444691339</date>
      <mdhash>1450f4a9083e635c51066c5f92f50397</mdhash>
      <filesize>59160</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1450f4a9083e635c51066c5f92f50397</md5>
          <size>59160</size>
          <filedate>1444691339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9be8fa21408e268d8f578b62a19e1684</md5>
          <size>69081</size>
          <filedate>1444691339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta3.tar.gz</download_link>
      <date>1432168081</date>
      <mdhash>11cc9a75c470f385f8f25fe53622bac9</mdhash>
      <filesize>56392</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11cc9a75c470f385f8f25fe53622bac9</md5>
          <size>56392</size>
          <filedate>1432168081</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea4f858edde900a7bc169082ad08316d</md5>
          <size>65195</size>
          <filedate>1432168081</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta2.tar.gz</download_link>
      <date>1425349381</date>
      <mdhash>dc1703bfec8256a1ce46a5ded6a9fbb6</mdhash>
      <filesize>55431</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc1703bfec8256a1ce46a5ded6a9fbb6</md5>
          <size>55431</size>
          <filedate>1425349381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2aedea94fd6766edd9ff4bc8c540135d</md5>
          <size>64174</size>
          <filedate>1425349381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta1.tar.gz</download_link>
      <date>1423102981</date>
      <mdhash>eb7c642016966a12a2089615b906e2c1</mdhash>
      <filesize>46719</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb7c642016966a12a2089615b906e2c1</md5>
          <size>46719</size>
          <filedate>1423102981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb4e734776316042c191f0e733569287</md5>
          <size>54619</size>
          <filedate>1423102981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>apdqc 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/apdqc/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/apdqc-7.x-1.x-dev.tar.gz</download_link>
      <date>1643925898</date>
      <mdhash>52316a23a3b881472ca74a4e60f81462</mdhash>
      <filesize>82391</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>52316a23a3b881472ca74a4e60f81462</md5>
          <size>82391</size>
          <filedate>1643925898</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/apdqc-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>59dccbe967b9b5b4333009e0fd5632f0</md5>
          <size>95716</size>
          <filedate>1643925898</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
