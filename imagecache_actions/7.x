<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>ImageCache Actions</title>
  <short_name>imagecache_actions</short_name>
  <dc:creator>dman</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/imagecache_actions</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>imagecache_actions 7.x-1.14</name>
      <version>7.x-1.14</version>
      <tag>7.x-1.14</tag>
      <version_major>1</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.14.tar.gz</download_link>
      <date>1689540299</date>
      <mdhash>a6a1654eab4a8dff62a2016fb8b7d6e8</mdhash>
      <filesize>2108406</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a6a1654eab4a8dff62a2016fb8b7d6e8</md5>
          <size>2108406</size>
          <filedate>1689540299</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>06ca141f8ba827ada392c35f7302971a</md5>
          <size>2150399</size>
          <filedate>1689540299</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.13.tar.gz</download_link>
      <date>1640174465</date>
      <mdhash>fa8a68f9d6ae312775a5950084d6c458</mdhash>
      <filesize>2108372</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa8a68f9d6ae312775a5950084d6c458</md5>
          <size>2108372</size>
          <filedate>1640174465</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>6048e8c46aa177b2495735dfc14f6398</md5>
          <size>2150307</size>
          <filedate>1640174465</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.12.tar.gz</download_link>
      <date>1592294434</date>
      <mdhash>d0531aa8a27106bcce51a7c0c61dca48</mdhash>
      <filesize>2108352</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d0531aa8a27106bcce51a7c0c61dca48</md5>
          <size>2108352</size>
          <filedate>1592294434</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>30f41445bc961ea8434bc32eb23d553f</md5>
          <size>2150258</size>
          <filedate>1592294434</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.11.tar.gz</download_link>
      <date>1565004485</date>
      <mdhash>fc9cd25b2cdcce1489cd1623676d04ef</mdhash>
      <filesize>2108165</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fc9cd25b2cdcce1489cd1623676d04ef</md5>
          <size>2108165</size>
          <filedate>1565004485</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>d7a008a2cb7b38022aeadc06b0c0be02</md5>
          <size>2149921</size>
          <filedate>1565004485</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.10.tar.gz</download_link>
      <date>1563346085</date>
      <mdhash>6d9bb9c19a9a9bf6a2f63596e3210140</mdhash>
      <filesize>2108156</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6d9bb9c19a9a9bf6a2f63596e3210140</md5>
          <size>2108156</size>
          <filedate>1563346085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>8479cc9a2805cf3a5ddf5ca8af52f07c</md5>
          <size>2149837</size>
          <filedate>1563346085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.9.tar.gz</download_link>
      <date>1521550384</date>
      <mdhash>3967d57b0781f847f334a78554161181</mdhash>
      <filesize>2108208</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3967d57b0781f847f334a78554161181</md5>
          <size>2108208</size>
          <filedate>1521550384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>6790ea00e1b9c9169d195ed282d4952b</md5>
          <size>2149554</size>
          <filedate>1521550384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.8.tar.gz</download_link>
      <date>1513596485</date>
      <mdhash>3c8d0aeaa42866f784f6ed78f36f6134</mdhash>
      <filesize>2104285</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3c8d0aeaa42866f784f6ed78f36f6134</md5>
          <size>2104285</size>
          <filedate>1513596485</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>9d57ad303adf7415984113532601eb57</md5>
          <size>2146119</size>
          <filedate>1513596485</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.7.tar.gz</download_link>
      <date>1455872639</date>
      <mdhash>f181eb8afbaf4daa351b248d51966201</mdhash>
      <filesize>2110299</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f181eb8afbaf4daa351b248d51966201</md5>
          <size>2110299</size>
          <filedate>1455872639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>356a3271181d05b983cc394f3cf79ac3</md5>
          <size>2142923</size>
          <filedate>1455872639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.6.tar.gz</download_link>
      <date>1455383639</date>
      <mdhash>b593d91ac9ed666321e215f2b9dbc9ae</mdhash>
      <filesize>2110225</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b593d91ac9ed666321e215f2b9dbc9ae</md5>
          <size>2110225</size>
          <filedate>1455383639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>7e2c15a87f4dee94bc20e098b1c0624d</md5>
          <size>2142790</size>
          <filedate>1455383639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.5.tar.gz</download_link>
      <date>1417256280</date>
      <mdhash>bc054fbe8fbbee6d66b6fa108c41fc3c</mdhash>
      <filesize>2104294</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bc054fbe8fbbee6d66b6fa108c41fc3c</md5>
          <size>2104294</size>
          <filedate>1417256280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>866431e715cdb48e8da786c0e1362f7f</md5>
          <size>2135444</size>
          <filedate>1417256280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.4.tar.gz</download_link>
      <date>1377368495</date>
      <mdhash>b135e4151bcf0b63b56ca864bcab2dda</mdhash>
      <filesize>2099108</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b135e4151bcf0b63b56ca864bcab2dda</md5>
          <size>2099108</size>
          <filedate>1377368495</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>cae5601d9d11f881b6ec72068c689e37</md5>
          <size>2130793</size>
          <filedate>1377368495</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.3.tar.gz</download_link>
      <date>1370376654</date>
      <mdhash>fa8f0f9f418f12109d5730c30e0cf59e</mdhash>
      <filesize>2096623</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa8f0f9f418f12109d5730c30e0cf59e</md5>
          <size>2096623</size>
          <filedate>1370376654</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6f6fba48e83c9c85f3ab302ae0bb761e</md5>
          <size>2127948</size>
          <filedate>1370376654</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.2.tar.gz</download_link>
      <date>1370113853</date>
      <mdhash>c9ae1391af383749d53d6fb4b6e5e5e3</mdhash>
      <filesize>2096425</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c9ae1391af383749d53d6fb4b6e5e5e3</md5>
          <size>2096425</size>
          <filedate>1370113853</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ea7671eedf51ed5ed8b361f61e3f427</md5>
          <size>2127729</size>
          <filedate>1370113854</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.1.tar.gz</download_link>
      <date>1354653754</date>
      <mdhash>3fdbd58d47cebd81f133524403993263</mdhash>
      <filesize>1788450</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3fdbd58d47cebd81f133524403993263</md5>
          <size>1788450</size>
          <filedate>1354653754</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>800311beb552c882de651b38f20dcf7c</md5>
          <size>1823553</size>
          <filedate>1354653754</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0.tar.gz</download_link>
      <date>1337423199</date>
      <mdhash>438d703caaa085195e80c77457cd841d</mdhash>
      <filesize>1915209</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>438d703caaa085195e80c77457cd841d</md5>
          <size>1915209</size>
          <filedate>1337423199</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7ef7399a3302d82cd650ebbdbc328705</md5>
          <size>1946595</size>
          <filedate>1337423199</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1336467057</date>
      <mdhash>97db71e3ac633aef05a3efc2bb4c9d82</mdhash>
      <filesize>1913923</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>97db71e3ac633aef05a3efc2bb4c9d82</md5>
          <size>1913923</size>
          <filedate>1336467057</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0c907d833cfa5238f1ffcbe3d11a3af1</md5>
          <size>1945555</size>
          <filedate>1336467057</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1336420556</date>
      <mdhash>8e5169485f9d348a253039e9e89256c1</mdhash>
      <filesize>1913767</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8e5169485f9d348a253039e9e89256c1</md5>
          <size>1913767</size>
          <filedate>1336420556</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3a3e6011a33138bc6c2a2e739946c739</md5>
          <size>1945383</size>
          <filedate>1336420557</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-0.0</name>
      <version>7.x-0.0</version>
      <tag>7.x-0.0</tag>
      <version_major>0</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-0.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-0.0.tar.gz</download_link>
      <date>1296768752</date>
      <mdhash>b1fcde3314b5e2a970f7f454d8300b6b</mdhash>
      <filesize>1436698</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-0.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b1fcde3314b5e2a970f7f454d8300b6b</md5>
          <size>1436698</size>
          <filedate>1296768752</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-0.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>1a8248b83f08f8d52b5a20f68df9c94a</md5>
          <size>1479914</size>
          <filedate>1296768753</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagecache_actions 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagecache_actions/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.x-dev.tar.gz</download_link>
      <date>1689540011</date>
      <mdhash>449160aaa89b67824534b5981d22a7e2</mdhash>
      <filesize>2108420</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>449160aaa89b67824534b5981d22a7e2</md5>
          <size>2108420</size>
          <filedate>1689540011</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagecache_actions-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8e3ceef9e6ce6c9e31da5bac248fd87a</md5>
          <size>2150444</size>
          <filedate>1689540011</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
