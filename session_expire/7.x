<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Session expire</title>
  <short_name>session_expire</short_name>
  <dc:creator>kbahey</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/session_expire</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>session_expire 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/session_expire/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/session_expire-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1314763320</date>
      <mdhash>d53e805ecc7798ea522ab38a16deade6</mdhash>
      <filesize>8247</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/session_expire-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d53e805ecc7798ea522ab38a16deade6</md5>
          <size>8247</size>
          <filedate>1314763320</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/session_expire-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>16d41373293c1633e7cde069ddbfa177</md5>
          <size>9132</size>
          <filedate>1314763320</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>session_expire 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/session_expire/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/session_expire-7.x-1.x-dev.tar.gz</download_link>
      <date>1417209186</date>
      <mdhash>72d2cea84c8cff2dec8f7e8c82a93c30</mdhash>
      <filesize>10333</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/session_expire-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>72d2cea84c8cff2dec8f7e8c82a93c30</md5>
          <size>10333</size>
          <filedate>1417209186</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/session_expire-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d1af8022c4f912a2490295d2e9e8890d</md5>
          <size>11678</size>
          <filedate>1417209186</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
