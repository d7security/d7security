<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Queue Unique</title>
  <short_name>queue_unique</short_name>
  <dc:creator>e0ipso</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/queue_unique</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>queue_unique 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_unique/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.0-beta1.tar.gz</download_link>
      <date>1443706139</date>
      <mdhash>b3eccc7d518000844dcb559fe67bde31</mdhash>
      <filesize>10264</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b3eccc7d518000844dcb559fe67bde31</md5>
          <size>10264</size>
          <filedate>1443706139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1929b7971594cc6109e7f4b639113999</md5>
          <size>12506</size>
          <filedate>1443706139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_unique 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_unique/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.0-beta2.tar.gz</download_link>
      <date>1443705839</date>
      <mdhash>00d450b74141e67501fa853286f53223</mdhash>
      <filesize>10324</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00d450b74141e67501fa853286f53223</md5>
          <size>10324</size>
          <filedate>1443705839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>73d652b51c0fd26c54f925fe6e3a6a0a</md5>
          <size>12852</size>
          <filedate>1443705839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_unique 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_unique/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.x-dev.tar.gz</download_link>
      <date>1443705839</date>
      <mdhash>fb7eca5a70c37ce8add9f4a9c84c32af</mdhash>
      <filesize>10260</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fb7eca5a70c37ce8add9f4a9c84c32af</md5>
          <size>10260</size>
          <filedate>1443705839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>3fe01d390c8b1dffde235dd274bc634a</md5>
          <size>12506</size>
          <filedate>1443705839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_unique 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_unique/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.x-dev.tar.gz</download_link>
      <date>1443704339</date>
      <mdhash>f52764ec47036828a89ba82a2ad26bfc</mdhash>
      <filesize>10319</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f52764ec47036828a89ba82a2ad26bfc</md5>
          <size>10319</size>
          <filedate>1443704339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_unique-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ff429ea2d91ed5fdc1594527e3c52bb3</md5>
          <size>12849</size>
          <filedate>1443704339</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
