<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Infinite Scroll</title>
  <short_name>views_infinite_scroll</short_name>
  <dc:creator>Remon</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_infinite_scroll</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_infinite_scroll 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.3.tar.gz</download_link>
      <date>1585764810</date>
      <mdhash>cd86268524342b885f01ff3a92cce4a3</mdhash>
      <filesize>10024</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cd86268524342b885f01ff3a92cce4a3</md5>
          <size>10024</size>
          <filedate>1585764810</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9038f01f5b4d046911f1ad6672825b9c</md5>
          <size>11935</size>
          <filedate>1585764810</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.2.tar.gz</download_link>
      <date>1585328230</date>
      <mdhash>826c748fae6e94643c2f2f5fa10e5ec0</mdhash>
      <filesize>10022</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>826c748fae6e94643c2f2f5fa10e5ec0</md5>
          <size>10022</size>
          <filedate>1585328230</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>67600433c4fe8bf318d1cc8123770283</md5>
          <size>11931</size>
          <filedate>1585328230</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.1.tar.gz</download_link>
      <date>1503022147</date>
      <mdhash>ec30d906cfecac75c95e8eba7b76a4cb</mdhash>
      <filesize>10005</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec30d906cfecac75c95e8eba7b76a4cb</md5>
          <size>10005</size>
          <filedate>1503022147</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3cd1d89aab8f49969f6ad5ac530e902c</md5>
          <size>11922</size>
          <filedate>1503022147</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0.tar.gz</download_link>
      <date>1458955139</date>
      <mdhash>3aa7a77da35c0c14b32dffa992669101</mdhash>
      <filesize>9978</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3aa7a77da35c0c14b32dffa992669101</md5>
          <size>9978</size>
          <filedate>1458955139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8fd110d1f9e282ecdda7c08e764b4d5c</md5>
          <size>11839</size>
          <filedate>1458955139</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0-rc1.tar.gz</download_link>
      <date>1455069545</date>
      <mdhash>e74b9dd60eda9e45c5bfd65e510f48c0</mdhash>
      <filesize>9984</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e74b9dd60eda9e45c5bfd65e510f48c0</md5>
          <size>9984</size>
          <filedate>1455069545</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb164731b68b90d1382323b969bbfde3</md5>
          <size>11845</size>
          <filedate>1455069545</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.1.tar.gz</download_link>
      <date>1335182196</date>
      <mdhash>e40ac7ed9b55ac08b13b8ac2ecf83051</mdhash>
      <filesize>11715</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e40ac7ed9b55ac08b13b8ac2ecf83051</md5>
          <size>11715</size>
          <filedate>1335182196</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>eabf442cbaf66f24a728d4c90548ad1e</md5>
          <size>14994</size>
          <filedate>1335182196</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.0.tar.gz</download_link>
      <date>1304622119</date>
      <mdhash>77b0968db9b473a109a125a7b2a45b75</mdhash>
      <filesize>10260</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>77b0968db9b473a109a125a7b2a45b75</md5>
          <size>10260</size>
          <filedate>1304622119</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>35fab1f97c1943b15c32da6342444125</md5>
          <size>13552</size>
          <filedate>1304622119</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.x-dev.tar.gz</download_link>
      <date>1621875480</date>
      <mdhash>82353d37bc2a06c2d6124df4fb4fb8ca</mdhash>
      <filesize>11060</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>82353d37bc2a06c2d6124df4fb4fb8ca</md5>
          <size>11060</size>
          <filedate>1621875480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>dae15a164886db19a549e817a8c225c1</md5>
          <size>13311</size>
          <filedate>1621875480</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_infinite_scroll 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_infinite_scroll/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.x-dev.tar.gz</download_link>
      <date>1412258932</date>
      <mdhash>ec9a93c9e135871060f244fa308d3f21</mdhash>
      <filesize>12227</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec9a93c9e135871060f244fa308d3f21</md5>
          <size>12227</size>
          <filedate>1412258932</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_infinite_scroll-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>621933fb3da9f2bf4b2857b903975377</md5>
          <size>15866</size>
          <filedate>1412258932</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
