<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views content cache</title>
  <short_name>views_content_cache</short_name>
  <dc:creator>steven jones</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/views_content_cache</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking new maintainer</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_content_cache 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/views_content_cache/-/releases/7.x-3.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67460978/packages/generic/views_content_cache/7.x-3.0/views_content_cache-7.x-3.0.tar.gz</download_link>
      <date>1740497526</date>
      <mdhash>195007b0c7a5997aca02f46069a58f4f</mdhash>
      <filesize>18007</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67460978/packages/generic/views_content_cache/7.x-3.0/views_content_cache-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>195007b0c7a5997aca02f46069a58f4f</md5>
          <size>18007</size>
          <filedate>1740497526</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67460978/packages/generic/views_content_cache/7.x-3.0/views_content_cache-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>63781ee6627b682064686e12171dbbdb</md5>
          <size>24497</size>
          <filedate>1740497526</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>views_content_cache 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_content_cache/releases/7.x-3.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha3.tar.gz</download_link>
      <date>1383658105</date>
      <mdhash>cc8d06d74c9066cfeab8f47dd93c69de</mdhash>
      <filesize>17606</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cc8d06d74c9066cfeab8f47dd93c69de</md5>
          <size>17606</size>
          <filedate>1383658105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7c62f37863846a07020e3c515df1abdb</md5>
          <size>23586</size>
          <filedate>1383658105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_content_cache 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_content_cache/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1369601463</date>
      <mdhash>2921496f6c1e909cfc5710cb7a10ae54</mdhash>
      <filesize>17344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2921496f6c1e909cfc5710cb7a10ae54</md5>
          <size>17344</size>
          <filedate>1369601463</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0c5bc3d5a4db1a638dbf474f882268ab</md5>
          <size>23406</size>
          <filedate>1369601463</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_content_cache 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_content_cache/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1347824110</date>
      <mdhash>2139ef175e0dc42836b2a21890be2482</mdhash>
      <filesize>18235</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2139ef175e0dc42836b2a21890be2482</md5>
          <size>18235</size>
          <filedate>1347824110</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8b4745b580ccb8aaf0765327a7d1b752</md5>
          <size>24362</size>
          <filedate>1347824110</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_content_cache 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_content_cache/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.x-dev.tar.gz</download_link>
      <date>1566884887</date>
      <mdhash>784f829ab1c9ecb0f69398ba349eb5a6</mdhash>
      <filesize>17531</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>784f829ab1c9ecb0f69398ba349eb5a6</md5>
          <size>17531</size>
          <filedate>1566884887</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_content_cache-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>297b2c551c5fbd596e3fa6c80ee14e64</md5>
          <size>23982</size>
          <filedate>1566884887</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
