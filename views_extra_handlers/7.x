<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Extra Views Handlers</title>
  <short_name>views_extra_handlers</short_name>
  <dc:creator>TechNikh</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_extra_handlers</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_extra_handlers 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-beta1.tar.gz</download_link>
      <date>1490023684</date>
      <mdhash>aba3ee948a68ac208eb270e08f0301de</mdhash>
      <filesize>13973</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aba3ee948a68ac208eb270e08f0301de</md5>
          <size>13973</size>
          <filedate>1490023684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b214572daaad29b61e2fadc54bcd922f</md5>
          <size>17089</size>
          <filedate>1490023684</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha11</name>
      <version>7.x-1.0-alpha11</version>
      <tag>7.x-1.0-alpha11</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha11</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha11.tar.gz</download_link>
      <date>1418668605</date>
      <mdhash>2ea3b02fee21c15db648cf2e709bd931</mdhash>
      <filesize>13740</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2ea3b02fee21c15db648cf2e709bd931</md5>
          <size>13740</size>
          <filedate>1418668605</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha11.zip</url>
          <archive_type>zip</archive_type>
          <md5>b42bcca333bbd23e27a140d1a677eee4</md5>
          <size>16819</size>
          <filedate>1418668605</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha10</name>
      <version>7.x-1.0-alpha10</version>
      <tag>7.x-1.0-alpha10</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha10.tar.gz</download_link>
      <date>1418413380</date>
      <mdhash>15171bf72a3d8f1220181fb293c0a36e</mdhash>
      <filesize>13285</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>15171bf72a3d8f1220181fb293c0a36e</md5>
          <size>13285</size>
          <filedate>1418413380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha10.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ad49e4615e64dbe21107015bafd691e</md5>
          <size>16373</size>
          <filedate>1418413380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha9</name>
      <version>7.x-1.0-alpha9</version>
      <tag>7.x-1.0-alpha9</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha9.tar.gz</download_link>
      <date>1392161305</date>
      <mdhash>bd0932b210de86b74b4eb634fa5783de</mdhash>
      <filesize>12950</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bd0932b210de86b74b4eb634fa5783de</md5>
          <size>12950</size>
          <filedate>1392161305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha9.zip</url>
          <archive_type>zip</archive_type>
          <md5>620ff51c695e843733c260632885fadf</md5>
          <size>15896</size>
          <filedate>1392161305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha8</name>
      <version>7.x-1.0-alpha8</version>
      <tag>7.x-1.0-alpha8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha8.tar.gz</download_link>
      <date>1371144651</date>
      <mdhash>661b779f6e8037a7804a917a7680f192</mdhash>
      <filesize>12744</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>661b779f6e8037a7804a917a7680f192</md5>
          <size>12744</size>
          <filedate>1371144651</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha8.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d373f58f20e15b95bbdeba515e6c572</md5>
          <size>15679</size>
          <filedate>1371144651</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha7</name>
      <version>7.x-1.0-alpha7</version>
      <tag>7.x-1.0-alpha7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha7.tar.gz</download_link>
      <date>1370971855</date>
      <mdhash>b85698bd3f62354a4f0f27f4a1885099</mdhash>
      <filesize>12543</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b85698bd3f62354a4f0f27f4a1885099</md5>
          <size>12543</size>
          <filedate>1370971855</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>d78f75a1865ee9fa0aaff293a16ac411</md5>
          <size>15448</size>
          <filedate>1370971855</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha6</name>
      <version>7.x-1.0-alpha6</version>
      <tag>7.x-1.0-alpha6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha6.tar.gz</download_link>
      <date>1368210611</date>
      <mdhash>ba71c4f73fa283900d5d51155b7cb64c</mdhash>
      <filesize>12484</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ba71c4f73fa283900d5d51155b7cb64c</md5>
          <size>12484</size>
          <filedate>1368210611</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>23c85e440f607521883b0b705318c7be</md5>
          <size>15385</size>
          <filedate>1368210612</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1367873714</date>
      <mdhash>aba211ba9e347db204d9c37fee5ea095</mdhash>
      <filesize>11995</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aba211ba9e347db204d9c37fee5ea095</md5>
          <size>11995</size>
          <filedate>1367873714</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>810f63346350845bbe314d41c655a483</md5>
          <size>14866</size>
          <filedate>1367873714</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1363806016</date>
      <mdhash>e79ac12fd058f6192252d8cf6c037078</mdhash>
      <filesize>11279</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e79ac12fd058f6192252d8cf6c037078</md5>
          <size>11279</size>
          <filedate>1363806016</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>5bcff65f7c16da14d32d11f2f2c5f25e</md5>
          <size>13675</size>
          <filedate>1363806016</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1363642525</date>
      <mdhash>d19e0631f7e1f329437504c5b6011615</mdhash>
      <filesize>11069</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d19e0631f7e1f329437504c5b6011615</md5>
          <size>11069</size>
          <filedate>1363642525</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>67175c60208cf0543da7cb64d118d970</md5>
          <size>13423</size>
          <filedate>1363642525</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1363383921</date>
      <mdhash>b8e78aeef0ef79b168178add65fb7c6b</mdhash>
      <filesize>11028</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8e78aeef0ef79b168178add65fb7c6b</md5>
          <size>11028</size>
          <filedate>1363383921</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e2e2185ec5a2039f716f467640ab5c32</md5>
          <size>13372</size>
          <filedate>1363383921</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1363379121</date>
      <mdhash>5642c2854512e936fbad6bb63cb2b086</mdhash>
      <filesize>11025</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5642c2854512e936fbad6bb63cb2b086</md5>
          <size>11025</size>
          <filedate>1363379121</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>efac021ae39d69ffa939af0360789b33</md5>
          <size>13378</size>
          <filedate>1363379121</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>views_extra_handlers 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_extra_handlers/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.x-dev.tar.gz</download_link>
      <date>1490023383</date>
      <mdhash>ce7bdcecff2fc0118a14b50cd3ebb858</mdhash>
      <filesize>13974</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce7bdcecff2fc0118a14b50cd3ebb858</md5>
          <size>13974</size>
          <filedate>1490023383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_extra_handlers-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>7b95b51f39b47033b78742ba236cf147</md5>
          <size>17093</size>
          <filedate>1490023383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
