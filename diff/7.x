<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Diff</title>
  <short_name>diff</short_name>
  <dc:creator>yhahn</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/diff</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>diff 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.5.tar.gz</download_link>
      <date>1734376747</date>
      <mdhash>5b303a4638974c0adaa68944802352f2</mdhash>
      <filesize>43770</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5b303a4638974c0adaa68944802352f2</md5>
          <size>43770</size>
          <filedate>1734376747</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>70769d0efbc2d820f13afa098aed4b3d</md5>
          <size>55765</size>
          <filedate>1734376747</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.4.tar.gz</download_link>
      <date>1541401381</date>
      <mdhash>614ec11ce56850b46150702e0583140a</mdhash>
      <filesize>43635</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>614ec11ce56850b46150702e0583140a</md5>
          <size>43635</size>
          <filedate>1541401381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d66ab735aa510912cd6f5dbb1a825ea</md5>
          <size>55604</size>
          <filedate>1541401381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.3.tar.gz</download_link>
      <date>1482211683</date>
      <mdhash>8bfdbe10c7f963fd2bc7885d028cf913</mdhash>
      <filesize>43510</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8bfdbe10c7f963fd2bc7885d028cf913</md5>
          <size>43510</size>
          <filedate>1482211683</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d2e661aa56243b882f800c00682b118</md5>
          <size>55425</size>
          <filedate>1482211683</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.2.tar.gz</download_link>
      <date>1352784357</date>
      <mdhash>05bce6125233fdae361f78201f7a7643</mdhash>
      <filesize>43842</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>05bce6125233fdae361f78201f7a7643</md5>
          <size>43842</size>
          <filedate>1352784357</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f158d656e7fd7686c64f3d9e42c0943f</md5>
          <size>54065</size>
          <filedate>1352784357</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.1.tar.gz</download_link>
      <date>1352609757</date>
      <mdhash>72c98e2c88e98994edb727bd810d9a49</mdhash>
      <filesize>43846</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>72c98e2c88e98994edb727bd810d9a49</md5>
          <size>43846</size>
          <filedate>1352609757</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>febd3f06a4587bbf356521f1dbfb73ef</md5>
          <size>54064</size>
          <filedate>1352609757</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.0.tar.gz</download_link>
      <date>1352536262</date>
      <mdhash>73eb63fd80c4decbb43cd8a4e52d4304</mdhash>
      <filesize>43761</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>73eb63fd80c4decbb43cd8a4e52d4304</md5>
          <size>43761</size>
          <filedate>1352536262</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a10906fea3dccabe99f4d772e2b4de69</md5>
          <size>53966</size>
          <filedate>1352536262</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1342343483</date>
      <mdhash>7849fe1ee969270d7d0089e79f4c3214</mdhash>
      <filesize>45136</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7849fe1ee969270d7d0089e79f4c3214</md5>
          <size>45136</size>
          <filedate>1342343483</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>221903b97895e35c3ccbab0e56824ac0</md5>
          <size>55308</size>
          <filedate>1342343483</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>diff 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-2.0.tar.gz</download_link>
      <date>1311239815</date>
      <mdhash>e25c84e6b7c7ec21ae2caeefe176a7b5</mdhash>
      <filesize>25497</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e25c84e6b7c7ec21ae2caeefe176a7b5</md5>
          <size>25497</size>
          <filedate>1311239815</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9a38506a01402ac639568446206e7450</md5>
          <size>28228</size>
          <filedate>1311239815</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>diff 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta2.tar.gz</download_link>
      <date>1285271161</date>
      <mdhash>b6f687640f4d5c33d622f1a19150bd05</mdhash>
      <filesize>32487</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b6f687640f4d5c33d622f1a19150bd05</md5>
          <size>32487</size>
          <filedate>1285271161</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9a3e66a7f9486e923b80313664b77749</md5>
          <size>42679</size>
          <filedate>1293230754</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>diff 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta1.tar.gz</download_link>
      <date>1284074208</date>
      <mdhash>471607ab84a5c5179cd7f81c3a6b9814</mdhash>
      <filesize>32429</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>471607ab84a5c5179cd7f81c3a6b9814</md5>
          <size>32429</size>
          <filedate>1284074208</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6b58427fd6fdb8844df1e7fced25f08</md5>
          <size>42685</size>
          <filedate>1293230755</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>diff 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-3.x-dev.tar.gz</download_link>
      <date>1734376636</date>
      <mdhash>2a44d0189b9b988df4eca75832b5f873</mdhash>
      <filesize>43774</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a44d0189b9b988df4eca75832b5f873</md5>
          <size>43774</size>
          <filedate>1734376636</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b434214d93839324dc6dd82332da508</md5>
          <size>55771</size>
          <filedate>1734376636</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>diff 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-2.x-dev.tar.gz</download_link>
      <date>1380569556</date>
      <mdhash>a4ca33af2b5284bbd11df5037dd32825</mdhash>
      <filesize>27039</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a4ca33af2b5284bbd11df5037dd32825</md5>
          <size>27039</size>
          <filedate>1380569556</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>89d1067ebe3aabed81cb60b52b245b83</md5>
          <size>29816</size>
          <filedate>1380569556</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>diff 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>master</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/diff/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/diff-7.x-1.x-dev.tar.gz</download_link>
      <date>1380569552</date>
      <mdhash>a9beb070c1fe38c103785831b0868440</mdhash>
      <filesize>21361</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a9beb070c1fe38c103785831b0868440</md5>
          <size>21361</size>
          <filedate>1380569552</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/diff-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9fde11ae267c7ffb2f74caccfc6cef5</md5>
          <size>23374</size>
          <filedate>1380569553</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
