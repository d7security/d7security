<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Cloudflare Turnstile</title>
  <short_name>turnstile</short_name>
  <dc:creator>greatmatter</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/turnstile</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>turnstile 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/turnstile/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/turnstile-7.x-1.0.tar.gz</download_link>
      <date>1677689648</date>
      <mdhash>9bb83228fd6464388de50746ddd9089b</mdhash>
      <filesize>11660</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/turnstile-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9bb83228fd6464388de50746ddd9089b</md5>
          <size>11660</size>
          <filedate>1677689648</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/turnstile-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>725531c1f684f3e8b1d889d9f74fac24</md5>
          <size>14511</size>
          <filedate>1677689648</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
  </releases>
</project>
