<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Weight</title>
  <short_name>weight</short_name>
  <dc:creator>NancyDru</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/weight</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>weight 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.3.tar.gz</download_link>
      <date>1729637457</date>
      <mdhash>507de6a0ce912428f7ab40b66e107648</mdhash>
      <filesize>11796</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>507de6a0ce912428f7ab40b66e107648</md5>
          <size>11796</size>
          <filedate>1729637457</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c40fde0107e6189a44978ee7c1a9cb14</md5>
          <size>14094</size>
          <filedate>1729637457</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.2.tar.gz</download_link>
      <date>1677046807</date>
      <mdhash>98179ed369fb1ab915111e7d3e11fb51</mdhash>
      <filesize>11090</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>98179ed369fb1ab915111e7d3e11fb51</md5>
          <size>11090</size>
          <filedate>1677046807</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>880eabc12382fdfeca7f7168fa012862</md5>
          <size>13194</size>
          <filedate>1677046807</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.1.tar.gz</download_link>
      <date>1449686339</date>
      <mdhash>b150e7a495ca2dfbb49ab06e118f5360</mdhash>
      <filesize>10265</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b150e7a495ca2dfbb49ab06e118f5360</md5>
          <size>10265</size>
          <filedate>1449686339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>63c4310a9c8159bcc442174d2b19508d</md5>
          <size>12286</size>
          <filedate>1449686339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.0.tar.gz</download_link>
      <date>1448735340</date>
      <mdhash>24c9442f584f76ce8d9a2a767166fdb2</mdhash>
      <filesize>10295</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24c9442f584f76ce8d9a2a767166fdb2</md5>
          <size>10295</size>
          <filedate>1448735340</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9cdd351b766d9d11b6e55918aec7b099</md5>
          <size>12319</size>
          <filedate>1448735340</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta2.tar.gz</download_link>
      <date>1430680381</date>
      <mdhash>14bf34605e1a0347c66586a1a61b5d95</mdhash>
      <filesize>10234</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14bf34605e1a0347c66586a1a61b5d95</md5>
          <size>10234</size>
          <filedate>1430680381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>23189efa9e7e80539da024d25e9ab94c</md5>
          <size>12231</size>
          <filedate>1430680381</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta1.tar.gz</download_link>
      <date>1417652880</date>
      <mdhash>7487a6199e8945b9bffd7428e534491b</mdhash>
      <filesize>10308</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7487a6199e8945b9bffd7428e534491b</md5>
          <size>10308</size>
          <filedate>1417652880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>091d340e1bec042b82a680ae0deb6b68</md5>
          <size>12311</size>
          <filedate>1417652880</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.5.tar.gz</download_link>
      <date>1430342581</date>
      <mdhash>2a32b05f8bf5c315dcfc28275a3f5fdf</mdhash>
      <filesize>19230</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a32b05f8bf5c315dcfc28275a3f5fdf</md5>
          <size>19230</size>
          <filedate>1430342581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>53121c4de4675be3cde55c2e5cbdc083</md5>
          <size>23025</size>
          <filedate>1430342581</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.4.tar.gz</download_link>
      <date>1417653780</date>
      <mdhash>855868e5c131d641e76b9f7594e9f03d</mdhash>
      <filesize>18960</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>855868e5c131d641e76b9f7594e9f03d</md5>
          <size>18960</size>
          <filedate>1417653780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7e7632cb2bbf1123924648173274f9a8</md5>
          <size>22754</size>
          <filedate>1417653780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.3.tar.gz</download_link>
      <date>1372087551</date>
      <mdhash>ac7eaa4eaf4196f44134770892f5b8d5</mdhash>
      <filesize>18185</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac7eaa4eaf4196f44134770892f5b8d5</md5>
          <size>18185</size>
          <filedate>1372087551</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b7d3ccb8aa70dddbe1e35d650886e385</md5>
          <size>21512</size>
          <filedate>1372087551</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.2.tar.gz</download_link>
      <date>1360099063</date>
      <mdhash>600f6d34f7708125a13c968bb4340bd2</mdhash>
      <filesize>17463</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>600f6d34f7708125a13c968bb4340bd2</md5>
          <size>17463</size>
          <filedate>1360099063</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>96a8f799eaf0726a235bbc325020c2c7</md5>
          <size>20693</size>
          <filedate>1360099063</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.1.tar.gz</download_link>
      <date>1341880989</date>
      <mdhash>434675cfd1a6d2f668e82f8cfb1ce765</mdhash>
      <filesize>16018</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>434675cfd1a6d2f668e82f8cfb1ce765</md5>
          <size>16018</size>
          <filedate>1341880989</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>03d88d198763daa7a549a09d4fec2c77</md5>
          <size>18852</size>
          <filedate>1341880989</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0.tar.gz</download_link>
      <date>1337468498</date>
      <mdhash>5cd44f52c88191d325f0723809e188ad</mdhash>
      <filesize>15834</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5cd44f52c88191d325f0723809e188ad</md5>
          <size>15834</size>
          <filedate>1337468498</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>f26c6b403a98678e8c2dfee2b9887845</md5>
          <size>18644</size>
          <filedate>1337468498</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc2.tar.gz</download_link>
      <date>1334713023</date>
      <mdhash>b537c8cb472cf6c8ac607ccfe75632a8</mdhash>
      <filesize>15282</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b537c8cb472cf6c8ac607ccfe75632a8</md5>
          <size>15282</size>
          <filedate>1334713023</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7a2112f037b3444949e230f339594880</md5>
          <size>18115</size>
          <filedate>1334713023</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc1.tar.gz</download_link>
      <date>1333374372</date>
      <mdhash>395a9b6548160160b73733607aaae425</mdhash>
      <filesize>15140</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>395a9b6548160160b73733607aaae425</md5>
          <size>15140</size>
          <filedate>1333374372</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>02a9bb68a14119afa5bcb35f2f13829d</md5>
          <size>17925</size>
          <filedate>1333374372</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta3.tar.gz</download_link>
      <date>1330824045</date>
      <mdhash>d15bfc9efd0ed995251938eee5ed0d95</mdhash>
      <filesize>15724</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d15bfc9efd0ed995251938eee5ed0d95</md5>
          <size>15724</size>
          <filedate>1330824045</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>cec366eb21c5f124e46efb2327538ef4</md5>
          <size>18816</size>
          <filedate>1330824045</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta2.tar.gz</download_link>
      <date>1328624752</date>
      <mdhash>66af097424cc272d0c2e728997e06ee2</mdhash>
      <filesize>14318</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66af097424cc272d0c2e728997e06ee2</md5>
          <size>14318</size>
          <filedate>1328624752</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>083be3394cf1133fc0bab62f7438df00</md5>
          <size>16944</size>
          <filedate>1328624752</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta1.tar.gz</download_link>
      <date>1327347352</date>
      <mdhash>b47b9e3cf0dfb6205bc545b2808ad583</mdhash>
      <filesize>13676</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b47b9e3cf0dfb6205bc545b2808ad583</md5>
          <size>13676</size>
          <filedate>1327347352</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c6e96a208c55eed7cbc663fd10633c5e</md5>
          <size>15916</size>
          <filedate>1327347352</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-1.0.tar.gz</download_link>
      <date>1316989908</date>
      <mdhash>a671a07ffa8f5222ebf2e324e06a376a</mdhash>
      <filesize>17749</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a671a07ffa8f5222ebf2e324e06a376a</md5>
          <size>17749</size>
          <filedate>1316989908</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>eb2bfe05247380b381d1c6e03d6abea5</md5>
          <size>22433</size>
          <filedate>1316989908</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>weight 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-1.0-rc1.tar.gz</download_link>
      <date>1315094221</date>
      <mdhash>2378034f88632178cb81e4770016e056</mdhash>
      <filesize>16131</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2378034f88632178cb81e4770016e056</md5>
          <size>16131</size>
          <filedate>1315094221</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>31b90453e429aeff447c999e969dd23e</md5>
          <size>20197</size>
          <filedate>1315094221</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-3.x-dev.tar.gz</download_link>
      <date>1729637027</date>
      <mdhash>30557a0fbe19c787d61913018f5f52f8</mdhash>
      <filesize>11799</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>30557a0fbe19c787d61913018f5f52f8</md5>
          <size>11799</size>
          <filedate>1729637027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ddaaa96d9eaa7cecb51bafefa8c131d5</md5>
          <size>14098</size>
          <filedate>1729637027</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-2.x-dev.tar.gz</download_link>
      <date>1448732940</date>
      <mdhash>d32992947882db60b2d7840b879e78ff</mdhash>
      <filesize>19056</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d32992947882db60b2d7840b879e78ff</md5>
          <size>19056</size>
          <filedate>1448732940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c9f34203aefecaaa66330da914a80bfc</md5>
          <size>22867</size>
          <filedate>1448732940</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>weight 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/weight/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/weight-7.x-1.x-dev.tar.gz</download_link>
      <date>1382756402</date>
      <mdhash>007427488008333730f5d56b1c468b71</mdhash>
      <filesize>18093</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>007427488008333730f5d56b1c468b71</md5>
          <size>18093</size>
          <filedate>1382756402</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/weight-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e055b5e8fded9d46f7ab14825f053a20</md5>
          <size>22720</size>
          <filedate>1382756403</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
