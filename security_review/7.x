<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Security Review</title>
  <short_name>security_review</short_name>
  <dc:creator>greggles</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/security_review</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>security_review 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/security_review/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/security_review-7.x-1.3.tar.gz</download_link>
      <date>1501101843</date>
      <mdhash>1a0f5f8f418f9748cbeb570d88953d18</mdhash>
      <filesize>36207</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1a0f5f8f418f9748cbeb570d88953d18</md5>
          <size>36207</size>
          <filedate>1501101843</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bca8be536a789c9a27fea9d2b9caae05</md5>
          <size>43136</size>
          <filedate>1501101843</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>security_review 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/security_review/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/security_review-7.x-1.2.tar.gz</download_link>
      <date>1410036831</date>
      <mdhash>a197b4992fe41acbf2a06b643f878b1b</mdhash>
      <filesize>34685</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a197b4992fe41acbf2a06b643f878b1b</md5>
          <size>34685</size>
          <filedate>1410036831</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6f198b415effcd8ffad732452b563c69</md5>
          <size>40647</size>
          <filedate>1410036831</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>security_review 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/security_review/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/security_review-7.x-1.1.tar.gz</download_link>
      <date>1380217585</date>
      <mdhash>ceced2ec0b60224089ab190f05df6abc</mdhash>
      <filesize>33899</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ceced2ec0b60224089ab190f05df6abc</md5>
          <size>33899</size>
          <filedate>1380217585</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>505fa8bb32cb9f5d1a25dbea13075232</md5>
          <size>39846</size>
          <filedate>1380217585</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>security_review 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/security_review/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/security_review-7.x-1.0.tar.gz</download_link>
      <date>1322606446</date>
      <mdhash>21b40f872c569fab30021f84363f532a</mdhash>
      <filesize>26729</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>21b40f872c569fab30021f84363f532a</md5>
          <size>26729</size>
          <filedate>1322606446</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>67381998932eea190db903b922bb472a</md5>
          <size>30780</size>
          <filedate>1322606446</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>security_review 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/security_review/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/security_review-7.x-1.x-dev.tar.gz</download_link>
      <date>1674001707</date>
      <mdhash>ac59274e095fb2a8e2ebedd9fc51ea67</mdhash>
      <filesize>36345</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac59274e095fb2a8e2ebedd9fc51ea67</md5>
          <size>36345</size>
          <filedate>1674001707</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/security_review-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6861bdbc6443abf39fa15ebb4eb4625d</md5>
          <size>43306</size>
          <filedate>1674001707</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
