<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Administration menu</title>
  <short_name>admin_menu</short_name>
  <dc:creator>sun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/admin_menu</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>admin_menu 7.x-3.0-rc7</name>
      <version>7.x-3.0-rc7</version>
      <tag>7.x-3.0-rc7</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc7.tar.gz</download_link>
      <date>1685019870</date>
      <mdhash>f95d9eede1145865195ee156ea93f34e</mdhash>
      <filesize>56442</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f95d9eede1145865195ee156ea93f34e</md5>
          <size>56442</size>
          <filedate>1685019870</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc7.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc4c64c52d4a2948b1e985c3bfc445c5</md5>
          <size>67976</size>
          <filedate>1685019870</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc6</name>
      <version>7.x-3.0-rc6</version>
      <tag>7.x-3.0-rc6</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc6.tar.gz</download_link>
      <date>1543859280</date>
      <mdhash>91f08440f64fc2ddee565aab3198cb70</mdhash>
      <filesize>53905</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>91f08440f64fc2ddee565aab3198cb70</md5>
          <size>53905</size>
          <filedate>1543859280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc6.zip</url>
          <archive_type>zip</archive_type>
          <md5>2b92bb1bf2ffa90adf6bfc845af6ce76</md5>
          <size>65117</size>
          <filedate>1543859280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc5</name>
      <version>7.x-3.0-rc5</version>
      <tag>7.x-3.0-rc5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc5.tar.gz</download_link>
      <date>1419029280</date>
      <mdhash>4f1a0c14001c880bd7eb170318b91303</mdhash>
      <filesize>53401</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4f1a0c14001c880bd7eb170318b91303</md5>
          <size>53401</size>
          <filedate>1419029280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ece8aba426fb0886d165e8ceb579704</md5>
          <size>64145</size>
          <filedate>1419029280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc4</name>
      <version>7.x-3.0-rc4</version>
      <tag>7.x-3.0-rc4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc4.tar.gz</download_link>
      <date>1359651687</date>
      <mdhash>3d8359538878723720fca6ddb2f82c7a</mdhash>
      <filesize>55064</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3d8359538878723720fca6ddb2f82c7a</md5>
          <size>55064</size>
          <filedate>1359651687</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>fcaddb19e61801e6a03fbde70fa3bcdd</md5>
          <size>64782</size>
          <filedate>1359651687</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc3</name>
      <version>7.x-3.0-rc3</version>
      <tag>7.x-3.0-rc3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc3.tar.gz</download_link>
      <date>1337292349</date>
      <mdhash>86b659c35823d541354179eccbfdc2d4</mdhash>
      <filesize>50950</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>86b659c35823d541354179eccbfdc2d4</md5>
          <size>50950</size>
          <filedate>1337292349</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d6341a8243b44a0e3ac565efdec4c898</md5>
          <size>60539</size>
          <filedate>1337292349</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc2</name>
      <version>7.x-3.0-rc2</version>
      <tag>7.x-3.0-rc2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc2.tar.gz</download_link>
      <date>1335198071</date>
      <mdhash>47761c79c351697f295d933b85441328</mdhash>
      <filesize>49988</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>47761c79c351697f295d933b85441328</md5>
          <size>49988</size>
          <filedate>1335198071</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e1f8de894fe411ff693ab6aff9a2661a</md5>
          <size>59451</size>
          <filedate>1335198071</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc1.tar.gz</download_link>
      <date>1294378871</date>
      <mdhash>9d0cfb26adda4da7bd309bdf5be305a0</mdhash>
      <filesize>70961</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9d0cfb26adda4da7bd309bdf5be305a0</md5>
          <size>70961</size>
          <filedate>1294378871</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ead62907ea15455b0f5c7fa5477ce927</md5>
          <size>107923</size>
          <filedate>1294378871</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>admin_menu 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/admin_menu/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.x-dev.tar.gz</download_link>
      <date>1648505410</date>
      <mdhash>14aa7f2f4371f1220bf9e79c5a7743e9</mdhash>
      <filesize>56440</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14aa7f2f4371f1220bf9e79c5a7743e9</md5>
          <size>56440</size>
          <filedate>1648505410</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/admin_menu-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b879ab80a23c424d2afc598f6735226</md5>
          <size>67993</size>
          <filedate>1648505410</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
