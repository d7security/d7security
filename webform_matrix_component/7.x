<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Webform Matrix Component</title>
  <short_name>webform_matrix_component</short_name>
  <dc:creator>chetan-singhal</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/webform_matrix_component</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>webform_matrix_component 7.x-7.22</name>
      <version>7.x-7.22</version>
      <tag>7.x-7.22</tag>
      <version_major>7</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-7.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-7.22.tar.gz</download_link>
      <date>1461338639</date>
      <mdhash>cbf6158ccd776d08d78543d9790250c7</mdhash>
      <filesize>17981</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-7.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cbf6158ccd776d08d78543d9790250c7</md5>
          <size>17981</size>
          <filedate>1461338639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-7.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d41138e2727346d3096960292c70e6d</md5>
          <size>20693</size>
          <filedate>1461338639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.30</name>
      <version>7.x-4.30</version>
      <tag>7.x-4.30</tag>
      <version_major>4</version_major>
      <version_patch>30</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.30</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.30.tar.gz</download_link>
      <date>1609774925</date>
      <mdhash>9d178db643759471806e6c309de2f806</mdhash>
      <filesize>20204</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.30.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9d178db643759471806e6c309de2f806</md5>
          <size>20204</size>
          <filedate>1609774925</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.30.zip</url>
          <archive_type>zip</archive_type>
          <md5>553d146d37018e14e3fa472ba644821a</md5>
          <size>23278</size>
          <filedate>1609774925</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.28</name>
      <version>7.x-4.28</version>
      <tag>7.x-4.28</tag>
      <version_major>4</version_major>
      <version_patch>28</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.28</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.28.tar.gz</download_link>
      <date>1500860644</date>
      <mdhash>1fff97504548138e6dabc8c66a98dec6</mdhash>
      <filesize>19960</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.28.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1fff97504548138e6dabc8c66a98dec6</md5>
          <size>19960</size>
          <filedate>1500860644</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.28.zip</url>
          <archive_type>zip</archive_type>
          <md5>cea21d86f440c2a86278d54fa8316d3d</md5>
          <size>23007</size>
          <filedate>1500860644</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.27</name>
      <version>7.x-4.27</version>
      <tag>7.x-4.27</tag>
      <version_major>4</version_major>
      <version_patch>27</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.27</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.27.tar.gz</download_link>
      <date>1476773039</date>
      <mdhash>1d633213e6345dcdc06962df0a008ff2</mdhash>
      <filesize>19669</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.27.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1d633213e6345dcdc06962df0a008ff2</md5>
          <size>19669</size>
          <filedate>1476773039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.27.zip</url>
          <archive_type>zip</archive_type>
          <md5>ce7268c5a627b6276f558f2b51b13053</md5>
          <size>22911</size>
          <filedate>1476773039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.26</name>
      <version>7.x-4.26</version>
      <tag>7.x-4.26</tag>
      <version_major>4</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.26.tar.gz</download_link>
      <date>1476443639</date>
      <mdhash>46b6ec9458690aafbecd9f250d10d5d6</mdhash>
      <filesize>19601</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>46b6ec9458690aafbecd9f250d10d5d6</md5>
          <size>19601</size>
          <filedate>1476443639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>af034ec86786198fcde54ce5de10af75</md5>
          <size>22825</size>
          <filedate>1476443639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.25</name>
      <version>7.x-4.25</version>
      <tag>7.x-4.25</tag>
      <version_major>4</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.25.tar.gz</download_link>
      <date>1476352528</date>
      <mdhash>548108708e692042b1b41c8c47819460</mdhash>
      <filesize>19244</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>548108708e692042b1b41c8c47819460</md5>
          <size>19244</size>
          <filedate>1476352528</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>6505ea4957b32093d075957b71d288ce</md5>
          <size>22558</size>
          <filedate>1476352528</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.24</name>
      <version>7.x-4.24</version>
      <tag>7.x-4.24</tag>
      <version_major>4</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.24.tar.gz</download_link>
      <date>1472354039</date>
      <mdhash>a1639a8c4868eb0fe0045d437a4993d2</mdhash>
      <filesize>19516</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a1639a8c4868eb0fe0045d437a4993d2</md5>
          <size>19516</size>
          <filedate>1472354039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>7a6ee0453e931776151f933ade02d0f6</md5>
          <size>22509</size>
          <filedate>1472354039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.23</name>
      <version>7.x-4.23</version>
      <tag>7.x-4.23</tag>
      <version_major>4</version_major>
      <version_patch>23</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.23</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.23.tar.gz</download_link>
      <date>1472315939</date>
      <mdhash>b378fb820b71f4b6cd36a42f6759f7ad</mdhash>
      <filesize>18189</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.23.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b378fb820b71f4b6cd36a42f6759f7ad</md5>
          <size>18189</size>
          <filedate>1472315939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.23.zip</url>
          <archive_type>zip</archive_type>
          <md5>3cf12ac52892af5429578ffe7ff06008</md5>
          <size>20891</size>
          <filedate>1472315939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.22</name>
      <version>7.x-4.22</version>
      <tag>7.x-4.22</tag>
      <version_major>4</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.22.tar.gz</download_link>
      <date>1461338940</date>
      <mdhash>4a0d8cef3003689932c0ee62aa4e8070</mdhash>
      <filesize>17980</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a0d8cef3003689932c0ee62aa4e8070</md5>
          <size>17980</size>
          <filedate>1461338940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd546b1fd00a4fcb00e1148ab09c7a7b</md5>
          <size>20692</size>
          <filedate>1461338940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.21</name>
      <version>7.x-4.21</version>
      <tag>7.x-4.21</tag>
      <version_major>4</version_major>
      <version_patch>21</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.21</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.21.tar.gz</download_link>
      <date>1460643839</date>
      <mdhash>978c24594e5f4a5b11cb66287f37bf2b</mdhash>
      <filesize>17412</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.21.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>978c24594e5f4a5b11cb66287f37bf2b</md5>
          <size>17412</size>
          <filedate>1460643839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.21.zip</url>
          <archive_type>zip</archive_type>
          <md5>c402004d88ca34ef8c36f3d12bd8498a</md5>
          <size>20100</size>
          <filedate>1460643839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.20</name>
      <version>7.x-4.20</version>
      <tag>7.x-4.20</tag>
      <version_major>4</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.20.tar.gz</download_link>
      <date>1460290139</date>
      <mdhash>0c6e239581d158d0e35d78d97f98f0b2</mdhash>
      <filesize>17336</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0c6e239581d158d0e35d78d97f98f0b2</md5>
          <size>17336</size>
          <filedate>1460290139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>85682e4836edfd237c6273fadfc32a96</md5>
          <size>20001</size>
          <filedate>1460290139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.18</name>
      <version>7.x-4.18</version>
      <tag>7.x-4.18</tag>
      <version_major>4</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.18.tar.gz</download_link>
      <date>1459131242</date>
      <mdhash>0646f644986f30f4fa15057e9876addd</mdhash>
      <filesize>17169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0646f644986f30f4fa15057e9876addd</md5>
          <size>17169</size>
          <filedate>1459131242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>4cb169e71e2ce37ced9694c418877a3c</md5>
          <size>19829</size>
          <filedate>1459131242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.17</name>
      <version>7.x-4.17</version>
      <tag>7.x-4.17</tag>
      <version_major>4</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.17.tar.gz</download_link>
      <date>1441735439</date>
      <mdhash>9bfbeaa907406fe96b0d7031261bbf53</mdhash>
      <filesize>16443</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9bfbeaa907406fe96b0d7031261bbf53</md5>
          <size>16443</size>
          <filedate>1441735439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0a322a35ae913cfa7f8560f1d404fe1</md5>
          <size>19144</size>
          <filedate>1441735439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.16</name>
      <version>7.x-4.16</version>
      <tag>7.x-4.16</tag>
      <version_major>4</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.16.tar.gz</download_link>
      <date>1433695080</date>
      <mdhash>fd4f14b0075efb34c86274753c03034b</mdhash>
      <filesize>16446</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd4f14b0075efb34c86274753c03034b</md5>
          <size>16446</size>
          <filedate>1433695080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc7155ac8f67f18f99806ef30b2e4f57</md5>
          <size>19146</size>
          <filedate>1433695080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.15</name>
      <version>7.x-4.15</version>
      <tag>7.x-4.15</tag>
      <version_major>4</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.15.tar.gz</download_link>
      <date>1433297281</date>
      <mdhash>4a03eb50e306c421160a67f80708073a</mdhash>
      <filesize>16445</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a03eb50e306c421160a67f80708073a</md5>
          <size>16445</size>
          <filedate>1433297281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>b988ddf10c40a818782aeffb47c986af</md5>
          <size>19149</size>
          <filedate>1433297281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.14</name>
      <version>7.x-4.14</version>
      <tag>7.x-4.14</tag>
      <version_major>4</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.14.tar.gz</download_link>
      <date>1431367981</date>
      <mdhash>2c373e066bbbeb24e71401a434655f06</mdhash>
      <filesize>16263</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2c373e066bbbeb24e71401a434655f06</md5>
          <size>16263</size>
          <filedate>1431367981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>8021a4324327dd854153e045287fd558</md5>
          <size>18930</size>
          <filedate>1431367981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.13</name>
      <version>7.x-4.13</version>
      <tag>7.x-4.13</tag>
      <version_major>4</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.13.tar.gz</download_link>
      <date>1425045594</date>
      <mdhash>cb73258e16cbe595c2fd160439182d8f</mdhash>
      <filesize>16261</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb73258e16cbe595c2fd160439182d8f</md5>
          <size>16261</size>
          <filedate>1425045594</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9c976fc0ab1aefe84f92daa5264cc0b</md5>
          <size>18927</size>
          <filedate>1425045594</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.12</name>
      <version>7.x-4.12</version>
      <tag>7.x-4.12</tag>
      <version_major>4</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.12.tar.gz</download_link>
      <date>1425030481</date>
      <mdhash>064342799366d504e6002c383d46ff01</mdhash>
      <filesize>16126</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>064342799366d504e6002c383d46ff01</md5>
          <size>16126</size>
          <filedate>1425030481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>40a4029001ef1b217858b93f4c642f29</md5>
          <size>18752</size>
          <filedate>1425030481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.11</name>
      <version>7.x-4.11</version>
      <tag>7.x-4.11</tag>
      <version_major>4</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.11.tar.gz</download_link>
      <date>1410839028</date>
      <mdhash>67b2de09b6180f693ab48b461a18ead2</mdhash>
      <filesize>15808</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>67b2de09b6180f693ab48b461a18ead2</md5>
          <size>15808</size>
          <filedate>1410839028</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>6da8f21b4974b829d75bc7dc5361f062</md5>
          <size>18388</size>
          <filedate>1410839028</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.0.tar.gz</download_link>
      <date>1408949628</date>
      <mdhash>c7b8011d9e8ec082f07af2e7c43d4b40</mdhash>
      <filesize>16009</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c7b8011d9e8ec082f07af2e7c43d4b40</md5>
          <size>16009</size>
          <filedate>1408949628</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4fc37017e2a5579d6052e12b301c3d6</md5>
          <size>18531</size>
          <filedate>1408949628</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.4.tar.gz</download_link>
      <date>1387345105</date>
      <mdhash>65f3ad7c6c96c4bd5cc5c499c873c38b</mdhash>
      <filesize>14916</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>65f3ad7c6c96c4bd5cc5c499c873c38b</md5>
          <size>14916</size>
          <filedate>1387345105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>959ab325f0a8d135ef685654c0f8cd1f</md5>
          <size>16845</size>
          <filedate>1387345105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.3.tar.gz</download_link>
      <date>1386686904</date>
      <mdhash>7e2b21afb74898c7b81dbb6f4e2353fa</mdhash>
      <filesize>14889</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e2b21afb74898c7b81dbb6f4e2353fa</md5>
          <size>14889</size>
          <filedate>1386686904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>17c0df4fecafd7f3f513ef8d26da77e4</md5>
          <size>16819</size>
          <filedate>1386686904</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.2.tar.gz</download_link>
      <date>1386685704</date>
      <mdhash>06b42f12963502f7193ce98514b51a63</mdhash>
      <filesize>14876</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>06b42f12963502f7193ce98514b51a63</md5>
          <size>14876</size>
          <filedate>1386685704</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>22bb7807142ecc6cc0cabb1c8dfb6a12</md5>
          <size>16809</size>
          <filedate>1386685704</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.1.tar.gz</download_link>
      <date>1385542404</date>
      <mdhash>ac919072301d029f0da2543c9092eaa4</mdhash>
      <filesize>14865</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac919072301d029f0da2543c9092eaa4</md5>
          <size>14865</size>
          <filedate>1385542404</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e1fbb01ffbe4e90a6dc49318649285b</md5>
          <size>17023</size>
          <filedate>1385542404</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.0.tar.gz</download_link>
      <date>1380780972</date>
      <mdhash>7ba731b36b612b25c9cdb5be3a5951e1</mdhash>
      <filesize>14252</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ba731b36b612b25c9cdb5be3a5951e1</md5>
          <size>14252</size>
          <filedate>1380780972</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>d84ec66a4e71800e4841828da64e6b3a</md5>
          <size>16505</size>
          <filedate>1380780972</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.1.tar.gz</download_link>
      <date>1369917742</date>
      <mdhash>f4a213fa7bfafdf64bfa5a8052084592</mdhash>
      <filesize>14198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f4a213fa7bfafdf64bfa5a8052084592</md5>
          <size>14198</size>
          <filedate>1369917742</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0e718898996190a8593338f872961748</md5>
          <size>16456</size>
          <filedate>1369917742</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.x-dev.tar.gz</download_link>
      <date>1609599214</date>
      <mdhash>b716ad51cec22ec79b2407e64bf731d9</mdhash>
      <filesize>20205</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b716ad51cec22ec79b2407e64bf731d9</md5>
          <size>20205</size>
          <filedate>1609599214</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d8974ef3a67ca25c3a6ef2f74fadb863</md5>
          <size>23280</size>
          <filedate>1609599214</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.x-dev.tar.gz</download_link>
      <date>1401383027</date>
      <mdhash>a514f639af82a8998d8392d61a2e063c</mdhash>
      <filesize>15429</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a514f639af82a8998d8392d61a2e063c</md5>
          <size>15429</size>
          <filedate>1401383027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>14a7ab3a23bfbd94a7ac0f2dcd4e21e7</md5>
          <size>23522</size>
          <filedate>1401383027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-2.x-dev.tar.gz</download_link>
      <date>1382755891</date>
      <mdhash>2b69c26a4bd57d2de0dfd861f6e8512c</mdhash>
      <filesize>14265</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2b69c26a4bd57d2de0dfd861f6e8512c</md5>
          <size>14265</size>
          <filedate>1382755891</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>db664e7fdd9e147e71206ac18d4ee674</md5>
          <size>16509</size>
          <filedate>1382755891</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_matrix_component 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_matrix_component/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.x-dev.tar.gz</download_link>
      <date>1382755892</date>
      <mdhash>abd5f8f9ef92b506458923cebe3de33b</mdhash>
      <filesize>14514</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>abd5f8f9ef92b506458923cebe3de33b</md5>
          <size>14514</size>
          <filedate>1382755892</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_matrix_component-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a465808d5001d85893be65b274d78642</md5>
          <size>16887</size>
          <filedate>1382755892</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
