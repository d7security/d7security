<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Prevent JS alerts [D7]</title>
  <short_name>prevent_js_alerts</short_name>
  <dc:creator>anybody</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/prevent_js_alerts</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>prevent_js_alerts 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/prevent_js_alerts/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.0.tar.gz</download_link>
      <date>1389280405</date>
      <mdhash>b9c18d5063d6373f21ebfbd2e9fd2189</mdhash>
      <filesize>8709</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9c18d5063d6373f21ebfbd2e9fd2189</md5>
          <size>8709</size>
          <filedate>1389280405</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>adfac1a806e5d6a8443382c6d727893e</md5>
          <size>10138</size>
          <filedate>1389280405</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>prevent_js_alerts 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/prevent_js_alerts/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.x-dev.tar.gz</download_link>
      <date>1389280405</date>
      <mdhash>07d59fcad6219cf3121e3a84dffe9f59</mdhash>
      <filesize>8715</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>07d59fcad6219cf3121e3a84dffe9f59</md5>
          <size>8715</size>
          <filedate>1389280405</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/prevent_js_alerts-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>982185b64945a4be258256bd67ee9e03</md5>
          <size>10143</size>
          <filedate>1389280405</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
