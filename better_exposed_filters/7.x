<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Better Exposed Filters</title>
  <short_name>better_exposed_filters</short_name>
  <dc:creator>rlhawk</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/better_exposed_filters</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>better_exposed_filters 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.6.tar.gz</download_link>
      <date>1536016681</date>
      <mdhash>2fbc6103849e387295fe5d9449565f61</mdhash>
      <filesize>51278</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2fbc6103849e387295fe5d9449565f61</md5>
          <size>51278</size>
          <filedate>1536016681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>9f1b6bc43dbcb64a089870ae9c0aaf24</md5>
          <size>60389</size>
          <filedate>1536016681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.5.tar.gz</download_link>
      <date>1508952844</date>
      <mdhash>a1275ae33a38e87cafdfae536536e0ac</mdhash>
      <filesize>51116</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a1275ae33a38e87cafdfae536536e0ac</md5>
          <size>51116</size>
          <filedate>1508952844</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3aa5636049111d47d6924f43c1d9384a</md5>
          <size>60205</size>
          <filedate>1508952844</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.4.tar.gz</download_link>
      <date>1485926283</date>
      <mdhash>0a149a565aa4885faf6015c1ce6b929b</mdhash>
      <filesize>51110</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0a149a565aa4885faf6015c1ce6b929b</md5>
          <size>51110</size>
          <filedate>1485926283</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>43bcc01fadd93716f32ad0f683ef3c78</md5>
          <size>60197</size>
          <filedate>1485926283</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.3.tar.gz</download_link>
      <date>1484287085</date>
      <mdhash>fd788f73765e10d63b29c8552a1565cf</mdhash>
      <filesize>51087</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd788f73765e10d63b29c8552a1565cf</md5>
          <size>51087</size>
          <filedate>1484287085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>e1cae0dba996301ebff6390328234aec</md5>
          <size>60172</size>
          <filedate>1484287085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.2.tar.gz</download_link>
      <date>1428077881</date>
      <mdhash>3134e2c13500d96fd3c10d0fa07181fa</mdhash>
      <filesize>49437</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3134e2c13500d96fd3c10d0fa07181fa</md5>
          <size>49437</size>
          <filedate>1428077881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8963817581cc88608f0fa0680674c2f1</md5>
          <size>58427</size>
          <filedate>1428077881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.1.tar.gz</download_link>
      <date>1427969281</date>
      <mdhash>017ab6ca581c48a78ec8f64d03e2e0e4</mdhash>
      <filesize>48222</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>017ab6ca581c48a78ec8f64d03e2e0e4</md5>
          <size>48222</size>
          <filedate>1427969281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a067beb0130e52cf7c55c5d2843b692e</md5>
          <size>57151</size>
          <filedate>1427969281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0.tar.gz</download_link>
      <date>1418734682</date>
      <mdhash>38a786d3a3b3b771c8c2628a1ce21bd3</mdhash>
      <filesize>46503</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38a786d3a3b3b771c8c2628a1ce21bd3</md5>
          <size>46503</size>
          <filedate>1418734682</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>d60769c1c7b87c06577efbc6c4b0262c</md5>
          <size>54989</size>
          <filedate>1418734682</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.0-beta4</name>
      <version>7.x-3.0-beta4</version>
      <tag>7.x-3.0-beta4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta4.tar.gz</download_link>
      <date>1392255205</date>
      <mdhash>f6537a3b09c0e3e9dd257928eae8198e</mdhash>
      <filesize>44243</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f6537a3b09c0e3e9dd257928eae8198e</md5>
          <size>44243</size>
          <filedate>1392255205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>e52b5afeb3a7be4f4437875e474d1de3</md5>
          <size>51422</size>
          <filedate>1392255205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.0-beta3</name>
      <version>7.x-3.0-beta3</version>
      <tag>7.x-3.0-beta3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.0-beta3</release_link>
      <download_link/>
      <date>1349281866</date>
      <mdhash>bf1378218e4cc0db6ecf856813a90d23</mdhash>
      <filesize>29586</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>bf1378218e4cc0db6ecf856813a90d23</md5>
          <size>29586</size>
          <filedate>1349281866</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>1a59b22b7ede0b14966663bb7e5a4bcb</md5>
          <size>33805</size>
          <filedate>1349281866</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta2.tar.gz</download_link>
      <date>1347562608</date>
      <mdhash>0be78b0c7baec31077c41624fbd58087</mdhash>
      <filesize>29237</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0be78b0c7baec31077c41624fbd58087</md5>
          <size>29237</size>
          <filedate>1347562608</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>40f9d34b9ebc05df8b2b60fcd7874ec0</md5>
          <size>33453</size>
          <filedate>1347562608</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta1.tar.gz</download_link>
      <date>1312327014</date>
      <mdhash>3ae8db4a8857ef0ba86083b968191feb</mdhash>
      <filesize>22389</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ae8db4a8857ef0ba86083b968191feb</md5>
          <size>22389</size>
          <filedate>1312327014</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f4e14d72e5399f6417d3fafb6dde50c2</md5>
          <size>26462</size>
          <filedate>1312327014</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.x-dev.tar.gz</download_link>
      <date>1712777024</date>
      <mdhash>b57c0e3a3b946322687777286fab7a47</mdhash>
      <filesize>51602</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b57c0e3a3b946322687777286fab7a47</md5>
          <size>51602</size>
          <filedate>1712777024</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6c8b8ea5d71da96b16e383ef4ce2be73</md5>
          <size>60724</size>
          <filedate>1712777024</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>better_exposed_filters 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/better_exposed_filters/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-1.x-dev.tar.gz</download_link>
      <date>1380555926</date>
      <mdhash>0ffdc1ce5f0cff0254ddbaf787ce930a</mdhash>
      <filesize>22828</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0ffdc1ce5f0cff0254ddbaf787ce930a</md5>
          <size>22828</size>
          <filedate>1380555926</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/better_exposed_filters-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9d7e51867c5e509503d7a17cb243488</md5>
          <size>26952</size>
          <filedate>1380555926</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
