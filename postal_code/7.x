<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Postal Code</title>
  <short_name>postal_code</short_name>
  <dc:creator>jeremyclassic</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/postal_code</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>postal_code 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/postal_code/-/releases/7.x-1.10</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67452458/packages/generic/postal_code/7.x-1.10/postal_code-7.x-1.10.tar.gz</download_link>
      <date>1740484572</date>
      <mdhash>80af8e1b9f79941075c7a1418775e507</mdhash>
      <filesize>12533</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67452458/packages/generic/postal_code/7.x-1.10/postal_code-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>80af8e1b9f79941075c7a1418775e507</md5>
          <size>12533</size>
          <filedate>1740484572</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67452458/packages/generic/postal_code/7.x-1.10/postal_code-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>1055c1ff75df928987d098393b1484f2</md5>
          <size>14955</size>
          <filedate>1740484572</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.9.tar.gz</download_link>
      <date>1418188081</date>
      <mdhash>2623fbe2507cb992bbe6d07b8bc87a6a</mdhash>
      <filesize>12136</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2623fbe2507cb992bbe6d07b8bc87a6a</md5>
          <size>12136</size>
          <filedate>1418188081</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>10f3ae6cde61b684c131a4b8d532a752</md5>
          <size>14110</size>
          <filedate>1418188081</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.8.tar.gz</download_link>
      <date>1402679629</date>
      <mdhash>b1e37e65b87101959df17d9bf0a12a13</mdhash>
      <filesize>12144</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b1e37e65b87101959df17d9bf0a12a13</md5>
          <size>12144</size>
          <filedate>1402679629</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>1533584eaadc247dd9d5a93d2aac4ba7</md5>
          <size>13966</size>
          <filedate>1402679629</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.7.tar.gz</download_link>
      <date>1399592627</date>
      <mdhash>643b3d85cb230c07cacb1960dd7791b0</mdhash>
      <filesize>12145</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>643b3d85cb230c07cacb1960dd7791b0</md5>
          <size>12145</size>
          <filedate>1399592627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>f6463bf0df09394018f7e90eba1cdd8c</md5>
          <size>13972</size>
          <filedate>1399592627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.6.tar.gz</download_link>
      <date>1399586627</date>
      <mdhash>e1ca71fd6bd452edaaf4f5444c7317aa</mdhash>
      <filesize>12145</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e1ca71fd6bd452edaaf4f5444c7317aa</md5>
          <size>12145</size>
          <filedate>1399586627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba8bee6b8fcdfa8807b2f9b387b02ffd</md5>
          <size>13969</size>
          <filedate>1399586627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.5.tar.gz</download_link>
      <date>1367609713</date>
      <mdhash>3fe789b20eb5bf1345b5644def50d081</mdhash>
      <filesize>11394</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3fe789b20eb5bf1345b5644def50d081</md5>
          <size>11394</size>
          <filedate>1367609713</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>18e8619f62009c92784493ca82927f47</md5>
          <size>12977</size>
          <filedate>1367609713</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.4</release_link>
      <download_link/>
      <date>1358282051</date>
      <mdhash>5b2daba62aad7429f4581d092e3fd831</mdhash>
      <filesize>11393</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>5b2daba62aad7429f4581d092e3fd831</md5>
          <size>11393</size>
          <filedate>1358282051</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>ca30388937f78473fb7a70615408c37a</md5>
          <size>12974</size>
          <filedate>1358282051</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.3.tar.gz</download_link>
      <date>1357204157</date>
      <mdhash>02b69257520b78c5c81d8d0e6280b199</mdhash>
      <filesize>11376</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>02b69257520b78c5c81d8d0e6280b199</md5>
          <size>11376</size>
          <filedate>1357204157</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d1b32d769638fdb4550ca61a0790087c</md5>
          <size>12955</size>
          <filedate>1357204157</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.2.tar.gz</download_link>
      <date>1349916718</date>
      <mdhash>6687aea93c3f1482e04c6ae9405eb1e2</mdhash>
      <filesize>11362</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6687aea93c3f1482e04c6ae9405eb1e2</md5>
          <size>11362</size>
          <filedate>1349916718</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d74da692ff9b902dbf3f880d4640a21d</md5>
          <size>12940</size>
          <filedate>1349916718</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.1.tar.gz</download_link>
      <date>1349869018</date>
      <mdhash>7794330e0a7cca2207b1eb2b4890d621</mdhash>
      <filesize>11355</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7794330e0a7cca2207b1eb2b4890d621</md5>
          <size>11355</size>
          <filedate>1349869018</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9bb933f17b089eed689e0def3fadc5d2</md5>
          <size>12938</size>
          <filedate>1349869018</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.0.tar.gz</download_link>
      <date>1344253940</date>
      <mdhash>bee15a470b558eaeaef63dd22572748e</mdhash>
      <filesize>11360</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bee15a470b558eaeaef63dd22572748e</md5>
          <size>11360</size>
          <filedate>1344253940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>08b4d38afdd649aedd9f87390e63994d</md5>
          <size>12938</size>
          <filedate>1344253940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>postal_code 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/postal_code/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/postal_code-7.x-1.x-dev.tar.gz</download_link>
      <date>1464259739</date>
      <mdhash>38f1e3f47b83e7d6b569a288781afb2f</mdhash>
      <filesize>12220</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38f1e3f47b83e7d6b569a288781afb2f</md5>
          <size>12220</size>
          <filedate>1464259739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/postal_code-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>0495272de19c5800245c76b88e725e5d</md5>
          <size>14356</size>
          <filedate>1464259739</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
