<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Composer Vendor Autoload</title>
  <short_name>composer_autoloader</short_name>
  <dc:creator>davidwbarratt</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/composer_autoloader</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>composer_autoloader 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/composer_autoloader/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.3.tar.gz</download_link>
      <date>1520741884</date>
      <mdhash>d858d73ad593b0ebd4e8c1cd3e5f523c</mdhash>
      <filesize>8086</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d858d73ad593b0ebd4e8c1cd3e5f523c</md5>
          <size>8086</size>
          <filedate>1520741884</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c14cb4754328908cf4aa875893180745</md5>
          <size>9222</size>
          <filedate>1520741884</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>composer_autoloader 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/composer_autoloader/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.2.tar.gz</download_link>
      <date>1498877944</date>
      <mdhash>a0b7bc3936fc2a5c8a4315efad0ef960</mdhash>
      <filesize>8108</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a0b7bc3936fc2a5c8a4315efad0ef960</md5>
          <size>8108</size>
          <filedate>1498877944</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e9f214ad91082e5f6fec46cc0c35c344</md5>
          <size>9271</size>
          <filedate>1498877944</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>composer_autoloader 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/composer_autoloader/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.1.tar.gz</download_link>
      <date>1419453780</date>
      <mdhash>f48004c3d2ab2d98576605be5d4057c0</mdhash>
      <filesize>8203</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f48004c3d2ab2d98576605be5d4057c0</md5>
          <size>8203</size>
          <filedate>1419453780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6aaf458c9f7d9e2ded256a6109f7ad52</md5>
          <size>9364</size>
          <filedate>1419453780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>composer_autoloader 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/composer_autoloader/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.0.tar.gz</download_link>
      <date>1409023128</date>
      <mdhash>cb6e6728f047746c87702563f4c594b3</mdhash>
      <filesize>8233</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb6e6728f047746c87702563f4c594b3</md5>
          <size>8233</size>
          <filedate>1409023128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a53e54b3be113e7a44399415e63bac5c</md5>
          <size>9387</size>
          <filedate>1409023128</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>composer_autoloader 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/composer_autoloader/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.x-dev.tar.gz</download_link>
      <date>1521035280</date>
      <mdhash>6bd8a81f27e2161aa929a2bbb6f2df2b</mdhash>
      <filesize>8096</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6bd8a81f27e2161aa929a2bbb6f2df2b</md5>
          <size>8096</size>
          <filedate>1521035280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/composer_autoloader-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d51641abf89060b6e7e32f667891475b</md5>
          <size>9227</size>
          <filedate>1521035280</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
