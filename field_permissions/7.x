<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Field Permissions</title>
  <short_name>field_permissions</short_name>
  <dc:creator>markus_petrux</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/field_permissions</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>field_permissions 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.1.tar.gz</download_link>
      <date>1570136286</date>
      <mdhash>172759698a39243893327de2b55161b9</mdhash>
      <filesize>24815</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>172759698a39243893327de2b55161b9</md5>
          <size>24815</size>
          <filedate>1570136286</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>afefa811f4fabd2e367d5c98d3759304</md5>
          <size>29703</size>
          <filedate>1570136286</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_permissions 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0.tar.gz</download_link>
      <date>1476649739</date>
      <mdhash>d600e0cae242374ed0fca499ebfc3dba</mdhash>
      <filesize>25014</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d600e0cae242374ed0fca499ebfc3dba</md5>
          <size>25014</size>
          <filedate>1476649739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>44c97e90a9cf473ba5bc5980503ca1bf</md5>
          <size>29696</size>
          <filedate>1476649739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_permissions 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta2.tar.gz</download_link>
      <date>1327510549</date>
      <mdhash>9427a7e9467a9789cfc66fdb52cee26c</mdhash>
      <filesize>22975</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9427a7e9467a9789cfc66fdb52cee26c</md5>
          <size>22975</size>
          <filedate>1327510549</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d00f742a8b8f51176c5f122ca686e63</md5>
          <size>26905</size>
          <filedate>1327510549</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_permissions 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta1.tar.gz</download_link>
      <date>1318993831</date>
      <mdhash>a07fb8f2a0a296ff56a2b56965e04b20</mdhash>
      <filesize>22171</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a07fb8f2a0a296ff56a2b56965e04b20</md5>
          <size>22171</size>
          <filedate>1318993831</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f93fa704ec3bbdeede15c05fc011cf41</md5>
          <size>26025</size>
          <filedate>1318993831</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_permissions 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1301517068</date>
      <mdhash>f1e871930df8545aaac04f29388fb3e1</mdhash>
      <filesize>16879</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f1e871930df8545aaac04f29388fb3e1</md5>
          <size>16879</size>
          <filedate>1301517068</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>141d966b1034599ac082659849cb09bb</md5>
          <size>20469</size>
          <filedate>1301517068</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_permissions 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_permissions/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.x-dev.tar.gz</download_link>
      <date>1560180181</date>
      <mdhash>0d7366a3e447e37200f7c09b2d3f8cc5</mdhash>
      <filesize>24808</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d7366a3e447e37200f7c09b2d3f8cc5</md5>
          <size>24808</size>
          <filedate>1560180181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_permissions-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d8928d866314c34d07397873faa2f37b</md5>
          <size>29707</size>
          <filedate>1560180181</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
