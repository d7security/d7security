<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Automatic Entity Label</title>
  <short_name>auto_entitylabel</short_name>
  <dc:creator>bforchhammer</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/auto_entitylabel</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>auto_entitylabel 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.4.tar.gz</download_link>
      <date>1493219643</date>
      <mdhash>b68076a937e66d6294b84a75a96f4756</mdhash>
      <filesize>17355</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b68076a937e66d6294b84a75a96f4756</md5>
          <size>17355</size>
          <filedate>1493219643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9afef50cc1de67ded9128936189964e3</md5>
          <size>20795</size>
          <filedate>1493219643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>auto_entitylabel 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.3.tar.gz</download_link>
      <date>1419756180</date>
      <mdhash>cd93434ccbd8c48588f883be6fffddd7</mdhash>
      <filesize>15740</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cd93434ccbd8c48588f883be6fffddd7</md5>
          <size>15740</size>
          <filedate>1419756180</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c41735d1c3cf608e13d15bed5e6b9970</md5>
          <size>18294</size>
          <filedate>1419756180</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>auto_entitylabel 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.2.tar.gz</download_link>
      <date>1369446962</date>
      <mdhash>4e6f413bee9a270d184d684960109cc3</mdhash>
      <filesize>15353</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e6f413bee9a270d184d684960109cc3</md5>
          <size>15353</size>
          <filedate>1369446962</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>308aa0163c3b01c66cba330b84d0ec1b</md5>
          <size>17622</size>
          <filedate>1369446962</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>auto_entitylabel 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.1.tar.gz</download_link>
      <date>1346605601</date>
      <mdhash>34728bd7eb5e8e09c8bfc2d6b1c6540b</mdhash>
      <filesize>14746</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>34728bd7eb5e8e09c8bfc2d6b1c6540b</md5>
          <size>14746</size>
          <filedate>1346605601</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4fec568adc4a2448726fa8d5680e9963</md5>
          <size>16932</size>
          <filedate>1346605601</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>auto_entitylabel 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.0.tar.gz</download_link>
      <date>1336044050</date>
      <mdhash>8c98c8b2c18c98b713fb60d6cbab75da</mdhash>
      <filesize>14053</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8c98c8b2c18c98b713fb60d6cbab75da</md5>
          <size>14053</size>
          <filedate>1336044050</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8481be96bfad59953753916b3bd88ba3</md5>
          <size>16271</size>
          <filedate>1336044050</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>auto_entitylabel 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/auto_entitylabel/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.x-dev.tar.gz</download_link>
      <date>1501772644</date>
      <mdhash>e47fb5336e1062f0956d7a62af9c36d1</mdhash>
      <filesize>17682</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e47fb5336e1062f0956d7a62af9c36d1</md5>
          <size>17682</size>
          <filedate>1501772644</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/auto_entitylabel-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e8b14b2ae42788b4665ceeccee5ccb89</md5>
          <size>21613</size>
          <filedate>1501772644</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
