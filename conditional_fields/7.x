<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Conditional Fields</title>
  <short_name>conditional_fields</short_name>
  <dc:creator>peterpoe</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/conditional_fields</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>conditional_fields 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/conditional_fields/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1443545939</date>
      <mdhash>439f4c25bcd3a78d0e59ffaf0c3e8693</mdhash>
      <filesize>39967</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>439f4c25bcd3a78d0e59ffaf0c3e8693</md5>
          <size>39967</size>
          <filedate>1443545939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>5d380ab35350dad49d8368baf0f1a864</md5>
          <size>45588</size>
          <filedate>1443545939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>conditional_fields 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/conditional_fields/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1384798705</date>
      <mdhash>44fa8baac9d207eabe4d2394d313e19b</mdhash>
      <filesize>44468</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>44fa8baac9d207eabe4d2394d313e19b</md5>
          <size>44468</size>
          <filedate>1384798705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9310d67f7e0eb3d422596332910bc2b</md5>
          <size>50481</size>
          <filedate>1384798705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>conditional_fields 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/conditional_fields/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.x-dev.tar.gz</download_link>
      <date>1709391849</date>
      <mdhash>f4fb094f04a618a4e34101b2b5b84ba6</mdhash>
      <filesize>40559</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f4fb094f04a618a4e34101b2b5b84ba6</md5>
          <size>40559</size>
          <filedate>1709391849</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/conditional_fields-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa0b3c4c667d423eeeb08e5e3ce3271b</md5>
          <size>46541</size>
          <filedate>1709391849</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
