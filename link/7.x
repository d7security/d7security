<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Link</title>
  <short_name>link</short_name>
  <dc:creator>jcfiala</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/link</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>link 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.11.tar.gz</download_link>
      <date>1651671645</date>
      <mdhash>64b0fa101621d98470c98f068192fe32</mdhash>
      <filesize>56162</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>64b0fa101621d98470c98f068192fe32</md5>
          <size>56162</size>
          <filedate>1651671645</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>4cd6175d5515f19d1bdd434cd1c347fb</md5>
          <size>69132</size>
          <filedate>1651671645</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.10.tar.gz</download_link>
      <date>1651670161</date>
      <mdhash>bce92f64542416186478797877b0bd15</mdhash>
      <filesize>55982</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bce92f64542416186478797877b0bd15</md5>
          <size>55982</size>
          <filedate>1651670161</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>7de4004fb497a1299e894b0ad8998872</md5>
          <size>68931</size>
          <filedate>1651670161</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.9.tar.gz</download_link>
      <date>1618831237</date>
      <mdhash>87442789f6d2aeb919d5b19fe98a6022</mdhash>
      <filesize>55020</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>87442789f6d2aeb919d5b19fe98a6022</md5>
          <size>55020</size>
          <filedate>1618831237</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>c3b58edda7c8ef25264246fbea0e8298</md5>
          <size>67971</size>
          <filedate>1618831237</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.8.tar.gz</download_link>
      <date>1618587669</date>
      <mdhash>ccaaf5c035ff239617b325eb7884b9e3</mdhash>
      <filesize>54972</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ccaaf5c035ff239617b325eb7884b9e3</md5>
          <size>54972</size>
          <filedate>1618587669</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>deb1ae91f659d5039704c24e4d114efc</md5>
          <size>67922</size>
          <filedate>1618587669</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.7.tar.gz</download_link>
      <date>1573731186</date>
      <mdhash>b1d184aa3e6484131593f59973b012b5</mdhash>
      <filesize>49298</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b1d184aa3e6484131593f59973b012b5</md5>
          <size>49298</size>
          <filedate>1573731186</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c8646bfb8a8cab45b7f732cc4fccd93</md5>
          <size>60625</size>
          <filedate>1573731186</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.6.tar.gz</download_link>
      <date>1550680684</date>
      <mdhash>119888100f5794896ecf7086afe3abb4</mdhash>
      <filesize>38978</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>119888100f5794896ecf7086afe3abb4</md5>
          <size>38978</size>
          <filedate>1550680684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>9a4dc0a9670b26646b2913284472dfef</md5>
          <size>49220</size>
          <filedate>1550680684</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.5.tar.gz</download_link>
      <date>1526190484</date>
      <mdhash>0a7979864f22f72c5d2fc57052296026</mdhash>
      <filesize>38933</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0a7979864f22f72c5d2fc57052296026</md5>
          <size>38933</size>
          <filedate>1526190484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>584424d2813bcc06e569519caaed8cdb</md5>
          <size>49179</size>
          <filedate>1526190484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.5-beta3</name>
      <version>7.x-1.5-beta3</version>
      <tag>7.x-1.5-beta3</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.5-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta3.tar.gz</download_link>
      <date>1519222085</date>
      <mdhash>a7b7e1b02e57677ccf3409fa7e54c071</mdhash>
      <filesize>39086</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a7b7e1b02e57677ccf3409fa7e54c071</md5>
          <size>39086</size>
          <filedate>1519222085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b8b5f14b6ed5c5f0fd3a473949c3d9c5</md5>
          <size>49314</size>
          <filedate>1519222085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.5-beta2</name>
      <version>7.x-1.5-beta2</version>
      <tag>7.x-1.5-beta2</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.5-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta2.tar.gz</download_link>
      <date>1500985442</date>
      <mdhash>7ffe7148234f8c237a4893abd82c8e0e</mdhash>
      <filesize>38156</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ffe7148234f8c237a4893abd82c8e0e</md5>
          <size>38156</size>
          <filedate>1500985442</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>405872bcdf5c27bce5e2605bb62ddd40</md5>
          <size>48248</size>
          <filedate>1500985442</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.5-beta1</name>
      <version>7.x-1.5-beta1</version>
      <tag>7.x-1.5-beta1</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.5-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta1.tar.gz</download_link>
      <date>1499171942</date>
      <mdhash>0589fb97c7b74f82e429a34677eb1fb8</mdhash>
      <filesize>37592</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0589fb97c7b74f82e429a34677eb1fb8</md5>
          <size>37592</size>
          <filedate>1499171942</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.5-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>bafbbe7bc85f1b34c5619326c0dbeb83</md5>
          <size>47677</size>
          <filedate>1499171942</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.4.tar.gz</download_link>
      <date>1452830639</date>
      <mdhash>10cb0a7bb75aaa6cd709c88fd7f79e9f</mdhash>
      <filesize>36913</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10cb0a7bb75aaa6cd709c88fd7f79e9f</md5>
          <size>36913</size>
          <filedate>1452830639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa8332fde15fc197dca916c30e0685f4</md5>
          <size>46503</size>
          <filedate>1452830639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.3.tar.gz</download_link>
      <date>1413924828</date>
      <mdhash>cc9ccec6ca028f73cf60f8cab09e5a4d</mdhash>
      <filesize>33988</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cc9ccec6ca028f73cf60f8cab09e5a4d</md5>
          <size>33988</size>
          <filedate>1413924828</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>1645e0664fceb6eb8e3581b0432b6fff</md5>
          <size>42478</size>
          <filedate>1413924828</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.2.tar.gz</download_link>
      <date>1385335704</date>
      <mdhash>100c0b2416d455908a818f0a547e9938</mdhash>
      <filesize>33858</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>100c0b2416d455908a818f0a547e9938</md5>
          <size>33858</size>
          <filedate>1385335704</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>95b3e56e1a6b0d12a23a6102b293594b</md5>
          <size>41335</size>
          <filedate>1385335704</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.1</release_link>
      <download_link/>
      <date>1360444360</date>
      <mdhash>416691004d5a09681869dcd93188238b</mdhash>
      <filesize>33160</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>416691004d5a09681869dcd93188238b</md5>
          <size>33160</size>
          <filedate>1360444360</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>d3b1f866fecf8d8f862d2eb8f2f6daec</md5>
          <size>40789</size>
          <filedate>1360444360</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.0.tar.gz</download_link>
      <date>1319392535</date>
      <mdhash>0b237d7b329c0454f437f5687d607743</mdhash>
      <filesize>31440</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b237d7b329c0454f437f5687d607743</md5>
          <size>31440</size>
          <filedate>1319392535</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>949a98007d24c7ab5cfb674a3e792eb8</md5>
          <size>38374</size>
          <filedate>1319392535</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>link 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.0-beta1.tar.gz</download_link>
      <date>1317011508</date>
      <mdhash>05802cd5a599cbf0b205a084016779a8</mdhash>
      <filesize>30289</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>05802cd5a599cbf0b205a084016779a8</md5>
          <size>30289</size>
          <filedate>1317011508</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>04bb38ae534372f9d0c9674b9ed082cb</md5>
          <size>37179</size>
          <filedate>1317011508</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1297061216</date>
      <mdhash>372ddf2818f9ae6754274b4a8b6b640b</mdhash>
      <filesize>27724</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>372ddf2818f9ae6754274b4a8b6b640b</md5>
          <size>27724</size>
          <filedate>1297061216</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d243ef3ad19cb5f5f20861a78f1c1f9c</md5>
          <size>36960</size>
          <filedate>1297061216</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha-2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.0-alpha2</release_link>
      <download_link/>
      <date>1295189003</date>
      <mdhash>c74d7846f210f1bf4a8f14cc8a2d4dbd</mdhash>
      <filesize>20558</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c74d7846f210f1bf4a8f14cc8a2d4dbd</md5>
          <size>20558</size>
          <filedate>1295189003</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>d8050ecb0cfc61bf835b709649687f53</md5>
          <size>26379</size>
          <filedate>1295189003</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha- releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1291518680</date>
      <mdhash>e9299a741ec5b8ac631f3ba701df7c14</mdhash>
      <filesize>20843</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e9299a741ec5b8ac631f3ba701df7c14</md5>
          <size>20843</size>
          <filedate>1291518680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9454b7f888b96959a8cc68471545b586</md5>
          <size>25831</size>
          <filedate>1293232639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>link 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/link/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/link-7.x-1.x-dev.tar.gz</download_link>
      <date>1725100757</date>
      <mdhash>3290106ada0ebb1f714c8940d80c5f52</mdhash>
      <filesize>56610</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3290106ada0ebb1f714c8940d80c5f52</md5>
          <size>56610</size>
          <filedate>1725100757</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/link-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c4584aa219dbe2ffcf223affbab92445</md5>
          <size>69628</size>
          <filedate>1725100757</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
