<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Multifield</title>
  <short_name>multifield</short_name>
  <dc:creator>dave reid</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/multifield</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>multifield 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1466101935</date>
      <mdhash>d79d78098e58959c7bf4e228499e37ae</mdhash>
      <filesize>31650</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d79d78098e58959c7bf4e228499e37ae</md5>
          <size>31650</size>
          <filedate>1466101935</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>3b4fac8c7edac685d6b7b3accdae8409</md5>
          <size>40909</size>
          <filedate>1466101935</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1464300245</date>
      <mdhash>86ad5f7ac8a16b01b04dd8dd880c0f37</mdhash>
      <filesize>31126</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>86ad5f7ac8a16b01b04dd8dd880c0f37</md5>
          <size>31126</size>
          <filedate>1464300245</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f52f51ecd28e5ac4dab1d36561bab857</md5>
          <size>39050</size>
          <filedate>1464300245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1463091246</date>
      <mdhash>2a072d9eb41b57f09a324c12c6b5d94f</mdhash>
      <filesize>29220</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a072d9eb41b57f09a324c12c6b5d94f</md5>
          <size>29220</size>
          <filedate>1463091246</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>91e3113174b94aabb16fc13da18db0e6</md5>
          <size>36308</size>
          <filedate>1463091246</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1408468128</date>
      <mdhash>bb7fa9c22d4d452174d7c64800767351</mdhash>
      <filesize>26478</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb7fa9c22d4d452174d7c64800767351</md5>
          <size>26478</size>
          <filedate>1408468128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0079c40306b3b3bfe78d3d2e8b5cc3d1</md5>
          <size>32487</size>
          <filedate>1408468128</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable9</name>
      <version>7.x-1.0-unstable9</version>
      <tag>7.x-1.0-unstable9</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable9.tar.gz</download_link>
      <date>1392625106</date>
      <mdhash>6ebe4ba3d010c0f8451a7dd1ffadc8a5</mdhash>
      <filesize>22317</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6ebe4ba3d010c0f8451a7dd1ffadc8a5</md5>
          <size>22317</size>
          <filedate>1392625106</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable9.zip</url>
          <archive_type>zip</archive_type>
          <md5>2fa47614758136e680f1c038fe120820</md5>
          <size>26052</size>
          <filedate>1392625106</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable8</name>
      <version>7.x-1.0-unstable8</version>
      <tag>7.x-1.0-unstable8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable8.tar.gz</download_link>
      <date>1391744305</date>
      <mdhash>6453cdfdb6d16e9c19e9582e7c7baac5</mdhash>
      <filesize>20772</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6453cdfdb6d16e9c19e9582e7c7baac5</md5>
          <size>20772</size>
          <filedate>1391744305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable8.zip</url>
          <archive_type>zip</archive_type>
          <md5>d06080d0b0108bd342e33930dea8df78</md5>
          <size>23638</size>
          <filedate>1391744305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable7</name>
      <version>7.x-1.0-unstable7</version>
      <tag>7.x-1.0-unstable7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable7.tar.gz</download_link>
      <date>1391213905</date>
      <mdhash>fa53dffac75256f8a62c046fdd8ec454</mdhash>
      <filesize>20301</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa53dffac75256f8a62c046fdd8ec454</md5>
          <size>20301</size>
          <filedate>1391213905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable7.zip</url>
          <archive_type>zip</archive_type>
          <md5>55fb78506691ee4e0e150b42f960d90b</md5>
          <size>23131</size>
          <filedate>1391213905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable6</name>
      <version>7.x-1.0-unstable6</version>
      <tag>7.x-1.0-unstable6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable6.tar.gz</download_link>
      <date>1378621913</date>
      <mdhash>c27662061cb84cd95a94a6f65fe6b132</mdhash>
      <filesize>18247</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c27662061cb84cd95a94a6f65fe6b132</md5>
          <size>18247</size>
          <filedate>1378621913</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable6.zip</url>
          <archive_type>zip</archive_type>
          <md5>e477e070a47d169de26769ae262e4387</md5>
          <size>20478</size>
          <filedate>1378621913</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable5</name>
      <version>7.x-1.0-unstable5</version>
      <tag>7.x-1.0-unstable5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable5.tar.gz</download_link>
      <date>1378579940</date>
      <mdhash>edd69b52f23088d6a1a90c5eb505546c</mdhash>
      <filesize>17255</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>edd69b52f23088d6a1a90c5eb505546c</md5>
          <size>17255</size>
          <filedate>1378579940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable5.zip</url>
          <archive_type>zip</archive_type>
          <md5>5bd2c757ea3711ce78b1477bdf8b6411</md5>
          <size>19931</size>
          <filedate>1378579940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable4</name>
      <version>7.x-1.0-unstable4</version>
      <tag>7.x-1.0-unstable4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable4.tar.gz</download_link>
      <date>1375231869</date>
      <mdhash>7ec18dc1cf7279e9938ca1c6d7052416</mdhash>
      <filesize>16421</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ec18dc1cf7279e9938ca1c6d7052416</md5>
          <size>16421</size>
          <filedate>1375231869</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable4.zip</url>
          <archive_type>zip</archive_type>
          <md5>dec6c36c91b09d97e8d5d7c97dfa3267</md5>
          <size>18984</size>
          <filedate>1375231869</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable3</name>
      <version>7.x-1.0-unstable3</version>
      <tag>7.x-1.0-unstable3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable3.tar.gz</download_link>
      <date>1373738768</date>
      <mdhash>89df09eea133e4275f1dbd09d3142787</mdhash>
      <filesize>15578</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89df09eea133e4275f1dbd09d3142787</md5>
          <size>15578</size>
          <filedate>1373738768</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a680cd3657fc33f201a8f8747d3c1a58</md5>
          <size>18136</size>
          <filedate>1373738768</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable2</name>
      <version>7.x-1.0-unstable2</version>
      <tag>7.x-1.0-unstable2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable2.tar.gz</download_link>
      <date>1373522170</date>
      <mdhash>8d10a89ce7c7f8ee22c68399b0379357</mdhash>
      <filesize>14475</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8d10a89ce7c7f8ee22c68399b0379357</md5>
          <size>14475</size>
          <filedate>1373522170</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable2.zip</url>
          <archive_type>zip</archive_type>
          <md5>93f7e23adc1460efc2a6133eaa47ba76</md5>
          <size>16315</size>
          <filedate>1373522170</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.0-unstable1</name>
      <version>7.x-1.0-unstable1</version>
      <tag>7.x-1.0-unstable1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.0-unstable1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable1.tar.gz</download_link>
      <date>1373494568</date>
      <mdhash>46af715b6a04958a827098001920727e</mdhash>
      <filesize>12246</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>46af715b6a04958a827098001920727e</md5>
          <size>12246</size>
          <filedate>1373494568</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.0-unstable1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a084b5ea147670b50cc715c57a611d81</md5>
          <size>14028</size>
          <filedate>1373494568</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>multifield 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield-7.x-1.x-dev.tar.gz</download_link>
      <date>1466118239</date>
      <mdhash>689d5c00de174d046bfd43f73f553034</mdhash>
      <filesize>32704</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>689d5c00de174d046bfd43f73f553034</md5>
          <size>32704</size>
          <filedate>1466118239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d845cfa4d89881f451ab6f632ffc224b</md5>
          <size>42386</size>
          <filedate>1466118239</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
