<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Services</title>
  <short_name>services</short_name>
  <dc:creator>kylebrowning</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/services</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking new maintainer</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>services 7.x-3.29</name>
      <version>7.x-3.29</version>
      <tag>7.x-3.29</tag>
      <version_major>3</version_major>
      <version_patch>29</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.29</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.29.tar.gz</download_link>
      <date>1693524037</date>
      <mdhash>79673d093f1e8586645d3087e5fe708d</mdhash>
      <filesize>110000</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.29.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>79673d093f1e8586645d3087e5fe708d</md5>
          <size>110000</size>
          <filedate>1693524037</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.29.zip</url>
          <archive_type>zip</archive_type>
          <md5>f97bb0680f919b3a948bfb657c3b3f82</md5>
          <size>153776</size>
          <filedate>1693524037</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.28</name>
      <version>7.x-3.28</version>
      <tag>7.x-3.28</tag>
      <version_major>3</version_major>
      <version_patch>28</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.28</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.28.tar.gz</download_link>
      <date>1650563148</date>
      <mdhash>dd9454bf095feee9fdfd30c8954eb5ae</mdhash>
      <filesize>109991</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.28.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dd9454bf095feee9fdfd30c8954eb5ae</md5>
          <size>109991</size>
          <filedate>1650563148</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.28.zip</url>
          <archive_type>zip</archive_type>
          <md5>318aa5bb97eb71a783d6b4b247625f6b</md5>
          <size>153770</size>
          <filedate>1650563148</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.27</name>
      <version>7.x-3.27</version>
      <tag>7.x-3.27</tag>
      <version_major>3</version_major>
      <version_patch>27</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.27</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.27.tar.gz</download_link>
      <date>1591213029</date>
      <mdhash>219071f10e6cd5936f9e59c9c4634f81</mdhash>
      <filesize>109895</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.27.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>219071f10e6cd5936f9e59c9c4634f81</md5>
          <size>109895</size>
          <filedate>1591213029</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.27.zip</url>
          <archive_type>zip</archive_type>
          <md5>722b1da3926b31a10e71d96282b45698</md5>
          <size>153676</size>
          <filedate>1591213029</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.26</name>
      <version>7.x-3.26</version>
      <tag>7.x-3.26</tag>
      <version_major>3</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.26.tar.gz</download_link>
      <date>1591118980</date>
      <mdhash>fb7bd3746d041eb4643414cd925a41f4</mdhash>
      <filesize>109904</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fb7bd3746d041eb4643414cd925a41f4</md5>
          <size>109904</size>
          <filedate>1591118980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>8016a8b8adcd0fc8c1930e1d7b6f754f</md5>
          <size>153680</size>
          <filedate>1591118980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.25</name>
      <version>7.x-3.25</version>
      <tag>7.x-3.25</tag>
      <version_major>3</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.25.tar.gz</download_link>
      <date>1558645086</date>
      <mdhash>f1c16476c9df8c45b209b9165e932379</mdhash>
      <filesize>109841</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f1c16476c9df8c45b209b9165e932379</md5>
          <size>109841</size>
          <filedate>1558645086</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>abae360db0118654086431fa6a4a08a3</md5>
          <size>153612</size>
          <filedate>1558645086</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.24</name>
      <version>7.x-3.24</version>
      <tag>7.x-3.24</tag>
      <version_major>3</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.24.tar.gz</download_link>
      <date>1554313085</date>
      <mdhash>d8aecbfb2a40fb67869afea926b4a66d</mdhash>
      <filesize>109481</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d8aecbfb2a40fb67869afea926b4a66d</md5>
          <size>109481</size>
          <filedate>1554313085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>c1d0c4924e95e695281ebbc3bcbbc5d7</md5>
          <size>153344</size>
          <filedate>1554313085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.23</name>
      <version>7.x-3.23</version>
      <tag>7.x-3.23</tag>
      <version_major>3</version_major>
      <version_patch>23</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.23</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.23.tar.gz</download_link>
      <date>1551302285</date>
      <mdhash>b941343f076d222d4fe877d8aea8951b</mdhash>
      <filesize>109345</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.23.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b941343f076d222d4fe877d8aea8951b</md5>
          <size>109345</size>
          <filedate>1551302285</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.23.zip</url>
          <archive_type>zip</archive_type>
          <md5>ef7d1efa129ad512b6dba8cfb0f27e0c</md5>
          <size>152130</size>
          <filedate>1551302285</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.22</name>
      <version>7.x-3.22</version>
      <tag>7.x-3.22</tag>
      <version_major>3</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.22.tar.gz</download_link>
      <date>1551285485</date>
      <mdhash>dae9fafcdab9aac82e8099988f9abee6</mdhash>
      <filesize>109343</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dae9fafcdab9aac82e8099988f9abee6</md5>
          <size>109343</size>
          <filedate>1551285485</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>c40cf44e6339af0e8189c47b47f54932</md5>
          <size>152132</size>
          <filedate>1551285485</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.21</name>
      <version>7.x-3.21</version>
      <tag>7.x-3.21</tag>
      <version_major>3</version_major>
      <version_patch>21</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.21</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.21.tar.gz</download_link>
      <date>1547922780</date>
      <mdhash>5199120324c82e76f4163eb66c317e69</mdhash>
      <filesize>109201</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.21.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5199120324c82e76f4163eb66c317e69</md5>
          <size>109201</size>
          <filedate>1547922780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.21.zip</url>
          <archive_type>zip</archive_type>
          <md5>d825c537f476a353db3cfc29bed86019</md5>
          <size>151985</size>
          <filedate>1547922780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.20</name>
      <version>7.x-3.20</version>
      <tag>7.x-3.20</tag>
      <version_major>3</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.20.tar.gz</download_link>
      <date>1498574943</date>
      <mdhash>14b44ed8e020f9085e1c0be0dab5386c</mdhash>
      <filesize>109037</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14b44ed8e020f9085e1c0be0dab5386c</md5>
          <size>109037</size>
          <filedate>1498574943</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>42bd042e0dd8be7600d567894ef2ae51</md5>
          <size>151826</size>
          <filedate>1498574943</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.19</name>
      <version>7.x-3.19</version>
      <tag>7.x-3.19</tag>
      <version_major>3</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.19.tar.gz</download_link>
      <date>1488903483</date>
      <mdhash>d240318fdd8c26f7b6ef03268d16b33b</mdhash>
      <filesize>108902</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d240318fdd8c26f7b6ef03268d16b33b</md5>
          <size>108902</size>
          <filedate>1488903483</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>856166bc6783be186394174cf53d181e</md5>
          <size>151649</size>
          <filedate>1488903483</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.18</name>
      <version>7.x-3.18</version>
      <tag>7.x-3.18</tag>
      <version_major>3</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.18.tar.gz</download_link>
      <date>1482949683</date>
      <mdhash>10a39b32ec5529f4e178560ba23a3dc1</mdhash>
      <filesize>108897</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10a39b32ec5529f4e178560ba23a3dc1</md5>
          <size>108897</size>
          <filedate>1482949683</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>c77a296110d0212cbdc45abc031c520f</md5>
          <size>151656</size>
          <filedate>1482949683</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.17</name>
      <version>7.x-3.17</version>
      <tag>7.x-3.17</tag>
      <version_major>3</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.17.tar.gz</download_link>
      <date>1471899239</date>
      <mdhash>412bfc80bc309ca35b36731b39054870</mdhash>
      <filesize>109795</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>412bfc80bc309ca35b36731b39054870</md5>
          <size>109795</size>
          <filedate>1471899239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>e6bd95b6fd45fab64abb5d5a99d9512f</md5>
          <size>151286</size>
          <filedate>1471899239</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.16</name>
      <version>7.x-3.16</version>
      <tag>7.x-3.16</tag>
      <version_major>3</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.16.tar.gz</download_link>
      <date>1470071339</date>
      <mdhash>b84253af59d900478ba7ba9b35a70acc</mdhash>
      <filesize>109627</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b84253af59d900478ba7ba9b35a70acc</md5>
          <size>109627</size>
          <filedate>1470071339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>96a5306bf71e8be4567c26d6292c0094</md5>
          <size>151146</size>
          <filedate>1470071339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.15</name>
      <version>7.x-3.15</version>
      <tag>7.x-3.15</tag>
      <version_major>3</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.15.tar.gz</download_link>
      <date>1467420539</date>
      <mdhash>7673b2a075643e26df8e454b2bc4ab5a</mdhash>
      <filesize>109651</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7673b2a075643e26df8e454b2bc4ab5a</md5>
          <size>109651</size>
          <filedate>1467420539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>c29cff099a61d2eb375982b7fe5d45d2</md5>
          <size>151158</size>
          <filedate>1467420539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.14</name>
      <version>7.x-3.14</version>
      <tag>7.x-3.14</tag>
      <version_major>3</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.14.tar.gz</download_link>
      <date>1457714641</date>
      <mdhash>be6a5963cdc6062641fffde842e13da4</mdhash>
      <filesize>108438</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>be6a5963cdc6062641fffde842e13da4</md5>
          <size>108438</size>
          <filedate>1457714641</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>662e6d6216d7be4e393ab8cac842e9f7</md5>
          <size>150137</size>
          <filedate>1457714641</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.13</name>
      <version>7.x-3.13</version>
      <tag>7.x-3.13</tag>
      <version_major>3</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.13.tar.gz</download_link>
      <date>1453497539</date>
      <mdhash>6b9c768a7a46ccbdefbf9d5b579858cf</mdhash>
      <filesize>107885</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b9c768a7a46ccbdefbf9d5b579858cf</md5>
          <size>107885</size>
          <filedate>1453497539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>c4018d2ca33a5d978a2b13665ff348f0</md5>
          <size>149776</size>
          <filedate>1453497539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.12</name>
      <version>7.x-3.12</version>
      <tag>7.x-3.12</tag>
      <version_major>3</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.12.tar.gz</download_link>
      <date>1429118281</date>
      <mdhash>d005d8cf43773e4fe6096b003b731d94</mdhash>
      <filesize>107270</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d005d8cf43773e4fe6096b003b731d94</md5>
          <size>107270</size>
          <filedate>1429118281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>9aa54f3cdd97def4a012263cdb0b94f3</md5>
          <size>149005</size>
          <filedate>1429118281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.11.tar.gz</download_link>
      <date>1416265377</date>
      <mdhash>328f448fcc535bc6b9c05e1511daf3ea</mdhash>
      <filesize>105346</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>328f448fcc535bc6b9c05e1511daf3ea</md5>
          <size>105346</size>
          <filedate>1416265377</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>c34f1ba685a264b12c0443753bb15a14</md5>
          <size>147259</size>
          <filedate>1416265377</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.10.tar.gz</download_link>
      <date>1411583327</date>
      <mdhash>dfc142f4d1507ef7e9233523da2ab57e</mdhash>
      <filesize>105187</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dfc142f4d1507ef7e9233523da2ab57e</md5>
          <size>105187</size>
          <filedate>1411583327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c4638490353e7417259ed8f2269d228</md5>
          <size>147096</size>
          <filedate>1411583327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.7.tar.gz</download_link>
      <date>1391207905</date>
      <mdhash>f19a7cafb9726e75929d1de550fc4f9b</mdhash>
      <filesize>99191</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f19a7cafb9726e75929d1de550fc4f9b</md5>
          <size>99191</size>
          <filedate>1391207905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>16108175b1e926066f33cb83b01bc1c0</md5>
          <size>138282</size>
          <filedate>1391207905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.6.tar.gz</download_link>
      <date>1390520605</date>
      <mdhash>1b43a1e250b343a04ba96c8239bce1d1</mdhash>
      <filesize>98927</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b43a1e250b343a04ba96c8239bce1d1</md5>
          <size>98927</size>
          <filedate>1390520605</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>2338badb2dde79fc25a3ea4ef8dc74a7</md5>
          <size>137981</size>
          <filedate>1390520605</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.5</release_link>
      <download_link/>
      <date>1375238611</date>
      <mdhash>edc88b7601bffb02084dc27bd1a0f561</mdhash>
      <filesize>93554</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>edc88b7601bffb02084dc27bd1a0f561</md5>
          <size>93554</size>
          <filedate>1375238611</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>c68b42d728cea312072cded9e792f872</md5>
          <size>134549</size>
          <filedate>1375238611</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.4</release_link>
      <download_link/>
      <date>1370415955</date>
      <mdhash>10a37f1a9f08b449882d362c8792f0f2</mdhash>
      <filesize>93211</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>10a37f1a9f08b449882d362c8792f0f2</md5>
          <size>93211</size>
          <filedate>1370415955</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>d9b51d6f3c48fb77e13f85a847310f2e</md5>
          <size>134222</size>
          <filedate>1370415955</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.3</release_link>
      <download_link/>
      <date>1353015205</date>
      <mdhash>3cde72e2711205442c637c058421c51a</mdhash>
      <filesize>88287</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>3cde72e2711205442c637c058421c51a</md5>
          <size>88287</size>
          <filedate>1353015205</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>800a3543ede00f2fa06e808c62bb0321</md5>
          <size>127060</size>
          <filedate>1353015205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.2.tar.gz</download_link>
      <date>1349822238</date>
      <mdhash>f5af300edfb5e9e17312ad6ba78dc12d</mdhash>
      <filesize>87747</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f5af300edfb5e9e17312ad6ba78dc12d</md5>
          <size>87747</size>
          <filedate>1349822238</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>10a70d09a2912b2df47c8e2a3f97ffce</md5>
          <size>126434</size>
          <filedate>1349822238</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.1.tar.gz</download_link>
      <date>1321482643</date>
      <mdhash>dac49d2f3d16e012a4756aaeddaddfc2</mdhash>
      <filesize>76039</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dac49d2f3d16e012a4756aaeddaddfc2</md5>
          <size>76039</size>
          <filedate>1321482643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5f6287dd5d8e0d5236cb593cbc80d68</md5>
          <size>108974</size>
          <filedate>1321482643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0.tar.gz</download_link>
      <date>1317342461</date>
      <mdhash>29110697d535610519346c802c7ce7c8</mdhash>
      <filesize>83473</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>29110697d535610519346c802c7ce7c8</md5>
          <size>83473</size>
          <filedate>1317342461</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>e8adcf4ae3e725f0b6fac25ebb867766</md5>
          <size>116268</size>
          <filedate>1317342461</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc6</name>
      <version>7.x-3.0-rc6</version>
      <tag>7.x-3.0-rc6</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc6.tar.gz</download_link>
      <date>1316460709</date>
      <mdhash>9fb622e75220b3d9b47a8fb771d4ae9d</mdhash>
      <filesize>82807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9fb622e75220b3d9b47a8fb771d4ae9d</md5>
          <size>82807</size>
          <filedate>1316460709</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc6.zip</url>
          <archive_type>zip</archive_type>
          <md5>78809da6a118d6ccfd17bdee94e8ed62</md5>
          <size>115338</size>
          <filedate>1316460709</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc5</name>
      <version>7.x-3.0-rc5</version>
      <tag>7.x-3.0-rc5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc5.tar.gz</download_link>
      <date>1312337821</date>
      <mdhash>c03f5ceac7ff62b7b660df05a26e8fba</mdhash>
      <filesize>81116</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c03f5ceac7ff62b7b660df05a26e8fba</md5>
          <size>81116</size>
          <filedate>1312337821</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea87b6fda34170e6feba48d9b290403d</md5>
          <size>113682</size>
          <filedate>1312337821</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc4</name>
      <version>7.x-3.0-rc4</version>
      <tag>7.x-3.0-rc4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc4.tar.gz</download_link>
      <date>1311888120</date>
      <mdhash>b8629057cb5f3829ff0672c3f78cc7cd</mdhash>
      <filesize>80441</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8629057cb5f3829ff0672c3f78cc7cd</md5>
          <size>80441</size>
          <filedate>1311888120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>fd7c84599c0cca09fcf907acd6843845</md5>
          <size>113032</size>
          <filedate>1311888120</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc3</name>
      <version>7.x-3.0-rc3</version>
      <tag>7.x-3.0-rc3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc3.tar.gz</download_link>
      <date>1304635316</date>
      <mdhash>5ca7fa0ae740171746a157dc8b79b91d</mdhash>
      <filesize>74775</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ca7fa0ae740171746a157dc8b79b91d</md5>
          <size>74775</size>
          <filedate>1304635316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>26f0c176d6bb6e7ae6f4ae61c0a68064</md5>
          <size>104859</size>
          <filedate>1304635316</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc2</name>
      <version>7.x-3.0-rc2</version>
      <tag>7.x-3.0-rc2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc2.tar.gz</download_link>
      <date>1303505817</date>
      <mdhash>0be2d86fcce06250f109cdf194744b82</mdhash>
      <filesize>74345</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0be2d86fcce06250f109cdf194744b82</md5>
          <size>74345</size>
          <filedate>1303505817</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0c134f652d25304301eda8410fa4363f</md5>
          <size>104438</size>
          <filedate>1303505817</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc1.tar.gz</download_link>
      <date>1294432577</date>
      <mdhash>57d95f2b4a2603ecf02c585c556e70b1</mdhash>
      <filesize>62059</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57d95f2b4a2603ecf02c585c556e70b1</md5>
          <size>62059</size>
          <filedate>1294432577</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6fb563c54fc7923a2a26a09fa8347cdf</md5>
          <size>86167</size>
          <filedate>1294432577</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta2.tar.gz</download_link>
      <date>1293213901</date>
      <mdhash>43095ae3fe87be4ceaf2d2193f9652cb</mdhash>
      <filesize>62075</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>43095ae3fe87be4ceaf2d2193f9652cb</md5>
          <size>62075</size>
          <filedate>1293213901</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>216d9aae31deffefc4e91e4089bb5e24</md5>
          <size>85154</size>
          <filedate>1293234441</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta1.tar.gz</download_link>
      <date>1292004315</date>
      <mdhash>1a6898f320e68d82d03f3685ceb9a116</mdhash>
      <filesize>59154</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1a6898f320e68d82d03f3685ceb9a116</md5>
          <size>59154</size>
          <filedate>1292004315</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9977642ae28548c9bd0ea42c10d67a65</md5>
          <size>81288</size>
          <filedate>1293234442</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-3.x-dev.tar.gz</download_link>
      <date>1659383427</date>
      <mdhash>ebfcd9f8b068a4dc1304e2833762db70</mdhash>
      <filesize>109999</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ebfcd9f8b068a4dc1304e2833762db70</md5>
          <size>109999</size>
          <filedate>1659383427</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>47ee7a414c9bb2887fab60f988c66218</md5>
          <size>153805</size>
          <filedate>1659383427</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>services 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/services/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/services-7.x-1.x-dev.tar.gz</download_link>
      <date>1380627299</date>
      <mdhash>8406e0a32ff3870601f068e864482626</mdhash>
      <filesize>36315</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8406e0a32ff3870601f068e864482626</md5>
          <size>36315</size>
          <filedate>1380627299</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/services-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>22f114d09449b3e77d6e20744fb9affa</md5>
          <size>57504</size>
          <filedate>1380627299</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
