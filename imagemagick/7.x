<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>ImageMagick</title>
  <short_name>imagemagick</short_name>
  <dc:creator>sun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/imagemagick</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>imagemagick 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagemagick/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0.tar.gz</download_link>
      <date>1362244512</date>
      <mdhash>dc93720ce4e4c7948f074acf4d551987</mdhash>
      <filesize>15558</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc93720ce4e4c7948f074acf4d551987</md5>
          <size>15558</size>
          <filedate>1362244512</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>2dab021f065d2320132681e68ebece0c</md5>
          <size>18746</size>
          <filedate>1362244512</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagemagick 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagemagick/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1308791216</date>
      <mdhash>895a12add47155a970efa8bb901d143e</mdhash>
      <filesize>14564</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>895a12add47155a970efa8bb901d143e</md5>
          <size>14564</size>
          <filedate>1308791216</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>69f246181dc3102384b2b381784c4f5e</md5>
          <size>18777</size>
          <filedate>1308791216</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imagemagick 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagemagick/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1296428660</date>
      <mdhash>ee21380b600bc22fb25c238bbafe0fff</mdhash>
      <filesize>12116</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee21380b600bc22fb25c238bbafe0fff</md5>
          <size>12116</size>
          <filedate>1296428660</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>25bf09e63f30f06489c09d32fed95960</md5>
          <size>15404</size>
          <filedate>1296428660</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>imagemagick 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagemagick/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.x-dev.tar.gz</download_link>
      <date>1380582831</date>
      <mdhash>c32862e9ad7cd15961bd55a71965df10</mdhash>
      <filesize>15573</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c32862e9ad7cd15961bd55a71965df10</md5>
          <size>15573</size>
          <filedate>1380582831</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagemagick-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ae67821797810e15e808de04b2a2379e</md5>
          <size>18757</size>
          <filedate>1380582831</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
