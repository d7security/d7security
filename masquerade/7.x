<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Masquerade</title>
  <short_name>masquerade</short_name>
  <dc:creator>andypost</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/masquerade</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>masquerade 7.x-1.0-rc7</name>
      <version>7.x-1.0-rc7</version>
      <tag>7.x-1.0-rc7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc7.tar.gz</download_link>
      <date>1394333605</date>
      <mdhash>43b804a3302461c1105dfd08594c877e</mdhash>
      <filesize>16841</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>43b804a3302461c1105dfd08594c877e</md5>
          <size>16841</size>
          <filedate>1394333605</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc7.zip</url>
          <archive_type>zip</archive_type>
          <md5>b43e64f71552a86854c611f73aaaa4aa</md5>
          <size>18357</size>
          <filedate>1394333605</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc6</name>
      <version>7.x-1.0-rc6</version>
      <tag>7.x-1.0-rc6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc6.tar.gz</download_link>
      <date>1393981705</date>
      <mdhash>36f2edc1963416c042ba3412330367a6</mdhash>
      <filesize>16541</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>36f2edc1963416c042ba3412330367a6</md5>
          <size>16541</size>
          <filedate>1393981705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc6.zip</url>
          <archive_type>zip</archive_type>
          <md5>43f29f690046d8c3ca8efd8594c269d9</md5>
          <size>17991</size>
          <filedate>1393981705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc5</name>
      <version>7.x-1.0-rc5</version>
      <tag>7.x-1.0-rc5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc5.tar.gz</download_link>
      <date>1351786623</date>
      <mdhash>a404a7e7a5ead67c7f9331a478a1714e</mdhash>
      <filesize>16180</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a404a7e7a5ead67c7f9331a478a1714e</md5>
          <size>16180</size>
          <filedate>1351786623</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ecc00bef113608d5041de7a663502738</md5>
          <size>17720</size>
          <filedate>1351786623</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc4</name>
      <version>7.x-1.0-rc4</version>
      <tag>7.x-1.0-rc4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc4.tar.gz</download_link>
      <date>1316220424</date>
      <mdhash>b1a2d5b3ba612852d5b9d8a9e691f4eb</mdhash>
      <filesize>14832</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b1a2d5b3ba612852d5b9d8a9e691f4eb</md5>
          <size>14832</size>
          <filedate>1316220424</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc323756f784453dd6d382703bc1c4bc</md5>
          <size>16481</size>
          <filedate>1316220424</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc3.tar.gz</download_link>
      <date>1300939868</date>
      <mdhash>1128354d20f61c0f6bd96450784f8cf4</mdhash>
      <filesize>14164</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1128354d20f61c0f6bd96450784f8cf4</md5>
          <size>14164</size>
          <filedate>1300939868</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>80da21df2847dff2fe80025e1fe7e4ca</md5>
          <size>15439</size>
          <filedate>1300939868</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc2.tar.gz</download_link>
      <date>1296450365</date>
      <mdhash>e4233459f3f61e176cba1419fafd2639</mdhash>
      <filesize>14241</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4233459f3f61e176cba1419fafd2639</md5>
          <size>14241</size>
          <filedate>1296450365</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c6d3fd31a6c4ff6f88043daf43411be</md5>
          <size>15888</size>
          <filedate>1296450365</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc1.tar.gz</download_link>
      <date>1293198402</date>
      <mdhash>5263d08d254ce945b453cad39c0ce33f</mdhash>
      <filesize>14219</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5263d08d254ce945b453cad39c0ce33f</md5>
          <size>14219</size>
          <filedate>1293198402</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>741f3427da8fe91ad9f6cca23cf29559</md5>
          <size>15780</size>
          <filedate>1293232820</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>masquerade 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/masquerade/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/masquerade-7.x-1.x-dev.tar.gz</download_link>
      <date>1478117643</date>
      <mdhash>ae0778f17f10205790e78840120dbf39</mdhash>
      <filesize>16782</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ae0778f17f10205790e78840120dbf39</md5>
          <size>16782</size>
          <filedate>1478117643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/masquerade-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab4982554cf369d929d5f8a6eda80e29</md5>
          <size>18485</size>
          <filedate>1478117643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
