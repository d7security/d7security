<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>GMap Module</title>
  <short_name>gmap</short_name>
  <dc:creator>bdragon</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/gmap</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>gmap 7.x-2.12</name>
      <version>7.x-2.12</version>
      <tag>7.x-2.12</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.12.tar.gz</download_link>
      <date>1669033328</date>
      <mdhash>eeb3b40d61aa0b8a6ff4439dc4f1a6bb</mdhash>
      <filesize>300117</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eeb3b40d61aa0b8a6ff4439dc4f1a6bb</md5>
          <size>300117</size>
          <filedate>1669033328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>57020eb6bb756513fc943301f54075bd</md5>
          <size>402384</size>
          <filedate>1669033328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.11.tar.gz</download_link>
      <date>1457698139</date>
      <mdhash>a457b062ef0f7a8f2ff4caa6e1b4299c</mdhash>
      <filesize>316275</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a457b062ef0f7a8f2ff4caa6e1b4299c</md5>
          <size>316275</size>
          <filedate>1457698139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>10e4608125365c631fc49b583efe085a</md5>
          <size>414336</size>
          <filedate>1457698139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.11-beta1</name>
      <version>7.x-2.11-beta1</version>
      <tag>7.x-2.11-beta1</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.11-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.11-beta1.tar.gz</download_link>
      <date>1452250439</date>
      <mdhash>80102c115dc67944f35adda2b6bbd37c</mdhash>
      <filesize>316209</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.11-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>80102c115dc67944f35adda2b6bbd37c</md5>
          <size>316209</size>
          <filedate>1452250439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.11-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f47f90937cf1c424e51148cf78474070</md5>
          <size>414275</size>
          <filedate>1452250439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10.tar.gz</download_link>
      <date>1441725539</date>
      <mdhash>af2d99281f1782d3f68639323f8d9363</mdhash>
      <filesize>316057</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af2d99281f1782d3f68639323f8d9363</md5>
          <size>316057</size>
          <filedate>1441725539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>75a715d0755b8d6995e70920cc64e4d0</md5>
          <size>414116</size>
          <filedate>1441725539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.10-rc4</name>
      <version>7.x-2.10-rc4</version>
      <tag>7.x-2.10-rc4</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc4.tar.gz</download_link>
      <date>1438698839</date>
      <mdhash>d480f4bef315b6ed97753823d398eeb6</mdhash>
      <filesize>315814</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d480f4bef315b6ed97753823d398eeb6</md5>
          <size>315814</size>
          <filedate>1438698839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>097b7e208eb2fec65ef4dc73666c672d</md5>
          <size>413917</size>
          <filedate>1438698839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.10-rc3</name>
      <version>7.x-2.10-rc3</version>
      <tag>7.x-2.10-rc3</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc3.tar.gz</download_link>
      <date>1423589582</date>
      <mdhash>ee2a7550617d58fab2f1a62ffdb85c57</mdhash>
      <filesize>314722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee2a7550617d58fab2f1a62ffdb85c57</md5>
          <size>314722</size>
          <filedate>1423589582</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bd8db3ccb023aabbf8381376e4ab8594</md5>
          <size>411498</size>
          <filedate>1423589582</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.10-rc2</name>
      <version>7.x-2.10-rc2</version>
      <tag>7.x-2.10-rc2</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc2.tar.gz</download_link>
      <date>1422534481</date>
      <mdhash>76798c294f7249f4a003690c3e23f642</mdhash>
      <filesize>314046</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>76798c294f7249f4a003690c3e23f642</md5>
          <size>314046</size>
          <filedate>1422534481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>918d4aabfa77328f8ac9ece11cb39ac9</md5>
          <size>410432</size>
          <filedate>1422534481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.10-rc1</name>
      <version>7.x-2.10-rc1</version>
      <tag>7.x-2.10-rc1</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc1.tar.gz</download_link>
      <date>1415526480</date>
      <mdhash>5b2a9d7d87bc9fdacb4a7ce9b458bf43</mdhash>
      <filesize>314025</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5b2a9d7d87bc9fdacb4a7ce9b458bf43</md5>
          <size>314025</size>
          <filedate>1415526480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>080b7bbd7c96af71d96e0ae95c689750</md5>
          <size>410429</size>
          <filedate>1415526480</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.10-beta1</name>
      <version>7.x-2.10-beta1</version>
      <tag>7.x-2.10-beta1</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.10-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-beta1.tar.gz</download_link>
      <date>1403364227</date>
      <mdhash>509e9cb9a02f4cc8d33fd54e338d5b5a</mdhash>
      <filesize>314579</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>509e9cb9a02f4cc8d33fd54e338d5b5a</md5>
          <size>314579</size>
          <filedate>1403364227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.10-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>28102b1045e6fb7c9dc19477d855bc39</md5>
          <size>404232</size>
          <filedate>1403364227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.9.tar.gz</download_link>
      <date>1394461105</date>
      <mdhash>a6ee1f4e0fa2b0320a7f990adc558f0d</mdhash>
      <filesize>220797</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a6ee1f4e0fa2b0320a7f990adc558f0d</md5>
          <size>220797</size>
          <filedate>1394461105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>f8c120c9167de60911c8df95e93b9487</md5>
          <size>295350</size>
          <filedate>1394461105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.9-alpha1</name>
      <version>7.x-2.9-alpha1</version>
      <tag>7.x-2.9-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.9-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.9-alpha1.tar.gz</download_link>
      <date>1387385905</date>
      <mdhash>20c771297a83dda561bc9f7a9c29a48e</mdhash>
      <filesize>200559</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.9-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20c771297a83dda561bc9f7a9c29a48e</md5>
          <size>200559</size>
          <filedate>1387385905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.9-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>22763abbe29597e4992375f1a902ab67</md5>
          <size>272241</size>
          <filedate>1387385905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.8</release_link>
      <download_link/>
      <date>1382625633</date>
      <mdhash>b69927c5831df59a0b3ed5a0196e84fb</mdhash>
      <filesize>200069</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>b69927c5831df59a0b3ed5a0196e84fb</md5>
          <size>200069</size>
          <filedate>1382625633</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>4db758010cc9adcdb5dcd792f2a35810</md5>
          <size>267978</size>
          <filedate>1382625634</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.8-beta5</name>
      <version>7.x-2.8-beta5</version>
      <tag>7.x-2.8-beta5</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.8-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta5.tar.gz</download_link>
      <date>1378834925</date>
      <mdhash>485d5644b6c5adb71fd4e61a57bbff4e</mdhash>
      <filesize>200024</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>485d5644b6c5adb71fd4e61a57bbff4e</md5>
          <size>200024</size>
          <filedate>1378834925</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>372e128c6f6c7d01785fe92b17ea161d</md5>
          <size>267945</size>
          <filedate>1378834926</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.8-beta4</name>
      <version>7.x-2.8-beta4</version>
      <tag>7.x-2.8-beta4</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.8-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta4.tar.gz</download_link>
      <date>1378207599</date>
      <mdhash>244e89274d1ca94d941c8b32c1e8330d</mdhash>
      <filesize>199974</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>244e89274d1ca94d941c8b32c1e8330d</md5>
          <size>199974</size>
          <filedate>1378207599</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>64bda3518e23b8f038bf8dfe3a7dd8e0</md5>
          <size>267882</size>
          <filedate>1378207599</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.8-beta2</name>
      <version>7.x-2.8-beta2</version>
      <tag>7.x-2.8-beta2</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.8-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta2.tar.gz</download_link>
      <date>1377527501</date>
      <mdhash>69de6a58a1c14bfbaf6a6eb9bbd22aff</mdhash>
      <filesize>200693</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69de6a58a1c14bfbaf6a6eb9bbd22aff</md5>
          <size>200693</size>
          <filedate>1377527501</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1dde131791c0db77893e6cfd60200f20</md5>
          <size>267853</size>
          <filedate>1377527501</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.8-beta1</name>
      <version>7.x-2.8-beta1</version>
      <tag>7.x-2.8-beta1</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.8-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta1.tar.gz</download_link>
      <date>1376472095</date>
      <mdhash>3f7f08ce759d5fb88019e3926f216e7d</mdhash>
      <filesize>199140</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3f7f08ce759d5fb88019e3926f216e7d</md5>
          <size>199140</size>
          <filedate>1376472095</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.8-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1aa4cd772648fdf5a02069af9cdefe5a</md5>
          <size>266970</size>
          <filedate>1376472095</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7.tar.gz</download_link>
      <date>1371720662</date>
      <mdhash>4da5217ee037fda67b7ad80f96edb692</mdhash>
      <filesize>198666</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4da5217ee037fda67b7ad80f96edb692</md5>
          <size>198666</size>
          <filedate>1371720662</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>75db839d7aa7a7c20747e0ae6fab2bd3</md5>
          <size>264860</size>
          <filedate>1371720662</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.7-rc1</name>
      <version>7.x-2.7-rc1</version>
      <tag>7.x-2.7-rc1</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-rc1.tar.gz</download_link>
      <date>1368107117</date>
      <mdhash>9cbf9005dbcf9739f56c708702b2c6c0</mdhash>
      <filesize>197712</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9cbf9005dbcf9739f56c708702b2c6c0</md5>
          <size>197712</size>
          <filedate>1368107117</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0abcd329b085234789491b1316e23803</md5>
          <size>263918</size>
          <filedate>1368107117</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.7-beta4</name>
      <version>7.x-2.7-beta4</version>
      <tag>7.x-2.7-beta4</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta4.tar.gz</download_link>
      <date>1367862313</date>
      <mdhash>67ed24981108bcea20174909eb3dc206</mdhash>
      <filesize>197720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>67ed24981108bcea20174909eb3dc206</md5>
          <size>197720</size>
          <filedate>1367862313</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f41b32fe23fbd6f0a5596ec4a31d8bc0</md5>
          <size>263898</size>
          <filedate>1367862313</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.7-beta3</name>
      <version>7.x-2.7-beta3</version>
      <tag>7.x-2.7-beta3</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta3.tar.gz</download_link>
      <date>1367760316</date>
      <mdhash>4721815d4571c203c1f03838ac6d0c73</mdhash>
      <filesize>196731</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4721815d4571c203c1f03838ac6d0c73</md5>
          <size>196731</size>
          <filedate>1367760316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc0b61c678dfa1fe91bd508b59deb889</md5>
          <size>263003</size>
          <filedate>1367760316</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.7-beta2</name>
      <version>7.x-2.7-beta2</version>
      <tag>7.x-2.7-beta2</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta2.tar.gz</download_link>
      <date>1366135867</date>
      <mdhash>a6bf452f9085f4997f9387e5ab57b8e7</mdhash>
      <filesize>192559</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a6bf452f9085f4997f9387e5ab57b8e7</md5>
          <size>192559</size>
          <filedate>1366135867</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4709b1be90c43de4e08103c01657cace</md5>
          <size>258913</size>
          <filedate>1366135867</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.7-beta1</name>
      <version>7.x-2.7-beta1</version>
      <tag>7.x-2.7-beta1</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.7-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta1.tar.gz</download_link>
      <date>1365701712</date>
      <mdhash>fb37c7904e81e1d839bc1c89ffd916d0</mdhash>
      <filesize>191992</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fb37c7904e81e1d839bc1c89ffd916d0</md5>
          <size>191992</size>
          <filedate>1365701712</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.7-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>344b88e9d84603eb829d871e9b1d1ae7</md5>
          <size>258336</size>
          <filedate>1365701712</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.6.tar.gz</download_link>
      <date>1364754012</date>
      <mdhash>672ebeaa5e6ab5a19bdbe8b7ed8f6abf</mdhash>
      <filesize>191923</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>672ebeaa5e6ab5a19bdbe8b7ed8f6abf</md5>
          <size>191923</size>
          <filedate>1364754012</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>8850c14472721e1d733f7602e690226c</md5>
          <size>258235</size>
          <filedate>1364754012</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5.tar.gz</download_link>
      <date>1364409312</date>
      <mdhash>aa57eaa702860e13b9ed16f260e95b67</mdhash>
      <filesize>181745</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aa57eaa702860e13b9ed16f260e95b67</md5>
          <size>181745</size>
          <filedate>1364409312</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>b743ad6d2e9b3edaf72e4799c701c915</md5>
          <size>240864</size>
          <filedate>1364409312</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-beta3</name>
      <version>7.x-2.5-beta3</version>
      <tag>7.x-2.5-beta3</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta3.tar.gz</download_link>
      <date>1364382917</date>
      <mdhash>56fd3ec7bf63d016102a709a53a0d2cb</mdhash>
      <filesize>191946</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>56fd3ec7bf63d016102a709a53a0d2cb</md5>
          <size>191946</size>
          <filedate>1364382917</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>67b79af9e88a9449c0bb7e6222428e6b</md5>
          <size>258262</size>
          <filedate>1364382917</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-beta2</name>
      <version>7.x-2.5-beta2</version>
      <tag>7.x-2.5-beta2</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta2.tar.gz</download_link>
      <date>1364322612</date>
      <mdhash>a92c4da24ba4ce4f90026774f0b491d7</mdhash>
      <filesize>191974</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a92c4da24ba4ce4f90026774f0b491d7</md5>
          <size>191974</size>
          <filedate>1364322612</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e0cdc6ed03278b6356572aef329b5bcd</md5>
          <size>258297</size>
          <filedate>1364322612</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-beta1</name>
      <version>7.x-2.5-beta1</version>
      <tag>7.x-2.5-beta1</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta1.tar.gz</download_link>
      <date>1363700266</date>
      <mdhash>98b88991d866f6695fc23ee108a153da</mdhash>
      <filesize>191828</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>98b88991d866f6695fc23ee108a153da</md5>
          <size>191828</size>
          <filedate>1363700266</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>891ec078d44ff05d501125c907e573b9</md5>
          <size>258100</size>
          <filedate>1363700266</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-alpha4</name>
      <version>7.x-2.5-alpha4</version>
      <tag>7.x-2.5-alpha4</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha4.tar.gz</download_link>
      <date>1363181711</date>
      <mdhash>6bdb36c3f721b1ba78df14775e46ff14</mdhash>
      <filesize>191809</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6bdb36c3f721b1ba78df14775e46ff14</md5>
          <size>191809</size>
          <filedate>1363181711</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>39224da887b28af950437631b4466053</md5>
          <size>258104</size>
          <filedate>1363181712</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-alpha3</name>
      <version>7.x-2.5-alpha3</version>
      <tag>7.x-2.5-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha3.tar.gz</download_link>
      <date>1363080018</date>
      <mdhash>1540aae5b23e5d16468a48adf0c2c765</mdhash>
      <filesize>191757</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1540aae5b23e5d16468a48adf0c2c765</md5>
          <size>191757</size>
          <filedate>1363080018</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a12f49b67bedd96e0ead14d483c6b13f</md5>
          <size>258012</size>
          <filedate>1363080018</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-alpha2</name>
      <version>7.x-2.5-alpha2</version>
      <tag>7.x-2.5-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha2.tar.gz</download_link>
      <date>1362909011</date>
      <mdhash>f758f198a914441515888c9bcb8ba4d5</mdhash>
      <filesize>191992</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f758f198a914441515888c9bcb8ba4d5</md5>
          <size>191992</size>
          <filedate>1362909011</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b3707adc81f815d91b8bcff5bb2597d6</md5>
          <size>258138</size>
          <filedate>1362909011</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.5-alpha1</name>
      <version>7.x-2.5-alpha1</version>
      <tag>7.x-2.5-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.5-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha1.tar.gz</download_link>
      <date>1362873620</date>
      <mdhash>bfb4f1292f7a027f529b6e794e2ce1c4</mdhash>
      <filesize>189076</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bfb4f1292f7a027f529b6e794e2ce1c4</md5>
          <size>189076</size>
          <filedate>1362873620</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.5-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a8eb7cb6f66d57101344e140b7a6b9e9</md5>
          <size>250575</size>
          <filedate>1362873620</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.4.tar.gz</download_link>
      <date>1361832031</date>
      <mdhash>c6f951a6677729f98aaa8c5e569db1f6</mdhash>
      <filesize>182739</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c6f951a6677729f98aaa8c5e569db1f6</md5>
          <size>182739</size>
          <filedate>1361832031</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>a8662cfa5bb112e0ccc52e623dc8b0ac</md5>
          <size>242565</size>
          <filedate>1361832032</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.4-beta4</name>
      <version>7.x-2.4-beta4</version>
      <tag>7.x-2.4-beta4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.4-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta4.tar.gz</download_link>
      <date>1361811779</date>
      <mdhash>4e6020e2b6e714aeedb93bbc21016ef1</mdhash>
      <filesize>181927</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e6020e2b6e714aeedb93bbc21016ef1</md5>
          <size>181927</size>
          <filedate>1361811779</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>b7c19311e90abe5213dd04ffa55b1c82</md5>
          <size>241680</size>
          <filedate>1361811779</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.4-beta3</name>
      <version>7.x-2.4-beta3</version>
      <tag>7.x-2.4-beta3</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.4-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta3.tar.gz</download_link>
      <date>1361811784</date>
      <mdhash>d9d6a970e01f28752103aa9e43c22450</mdhash>
      <filesize>181942</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9d6a970e01f28752103aa9e43c22450</md5>
          <size>181942</size>
          <filedate>1361811784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ecf32753c8768da3a5a33d20f5806e8</md5>
          <size>241679</size>
          <filedate>1361811785</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.4-beta2</name>
      <version>7.x-2.4-beta2</version>
      <tag>7.x-2.4-beta2</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.4-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta2.tar.gz</download_link>
      <date>1361811782</date>
      <mdhash>b6d460f0fa41dda996d6428afb0d2df9</mdhash>
      <filesize>180474</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b6d460f0fa41dda996d6428afb0d2df9</md5>
          <size>180474</size>
          <filedate>1361811782</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>5ef46bb3bf3ae86b367d3c0738af0e24</md5>
          <size>239231</size>
          <filedate>1361811782</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.4-beta1</name>
      <version>7.x-2.4-beta1</version>
      <tag>7.x-2.4-beta1</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.4-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta1.tar.gz</download_link>
      <date>1360694706</date>
      <mdhash>e3bc6520c8cb23fc5077027f2b23fa52</mdhash>
      <filesize>180480</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e3bc6520c8cb23fc5077027f2b23fa52</md5>
          <size>180480</size>
          <filedate>1360694706</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.4-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>efcf46ff905d19fecfb37c9d4f359fe0</md5>
          <size>239238</size>
          <filedate>1360694706</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3.tar.gz</download_link>
      <date>1360250306</date>
      <mdhash>b51d90165b35fc8dd1e268330e7e19b6</mdhash>
      <filesize>180336</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b51d90165b35fc8dd1e268330e7e19b6</md5>
          <size>180336</size>
          <filedate>1360250306</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7a863f3e3bc100f658cce163b530449f</md5>
          <size>239074</size>
          <filedate>1360250306</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.3-beta2</name>
      <version>7.x-2.3-beta2</version>
      <tag>7.x-2.3-beta2</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta2.tar.gz</download_link>
      <date>1360248860</date>
      <mdhash>04fa4cd2d0d5e7a848f6b853dca32ee4</mdhash>
      <filesize>180356</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>04fa4cd2d0d5e7a848f6b853dca32ee4</md5>
          <size>180356</size>
          <filedate>1360248860</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a24f5ddd24fe14691f11313259e9ddc1</md5>
          <size>239093</size>
          <filedate>1360248860</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.3-beta1</name>
      <version>7.x-2.3-beta1</version>
      <tag>7.x-2.3-beta1</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta1.tar.gz</download_link>
      <date>1360245338</date>
      <mdhash>7635495a2c02c4b77e72f6f0a727b4b2</mdhash>
      <filesize>180358</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7635495a2c02c4b77e72f6f0a727b4b2</md5>
          <size>180358</size>
          <filedate>1360245338</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9bd36ccb2eb3026812490e2cefd69169</md5>
          <size>239093</size>
          <filedate>1360245338</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.3-alpha3</name>
      <version>7.x-2.3-alpha3</version>
      <tag>7.x-2.3-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha3.tar.gz</download_link>
      <date>1359654207</date>
      <mdhash>1c16d85fdc03939bc0f997399713445b</mdhash>
      <filesize>180352</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1c16d85fdc03939bc0f997399713445b</md5>
          <size>180352</size>
          <filedate>1359654207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b6452eea84e29ca949dd70b7cee98685</md5>
          <size>239095</size>
          <filedate>1359654207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.3-alpha2</name>
      <version>7.x-2.3-alpha2</version>
      <tag>7.x-2.3-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha2.tar.gz</download_link>
      <date>1352116316</date>
      <mdhash>d8c2c2063f976c9f3ccbbc65b1f38157</mdhash>
      <filesize>180062</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d8c2c2063f976c9f3ccbbc65b1f38157</md5>
          <size>180062</size>
          <filedate>1352116316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4e02bf9ad1bba89007c47a2eb58ebebc</md5>
          <size>238730</size>
          <filedate>1352116316</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.3-alpha1</name>
      <version>7.x-2.3-alpha1</version>
      <tag>7.x-2.3-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.3-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha1.tar.gz</download_link>
      <date>1352115711</date>
      <mdhash>e592ab70ac77b86ba82976f1badeeaec</mdhash>
      <filesize>180070</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e592ab70ac77b86ba82976f1badeeaec</md5>
          <size>180070</size>
          <filedate>1352115711</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.3-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf677fd0463fca23de2b0d1485a28618</md5>
          <size>238728</size>
          <filedate>1352115711</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.2.tar.gz</download_link>
      <date>1351989489</date>
      <mdhash>144f08ebd577066230d0797da4cac43b</mdhash>
      <filesize>179954</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>144f08ebd577066230d0797da4cac43b</md5>
          <size>179954</size>
          <filedate>1351989489</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>20187f46e72a1778aa7b092b40d698bb</md5>
          <size>238627</size>
          <filedate>1351989489</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.1.tar.gz</download_link>
      <date>1351984023</date>
      <mdhash>e7542921c9f3c372e0540f107f13c675</mdhash>
      <filesize>177256</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e7542921c9f3c372e0540f107f13c675</md5>
          <size>177256</size>
          <filedate>1351984023</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c9dd123b1b4ecf58c4e3803a5eb1585</md5>
          <size>235704</size>
          <filedate>1351984023</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>gmap 7.x-1.1-beta2</name>
      <version>7.x-1.1-beta2</version>
      <tag>7.x-1.1-beta2</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.1-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta2.tar.gz</download_link>
      <date>1361811787</date>
      <mdhash>7f35be05121081b921fcfa88b838cf23</mdhash>
      <filesize>180779</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7f35be05121081b921fcfa88b838cf23</md5>
          <size>180779</size>
          <filedate>1361811787</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c649720b40e69234b2fa07e4335ff15a</md5>
          <size>239923</size>
          <filedate>1361811787</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.1-beta1</name>
      <version>7.x-1.1-beta1</version>
      <tag>7.x-1.1-beta1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.1-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta1.tar.gz</download_link>
      <date>1360694709</date>
      <mdhash>84dfdea9f83e7d36bb17a7868117fa61</mdhash>
      <filesize>180764</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>84dfdea9f83e7d36bb17a7868117fa61</md5>
          <size>180764</size>
          <filedate>1360694709</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.1-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb46453dff73508cc74c5ef4311bceca</md5>
          <size>239893</size>
          <filedate>1360694709</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta3.tar.gz</download_link>
      <date>1360248862</date>
      <mdhash>2656da4f7a9c56ede1e2bb1ad717be27</mdhash>
      <filesize>180616</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2656da4f7a9c56ede1e2bb1ad717be27</md5>
          <size>180616</size>
          <filedate>1360248862</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4f2b56873d595b2bb2dc2fca43cc09a</md5>
          <size>239733</size>
          <filedate>1360248862</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta2.tar.gz</download_link>
      <date>1360245333</date>
      <mdhash>f3a5df66256d7305c512b1ab57195a98</mdhash>
      <filesize>180616</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3a5df66256d7305c512b1ab57195a98</md5>
          <size>180616</size>
          <filedate>1360245333</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e0ee6d2139205bcc864d0444c9112e1d</md5>
          <size>239733</size>
          <filedate>1360245333</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta1.tar.gz</download_link>
      <date>1352129815</date>
      <mdhash>f650dd20678b1cdf3862719dda18267d</mdhash>
      <filesize>180360</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f650dd20678b1cdf3862719dda18267d</md5>
          <size>180360</size>
          <filedate>1352129815</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d963bf4b46819eba4162441dc22f5c84</md5>
          <size>239441</size>
          <filedate>1352129815</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1352115113</date>
      <mdhash>14d831a66d83ecb5d80fe9ce67700460</mdhash>
      <filesize>180327</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14d831a66d83ecb5d80fe9ce67700460</md5>
          <size>180327</size>
          <filedate>1352115113</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e8c5fcb5456c17ac125b472d33f3f43b</md5>
          <size>239400</size>
          <filedate>1352115113</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-2.x-dev.tar.gz</download_link>
      <date>1669033209</date>
      <mdhash>c890dd906de829feeef375f6c3c33faf</mdhash>
      <filesize>300143</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c890dd906de829feeef375f6c3c33faf</md5>
          <size>300143</size>
          <filedate>1669033209</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b707aca000b460da5aa9e47c0d86e76e</md5>
          <size>402405</size>
          <filedate>1669033209</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>gmap 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/gmap/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/gmap-7.x-1.x-dev.tar.gz</download_link>
      <date>1380581052</date>
      <mdhash>ec68d07a8f79b4a0d51f88cc1078efb1</mdhash>
      <filesize>181744</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec68d07a8f79b4a0d51f88cc1078efb1</md5>
          <size>181744</size>
          <filedate>1380581052</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/gmap-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ee5b46ebb3e656a00343e124c29160d6</md5>
          <size>240872</size>
          <filedate>1380581053</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
