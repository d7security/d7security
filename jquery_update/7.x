<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>jQuery Update</title>
  <short_name>jquery_update</short_name>
  <dc:creator>jjeff</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/jquery_update</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>jquery_update 7.x-4.1</name>
      <version>7.x-4.1</version>
      <tag>7.x-4.1</tag>
      <version_major>4</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1.tar.gz</download_link>
      <date>1675437622</date>
      <mdhash>15348d52f7828b8366e42d381c66a05f</mdhash>
      <filesize>776866</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>15348d52f7828b8366e42d381c66a05f</md5>
          <size>776866</size>
          <filedate>1675437622</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>87cb2c9e0f12cb43bd6b748eaa69e19d</md5>
          <size>975261</size>
          <filedate>1675437622</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.1-rc1</name>
      <version>7.x-4.1-rc1</version>
      <tag>7.x-4.1-rc1</tag>
      <version_major>4</version_major>
      <version_patch>1</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.1-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1-rc1.tar.gz</download_link>
      <date>1675185957</date>
      <mdhash>94934ca7570d009430ed44fdcfdc73a2</mdhash>
      <filesize>776870</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94934ca7570d009430ed44fdcfdc73a2</md5>
          <size>776870</size>
          <filedate>1675185957</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>74bbf8091b92496e80ac974a99fe9c6e</md5>
          <size>975268</size>
          <filedate>1675185957</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0.tar.gz</download_link>
      <date>1669044747</date>
      <mdhash>f609cde55e14a00a5b793cc46df8f779</mdhash>
      <filesize>775827</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f609cde55e14a00a5b793cc46df8f779</md5>
          <size>775827</size>
          <filedate>1669044747</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b99497f761b4d0a9f6deefe66a6ca88</md5>
          <size>974395</size>
          <filedate>1669044747</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.0-rc2</name>
      <version>7.x-4.0-rc2</version>
      <tag>7.x-4.0-rc2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc2.tar.gz</download_link>
      <date>1667310849</date>
      <mdhash>021d721d48baf7624a28e30a6e1c5c6a</mdhash>
      <filesize>775834</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>021d721d48baf7624a28e30a6e1c5c6a</md5>
          <size>775834</size>
          <filedate>1667310849</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d753e949582b6e1eb01ec209d2f8c00d</md5>
          <size>974403</size>
          <filedate>1667310849</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.0-rc1</name>
      <version>7.x-4.0-rc1</version>
      <tag>7.x-4.0-rc1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc1.tar.gz</download_link>
      <date>1664485128</date>
      <mdhash>d5e20da2ed0a14b8edd71349101dfd37</mdhash>
      <filesize>774136</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d5e20da2ed0a14b8edd71349101dfd37</md5>
          <size>774136</size>
          <filedate>1664485128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>115ab9aeb20c175ac22f7c8530a4c9bf</md5>
          <size>972631</size>
          <filedate>1664485128</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.0-alpha2</name>
      <version>7.x-4.0-alpha2</version>
      <tag>7.x-4.0-alpha2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha2.tar.gz</download_link>
      <date>1659371626</date>
      <mdhash>13e258435940fd026b45e1458b7b7302</mdhash>
      <filesize>1983274</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>13e258435940fd026b45e1458b7b7302</md5>
          <size>1983274</size>
          <filedate>1659371626</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7177080c5961d7eaab0a96ba8efd3341</md5>
          <size>2200724</size>
          <filedate>1659371626</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.0-alpha1</name>
      <version>7.x-4.0-alpha1</version>
      <tag>7.x-4.0-alpha1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha1.tar.gz</download_link>
      <date>1658500929</date>
      <mdhash>35b5a5dca7e422ca3266f7b31985645e</mdhash>
      <filesize>1981466</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35b5a5dca7e422ca3266f7b31985645e</md5>
          <size>1981466</size>
          <filedate>1658500929</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7f8d779df689faad37f4cb04d457f12c</md5>
          <size>2199950</size>
          <filedate>1658500929</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.0-alpha5</name>
      <version>7.x-3.0-alpha5</version>
      <tag>7.x-3.0-alpha5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha5.tar.gz</download_link>
      <date>1490805183</date>
      <mdhash>48baebd87003519ade8c65dc5598bbe5</mdhash>
      <filesize>1980268</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>48baebd87003519ade8c65dc5598bbe5</md5>
          <size>1980268</size>
          <filedate>1490805183</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d82ac139754d483f35c977ccb61e8c9e</md5>
          <size>2197648</size>
          <filedate>1490805183</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.0-alpha4</name>
      <version>7.x-3.0-alpha4</version>
      <tag>7.x-3.0-alpha4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha4.tar.gz</download_link>
      <date>1489024086</date>
      <mdhash>b9cadf50cf17638df514aad3c188c776</mdhash>
      <filesize>1978968</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9cadf50cf17638df514aad3c188c776</md5>
          <size>1978968</size>
          <filedate>1489024086</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>c6ed662bffde7b12557e149d69abbb0d</md5>
          <size>2196619</size>
          <filedate>1489024086</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha3.tar.gz</download_link>
      <date>1445382239</date>
      <mdhash>599739c36c8095b1a268d82a8d5820ac</mdhash>
      <filesize>1463630</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>599739c36c8095b1a268d82a8d5820ac</md5>
          <size>1463630</size>
          <filedate>1445382239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>28a52f8975b7cf1f2cb1cb9daac93e98</md5>
          <size>1666939</size>
          <filedate>1445382239</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1432926481</date>
      <mdhash>99d161a138a599ee9ebd30434479e1ca</mdhash>
      <filesize>1542292</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>99d161a138a599ee9ebd30434479e1ca</md5>
          <size>1542292</size>
          <filedate>1432926481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>13cca0cf12317ad27f428a5ebf185cd7</md5>
          <size>1746791</size>
          <filedate>1432926481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1431975181</date>
      <mdhash>500a98ed3bec0c943994c43913422332</mdhash>
      <filesize>1541898</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>500a98ed3bec0c943994c43913422332</md5>
          <size>1541898</size>
          <filedate>1431975181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>160b014c81bf1bbd9a4a517be91dda00</md5>
          <size>1746831</size>
          <filedate>1431975181</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.7.tar.gz</download_link>
      <date>1445379839</date>
      <mdhash>85eaf02362c384979bc475df30ae7a3e</mdhash>
      <filesize>1135987</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>85eaf02362c384979bc475df30ae7a3e</md5>
          <size>1135987</size>
          <filedate>1445379839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>707b7ff209f608cca276c9dda48b19ec</md5>
          <size>1333153</size>
          <filedate>1445379839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.6.tar.gz</download_link>
      <date>1434549780</date>
      <mdhash>88b81a8defbfdf9a4674f876685543f1</mdhash>
      <filesize>1203578</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>88b81a8defbfdf9a4674f876685543f1</md5>
          <size>1203578</size>
          <filedate>1434549780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>c7862a48b4f8a931b632b6e1163707cc</md5>
          <size>1404203</size>
          <filedate>1434549780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.5.tar.gz</download_link>
      <date>1422221880</date>
      <mdhash>47f430531b0a2a821c791325b2d89388</mdhash>
      <filesize>1201320</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>47f430531b0a2a821c791325b2d89388</md5>
          <size>1201320</size>
          <filedate>1422221880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>6db833c8302d30bbc0f3dd730b69bfbb</md5>
          <size>1402029</size>
          <filedate>1422221880</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.4.tar.gz</download_link>
      <date>1396482245</date>
      <mdhash>9cc4f8b964202f1d6f005a43a08060c6</mdhash>
      <filesize>1199709</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9cc4f8b964202f1d6f005a43a08060c6</md5>
          <size>1199709</size>
          <filedate>1396482245</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>1ce9cf9593af10d447a894907eb74e8a</md5>
          <size>1395393</size>
          <filedate>1396482245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3.tar.gz</download_link>
      <date>1360375905</date>
      <mdhash>2d5a1e594a027fa849ed250f5dfef7c5</mdhash>
      <filesize>709788</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2d5a1e594a027fa849ed250f5dfef7c5</md5>
          <size>709788</size>
          <filedate>1360375905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5d548a973029691ee226ad7d221ef24e</md5>
          <size>877611</size>
          <filedate>1360375905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.3-alpha1</name>
      <version>7.x-2.3-alpha1</version>
      <tag>7.x-2.3-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.3-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3-alpha1.tar.gz</download_link>
      <date>1348790474</date>
      <mdhash>2fe04a30f74e47c0b1a4ce24023a39fa</mdhash>
      <filesize>709779</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2fe04a30f74e47c0b1a4ce24023a39fa</md5>
          <size>709779</size>
          <filedate>1348790474</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.3-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e37d389893b2673902accad5e70cc87</md5>
          <size>877615</size>
          <filedate>1348790474</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.2.tar.gz</download_link>
      <date>1301920269</date>
      <mdhash>944e097382c08d9a3ba5028ac700162b</mdhash>
      <filesize>482023</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>944e097382c08d9a3ba5028ac700162b</md5>
          <size>482023</size>
          <filedate>1301920269</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>993c4c1a3fbc278445ac7b566bb1d357</md5>
          <size>645581</size>
          <filedate>1301920269</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.1.tar.gz</download_link>
      <date>1300647067</date>
      <mdhash>6b6f9064e23aaede7ce9f04ded255a57</mdhash>
      <filesize>479984</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b6f9064e23aaede7ce9f04ded255a57</md5>
          <size>479984</size>
          <filedate>1300647067</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>89fb6c2a7e050967e51629dd342f3ef7</md5>
          <size>643493</size>
          <filedate>1300647067</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.0.tar.gz</download_link>
      <date>1297454705</date>
      <mdhash>de6d8095e0bdd4e231922974df3f8fc7</mdhash>
      <filesize>476204</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>de6d8095e0bdd4e231922974df3f8fc7</md5>
          <size>476204</size>
          <filedate>1297454705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d32794a1b613b068b5bd623dd984aeb</md5>
          <size>639911</size>
          <filedate>1297454705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.0.tar.gz</download_link>
      <date>1297453316</date>
      <mdhash>4fa3749b8e477b76341d72a5516344d8</mdhash>
      <filesize>476208</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4fa3749b8e477b76341d72a5516344d8</md5>
          <size>476208</size>
          <filedate>1297453316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>4a027586d7b8242d1473eaf3988304c1</md5>
          <size>639911</size>
          <filedate>1297453316</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>jquery_update 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.x-dev.tar.gz</download_link>
      <date>1727965657</date>
      <mdhash>f0aabdb7ff558d08696e32bed843be55</mdhash>
      <filesize>778947</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0aabdb7ff558d08696e32bed843be55</md5>
          <size>778947</size>
          <filedate>1727965657</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>dbb537938d4435aa2b5b8e09d5ef2092</md5>
          <size>977508</size>
          <filedate>1727965657</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.x-dev.tar.gz</download_link>
      <date>1549399380</date>
      <mdhash>14ca9f86c076bc0f1312cfaf761a20d8</mdhash>
      <filesize>1980314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14ca9f86c076bc0f1312cfaf761a20d8</md5>
          <size>1980314</size>
          <filedate>1549399380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>44a423106ec6c7d3923122d1a3badce0</md5>
          <size>2197702</size>
          <filedate>1549399380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.x-dev.tar.gz</download_link>
      <date>1445379839</date>
      <mdhash>b4c28c594a6ecf50ccb9cd9689e778c9</mdhash>
      <filesize>1135988</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b4c28c594a6ecf50ccb9cd9689e778c9</md5>
          <size>1135988</size>
          <filedate>1445379839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>3b89a29c545fcf16750ec84993272c94</md5>
          <size>1333158</size>
          <filedate>1445379839</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>jquery_update 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/jquery_update/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.x-dev.tar.gz</download_link>
      <date>1380584081</date>
      <mdhash>9d1b43a6e55ea389f03cd338ab9c5f35</mdhash>
      <filesize>472169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9d1b43a6e55ea389f03cd338ab9c5f35</md5>
          <size>472169</size>
          <filedate>1380584081</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/jquery_update-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>791896e793687bb26a179cead2b8a3e6</md5>
          <size>634862</size>
          <filedate>1380584082</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
