<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Token</title>
  <short_name>menu_token</short_name>
  <dc:creator>DevElCuy</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/menu_token</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_token 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta7.tar.gz</download_link>
      <date>1458066248</date>
      <mdhash>5a5e84dc72d14388b8bad6b552ae73be</mdhash>
      <filesize>14717</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5a5e84dc72d14388b8bad6b552ae73be</md5>
          <size>14717</size>
          <filedate>1458066248</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>36c87fb514553d103d8bd62312366f73</md5>
          <size>17932</size>
          <filedate>1458066248</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta6.tar.gz</download_link>
      <date>1448912940</date>
      <mdhash>9b27dd25176dbaca5fb99df6cd7282dc</mdhash>
      <filesize>14565</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9b27dd25176dbaca5fb99df6cd7282dc</md5>
          <size>14565</size>
          <filedate>1448912940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>a378dc3655b8b999fb31ab31c0323196</md5>
          <size>17744</size>
          <filedate>1448912940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta5.tar.gz</download_link>
      <date>1371232256</date>
      <mdhash>82086784d41f201f922f9f22a7aa9b5c</mdhash>
      <filesize>14366</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>82086784d41f201f922f9f22a7aa9b5c</md5>
          <size>14366</size>
          <filedate>1371232256</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ee6d13616b2f745e770af6d21d2b68ec</md5>
          <size>17353</size>
          <filedate>1371232256</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta4.tar.gz</download_link>
      <date>1359157010</date>
      <mdhash>f5856c9295f6841f6c73ec4c501f0f51</mdhash>
      <filesize>14200</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f5856c9295f6841f6c73ec4c501f0f51</md5>
          <size>14200</size>
          <filedate>1359157010</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ed78710cb7b85d55abc1b2ae36c3c385</md5>
          <size>17181</size>
          <filedate>1359157010</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta3.tar.gz</download_link>
      <date>1353184302</date>
      <mdhash>21ba0d0579cb228719c27d2e25f01427</mdhash>
      <filesize>13924</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>21ba0d0579cb228719c27d2e25f01427</md5>
          <size>13924</size>
          <filedate>1353184302</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b57fd7612d14c52115a85ebabe30b6e7</md5>
          <size>16877</size>
          <filedate>1353184302</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta2.tar.gz</download_link>
      <date>1353166987</date>
      <mdhash>88c585ad14c5bdf41d4a0cbfc7c559c2</mdhash>
      <filesize>13922</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>88c585ad14c5bdf41d4a0cbfc7c559c2</md5>
          <size>13922</size>
          <filedate>1353166987</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f2dd6967d0d72d3032562fbd2e570165</md5>
          <size>19053</size>
          <filedate>1353166987</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta1.tar.gz</download_link>
      <date>1332549048</date>
      <mdhash>4549ad75ad56df9ddb63e5729bafdb52</mdhash>
      <filesize>13667</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4549ad75ad56df9ddb63e5729bafdb52</md5>
          <size>13667</size>
          <filedate>1332549048</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>698a3eb79929147f464298a200903161</md5>
          <size>18790</size>
          <filedate>1332549048</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1316444206</date>
      <mdhash>e6fb0c1472662e66913c62b9a33ed179</mdhash>
      <filesize>13626</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e6fb0c1472662e66913c62b9a33ed179</md5>
          <size>13626</size>
          <filedate>1316444206</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>e56a38627714a377e9aadd180264aab8</md5>
          <size>19572</size>
          <filedate>1316444206</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1307952720</date>
      <mdhash>b7ad33d31e3b98992d16f875ee9af9ca</mdhash>
      <filesize>9304</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b7ad33d31e3b98992d16f875ee9af9ca</md5>
          <size>9304</size>
          <filedate>1307952720</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e908531936fa1f5d24df9b1d2d395799</md5>
          <size>11057</size>
          <filedate>1307952720</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1299796269</date>
      <mdhash>c4a9b152cc4142b206a7e24938316535</mdhash>
      <filesize>8402</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c4a9b152cc4142b206a7e24938316535</md5>
          <size>8402</size>
          <filedate>1299796269</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1415a64b2d791e101a252991c53db5a2</md5>
          <size>9526</size>
          <filedate>1299796269</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_token 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_token/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_token-7.x-1.x-dev.tar.gz</download_link>
      <date>1562626085</date>
      <mdhash>3532f733d91fc88e958275a272f1635e</mdhash>
      <filesize>16771</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3532f733d91fc88e958275a272f1635e</md5>
          <size>16771</size>
          <filedate>1562626085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_token-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>bd419f07be3db975511624ef65d56a02</md5>
          <size>21337</size>
          <filedate>1562626085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
