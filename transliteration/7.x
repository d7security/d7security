<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Transliteration</title>
  <short_name>transliteration</short_name>
  <dc:creator>smk-ka</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/transliteration</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site search</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>transliteration 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.3.tar.gz</download_link>
      <date>1707307305</date>
      <mdhash>6bb00f24bff6b1886ebad56c03f7ecbb</mdhash>
      <filesize>107688</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6bb00f24bff6b1886ebad56c03f7ecbb</md5>
          <size>107688</size>
          <filedate>1707307305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>633bcddbe45a9a796515ebe0bc04a93a</md5>
          <size>156494</size>
          <filedate>1707307305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>transliteration 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.2.tar.gz</download_link>
      <date>1395079431</date>
      <mdhash>c85087a13d2d2a6f17f0fec6dd73e8ee</mdhash>
      <filesize>113037</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c85087a13d2d2a6f17f0fec6dd73e8ee</md5>
          <size>113037</size>
          <filedate>1395079431</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>915345446394cc14b6cc60973f7a8d53</md5>
          <size>151781</size>
          <filedate>1395079431</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>transliteration 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.1.tar.gz</download_link>
      <date>1338540714</date>
      <mdhash>7f611858e3950c1c1dbab98e3e2793f5</mdhash>
      <filesize>111249</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7f611858e3950c1c1dbab98e3e2793f5</md5>
          <size>111249</size>
          <filedate>1338540714</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a621328da4a6180c55a367f04814534d</md5>
          <size>150696</size>
          <filedate>1338540714</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>transliteration 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0.tar.gz</download_link>
      <date>1318580200</date>
      <mdhash>b20b323557afeeeb1c29aad7e572a3d4</mdhash>
      <filesize>110304</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b20b323557afeeeb1c29aad7e572a3d4</md5>
          <size>110304</size>
          <filedate>1318580200</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>44760f80773a7f728f3def248329aa0b</md5>
          <size>149741</size>
          <filedate>1318580200</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>transliteration 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1279288508</date>
      <mdhash>1671ec32e605986cf98bb1bc953346ec</mdhash>
      <filesize>107543</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1671ec32e605986cf98bb1bc953346ec</md5>
          <size>107543</size>
          <filedate>1279288508</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d4f91ddd8c5877e8d24376fd75079ce</md5>
          <size>164113</size>
          <filedate>1293235068</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>transliteration 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/transliteration/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/transliteration-7.x-3.x-dev.tar.gz</download_link>
      <date>1664295360</date>
      <mdhash>12bf55cbe6ca5681c7aa2f4c3a2e7699</mdhash>
      <filesize>107688</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>12bf55cbe6ca5681c7aa2f4c3a2e7699</md5>
          <size>107688</size>
          <filedate>1664295360</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/transliteration-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>87590c682b102373653ce28981d893b3</md5>
          <size>156504</size>
          <filedate>1664295360</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
