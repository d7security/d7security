<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Entityreference backreference</title>
  <short_name>entityreference_backreference</short_name>
  <dc:creator>jsacksick</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/entityreference_backreference</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>entityreference_backreference 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_backreference/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta3.tar.gz</download_link>
      <date>1428667081</date>
      <mdhash>34bf42498ff13b9fa8003240d97dfac4</mdhash>
      <filesize>8180</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>34bf42498ff13b9fa8003240d97dfac4</md5>
          <size>8180</size>
          <filedate>1428667081</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9ce54955f5066b13fe99b3bd4a7cd5b6</md5>
          <size>8924</size>
          <filedate>1428667081</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>entityreference_backreference 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_backreference/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta2.tar.gz</download_link>
      <date>1406722427</date>
      <mdhash>e07cbdc555a91c49c44c241255b4dbda</mdhash>
      <filesize>8064</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e07cbdc555a91c49c44c241255b4dbda</md5>
          <size>8064</size>
          <filedate>1406722427</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0948b9de9d6e2482deace7b52adc1ef9</md5>
          <size>8714</size>
          <filedate>1406722427</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>entityreference_backreference 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_backreference/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta1.tar.gz</download_link>
      <date>1406718528</date>
      <mdhash>afa6e7d84e5ef2ccced4c61d46d13589</mdhash>
      <filesize>8050</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>afa6e7d84e5ef2ccced4c61d46d13589</md5>
          <size>8050</size>
          <filedate>1406718528</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5ec185398bb4537d1001f8c9c57ce9d</md5>
          <size>8702</size>
          <filedate>1406718528</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>entityreference_backreference 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/entityreference_backreference/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.x-dev.tar.gz</download_link>
      <date>1428666781</date>
      <mdhash>07d2240cc30a036b615065e4846ccdb6</mdhash>
      <filesize>8184</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>07d2240cc30a036b615065e4846ccdb6</md5>
          <size>8184</size>
          <filedate>1428666781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/entityreference_backreference-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>7f6e8bc7d76b56db2c0165b01e345329</md5>
          <size>8929</size>
          <filedate>1428666781</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
