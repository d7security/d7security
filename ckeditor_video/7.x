<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>CKEditor Video</title>
  <short_name>ckeditor_video</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ckeditor_video</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ckeditor_video 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_video/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.0-beta1.tar.gz</download_link>
      <date>1505582344</date>
      <mdhash>5f1ae4091cd0c743db6070b4e1379c10</mdhash>
      <filesize>81678</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5f1ae4091cd0c743db6070b4e1379c10</md5>
          <size>81678</size>
          <filedate>1505582344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>682d0125721f42d4fdb4fca69f8dc9af</md5>
          <size>102653</size>
          <filedate>1505582344</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ckeditor_video 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_video/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.x-dev.tar.gz</download_link>
      <date>1505657644</date>
      <mdhash>c6f13907278165c38bce7ef7b1703b59</mdhash>
      <filesize>81655</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c6f13907278165c38bce7ef7b1703b59</md5>
          <size>81655</size>
          <filedate>1505657644</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_video-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba753c145a4956990c30171b285ae165</md5>
          <size>102617</size>
          <filedate>1505657644</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
