<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Ubercart</title>
  <short_name>ubercart</short_name>
  <dc:creator>tr</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ubercart</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>E-commerce</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ubercart 7.x-3.13</name>
      <version>7.x-3.13</version>
      <tag>7.x-3.13</tag>
      <version_major>3</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.13.tar.gz</download_link>
      <date>1569942189</date>
      <mdhash>c0cb2ee62679786db6d35ba784321d89</mdhash>
      <filesize>623043</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c0cb2ee62679786db6d35ba784321d89</md5>
          <size>623043</size>
          <filedate>1569942189</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>7509ef7e15ef802d432b7aa49aa8d687</md5>
          <size>928927</size>
          <filedate>1569942189</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.12</name>
      <version>7.x-3.12</version>
      <tag>7.x-3.12</tag>
      <version_major>3</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.12.tar.gz</download_link>
      <date>1551862385</date>
      <mdhash>e1145c6dc611ba139b3a455242372acb</mdhash>
      <filesize>622670</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e1145c6dc611ba139b3a455242372acb</md5>
          <size>622670</size>
          <filedate>1551862385</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>781dcfa67d6d18ed313c17a3e9324da1</md5>
          <size>928613</size>
          <filedate>1551862385</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.11.tar.gz</download_link>
      <date>1518404583</date>
      <mdhash>f214d520e6b8e190005b857b1c219831</mdhash>
      <filesize>622514</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f214d520e6b8e190005b857b1c219831</md5>
          <size>622514</size>
          <filedate>1518404583</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>03e91924cd7f3f6758631b8a252fdfff</md5>
          <size>928182</size>
          <filedate>1518404583</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.10.tar.gz</download_link>
      <date>1468637039</date>
      <mdhash>7fa7e8418518b3bcb7d039d4d1aef4cf</mdhash>
      <filesize>623990</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7fa7e8418518b3bcb7d039d4d1aef4cf</md5>
          <size>623990</size>
          <filedate>1468637039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>aab994fcb9ea4fd7bfb24d804c24beeb</md5>
          <size>916545</size>
          <filedate>1468637039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.9</name>
      <version>7.x-3.9</version>
      <tag>7.x-3.9</tag>
      <version_major>3</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.9.tar.gz</download_link>
      <date>1449242939</date>
      <mdhash>ebd4debd9ab518b1adcbebc0651b2821</mdhash>
      <filesize>618921</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ebd4debd9ab518b1adcbebc0651b2821</md5>
          <size>618921</size>
          <filedate>1449242939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c52bc44f2b0899241ae14735a81ff47</md5>
          <size>906841</size>
          <filedate>1449242939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.8</name>
      <version>7.x-3.8</version>
      <tag>7.x-3.8</tag>
      <version_major>3</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.8.tar.gz</download_link>
      <date>1413965328</date>
      <mdhash>903cc85c46b6a3ac48084a1e8011dfe3</mdhash>
      <filesize>618222</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>903cc85c46b6a3ac48084a1e8011dfe3</md5>
          <size>618222</size>
          <filedate>1413965328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>b04dbf78a56b29531a9c722b99c6b0ef</md5>
          <size>905562</size>
          <filedate>1413965328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.7.tar.gz</download_link>
      <date>1410347328</date>
      <mdhash>8d574d94768d0ce151ef523137b64fe8</mdhash>
      <filesize>618045</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8d574d94768d0ce151ef523137b64fe8</md5>
          <size>618045</size>
          <filedate>1410347328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>5c8779f93f4ff0bfa505ecc2666e0c98</md5>
          <size>905289</size>
          <filedate>1410347328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.6.tar.gz</download_link>
      <date>1387304005</date>
      <mdhash>027f5917a06eecea4d7aa0357682780b</mdhash>
      <filesize>623607</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>027f5917a06eecea4d7aa0357682780b</md5>
          <size>623607</size>
          <filedate>1387304005</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>f82c3d6ed5479679bd9abed6a43df2c4</md5>
          <size>896527</size>
          <filedate>1387304005</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.5.tar.gz</download_link>
      <date>1373562072</date>
      <mdhash>30072fddd8dd14b9f8fe5b41b7c959d2</mdhash>
      <filesize>635025</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>30072fddd8dd14b9f8fe5b41b7c959d2</md5>
          <size>635025</size>
          <filedate>1373562072</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c9889cd41dcda850d9af096b53e975a</md5>
          <size>910533</size>
          <filedate>1373562072</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.4.tar.gz</download_link>
      <date>1361375074</date>
      <mdhash>41b74941d1cfcbf2a1416612cccd76e6</mdhash>
      <filesize>628712</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>41b74941d1cfcbf2a1416612cccd76e6</md5>
          <size>628712</size>
          <filedate>1361375074</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>5c18ed17c30bf3b3a4875c58513e8568</md5>
          <size>902313</size>
          <filedate>1361375074</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.3.tar.gz</download_link>
      <date>1355247565</date>
      <mdhash>3995c2453144623eb88d67e900a2dcec</mdhash>
      <filesize>625756</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3995c2453144623eb88d67e900a2dcec</md5>
          <size>625756</size>
          <filedate>1355247565</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>50097a455f3bd65d02a33240d18d4bca</md5>
          <size>895774</size>
          <filedate>1355247565</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.2.tar.gz</download_link>
      <date>1347317679</date>
      <mdhash>6236f2d4ff1d21cdd94ca66b04dbf026</mdhash>
      <filesize>613449</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6236f2d4ff1d21cdd94ca66b04dbf026</md5>
          <size>613449</size>
          <filedate>1347317679</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0272976fbe6f8b09f14a295e5e4eab5</md5>
          <size>882149</size>
          <filedate>1347317679</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.1.tar.gz</download_link>
      <date>1335375089</date>
      <mdhash>cf2cf49f7c8e94c2af6ab7b5fa9916b5</mdhash>
      <filesize>609210</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cf2cf49f7c8e94c2af6ab7b5fa9916b5</md5>
          <size>609210</size>
          <filedate>1335375089</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>23f0e9a8d029867c74b3682c6b33f0b4</md5>
          <size>875476</size>
          <filedate>1335375090</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0.tar.gz</download_link>
      <date>1328128566</date>
      <mdhash>8aed81546a4741c8d74d819622b59f19</mdhash>
      <filesize>603551</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8aed81546a4741c8d74d819622b59f19</md5>
          <size>603551</size>
          <filedate>1328128566</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>47a55ad72dc4172328ae86aef5113acd</md5>
          <size>862759</size>
          <filedate>1328128566</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-rc4</name>
      <version>7.x-3.0-rc4</version>
      <tag>7.x-3.0-rc4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc4.tar.gz</download_link>
      <date>1327337150</date>
      <mdhash>7d0c5d938b126043a4a40b9e73febc27</mdhash>
      <filesize>603627</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d0c5d938b126043a4a40b9e73febc27</md5>
          <size>603627</size>
          <filedate>1327337150</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f4fb7507318d89095eb57a689d2dcdf9</md5>
          <size>862770</size>
          <filedate>1327337151</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-rc3</name>
      <version>7.x-3.0-rc3</version>
      <tag>7.x-3.0-rc3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc3.tar.gz</download_link>
      <date>1323195643</date>
      <mdhash>904a0800ca0ffa211040c41072fcad23</mdhash>
      <filesize>591281</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>904a0800ca0ffa211040c41072fcad23</md5>
          <size>591281</size>
          <filedate>1323195643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>14037d0a3dd71ba08091c65bc094cbc6</md5>
          <size>846875</size>
          <filedate>1323195643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-rc2</name>
      <version>7.x-3.0-rc2</version>
      <tag>7.x-3.0-rc2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc2.tar.gz</download_link>
      <date>1318007808</date>
      <mdhash>6e144580e78d2c975c3221499ee5ec63</mdhash>
      <filesize>582402</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6e144580e78d2c975c3221499ee5ec63</md5>
          <size>582402</size>
          <filedate>1318007808</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e7b86c2f3f31c9cf8ddb9f8c326742fd</md5>
          <size>831520</size>
          <filedate>1318007808</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc1.tar.gz</download_link>
      <date>1317764207</date>
      <mdhash>083daa2223abbe84d339021682473785</mdhash>
      <filesize>582027</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>083daa2223abbe84d339021682473785</md5>
          <size>582027</size>
          <filedate>1317764207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a5628ffb1902e16a8af550d513ef53f7</md5>
          <size>830572</size>
          <filedate>1317764207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-beta4</name>
      <version>7.x-3.0-beta4</version>
      <tag>7.x-3.0-beta4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta4.tar.gz</download_link>
      <date>1311619619</date>
      <mdhash>cf5f46482af76b38cb0e4792ae50b2b0</mdhash>
      <filesize>578935</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cf5f46482af76b38cb0e4792ae50b2b0</md5>
          <size>578935</size>
          <filedate>1311619619</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>d268f9ba1ffc64107db2ac16848cc92d</md5>
          <size>826653</size>
          <filedate>1311619619</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-beta3</name>
      <version>7.x-3.0-beta3</version>
      <tag>7.x-3.0-beta3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta3.tar.gz</download_link>
      <date>1306429923</date>
      <mdhash>129f7dfcb517b635a5f7a5c8c0285903</mdhash>
      <filesize>579000</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>129f7dfcb517b635a5f7a5c8c0285903</md5>
          <size>579000</size>
          <filedate>1306429923</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f45dcc1ba9ccc1350af0d04713383125</md5>
          <size>826608</size>
          <filedate>1306429923</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta2.tar.gz</download_link>
      <date>1299861070</date>
      <mdhash>bfcfd455c2450dd0cf87728381254094</mdhash>
      <filesize>566356</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bfcfd455c2450dd0cf87728381254094</md5>
          <size>566356</size>
          <filedate>1299861070</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0ab5ff85d00e50c2a57df1a673ed0279</md5>
          <size>814621</size>
          <filedate>1299861070</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta1.tar.gz</download_link>
      <date>1291922445</date>
      <mdhash>86ef09f0c7b5b43c7fad892d7765e258</mdhash>
      <filesize>544732</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>86ef09f0c7b5b43c7fad892d7765e258</md5>
          <size>544732</size>
          <filedate>1291922445</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>eebfb176c9a53fb67f77ad267d28f645</md5>
          <size>734177</size>
          <filedate>1293235127</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha3.tar.gz</download_link>
      <date>1279312213</date>
      <mdhash>784a4bcd9ef2b5dfcf74360072ebb4b6</mdhash>
      <filesize>523429</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>784a4bcd9ef2b5dfcf74360072ebb4b6</md5>
          <size>523429</size>
          <filedate>1279312213</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>40b562501a73af6837ae21f347ee0d1f</md5>
          <size>704804</size>
          <filedate>1293235130</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1276284013</date>
      <mdhash>83e295da058eedcd073a6849f5e0c24c</mdhash>
      <filesize>539631</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83e295da058eedcd073a6849f5e0c24c</md5>
          <size>539631</size>
          <filedate>1276284013</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3e660cb48bef254af68eb9e25a8f853b</md5>
          <size>719072</size>
          <filedate>1293235117</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1271258109</date>
      <mdhash>595a852868dc64dd448215aaddaa0277</mdhash>
      <filesize>546015</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>595a852868dc64dd448215aaddaa0277</md5>
          <size>546015</size>
          <filedate>1271258109</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f3efa8ff81831d54486d92d54a957592</md5>
          <size>725829</size>
          <filedate>1293235131</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ubercart 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ubercart/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ubercart-7.x-3.x-dev.tar.gz</download_link>
      <date>1669673360</date>
      <mdhash>819e2f9a02515f4cd1786231c06f7907</mdhash>
      <filesize>623192</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>819e2f9a02515f4cd1786231c06f7907</md5>
          <size>623192</size>
          <filedate>1669673360</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ubercart-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d59443a298bbc7905fcb89fc35b66d2</md5>
          <size>929130</size>
          <filedate>1669673360</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
