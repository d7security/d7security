<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Universally Unique IDentifier</title>
  <short_name>uuid</short_name>
  <dc:creator>recidive</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/uuid</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>uuid 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/uuid/-/releases/7.x-1.4</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66133300/packages/generic/uuid/7.x-1.4/uuid-7.x-1.4.tar.gz</download_link>
      <date>1736853550</date>
      <mdhash>f54d8208c7c9563271ca070c045120be</mdhash>
      <filesize>38822</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66133300/packages/generic/uuid/7.x-1.4/uuid-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f54d8208c7c9563271ca070c045120be</md5>
          <size>38822</size>
          <filedate>1736853550</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66133300/packages/generic/uuid/7.x-1.4/uuid-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>5d1167533b0b256c895122d125c90652</md5>
          <size>50796</size>
          <filedate>1736853550</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>uuid 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.3.tar.gz</download_link>
      <date>1559150885</date>
      <mdhash>2645ca9ea69e1ac3034f6b9270730279</mdhash>
      <filesize>37704</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2645ca9ea69e1ac3034f6b9270730279</md5>
          <size>37704</size>
          <filedate>1559150885</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b637dab36f016b666d1b515b36c9d8f6</md5>
          <size>50538</size>
          <filedate>1559150885</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>uuid 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.2.tar.gz</download_link>
      <date>1531990681</date>
      <mdhash>c17ab6dec4b16ea5526f6f3f98d31376</mdhash>
      <filesize>36779</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c17ab6dec4b16ea5526f6f3f98d31376</md5>
          <size>36779</size>
          <filedate>1531990681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b23eb71bf45412ac6db0b7366f219b9a</md5>
          <size>48491</size>
          <filedate>1531990681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>uuid 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.1.tar.gz</download_link>
      <date>1530614920</date>
      <mdhash>48130150c1c7918c0eed588747d88806</mdhash>
      <filesize>36356</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>48130150c1c7918c0eed588747d88806</md5>
          <size>36356</size>
          <filedate>1530614920</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7b86d52452c445b42e77b16ae885b7d8</md5>
          <size>47662</size>
          <filedate>1530614920</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>uuid 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0.tar.gz</download_link>
      <date>1493291943</date>
      <mdhash>8b0e255654fe243d05c2d9b40253d37b</mdhash>
      <filesize>35318</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b0e255654fe243d05c2d9b40253d37b</md5>
          <size>35318</size>
          <filedate>1493291943</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8bf4f10d59445d584370f9ab65c1842c</md5>
          <size>45957</size>
          <filedate>1493291943</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta2.tar.gz</download_link>
      <date>1470153539</date>
      <mdhash>e8cd70170717c647a6164302ae613796</mdhash>
      <filesize>35760</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e8cd70170717c647a6164302ae613796</md5>
          <size>35760</size>
          <filedate>1470153539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7b6f719d3609a17bd5702c23eeb7e55b</md5>
          <size>45973</size>
          <filedate>1470153539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta1.tar.gz</download_link>
      <date>1444230839</date>
      <mdhash>8568d4d4b6738d492e9ac1598a0ebfc9</mdhash>
      <filesize>32766</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8568d4d4b6738d492e9ac1598a0ebfc9</md5>
          <size>32766</size>
          <filedate>1444230839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9326897cd5f9052e3f9545aab54731b3</md5>
          <size>42677</size>
          <filedate>1444230839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha6</name>
      <version>7.x-1.0-alpha6</version>
      <tag>7.x-1.0-alpha6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha6.tar.gz</download_link>
      <date>1411454808</date>
      <mdhash>629083e006338bede48093d2ed38a34f</mdhash>
      <filesize>32543</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>629083e006338bede48093d2ed38a34f</md5>
          <size>32543</size>
          <filedate>1411454808</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>dcfc1bdb3c101737b49e22bd36f01a5a</md5>
          <size>42931</size>
          <filedate>1411454808</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha5</release_link>
      <download_link/>
      <date>1373620283</date>
      <mdhash>91c830af4e0cc9f1af96045d35abe2f8</mdhash>
      <filesize>32971</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>91c830af4e0cc9f1af96045d35abe2f8</md5>
          <size>32971</size>
          <filedate>1373620283</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>b9cd6e5eb275896122c0b5b59b0a9098</md5>
          <size>43218</size>
          <filedate>1373620283</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1366984230</date>
      <mdhash>d86b882f9b9a7781b793451ab4312c31</mdhash>
      <filesize>32864</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d86b882f9b9a7781b793451ab4312c31</md5>
          <size>32864</size>
          <filedate>1366984230</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>23d07344bc7cdac777fd2322e306215c</md5>
          <size>43115</size>
          <filedate>1366984230</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha3</release_link>
      <download_link/>
      <date>1328017553</date>
      <mdhash>98e2129672b862c18b1f7ee7d0d77514</mdhash>
      <filesize>24904</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>98e2129672b862c18b1f7ee7d0d77514</md5>
          <size>24904</size>
          <filedate>1328017553</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>bce5ed42acee7f2be325d24f745e80fd</md5>
          <size>32123</size>
          <filedate>1328017553</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha2</release_link>
      <download_link/>
      <date>1313890923</date>
      <mdhash>2c167186753964124297efd73084a7dc</mdhash>
      <filesize>19618</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>2c167186753964124297efd73084a7dc</md5>
          <size>19618</size>
          <filedate>1313890923</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>bea9fea8581e821df1a7635d62f86e6e</md5>
          <size>26016</size>
          <filedate>1313890923</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1303997817</date>
      <mdhash>539f913961442ccd6574f2799c65b797</mdhash>
      <filesize>17751</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>539f913961442ccd6574f2799c65b797</md5>
          <size>17751</size>
          <filedate>1303997817</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e6c98153ad92feb5077d97608616f47d</md5>
          <size>31974</size>
          <filedate>1303997817</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>uuid 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/uuid/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.tar.gz</download_link>
      <date>1559213288</date>
      <mdhash>b959a2441df9fb566833b6324dc90756</mdhash>
      <filesize>37746</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b959a2441df9fb566833b6324dc90756</md5>
          <size>37746</size>
          <filedate>1559213288</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>24a499901e1a8d9cda3f5e2de19729de</md5>
          <size>50597</size>
          <filedate>1559213288</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
