<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Redirect</title>
  <short_name>redirect</short_name>
  <dc:creator>dave reid</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/redirect</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>redirect 7.x-1.0-rc4</name>
      <version>7.x-1.0-rc4</version>
      <tag>7.x-1.0-rc4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc4.tar.gz</download_link>
      <date>1615230905</date>
      <mdhash>79ff21333ef73d8759925447c07aeb8c</mdhash>
      <filesize>45996</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>79ff21333ef73d8759925447c07aeb8c</md5>
          <size>45996</size>
          <filedate>1615230905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>a2b514274533209e4970df4192b7e48d</md5>
          <size>56534</size>
          <filedate>1615230905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc3.tar.gz</download_link>
      <date>1436393339</date>
      <mdhash>8c91175510a0fa97a04d9360a25f8b91</mdhash>
      <filesize>40592</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8c91175510a0fa97a04d9360a25f8b91</md5>
          <size>40592</size>
          <filedate>1436393339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>407dd58a5e8c964b1687232045f7dad4</md5>
          <size>50032</size>
          <filedate>1436393339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc2.tar.gz</download_link>
      <date>1435607280</date>
      <mdhash>ce853d01b20d499c497c5c47c54bcf3b</mdhash>
      <filesize>40634</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce853d01b20d499c497c5c47c54bcf3b</md5>
          <size>40634</size>
          <filedate>1435607280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>75800e27f2b21b814a4aaffc1e41fdaf</md5>
          <size>50074</size>
          <filedate>1435607280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc1.tar.gz</download_link>
      <date>1347989995</date>
      <mdhash>e6663d34d310877c935558c7cb69cd04</mdhash>
      <filesize>38322</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e6663d34d310877c935558c7cb69cd04</md5>
          <size>38322</size>
          <filedate>1347989995</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0cc55733dd693377d6ecc92bde3f071</md5>
          <size>47328</size>
          <filedate>1347989995</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta4.tar.gz</download_link>
      <date>1323364843</date>
      <mdhash>0edd0c2c8db8efed9522360c109384c1</mdhash>
      <filesize>36331</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0edd0c2c8db8efed9522360c109384c1</md5>
          <size>36331</size>
          <filedate>1323364843</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>33b3176c7b53f02a25256a5a08868174</md5>
          <size>44762</size>
          <filedate>1323364843</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta3.tar.gz</download_link>
      <date>1304713016</date>
      <mdhash>2712ea3c39bf56334c069d1535bae9a2</mdhash>
      <filesize>33968</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2712ea3c39bf56334c069d1535bae9a2</md5>
          <size>33968</size>
          <filedate>1304713016</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>89d1a0e97074f65fe8225204a790b864</md5>
          <size>42416</size>
          <filedate>1304713016</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta2.tar.gz</download_link>
      <date>1302713216</date>
      <mdhash>c819b99b79add9d59a3370923af54e50</mdhash>
      <filesize>30190</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c819b99b79add9d59a3370923af54e50</md5>
          <size>30190</size>
          <filedate>1302713216</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd78396233021d09eed8aba89b0d3e38</md5>
          <size>37749</size>
          <filedate>1302713216</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta1.tar.gz</download_link>
      <date>1292361669</date>
      <mdhash>0e5d45d456139f0e805a45a404ee6051</mdhash>
      <filesize>27925</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0e5d45d456139f0e805a45a404ee6051</md5>
          <size>27925</size>
          <filedate>1292361669</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c506e559c165b1936dfe6a94bcced269</md5>
          <size>34915</size>
          <filedate>1293234234</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1289065848</date>
      <mdhash>9b1af969dc1611b1d0f83acd4404b5fe</mdhash>
      <filesize>27717</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9b1af969dc1611b1d0f83acd4404b5fe</md5>
          <size>27717</size>
          <filedate>1289065848</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e193f0a76d93184fabac080d417565f</md5>
          <size>34685</size>
          <filedate>1293234235</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1286222487</date>
      <mdhash>f47c4a6b44f0fcce7e1b556bc0a1a7dc</mdhash>
      <filesize>27663</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f47c4a6b44f0fcce7e1b556bc0a1a7dc</md5>
          <size>27663</size>
          <filedate>1286222487</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b5a50396c34bd51892564da8cf8c689</md5>
          <size>34662</size>
          <filedate>1293234234</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-2.x-dev.tar.gz</download_link>
      <date>1580065085</date>
      <mdhash>7edbcc0f7d25fa1b0a5e134a113dc9f8</mdhash>
      <filesize>45864</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7edbcc0f7d25fa1b0a5e134a113dc9f8</md5>
          <size>45864</size>
          <filedate>1580065085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>d990f3cb6b448bc9767b66ef4620ace6</md5>
          <size>56513</size>
          <filedate>1580065085</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redirect-7.x-1.x</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redirect/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redirect-7.x-1.x-dev.tar.gz</download_link>
      <date>1588807107</date>
      <mdhash>c0764663274f55b0db2a98e3c98203dd</mdhash>
      <filesize>45992</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c0764663274f55b0db2a98e3c98203dd</md5>
          <size>45992</size>
          <filedate>1588807107</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redirect-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a19a03157914aefe5efb627188ac030d</md5>
          <size>56542</size>
          <filedate>1588807107</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
