<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Google Analytics</title>
  <short_name>google_analytics</short_name>
  <dc:creator>budda</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/google_analytics</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Obsolete</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>google_analytics 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.8.tar.gz</download_link>
      <date>1681835715</date>
      <mdhash>c5bee23c20b28d21883139495a936d08</mdhash>
      <filesize>44431</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c5bee23c20b28d21883139495a936d08</md5>
          <size>44431</size>
          <filedate>1681835715</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>33a89cc11b6089d4e76b2fdd6d0ac232</md5>
          <size>50806</size>
          <filedate>1681835715</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.7.tar.gz</download_link>
      <date>1678735207</date>
      <mdhash>e5ed86ec967acd9fa908f90a13a8cd58</mdhash>
      <filesize>44428</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e5ed86ec967acd9fa908f90a13a8cd58</md5>
          <size>44428</size>
          <filedate>1678735207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>5bb25dd41d787e71ffd34049c9855e75</md5>
          <size>50821</size>
          <filedate>1678735207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.6.tar.gz</download_link>
      <date>1548968580</date>
      <mdhash>9b49876903972b83f366d84ce68f4dcf</mdhash>
      <filesize>42257</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9b49876903972b83f366d84ce68f4dcf</md5>
          <size>42257</size>
          <filedate>1548968580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>44247cf16c42ba00ed432da75492c8e8</md5>
          <size>49462</size>
          <filedate>1548968580</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.5.tar.gz</download_link>
      <date>1531469021</date>
      <mdhash>ac8ed0a62c4eab66545e1cb0637592ca</mdhash>
      <filesize>42180</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac8ed0a62c4eab66545e1cb0637592ca</md5>
          <size>42180</size>
          <filedate>1531469021</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>f320489326839990b3743c59a8920963</md5>
          <size>49400</size>
          <filedate>1531469021</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.4.tar.gz</download_link>
      <date>1506373144</date>
      <mdhash>fd67aeb5326527633a355a5336561eb0</mdhash>
      <filesize>42121</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd67aeb5326527633a355a5336561eb0</md5>
          <size>42121</size>
          <filedate>1506373144</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>88171db8943274b74b6e952c24a4aeda</md5>
          <size>49339</size>
          <filedate>1506373144</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.3.tar.gz</download_link>
      <date>1470779939</date>
      <mdhash>35a7f33c1afd643aebc32920b60fea20</mdhash>
      <filesize>41886</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35a7f33c1afd643aebc32920b60fea20</md5>
          <size>41886</size>
          <filedate>1470779939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ca44f44cf7bc27c93cc0c4c93341da58</md5>
          <size>48641</size>
          <filedate>1470779939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.2.tar.gz</download_link>
      <date>1461443639</date>
      <mdhash>7df37200cabd676bbb9e7d69a36875c7</mdhash>
      <filesize>41100</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7df37200cabd676bbb9e7d69a36875c7</md5>
          <size>41100</size>
          <filedate>1461443639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>801cd3c5fde484645975266a71cb6090</md5>
          <size>47909</size>
          <filedate>1461443639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.1.tar.gz</download_link>
      <date>1417276980</date>
      <mdhash>b02a7af5e305517b1f76ead2e4cd434e</mdhash>
      <filesize>39648</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b02a7af5e305517b1f76ead2e4cd434e</md5>
          <size>39648</size>
          <filedate>1417276980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b23f5fa7f1105990a22a8efba64f87f7</md5>
          <size>46925</size>
          <filedate>1417276980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.0.tar.gz</download_link>
      <date>1404257627</date>
      <mdhash>0000aff7ce9b43de37b92241e512118e</mdhash>
      <filesize>39487</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0000aff7ce9b43de37b92241e512118e</md5>
          <size>39487</size>
          <filedate>1404257627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>0ccf352ddbb78118288b8bfb85d65373</md5>
          <size>45548</size>
          <filedate>1404257627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.4.tar.gz</download_link>
      <date>1382021586</date>
      <mdhash>41829b341d5c8f85b1b1d446a4b6041f</mdhash>
      <filesize>32198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>41829b341d5c8f85b1b1d446a4b6041f</md5>
          <size>32198</size>
          <filedate>1382021586</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>e5a43a37a9413a327b3f477017c5b8eb</md5>
          <size>37122</size>
          <filedate>1382021586</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.3.tar.gz</download_link>
      <date>1351810914</date>
      <mdhash>45b8d5a485370fd027a63a1b2010ce36</mdhash>
      <filesize>31946</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>45b8d5a485370fd027a63a1b2010ce36</md5>
          <size>31946</size>
          <filedate>1351810914</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>0a0aafca02ee2e754ef3e18cecf02212</md5>
          <size>36860</size>
          <filedate>1351810914</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.2.tar.gz</download_link>
      <date>1301340367</date>
      <mdhash>a3bac699f10fe454f237bc999dbd43bc</mdhash>
      <filesize>28312</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a3bac699f10fe454f237bc999dbd43bc</md5>
          <size>28312</size>
          <filedate>1301340367</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d644130c9d161ebb9a3dcf09d6da653</md5>
          <size>32786</size>
          <filedate>1301340367</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.1.tar.gz</download_link>
      <date>1297103606</date>
      <mdhash>66ddbf256ba8acd4a1c8d2801de1eaa9</mdhash>
      <filesize>27422</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66ddbf256ba8acd4a1c8d2801de1eaa9</md5>
          <size>27422</size>
          <filedate>1297103606</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>101da24fa7fbef9c1815f699a3f6a574</md5>
          <size>32711</size>
          <filedate>1297103606</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.0.tar.gz</download_link>
      <date>1295564196</date>
      <mdhash>b5b4a16d5dd4a180faf6ccd38626e730</mdhash>
      <filesize>24111</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b5b4a16d5dd4a180faf6ccd38626e730</md5>
          <size>24111</size>
          <filedate>1295564196</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7233ebf5e469519bffc9927339fb71c8</md5>
          <size>29078</size>
          <filedate>1295564196</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>google_analytics 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.x-dev.tar.gz</download_link>
      <date>1679510853</date>
      <mdhash>0ed734c02ed54edd539af14a244de1c6</mdhash>
      <filesize>44426</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0ed734c02ed54edd539af14a244de1c6</md5>
          <size>44426</size>
          <filedate>1679510853</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e1436a5c6c8ed965c20446b0a3f805f2</md5>
          <size>50811</size>
          <filedate>1679510853</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>google_analytics 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/google_analytics/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.x-dev.tar.gz</download_link>
      <date>1403951327</date>
      <mdhash>0990ca3b9e04d72c25ac3fd3ba5d25c2</mdhash>
      <filesize>32673</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0990ca3b9e04d72c25ac3fd3ba5d25c2</md5>
          <size>32673</size>
          <filedate>1403951327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/google_analytics-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>90e9cdf0a930d509b5585e61eb9d7f43</md5>
          <size>38172</size>
          <filedate>1403951327</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
