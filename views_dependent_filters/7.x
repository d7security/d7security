<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Dependent Filters</title>
  <short_name>views_dependent_filters</short_name>
  <dc:creator>joachim</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_dependent_filters</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_dependent_filters 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_dependent_filters/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.1.tar.gz</download_link>
      <date>1342186377</date>
      <mdhash>daa62391da6fe457ffccabd0ffa439c8</mdhash>
      <filesize>12902</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>daa62391da6fe457ffccabd0ffa439c8</md5>
          <size>12902</size>
          <filedate>1342186377</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a71212f8d6aede173ac7a3fbfb5dc297</md5>
          <size>14817</size>
          <filedate>1342186377</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_dependent_filters 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_dependent_filters/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.0.tar.gz</download_link>
      <date>1328630152</date>
      <mdhash>80261b8a07f255224dbef1f33286a181</mdhash>
      <filesize>12364</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>80261b8a07f255224dbef1f33286a181</md5>
          <size>12364</size>
          <filedate>1328630152</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>fd57b0d7823161a51729827367eeeaf3</md5>
          <size>14190</size>
          <filedate>1328630152</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_dependent_filters 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_dependent_filters/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.x-dev.tar.gz</download_link>
      <date>1483630442</date>
      <mdhash>39ec5b5164e345171ec0f0208f9eef0a</mdhash>
      <filesize>12977</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>39ec5b5164e345171ec0f0208f9eef0a</md5>
          <size>12977</size>
          <filedate>1483630442</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_dependent_filters-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4289f4e27d35d211f0ded245fa1ea31f</md5>
          <size>15088</size>
          <filedate>1483630442</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
