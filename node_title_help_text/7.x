<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Node title help text</title>
  <short_name>node_title_help_text</short_name>
  <dc:creator>jadhavdevendra</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/node_title_help_text</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>node_title_help_text 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_title_help_text/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.1.tar.gz</download_link>
      <date>1489080488</date>
      <mdhash>e4093427ea0a36645c84073fdc945a75</mdhash>
      <filesize>8113</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4093427ea0a36645c84073fdc945a75</md5>
          <size>8113</size>
          <filedate>1489080488</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>008ee9836c38372bb053fde626c8beca</md5>
          <size>9355</size>
          <filedate>1489080488</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_title_help_text 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_title_help_text/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.0.tar.gz</download_link>
      <date>1424849281</date>
      <mdhash>f9b9df83d8924cd51e5c1ee594b1276a</mdhash>
      <filesize>8101</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f9b9df83d8924cd51e5c1ee594b1276a</md5>
          <size>8101</size>
          <filedate>1424849281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>c86b8e4cd6ab10785f21190635e2dda9</md5>
          <size>9332</size>
          <filedate>1424849281</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_title_help_text 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_title_help_text/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.x-dev.tar.gz</download_link>
      <date>1489079584</date>
      <mdhash>1b671e1ad0857d0c2e3c91c5f559009f</mdhash>
      <filesize>8116</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b671e1ad0857d0c2e3c91c5f559009f</md5>
          <size>8116</size>
          <filedate>1489079584</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_title_help_text-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6af4c7d65506adfefc4091d24347fed8</md5>
          <size>9363</size>
          <filedate>1489079584</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
