<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Two-factor Authentication (TFA)</title>
  <short_name>tfa</short_name>
  <dc:creator>coltrane</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/tfa</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>tfa 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.4.tar.gz</download_link>
      <date>1727879090</date>
      <mdhash>36f2cf73c0ecbbf0345f1565429f1fcb</mdhash>
      <filesize>28720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>36f2cf73c0ecbbf0345f1565429f1fcb</md5>
          <size>28720</size>
          <filedate>1727879090</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7230a97546a4719137d3b0d532ac5c4b</md5>
          <size>36433</size>
          <filedate>1727879090</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.3.tar.gz</download_link>
      <date>1669504624</date>
      <mdhash>5958b4c13cbf599a55c15b58094b1f88</mdhash>
      <filesize>27187</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5958b4c13cbf599a55c15b58094b1f88</md5>
          <size>27187</size>
          <filedate>1669504624</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ce58fff66d40e321667048ceee64008</md5>
          <size>34535</size>
          <filedate>1669504624</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.2.tar.gz</download_link>
      <date>1666300147</date>
      <mdhash>2fe2f86fa9096c2b458935e5bdb996e6</mdhash>
      <filesize>27147</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2fe2f86fa9096c2b458935e5bdb996e6</md5>
          <size>27147</size>
          <filedate>1666300147</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6dff092fe0b182d43b1b5a25e6553323</md5>
          <size>34505</size>
          <filedate>1666300147</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.1.tar.gz</download_link>
      <date>1605786939</date>
      <mdhash>5297945c3fe30c0884c9cf0ed92dc5cd</mdhash>
      <filesize>26942</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5297945c3fe30c0884c9cf0ed92dc5cd</md5>
          <size>26942</size>
          <filedate>1605786939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>05a5ca65478768cac48ad28465d1d88b</md5>
          <size>34272</size>
          <filedate>1605786939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.1-rc1</name>
      <version>7.x-2.1-rc1</version>
      <tag>7.x-2.1-rc1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.1-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.1-rc1.tar.gz</download_link>
      <date>1598117030</date>
      <mdhash>58d8a23f256073a69f63e61ec603b444</mdhash>
      <filesize>26563</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.1-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>58d8a23f256073a69f63e61ec603b444</md5>
          <size>26563</size>
          <filedate>1598117030</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.1-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1256b0f8ab38ec6dc7dc03b51e9d197b</md5>
          <size>33956</size>
          <filedate>1598117030</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.0.tar.gz</download_link>
      <date>1463169068</date>
      <mdhash>a22d0adcd83c2ff164fdb0b5fe164910</mdhash>
      <filesize>24947</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a22d0adcd83c2ff164fdb0b5fe164910</md5>
          <size>24947</size>
          <filedate>1463169068</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>66a9ac8baf05a985d818a29b296ae26d</md5>
          <size>32073</size>
          <filedate>1463169068</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta3.tar.gz</download_link>
      <date>1449761939</date>
      <mdhash>3674fe9b0e55faee37d9c9aab3e05415</mdhash>
      <filesize>24944</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3674fe9b0e55faee37d9c9aab3e05415</md5>
          <size>24944</size>
          <filedate>1449761939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>fdf6aefd69f4b26f48e10c0c1a5ebc6b</md5>
          <size>32080</size>
          <filedate>1449761939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta2.tar.gz</download_link>
      <date>1430776681</date>
      <mdhash>b0a0089ee17ece99d12f7dc96fb3adb2</mdhash>
      <filesize>24912</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b0a0089ee17ece99d12f7dc96fb3adb2</md5>
          <size>24912</size>
          <filedate>1430776681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>839af124fc142eb8fb2c38683ece0cd1</md5>
          <size>32047</size>
          <filedate>1430776681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta1.tar.gz</download_link>
      <date>1410905328</date>
      <mdhash>ee3ba60cff8056c5766cf5925f48530a</mdhash>
      <filesize>23539</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee3ba60cff8056c5766cf5925f48530a</md5>
          <size>23539</size>
          <filedate>1410905328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>66cf8ff317d4428f9d52be74dc291c3b</md5>
          <size>30535</size>
          <filedate>1410905328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-1.0.tar.gz</download_link>
      <date>1333920975</date>
      <mdhash>69ced65e5a188d10f5c9085c5b0c2a53</mdhash>
      <filesize>13516</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69ced65e5a188d10f5c9085c5b0c2a53</md5>
          <size>13516</size>
          <filedate>1333920975</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e218500de826ec31aa383776ea3c191</md5>
          <size>16447</size>
          <filedate>1333920975</filedate>
        </file>
      </files>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-2.x-dev.tar.gz</download_link>
      <date>1727878579</date>
      <mdhash>586467cb8696c4b052e57330b4dcfdbf</mdhash>
      <filesize>28735</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>586467cb8696c4b052e57330b4dcfdbf</md5>
          <size>28735</size>
          <filedate>1727878579</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>74abc5cb554ed2857c9983df9e70797b</md5>
          <size>36443</size>
          <filedate>1727878579</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
    <release>
      <name>tfa 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/tfa/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/tfa-7.x-1.x-dev.tar.gz</download_link>
      <date>1390598905</date>
      <mdhash>9c520e0b61ac02c722326e9229079787</mdhash>
      <filesize>14315</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9c520e0b61ac02c722326e9229079787</md5>
          <size>14315</size>
          <filedate>1390598905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/tfa-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ae9ff5d8936d7599e0f595ea7bfab256</md5>
          <size>17379</size>
          <filedate>1390598905</filedate>
        </file>
      </files>
      <security>Drupal 7 and earlier are not covered by Drupal security advisories!</security>
    </release>
  </releases>
</project>
