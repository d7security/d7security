<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Login Destination</title>
  <short_name>login_destination</short_name>
  <dc:creator>rsvelko</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/login_destination</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Automation</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>login_destination 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.4.tar.gz</download_link>
      <date>1453451939</date>
      <mdhash>dd3ca248c447698a34919bc6ddfe3af4</mdhash>
      <filesize>18117</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dd3ca248c447698a34919bc6ddfe3af4</md5>
          <size>18117</size>
          <filedate>1453451939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ad3d468bba72403b5cd8163eede4167</md5>
          <size>20988</size>
          <filedate>1453451939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>login_destination 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.3.tar.gz</download_link>
      <date>1449658740</date>
      <mdhash>3ec4ba51877829046d0bfdb8b67b1a25</mdhash>
      <filesize>18314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ec4ba51877829046d0bfdb8b67b1a25</md5>
          <size>18314</size>
          <filedate>1449658740</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4cba11b03ca00adb971edd9aaa008cc4</md5>
          <size>21206</size>
          <filedate>1449658740</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>login_destination 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.2.tar.gz</download_link>
      <date>1449647639</date>
      <mdhash>3bb2b0811e81d7b3f2cddf4dde18db93</mdhash>
      <filesize>18309</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3bb2b0811e81d7b3f2cddf4dde18db93</md5>
          <size>18309</size>
          <filedate>1449647639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>cbaf13572afa00d8831c8c0fbc560c60</md5>
          <size>21197</size>
          <filedate>1449647639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>login_destination 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.1.tar.gz</download_link>
      <date>1355853150</date>
      <mdhash>3a2b2bbfad270c363194221dba667a2b</mdhash>
      <filesize>16583</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3a2b2bbfad270c363194221dba667a2b</md5>
          <size>16583</size>
          <filedate>1355853150</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>36c7652102819d810cb09f50f7a98431</md5>
          <size>18970</size>
          <filedate>1355853150</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>login_destination 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0.tar.gz</download_link>
      <date>1313172120</date>
      <mdhash>a86d6cbe60b787b60f44f217e64fb93f</mdhash>
      <filesize>15314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a86d6cbe60b787b60f44f217e64fb93f</md5>
          <size>15314</size>
          <filedate>1313172120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa312a4629a43c26a3790c454761e43e</md5>
          <size>17782</size>
          <filedate>1313172121</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>login_destination 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta2.tar.gz</download_link>
      <date>1305387117</date>
      <mdhash>a57e11b6caa206f3a803d74d4aae37dc</mdhash>
      <filesize>14014</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a57e11b6caa206f3a803d74d4aae37dc</md5>
          <size>14014</size>
          <filedate>1305387117</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>078b94375e0e9a528db56d0b4f9fe939</md5>
          <size>15883</size>
          <filedate>1305387117</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>login_destination 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta1.tar.gz</download_link>
      <date>1301334068</date>
      <mdhash>86086ccb907dda72dcb0f5c9a247af6a</mdhash>
      <filesize>13869</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>86086ccb907dda72dcb0f5c9a247af6a</md5>
          <size>13869</size>
          <filedate>1301334068</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>62cbe3662876403ba6b604b604a42be2</md5>
          <size>15709</size>
          <filedate>1301334068</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>login_destination 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1300225268</date>
      <mdhash>7787dcfe0c6c246763710f51c90b4f47</mdhash>
      <filesize>13130</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7787dcfe0c6c246763710f51c90b4f47</md5>
          <size>13130</size>
          <filedate>1300225268</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>508d747e40ec182c6dcf1cf14562a2f8</md5>
          <size>15028</size>
          <filedate>1300225268</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>login_destination 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/login_destination/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/login_destination-7.x-1.x-dev.tar.gz</download_link>
      <date>1621097919</date>
      <mdhash>6844915c0afcb8a729555af05f35c05b</mdhash>
      <filesize>18860</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6844915c0afcb8a729555af05f35c05b</md5>
          <size>18860</size>
          <filedate>1621097919</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/login_destination-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d88a919727c52fbcadb075e98d28ccd</md5>
          <size>22004</size>
          <filedate>1621097919</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
