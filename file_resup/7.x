<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>File Resumable Upload</title>
  <short_name>file_resup</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/file_resup</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>file_resup 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.5.tar.gz</download_link>
      <date>1525336984</date>
      <mdhash>bf0dbb86bd5cfe92e9e4a39992ee1b5a</mdhash>
      <filesize>20223</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf0dbb86bd5cfe92e9e4a39992ee1b5a</md5>
          <size>20223</size>
          <filedate>1525336984</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>1ae8accd211d546afed2d6771bb159b5</md5>
          <size>23067</size>
          <filedate>1525336984</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.4.tar.gz</download_link>
      <date>1456739339</date>
      <mdhash>2dd48ebe24096bfdf764d0ec01908349</mdhash>
      <filesize>20165</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2dd48ebe24096bfdf764d0ec01908349</md5>
          <size>20165</size>
          <filedate>1456739339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d8bf68b171660c8a8c5d3890f296ce2</md5>
          <size>22919</size>
          <filedate>1456739339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.3.tar.gz</download_link>
      <date>1432043881</date>
      <mdhash>328f739004df59992cbaec71ea08db0d</mdhash>
      <filesize>19599</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>328f739004df59992cbaec71ea08db0d</md5>
          <size>19599</size>
          <filedate>1432043881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c15bc1e9a909fed4d23c9f33609a36f</md5>
          <size>22000</size>
          <filedate>1432043881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.2.tar.gz</download_link>
      <date>1424692381</date>
      <mdhash>d002c7f2d0890721e9f1a56626e5fa15</mdhash>
      <filesize>19501</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d002c7f2d0890721e9f1a56626e5fa15</md5>
          <size>19501</size>
          <filedate>1424692381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7db56c576a254206373921a3d9510dac</md5>
          <size>21899</size>
          <filedate>1424692381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.1.tar.gz</download_link>
      <date>1417456380</date>
      <mdhash>10824019fb21aa34f0edd215a1ffae8d</mdhash>
      <filesize>21114</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10824019fb21aa34f0edd215a1ffae8d</md5>
          <size>21114</size>
          <filedate>1417456380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b9710cfe990a82c50b2604af03e18299</md5>
          <size>24355</size>
          <filedate>1417456380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.0.tar.gz</download_link>
      <date>1413898428</date>
      <mdhash>9426aaa0d623fa45b262651a3883b9cc</mdhash>
      <filesize>19914</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9426aaa0d623fa45b262651a3883b9cc</md5>
          <size>19914</size>
          <filedate>1413898428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>d052b92f17c4cf3a7702feed185ed79e</md5>
          <size>23167</size>
          <filedate>1413898428</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>file_resup 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/file_resup/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/file_resup-7.x-1.x-dev.tar.gz</download_link>
      <date>1525336684</date>
      <mdhash>90d607cfab9020a3b17d8af1d447c937</mdhash>
      <filesize>20229</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>90d607cfab9020a3b17d8af1d447c937</md5>
          <size>20229</size>
          <filedate>1525336684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/file_resup-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>47fca2895bb774d9a3819c8973e2e7e2</md5>
          <size>23071</size>
          <filedate>1525336684</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
