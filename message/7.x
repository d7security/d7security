<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Message</title>
  <short_name>message</short_name>
  <dc:creator>amitaibu</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/message</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>message 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/message/-/releases/7.x-1.13</release_link>
      <download_link>https://gitlab.com/api/v4/projects/54031012/packages/generic/message/7.x-1.13/message-7.x-1.13.tar.gz</download_link>
      <date>1705660048</date>
      <mdhash>d6a3e1ac6a22b60fda8a21af942cfd44</mdhash>
      <filesize>52108</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/54031012/packages/generic/message/7.x-1.13/message-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d6a3e1ac6a22b60fda8a21af942cfd44</md5>
          <size>52108</size>
          <filedate>1705660048</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/54031012/packages/generic/message/7.x-1.13/message-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>5b2360cd1ed70552163aefca73dc3021</md5>
          <size>70707</size>
          <filedate>1705660048</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>message 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.12.tar.gz</download_link>
      <date>1468304039</date>
      <mdhash>bd4c7df7848fea70f00afbcfe76a205d</mdhash>
      <filesize>51254</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bd4c7df7848fea70f00afbcfe76a205d</md5>
          <size>51254</size>
          <filedate>1468304039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>0922c1d63e58943facfc8b378bcd082e</md5>
          <size>69159</size>
          <filedate>1468304039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.11.tar.gz</download_link>
      <date>1450768739</date>
      <mdhash>3a75a8a11f9531b178b79b89e597f4ee</mdhash>
      <filesize>48234</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3a75a8a11f9531b178b79b89e597f4ee</md5>
          <size>48234</size>
          <filedate>1450768739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>e6aed024fbbace7bce56bb57aa4e2ef4</md5>
          <size>62742</size>
          <filedate>1450768739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.10.tar.gz</download_link>
      <date>1429477681</date>
      <mdhash>264cb52ec60f97e1b1c6e033ffd56fe9</mdhash>
      <filesize>48255</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>264cb52ec60f97e1b1c6e033ffd56fe9</md5>
          <size>48255</size>
          <filedate>1429477681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ebccb4771c05345dd9daf624bbf3176</md5>
          <size>62736</size>
          <filedate>1429477681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.9.tar.gz</download_link>
      <date>1374541036</date>
      <mdhash>574a29b445f95f8b384fe43a8824294b</mdhash>
      <filesize>45898</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>574a29b445f95f8b384fe43a8824294b</md5>
          <size>45898</size>
          <filedate>1374541036</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>3dac25e1a8298580bcb5bbe9884ecd10</md5>
          <size>60211</size>
          <filedate>1374541036</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.8.tar.gz</download_link>
      <date>1366630873</date>
      <mdhash>d46128d324af8a3477ea162eae0537d9</mdhash>
      <filesize>45686</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d46128d324af8a3477ea162eae0537d9</md5>
          <size>45686</size>
          <filedate>1366630873</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>912601efc461942ddeed5938b23c6c0d</md5>
          <size>59939</size>
          <filedate>1366630873</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.7.tar.gz</download_link>
      <date>1355398352</date>
      <mdhash>7f8cc660445008c3bd016176c54c0dc0</mdhash>
      <filesize>44272</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7f8cc660445008c3bd016176c54c0dc0</md5>
          <size>44272</size>
          <filedate>1355398352</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>c1bc825622530f5f3a85cc3fa01a0c87</md5>
          <size>57962</size>
          <filedate>1355398352</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.6.tar.gz</download_link>
      <date>1352270207</date>
      <mdhash>faac88f17be5ce9df336b4ec099b5560</mdhash>
      <filesize>43926</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>faac88f17be5ce9df336b4ec099b5560</md5>
          <size>43926</size>
          <filedate>1352270207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>c42d84baa767e10d23bf8bc5805a4247</md5>
          <size>57601</size>
          <filedate>1352270207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.5.tar.gz</download_link>
      <date>1344628929</date>
      <mdhash>6b5f5e92d7cf041e2350ec80df0cf504</mdhash>
      <filesize>42358</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b5f5e92d7cf041e2350ec80df0cf504</md5>
          <size>42358</size>
          <filedate>1344628929</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>950d4768fae65967ef93f596fab30612</md5>
          <size>55469</size>
          <filedate>1344628929</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.4.tar.gz</download_link>
      <date>1339963279</date>
      <mdhash>052eeface60e26a82dfe9e2cc66cdcae</mdhash>
      <filesize>40499</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>052eeface60e26a82dfe9e2cc66cdcae</md5>
          <size>40499</size>
          <filedate>1339963279</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4e3d17b5ee6414ec387cbc82dff4161e</md5>
          <size>53432</size>
          <filedate>1339963279</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.3.tar.gz</download_link>
      <date>1334765499</date>
      <mdhash>51f77903393605cf30208a4576585422</mdhash>
      <filesize>38453</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51f77903393605cf30208a4576585422</md5>
          <size>38453</size>
          <filedate>1334765499</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ccdde1630a24410b86646b794639880e</md5>
          <size>49128</size>
          <filedate>1334765499</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.2.tar.gz</download_link>
      <date>1331068847</date>
      <mdhash>177c90c56a2993de66f83d0dc76bd203</mdhash>
      <filesize>26537</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>177c90c56a2993de66f83d0dc76bd203</md5>
          <size>26537</size>
          <filedate>1331068847</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f5a79d6496d7786c8aebd2208a3401b5</md5>
          <size>33596</size>
          <filedate>1331068847</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.1.tar.gz</download_link>
      <date>1320867935</date>
      <mdhash>bb7a79517dc768ce338d38b4f9834c77</mdhash>
      <filesize>20964</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb7a79517dc768ce338d38b4f9834c77</md5>
          <size>20964</size>
          <filedate>1320867935</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8df70b0ec47bf4c2f538710d82f18850</md5>
          <size>26500</size>
          <filedate>1320867935</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.0.tar.gz</download_link>
      <date>1294241763</date>
      <mdhash>4f3bb765c0a8ce23d0bf28992dc52bc0</mdhash>
      <filesize>14472</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4f3bb765c0a8ce23d0bf28992dc52bc0</md5>
          <size>14472</size>
          <filedate>1294241763</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>883a4304f36f101ae4c01a2566d07a34</md5>
          <size>18707</size>
          <filedate>1294241763</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>message 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/message/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/message-7.x-1.x-dev.tar.gz</download_link>
      <date>1466589239</date>
      <mdhash>472b101663af0966a2bd00e02f5898c2</mdhash>
      <filesize>51264</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>472b101663af0966a2bd00e02f5898c2</md5>
          <size>51264</size>
          <filedate>1466589239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/message-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>20511b835b0453756196556c2a1938d1</md5>
          <size>69179</size>
          <filedate>1466589239</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
