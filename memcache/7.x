<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Memcache API and Integration</title>
  <short_name>memcache</short_name>
  <dc:creator>robertdouglass</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/memcache</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>memcache 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/memcache/-/releases/7.x-1.9</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67460653/packages/generic/memcache/7.x-1.9/memcache-7.x-1.9.tar.gz</download_link>
      <date>1740496621</date>
      <mdhash>8c827a598b57dc4f7a2639ec2a2905ef</mdhash>
      <filesize>62711</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67460653/packages/generic/memcache/7.x-1.9/memcache-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8c827a598b57dc4f7a2639ec2a2905ef</md5>
          <size>62711</size>
          <filedate>1740496621</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67460653/packages/generic/memcache/7.x-1.9/memcache-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>68a480acbb6ab561861f1ddbae701ed4</md5>
          <size>70929</size>
          <filedate>1740496621</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.8.tar.gz</download_link>
      <date>1594981706</date>
      <mdhash>1f27e771dfc86c250260930cbe4b33dc</mdhash>
      <filesize>60410</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1f27e771dfc86c250260930cbe4b33dc</md5>
          <size>60410</size>
          <filedate>1594981706</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea6060b04df9ef39942995a59a8e5e07</md5>
          <size>70097</size>
          <filedate>1594981706</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.7.tar.gz</download_link>
      <date>1594876379</date>
      <mdhash>57a6e7c7d547a7b8a0bc4ab5f72c6ac4</mdhash>
      <filesize>60411</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57a6e7c7d547a7b8a0bc4ab5f72c6ac4</md5>
          <size>60411</size>
          <filedate>1594876379</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>39f2ee38b11396b8c6ab144fa98f5d2f</md5>
          <size>70096</size>
          <filedate>1594876379</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.7-rc1</name>
      <version>7.x-1.7-rc1</version>
      <tag>7.x-1.7-rc1</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.7-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-rc1.tar.gz</download_link>
      <date>1593188547</date>
      <mdhash>af6451bf4333660c99de9f4bacbddddb</mdhash>
      <filesize>60408</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af6451bf4333660c99de9f4bacbddddb</md5>
          <size>60408</size>
          <filedate>1593188547</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>da4e710137ec1100640fd0f1bad9687c</md5>
          <size>70107</size>
          <filedate>1593188547</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.7-beta2</name>
      <version>7.x-1.7-beta2</version>
      <tag>7.x-1.7-beta2</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.7-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta2.tar.gz</download_link>
      <date>1593062769</date>
      <mdhash>a98aae7e2660752340ecfcc9b1601f5a</mdhash>
      <filesize>59301</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a98aae7e2660752340ecfcc9b1601f5a</md5>
          <size>59301</size>
          <filedate>1593062769</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>dc8d47aef1ab0cea8f821357c63174a7</md5>
          <size>69095</size>
          <filedate>1593062769</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.7-beta1</name>
      <version>7.x-1.7-beta1</version>
      <tag>7.x-1.7-beta1</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.7-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta1.tar.gz</download_link>
      <date>1551804784</date>
      <mdhash>7cb868af63965b8c8737967a069152f5</mdhash>
      <filesize>55674</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7cb868af63965b8c8737967a069152f5</md5>
          <size>55674</size>
          <filedate>1551804784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.7-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>959c8b965bd2005e2a45bb810ef0bfcf</md5>
          <size>65308</size>
          <filedate>1551804784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.6.tar.gz</download_link>
      <date>1487953983</date>
      <mdhash>05ac969a104a98c90d3317c5c6bf5595</mdhash>
      <filesize>55344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>05ac969a104a98c90d3317c5c6bf5595</md5>
          <size>55344</size>
          <filedate>1487953983</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>a502a7411de6455a1b5f56b4e6ba2172</md5>
          <size>64749</size>
          <filedate>1487953983</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.6-rc3</name>
      <version>7.x-1.6-rc3</version>
      <tag>7.x-1.6-rc3</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.6-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc3.tar.gz</download_link>
      <date>1487346783</date>
      <mdhash>40c1aff4ef958a49b89a7f68e7ad3266</mdhash>
      <filesize>55355</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>40c1aff4ef958a49b89a7f68e7ad3266</md5>
          <size>55355</size>
          <filedate>1487346783</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>60d86b92c5429a1ad3ea1572de6a6a9f</md5>
          <size>64756</size>
          <filedate>1487346783</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.6-rc2</name>
      <version>7.x-1.6-rc2</version>
      <tag>7.x-1.6-rc2</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.6-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc2.tar.gz</download_link>
      <date>1486580583</date>
      <mdhash>5ba50a99b5fe2a3652cad3b71ff3ce09</mdhash>
      <filesize>54892</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ba50a99b5fe2a3652cad3b71ff3ce09</md5>
          <size>54892</size>
          <filedate>1486580583</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0094029bf466e5e926f4dd3c9a8584c</md5>
          <size>64171</size>
          <filedate>1486580583</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.6-rc1</name>
      <version>7.x-1.6-rc1</version>
      <tag>7.x-1.6-rc1</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.6-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc1.tar.gz</download_link>
      <date>1485184383</date>
      <mdhash>b831277498f096e25586f3023d777aa8</mdhash>
      <filesize>54775</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b831277498f096e25586f3023d777aa8</md5>
          <size>54775</size>
          <filedate>1485184383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.6-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f4ca80dac83b847300fc6648f980130b</md5>
          <size>64059</size>
          <filedate>1485184383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.5.tar.gz</download_link>
      <date>1422088380</date>
      <mdhash>3ea99c76b6429f0bbc8f922cd91eb459</mdhash>
      <filesize>56298</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ea99c76b6429f0bbc8f922cd91eb459</md5>
          <size>56298</size>
          <filedate>1422088380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>08871a9fbec3365d2efc2ad5caefc3c4</md5>
          <size>65510</size>
          <filedate>1422088380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.4.tar.gz</download_link>
      <date>1420624380</date>
      <mdhash>00b10402425ab32ddea6e6a2b7b8ef6f</mdhash>
      <filesize>56293</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00b10402425ab32ddea6e6a2b7b8ef6f</md5>
          <size>56293</size>
          <filedate>1420624380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9255a355da3a59661ca4071c9798fdb5</md5>
          <size>65495</size>
          <filedate>1420624380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.4-rc1</name>
      <version>7.x-1.4-rc1</version>
      <tag>7.x-1.4-rc1</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.4-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-rc1.tar.gz</download_link>
      <date>1419841980</date>
      <mdhash>c984fbad477a5b30b3c485acc0eff26d</mdhash>
      <filesize>56233</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c984fbad477a5b30b3c485acc0eff26d</md5>
          <size>56233</size>
          <filedate>1419841980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2cce6694be84cd07b829a60935efd812</md5>
          <size>65426</size>
          <filedate>1419841980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.4-beta1</name>
      <version>7.x-1.4-beta1</version>
      <tag>7.x-1.4-beta1</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.4-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-beta1.tar.gz</download_link>
      <date>1419066180</date>
      <mdhash>3f594b6ef959b72ecdf53da3403fd4cb</mdhash>
      <filesize>55929</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3f594b6ef959b72ecdf53da3403fd4cb</md5>
          <size>55929</size>
          <filedate>1419066180</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.4-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f51de45f5958dc466b232c99ab8733eb</md5>
          <size>65125</size>
          <filedate>1419066180</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.3.tar.gz</download_link>
      <date>1414805628</date>
      <mdhash>20723cad5bbea0b65cf177bac8d128e1</mdhash>
      <filesize>49881</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20723cad5bbea0b65cf177bac8d128e1</md5>
          <size>49881</size>
          <filedate>1414805628</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9176347be677b09bedb905d32977ce43</md5>
          <size>59015</size>
          <filedate>1414805628</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.3-rc1</name>
      <version>7.x-1.3-rc1</version>
      <tag>7.x-1.3-rc1</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.3-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.3-rc1.tar.gz</download_link>
      <date>1407282227</date>
      <mdhash>11e14591c5d572582af003c77001bd4f</mdhash>
      <filesize>49813</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.3-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11e14591c5d572582af003c77001bd4f</md5>
          <size>49813</size>
          <filedate>1407282227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.3-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea031c6cf320937361966ab890867875</md5>
          <size>58595</size>
          <filedate>1407282227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.2.tar.gz</download_link>
      <date>1403043527</date>
      <mdhash>c7dd98e13b421d35621056656e2d0d74</mdhash>
      <filesize>49602</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c7dd98e13b421d35621056656e2d0d74</md5>
          <size>49602</size>
          <filedate>1403043527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>441705c7e3c04f1e5ad959c78d8a928e</md5>
          <size>58320</size>
          <filedate>1403043527</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1.tar.gz</download_link>
      <date>1401836327</date>
      <mdhash>f6316d1917d5e01c662b59a6208375f1</mdhash>
      <filesize>49058</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f6316d1917d5e01c662b59a6208375f1</md5>
          <size>49058</size>
          <filedate>1401836327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3f385a0e307b0699140d227ea0d96b8b</md5>
          <size>57601</size>
          <filedate>1401836327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta6</name>
      <version>7.x-1.1-beta6</version>
      <tag>7.x-1.1-beta6</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta6.tar.gz</download_link>
      <date>1401806033</date>
      <mdhash>1b878cdb460e0e0c10eeb66b4cc20ac8</mdhash>
      <filesize>49052</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b878cdb460e0e0c10eeb66b4cc20ac8</md5>
          <size>49052</size>
          <filedate>1401806033</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>6da5e4644aec45400d7437e4b6afc85c</md5>
          <size>57598</size>
          <filedate>1401806033</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta5</name>
      <version>7.x-1.1-beta5</version>
      <tag>7.x-1.1-beta5</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta5.tar.gz</download_link>
      <date>1394488705</date>
      <mdhash>aea11f3a2a2752d3804cd662659ff152</mdhash>
      <filesize>48610</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aea11f3a2a2752d3804cd662659ff152</md5>
          <size>48610</size>
          <filedate>1394488705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>86b9377fb8369cb08a3e2196f2b9147c</md5>
          <size>57125</size>
          <filedate>1394488705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta4</name>
      <version>7.x-1.1-beta4</version>
      <tag>7.x-1.1-beta4</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta4.tar.gz</download_link>
      <date>1393275506</date>
      <mdhash>776ec866bf949ba5584f5c055e3093b7</mdhash>
      <filesize>47204</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>776ec866bf949ba5584f5c055e3093b7</md5>
          <size>47204</size>
          <filedate>1393275506</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f75f522b097996f6cadf64316c2afc3</md5>
          <size>55724</size>
          <filedate>1393275506</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta3</name>
      <version>7.x-1.1-beta3</version>
      <tag>7.x-1.1-beta3</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta3.tar.gz</download_link>
      <date>1392675505</date>
      <mdhash>46d8b0487226d9e752d19670cfc58ee2</mdhash>
      <filesize>44944</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>46d8b0487226d9e752d19670cfc58ee2</md5>
          <size>44944</size>
          <filedate>1392675505</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>16b9ff01526de06cfcb62fa7a50bbee7</md5>
          <size>53294</size>
          <filedate>1392675505</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta2</name>
      <version>7.x-1.1-beta2</version>
      <tag>7.x-1.1-beta2</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta2.tar.gz</download_link>
      <date>1392417805</date>
      <mdhash>b04a2f19c2dc0ce81ee6c1cbbd7191e5</mdhash>
      <filesize>44372</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b04a2f19c2dc0ce81ee6c1cbbd7191e5</md5>
          <size>44372</size>
          <filedate>1392417805</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>5d6d2e07e4192b35a53ab2d8df5587a2</md5>
          <size>57490</size>
          <filedate>1392417805</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.1-beta1</name>
      <version>7.x-1.1-beta1</version>
      <tag>7.x-1.1-beta1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.1-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta1.tar.gz</download_link>
      <date>1389583705</date>
      <mdhash>3b3801a6d9bd9fa2ed09d766cbd02db0</mdhash>
      <filesize>43427</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3b3801a6d9bd9fa2ed09d766cbd02db0</md5>
          <size>43427</size>
          <filedate>1389583705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.1-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5206a49972f4e2ce492d86fc427af8c9</md5>
          <size>51501</size>
          <filedate>1389583705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0.tar.gz</download_link>
      <date>1326973845</date>
      <mdhash>1963feb483f2af322c2df54430896689</mdhash>
      <filesize>39777</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1963feb483f2af322c2df54430896689</md5>
          <size>39777</size>
          <filedate>1326973845</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>ed03888807aa176ab4904f8eea7c93e5</md5>
          <size>49038</size>
          <filedate>1326973845</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc3.tar.gz</download_link>
      <date>1325917859</date>
      <mdhash>61f3d35a749e94b60253cafbc34fbb15</mdhash>
      <filesize>39774</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61f3d35a749e94b60253cafbc34fbb15</md5>
          <size>39774</size>
          <filedate>1325917859</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>87416169f6337dec826a38ce9fa83b10</md5>
          <size>49045</size>
          <filedate>1325917859</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc2.tar.gz</download_link>
      <date>1321325736</date>
      <mdhash>89f422230737f669c5c710d611f6bf1d</mdhash>
      <filesize>38344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89f422230737f669c5c710d611f6bf1d</md5>
          <size>38344</size>
          <filedate>1321325736</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>676dbc59b6d3a55a7539eed4df7c7589</md5>
          <size>47201</size>
          <filedate>1321325736</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc1.tar.gz</download_link>
      <date>1319732734</date>
      <mdhash>094c3137a705c4286623307ec4b891e8</mdhash>
      <filesize>37791</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>094c3137a705c4286623307ec4b891e8</md5>
          <size>37791</size>
          <filedate>1319732734</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>225753a2cb9cb0a6f30b071a2924b7aa</md5>
          <size>46353</size>
          <filedate>1319732734</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta6.tar.gz</download_link>
      <date>1485183782</date>
      <mdhash>635bac093c4bccd783261f578366a5d3</mdhash>
      <filesize>47678</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>635bac093c4bccd783261f578366a5d3</md5>
          <size>47678</size>
          <filedate>1485183782</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>c68d391733a6546768eb52e38ad39069</md5>
          <size>58029</size>
          <filedate>1485183782</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta4.tar.gz</download_link>
      <date>1306500116</date>
      <mdhash>3fe934ce5a76e6966d9a122ec6efff49</mdhash>
      <filesize>27955</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3fe934ce5a76e6966d9a122ec6efff49</md5>
          <size>27955</size>
          <filedate>1306500116</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b7c865c14125affeffb5c469c5016d0</md5>
          <size>32138</size>
          <filedate>1306500116</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta3.tar.gz</download_link>
      <date>1287443185</date>
      <mdhash>338fd0dd82dac276f776214d17cfee92</mdhash>
      <filesize>30222</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>338fd0dd82dac276f776214d17cfee92</md5>
          <size>30222</size>
          <filedate>1287443185</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>211c3e7f3fdef62f8f59db93f25b2d63</md5>
          <size>35787</size>
          <filedate>1293232908</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta2.tar.gz</download_link>
      <date>1285775462</date>
      <mdhash>38482a5c073f3b8780dccefe8e1c586e</mdhash>
      <filesize>30123</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38482a5c073f3b8780dccefe8e1c586e</md5>
          <size>30123</size>
          <filedate>1285775462</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>463613fb3e59a2d1849fc79c9754073a</md5>
          <size>35700</size>
          <filedate>1293232912</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta1.tar.gz</download_link>
      <date>1272072308</date>
      <mdhash>4972231bd5a4bee528d1dc40a4c7a462</mdhash>
      <filesize>27227</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4972231bd5a4bee528d1dc40a4c7a462</md5>
          <size>27227</size>
          <filedate>1272072308</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c3dad6745e4628c7f695eba2aca1ab9</md5>
          <size>32006</size>
          <filedate>1293232910</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>memcache 7.x-0.2</name>
      <version>7.x-0.2</version>
      <tag>7.x-0.2</tag>
      <version_major>0</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-0.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-0.2.tar.gz</download_link>
      <date>1319473235</date>
      <mdhash>7c1a2abafa43eaa510a8ff8bc7b8c160</mdhash>
      <filesize>37776</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7c1a2abafa43eaa510a8ff8bc7b8c160</md5>
          <size>37776</size>
          <filedate>1319473235</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>277de7491f3c1436ebe708459ad88851</md5>
          <size>46347</size>
          <filedate>1319473235</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-0.1</name>
      <version>7.x-0.1</version>
      <tag>7.x-0.1</tag>
      <version_major>0</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-0.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-0.1.tar.gz</download_link>
      <date>1316745408</date>
      <mdhash>1bc4c902a53deed5600dd97a6357bc6e</mdhash>
      <filesize>36720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1bc4c902a53deed5600dd97a6357bc6e</md5>
          <size>36720</size>
          <filedate>1316745408</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a5ca3b6954e28a98d1037489bc1e3e5d</md5>
          <size>45049</size>
          <filedate>1316745408</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-0.0</name>
      <version>7.x-0.0</version>
      <tag>7.x-0.0</tag>
      <version_major>0</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-0.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-0.0.tar.gz</download_link>
      <date>1311046623</date>
      <mdhash>3ccf5d66079e29a305e623d4ebe88899</mdhash>
      <filesize>31020</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ccf5d66079e29a305e623d4ebe88899</md5>
          <size>31020</size>
          <filedate>1311046623</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-0.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d6552785616fe6f6e1c365809911ed8</md5>
          <size>39441</size>
          <filedate>1311046623</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>memcache 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/memcache/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/memcache-7.x-1.x-dev.tar.gz</download_link>
      <date>1701823226</date>
      <mdhash>4fcf5f3a58b8f3b010612b9aadf9412d</mdhash>
      <filesize>60775</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4fcf5f3a58b8f3b010612b9aadf9412d</md5>
          <size>60775</size>
          <filedate>1701823226</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/memcache-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4667a4cba22ceecfb8f23f5d5e92568c</md5>
          <size>70481</size>
          <filedate>1701823226</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
