<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Lightweight Directory Access Protocol</title>
  <short_name>ldap</short_name>
  <dc:creator>johnbarclay</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/ldap</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access Control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ldap 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/ldap/-/releases/7.x-2.7</release_link>
      <download_link>https://gitlab.com/api/v4/projects/55398792/packages/generic/ldap/7.x-2.7/ldap-7.x-2.7.tar.gz</download_link>
      <date>1709171391</date>
      <mdhash>308dc857e3c73203c346536c20b10af6</mdhash>
      <filesize>382921</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/55398792/packages/generic/ldap/7.x-2.7/ldap-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>308dc857e3c73203c346536c20b10af6</md5>
          <size>382921</size>
          <filedate>1709171391</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/55398792/packages/generic/ldap/7.x-2.7/ldap-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c48a901d704352129589a69ec709a72</md5>
          <size>448344</size>
          <filedate>1709171391</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.6.tar.gz</download_link>
      <date>1617049656</date>
      <mdhash>b07fa59ebed57de2b16670820d578ab0</mdhash>
      <filesize>377911</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b07fa59ebed57de2b16670820d578ab0</md5>
          <size>377911</size>
          <filedate>1617049656</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>a2e928ed22fec57c7bb0a1e7f480696b</md5>
          <size>448466</size>
          <filedate>1617049656</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.5.tar.gz</download_link>
      <date>1555751585</date>
      <mdhash>90a93ec3715c76df7138e5f9426f442e</mdhash>
      <filesize>377786</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>90a93ec3715c76df7138e5f9426f442e</md5>
          <size>377786</size>
          <filedate>1555751585</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>4de1bf9879bd9c9c726f997a7e52b6e1</md5>
          <size>448389</size>
          <filedate>1555751585</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.4.tar.gz</download_link>
      <date>1534201080</date>
      <mdhash>a1150333d90d080219db6dcbfd034e1d</mdhash>
      <filesize>383280</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a1150333d90d080219db6dcbfd034e1d</md5>
          <size>383280</size>
          <filedate>1534201080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>086f5874ccd5826048cbc60d1776108a</md5>
          <size>453702</size>
          <filedate>1534201080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.3.tar.gz</download_link>
      <date>1503841444</date>
      <mdhash>cb7b235060185caf8da03fd5be5b7917</mdhash>
      <filesize>385044</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb7b235060185caf8da03fd5be5b7917</md5>
          <size>385044</size>
          <filedate>1503841444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d53037a6f620033980703009a4e7cb22</md5>
          <size>455363</size>
          <filedate>1503841444</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.2.tar.gz</download_link>
      <date>1496167144</date>
      <mdhash>98ffc824251eaffdd02cc164c90b3c22</mdhash>
      <filesize>390660</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>98ffc824251eaffdd02cc164c90b3c22</md5>
          <size>390660</size>
          <filedate>1496167144</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>927e6ceeaf468630ecadcd3f3ce1616b</md5>
          <size>462567</size>
          <filedate>1496167144</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.1.tar.gz</download_link>
      <date>1495392783</date>
      <mdhash>118dc9585e6bb36c11ee1e14a43b53a3</mdhash>
      <filesize>390664</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>118dc9585e6bb36c11ee1e14a43b53a3</md5>
          <size>390664</size>
          <filedate>1495392783</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8a152f578ca8ab6ae04fd55109f2634a</md5>
          <size>462584</size>
          <filedate>1495392783</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0.tar.gz</download_link>
      <date>1489858383</date>
      <mdhash>1389cf66861460eaa1920e8dc572de15</mdhash>
      <filesize>392559</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1389cf66861460eaa1920e8dc572de15</md5>
          <size>392559</size>
          <filedate>1489858383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>abd583df4c121a7a0b66360a94e64508</md5>
          <size>464537</size>
          <filedate>1489858383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta12</name>
      <version>7.x-2.0-beta12</version>
      <tag>7.x-2.0-beta12</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta12</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta12.tar.gz</download_link>
      <date>1487199783</date>
      <mdhash>5fbc5cf7c005a5029ecdb7beb7b78901</mdhash>
      <filesize>391436</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5fbc5cf7c005a5029ecdb7beb7b78901</md5>
          <size>391436</size>
          <filedate>1487199783</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta12.zip</url>
          <archive_type>zip</archive_type>
          <md5>123897763f69f8d44d1a0087e7c5331d</md5>
          <size>462713</size>
          <filedate>1487199783</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta11</name>
      <version>7.x-2.0-beta11</version>
      <tag>7.x-2.0-beta11</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta11</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta11.tar.gz</download_link>
      <date>1470608339</date>
      <mdhash>5e7caeb3210414d438dae1d97ab64b86</mdhash>
      <filesize>395952</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5e7caeb3210414d438dae1d97ab64b86</md5>
          <size>395952</size>
          <filedate>1470608339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta11.zip</url>
          <archive_type>zip</archive_type>
          <md5>dde01b3bafe8689eae967311306bfe69</md5>
          <size>468371</size>
          <filedate>1470608339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta10</name>
      <version>7.x-2.0-beta10</version>
      <tag>7.x-2.0-beta10</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta10.tar.gz</download_link>
      <date>1470607739</date>
      <mdhash>84072b9bc88b59762242b1604a7d59d7</mdhash>
      <filesize>395927</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>84072b9bc88b59762242b1604a7d59d7</md5>
          <size>395927</size>
          <filedate>1470607739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta10.zip</url>
          <archive_type>zip</archive_type>
          <md5>074d464a85524e362c52f3e04ded942e</md5>
          <size>468365</size>
          <filedate>1470607739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta9</name>
      <version>7.x-2.0-beta9</version>
      <tag>7.x-2.0-beta9</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta9.tar.gz</download_link>
      <date>1470372539</date>
      <mdhash>132bbeb6fb704bf7a18136ad6ae129d1</mdhash>
      <filesize>395932</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>132bbeb6fb704bf7a18136ad6ae129d1</md5>
          <size>395932</size>
          <filedate>1470372539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta9.zip</url>
          <archive_type>zip</archive_type>
          <md5>1b74ea3b0ab0d8d47439a5355702d64e</md5>
          <size>468355</size>
          <filedate>1470372539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta8</name>
      <version>7.x-2.0-beta8</version>
      <tag>7.x-2.0-beta8</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta8.tar.gz</download_link>
      <date>1389150505</date>
      <mdhash>35d7e26ff486d89bd64e7b3b9439c199</mdhash>
      <filesize>394843</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35d7e26ff486d89bd64e7b3b9439c199</md5>
          <size>394843</size>
          <filedate>1389150505</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>6c7d4118afc68b9367112c9228aa5796</md5>
          <size>464721</size>
          <filedate>1389150505</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta7</name>
      <version>7.x-2.0-beta7</version>
      <tag>7.x-2.0-beta7</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta7.tar.gz</download_link>
      <date>1389030821</date>
      <mdhash>615a8b60c382e0fa91b01725fb510afe</mdhash>
      <filesize>394837</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>615a8b60c382e0fa91b01725fb510afe</md5>
          <size>394837</size>
          <filedate>1389030821</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>eab0684baf195fe200382e5b632a9716</md5>
          <size>464725</size>
          <filedate>1389030821</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta6</name>
      <version>7.x-2.0-beta6</version>
      <tag>7.x-2.0-beta6</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta6.tar.gz</download_link>
      <date>1381233715</date>
      <mdhash>e7bfbf06e76a1772afcb4432372cd1d5</mdhash>
      <filesize>390527</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e7bfbf06e76a1772afcb4432372cd1d5</md5>
          <size>390527</size>
          <filedate>1381233715</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>836ebf7f8752614b7b34daf401c67865</md5>
          <size>459753</size>
          <filedate>1381233715</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta5</name>
      <version>7.x-2.0-beta5</version>
      <tag>7.x-2.0-beta5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta5.tar.gz</download_link>
      <date>1366003512</date>
      <mdhash>65370d759fddfbf5be776de0cc385820</mdhash>
      <filesize>387675</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>65370d759fddfbf5be776de0cc385820</md5>
          <size>387675</size>
          <filedate>1366003512</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>6cc77dfeaba549ac63be6089b84d9c28</md5>
          <size>457096</size>
          <filedate>1366003512</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta4.tar.gz</download_link>
      <date>1364473511</date>
      <mdhash>ea4e3110ce70b8f0bada9eef081104c3</mdhash>
      <filesize>386934</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ea4e3110ce70b8f0bada9eef081104c3</md5>
          <size>386934</size>
          <filedate>1364473511</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7ff66ae6d10128de689fb255033681c9</md5>
          <size>462635</size>
          <filedate>1364473511</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta3</release_link>
      <download_link/>
      <date>1354857150</date>
      <mdhash>6f21d6cebbdcfffeeb67dc5a6163bb1d</mdhash>
      <filesize>253313</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>6f21d6cebbdcfffeeb67dc5a6163bb1d</md5>
          <size>253313</size>
          <filedate>1354857150</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>0a5dd09d9bb67608a0642f8494b7c462</md5>
          <size>329335</size>
          <filedate>1354857150</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta2</release_link>
      <download_link/>
      <date>1353481650</date>
      <mdhash>45d236564c6b9b31772e72d0a86c95b3</mdhash>
      <filesize>237616</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>45d236564c6b9b31772e72d0a86c95b3</md5>
          <size>237616</size>
          <filedate>1353481650</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>e254e255e61001a61b40b3fd6d215019</md5>
          <size>303665</size>
          <filedate>1353481650</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta1.tar.gz</download_link>
      <date>1352748417</date>
      <mdhash>17b2b22e35be12306f5a38045fd0a82b</mdhash>
      <filesize>241972</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>17b2b22e35be12306f5a38045fd0a82b</md5>
          <size>241972</size>
          <filedate>1352748417</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>50813aefaba5a9d3d4bfd35d70ec79ac</md5>
          <size>310660</size>
          <filedate>1352748418</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta15</name>
      <version>7.x-1.0-beta15</version>
      <tag>7.x-1.0-beta15</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta15</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta15.tar.gz</download_link>
      <date>1503841743</date>
      <mdhash>1413cdf57c0704a0fb49339455af7c87</mdhash>
      <filesize>189701</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1413cdf57c0704a0fb49339455af7c87</md5>
          <size>189701</size>
          <filedate>1503841743</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta15.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c748872a06f99590fd94b5270e7ce56</md5>
          <size>273010</size>
          <filedate>1503841743</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta14</name>
      <version>7.x-1.0-beta14</version>
      <tag>7.x-1.0-beta14</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta14</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta14.tar.gz</download_link>
      <date>1470868139</date>
      <mdhash>5ff1a9fc6d2c2c5a038f291fa8b85ff4</mdhash>
      <filesize>197032</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ff1a9fc6d2c2c5a038f291fa8b85ff4</md5>
          <size>197032</size>
          <filedate>1470868139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta14.zip</url>
          <archive_type>zip</archive_type>
          <md5>06838dffa1071f8cc03d48bf4fdcdca8</md5>
          <size>282364</size>
          <filedate>1470868139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta13</name>
      <version>7.x-1.0-beta13</version>
      <tag>7.x-1.0-beta13</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta13</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta13.tar.gz</download_link>
      <date>1470372539</date>
      <mdhash>f46cff2c9c2652e07a2b4ef5bb44bad8</mdhash>
      <filesize>197032</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f46cff2c9c2652e07a2b4ef5bb44bad8</md5>
          <size>197032</size>
          <filedate>1470372539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta13.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f31e9a18ee88f2ea36afb1c20d30f4d</md5>
          <size>282363</size>
          <filedate>1470372539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta12</name>
      <version>7.x-1.0-beta12</version>
      <tag>7.x-1.0-beta12</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta12</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta12</release_link>
      <download_link/>
      <date>1345503424</date>
      <mdhash>d03d5eb0669c1195d9779348f384f513</mdhash>
      <filesize>196994</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>d03d5eb0669c1195d9779348f384f513</md5>
          <size>196994</size>
          <filedate>1345503424</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>89c44c943312dd0bc30b32aa653f1709</md5>
          <size>278000</size>
          <filedate>1345503424</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta11</name>
      <version>7.x-1.0-beta11</version>
      <tag>7.x-1.0-beta11</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta11</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta11.tar.gz</download_link>
      <date>1339643179</date>
      <mdhash>c8297479eadd87918a5aea98c84a3175</mdhash>
      <filesize>189569</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c8297479eadd87918a5aea98c84a3175</md5>
          <size>189569</size>
          <filedate>1339643179</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta11.zip</url>
          <archive_type>zip</archive_type>
          <md5>024ac65b33893771c2c5c67363a36886</md5>
          <size>268795</size>
          <filedate>1339643179</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta10</name>
      <version>7.x-1.0-beta10</version>
      <tag>7.x-1.0-beta10</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta10.tar.gz</download_link>
      <date>1337025356</date>
      <mdhash>a75f3cd0bd641b2b1d1dd6f11f292d8f</mdhash>
      <filesize>181746</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a75f3cd0bd641b2b1d1dd6f11f292d8f</md5>
          <size>181746</size>
          <filedate>1337025356</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta10.zip</url>
          <archive_type>zip</archive_type>
          <md5>931c06db21202d2aedf2db60960f6655</md5>
          <size>261544</size>
          <filedate>1337025356</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta9</name>
      <version>7.x-1.0-beta9</version>
      <tag>7.x-1.0-beta9</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta9</release_link>
      <download_link/>
      <date>1331004047</date>
      <mdhash>640e3dad3a31e5c651b755b3fb4df2f2</mdhash>
      <filesize>157473</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>640e3dad3a31e5c651b755b3fb4df2f2</md5>
          <size>157473</size>
          <filedate>1331004047</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>0f3d59c49dc1f2dda0ff7b854b3eda46</md5>
          <size>225553</size>
          <filedate>1331004047</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta8</name>
      <version>7.x-1.0-beta8</version>
      <tag>7.x-1.0-beta8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta8.tar.gz</download_link>
      <date>1331000155</date>
      <mdhash>12db5e566a415755767f181e818224ce</mdhash>
      <filesize>157418</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>12db5e566a415755767f181e818224ce</md5>
          <size>157418</size>
          <filedate>1331000155</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>88754be736eb685c02cfd38188964826</md5>
          <size>225482</size>
          <filedate>1331000155</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta7.tar.gz</download_link>
      <date>1330961149</date>
      <mdhash>a6091dddff5688d9842fb51095c1bddb</mdhash>
      <filesize>157431</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a6091dddff5688d9842fb51095c1bddb</md5>
          <size>157431</size>
          <filedate>1330961149</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b64e3d5effdb9e3d5c5e0dc4a1dd84d</md5>
          <size>225488</size>
          <filedate>1330961149</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta6.tar.gz</download_link>
      <date>1330957554</date>
      <mdhash>499196dcf1a43e32a56a20f194d14c3c</mdhash>
      <filesize>157425</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>499196dcf1a43e32a56a20f194d14c3c</md5>
          <size>157425</size>
          <filedate>1330957554</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>49ba7da91985a214d0152b1bd6106fad</md5>
          <size>225494</size>
          <filedate>1330957554</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta5.tar.gz</download_link>
      <date>1318263413</date>
      <mdhash>ca21456c66b987fca1a5bd44e3f2070f</mdhash>
      <filesize>137864</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ca21456c66b987fca1a5bd44e3f2070f</md5>
          <size>137864</size>
          <filedate>1318263413</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>728e14b92eda841dea4362e175d3dec3</md5>
          <size>196266</size>
          <filedate>1318263413</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta4.tar.gz</download_link>
      <date>1312774621</date>
      <mdhash>61d293264a7d9c7953c24cf345a1aa82</mdhash>
      <filesize>106541</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61d293264a7d9c7953c24cf345a1aa82</md5>
          <size>106541</size>
          <filedate>1312774621</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>1de56db57fdb6562770b11ce15da61ba</md5>
          <size>152113</size>
          <filedate>1312774621</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta3.tar.gz</download_link>
      <date>1310403720</date>
      <mdhash>408c305d6a56c72fa7ab810eaefa3549</mdhash>
      <filesize>101169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>408c305d6a56c72fa7ab810eaefa3549</md5>
          <size>101169</size>
          <filedate>1310403720</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>665ef2ff957b60c98a96a64728d2b3d8</md5>
          <size>146719</size>
          <filedate>1310403720</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta2.tar.gz</download_link>
      <date>1309638120</date>
      <mdhash>f0a752c3fb0c4290ef723b45cbf4bb19</mdhash>
      <filesize>99002</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0a752c3fb0c4290ef723b45cbf4bb19</md5>
          <size>99002</size>
          <filedate>1309638120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e346df854278d2a8786206698a0d76a3</md5>
          <size>143470</size>
          <filedate>1309638120</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta1.tar.gz</download_link>
      <date>1306479733</date>
      <mdhash>17299dda7942f5c93abc7618c1349d56</mdhash>
      <filesize>108372</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>17299dda7942f5c93abc7618c1349d56</md5>
          <size>108372</size>
          <filedate>1306479733</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>48e594231d7eb2500948c71d10393eb3</md5>
          <size>149345</size>
          <filedate>1306479733</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-unstable6</name>
      <version>7.x-1.0-unstable6</version>
      <tag>7.x-1.0-unstable6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-unstable6</release_link>
      <download_link/>
      <date>1302503217</date>
      <mdhash>5df399fc193e624960d95536fa6b266f</mdhash>
      <filesize>68679</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>5df399fc193e624960d95536fa6b266f</md5>
          <size>68679</size>
          <filedate>1302503217</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a639872f2711e38c8950378f44f82f09</md5>
          <size>94248</size>
          <filedate>1302503217</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-unstable5</name>
      <version>7.x-1.0-unstable5</version>
      <tag>7.x-1.0-unstable5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-unstable5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable5.tar.gz</download_link>
      <date>1297196515</date>
      <mdhash>89d29f46dd183581bfc8d5b94bd7f2e9</mdhash>
      <filesize>65807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89d29f46dd183581bfc8d5b94bd7f2e9</md5>
          <size>65807</size>
          <filedate>1297196515</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable5.zip</url>
          <archive_type>zip</archive_type>
          <md5>0d8d9b2a8c3b05bf291274234fbea0fa</md5>
          <size>91502</size>
          <filedate>1297196515</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-unstable4</name>
      <version>7.x-1.0-unstable4</version>
      <tag>7.x-1.0-unstable4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-unstable4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable4.tar.gz</download_link>
      <date>1294861891</date>
      <mdhash>fba7f00d5174db1b56944034f53eb126</mdhash>
      <filesize>58818</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fba7f00d5174db1b56944034f53eb126</md5>
          <size>58818</size>
          <filedate>1294861891</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable4.zip</url>
          <archive_type>zip</archive_type>
          <md5>3b263ab85f86b79f4f604d815154c8e5</md5>
          <size>82389</size>
          <filedate>1294861892</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-unstable3</name>
      <version>7.x-1.0-unstable3</version>
      <tag>7.x-1.0-unstable3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-unstable3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable3.tar.gz</download_link>
      <date>1294476065</date>
      <mdhash>e59a92b8dc03733ff65b4b9f958383ec</mdhash>
      <filesize>58662</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e59a92b8dc03733ff65b4b9f958383ec</md5>
          <size>58662</size>
          <filedate>1294476065</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable3.zip</url>
          <archive_type>zip</archive_type>
          <md5>05967d47ec2b203152b78f7480ef77bd</md5>
          <size>82293</size>
          <filedate>1294476066</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.0-unstable2</name>
      <version>7.x-1.0-unstable2</version>
      <tag>7.x-1.0-unstable2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.0-unstable2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable2.tar.gz</download_link>
      <date>1293601880</date>
      <mdhash>f3406509ced1358999aa39529e889e2b</mdhash>
      <filesize>57862</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3406509ced1358999aa39529e889e2b</md5>
          <size>57862</size>
          <filedate>1293601880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.0-unstable2.zip</url>
          <archive_type>zip</archive_type>
          <md5>357d6e15e1e718bf0f3ebdd66025d3d1</md5>
          <size>81214</size>
          <filedate>1293601880</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-2.x-dev.tar.gz</download_link>
      <date>1698290445</date>
      <mdhash>bdc1fb04b681ec6326913a0af4158928</mdhash>
      <filesize>379722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bdc1fb04b681ec6326913a0af4158928</md5>
          <size>379722</size>
          <filedate>1698290445</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>98e0f67f0c5ac9c2c2476bb987e7a0f6</md5>
          <size>449571</size>
          <filedate>1698290445</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ldap 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ldap/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ldap-7.x-1.x-dev.tar.gz</download_link>
      <date>1499377742</date>
      <mdhash>d0e5824ebb5af9d524365b0e3702dc84</mdhash>
      <filesize>189720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d0e5824ebb5af9d524365b0e3702dc84</md5>
          <size>189720</size>
          <filedate>1499377742</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ldap-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a41d3b3f3173fef4c97d49f224741640</md5>
          <size>273055</size>
          <filedate>1499377742</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
