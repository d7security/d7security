<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Multifield table</title>
  <short_name>multifield_table</short_name>
  <dc:creator>plopesc</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/multifield_table</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>multifield_table 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/multifield_table/-/releases/7.x-1.1</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67459426/packages/generic/multifield_table/7.x-1.1/multifield_table-7.x-1.1.tar.gz</download_link>
      <date>1740495407</date>
      <mdhash>c2922df698dc9bd2986371ae24c244d4</mdhash>
      <filesize>13009</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67459426/packages/generic/multifield_table/7.x-1.1/multifield_table-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c2922df698dc9bd2986371ae24c244d4</md5>
          <size>13009</size>
          <filedate>1740495407</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67459426/packages/generic/multifield_table/7.x-1.1/multifield_table-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8af980deaa7d7b2e4cf3b4f88275b153</md5>
          <size>14816</size>
          <filedate>1740495407</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>multifield_table 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield_table/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0.tar.gz</download_link>
      <date>1427120881</date>
      <mdhash>086684b7a1b5bc7ab16cf41af2825bf7</mdhash>
      <filesize>12814</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>086684b7a1b5bc7ab16cf41af2825bf7</md5>
          <size>12814</size>
          <filedate>1427120881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d3c1dbf9dfe8c53a7f8b532a3925d01</md5>
          <size>14534</size>
          <filedate>1427120881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multifield_table 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield_table/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta2.tar.gz</download_link>
      <date>1384207105</date>
      <mdhash>2ed9d4f0d1c940971e85039262f6451b</mdhash>
      <filesize>9895</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2ed9d4f0d1c940971e85039262f6451b</md5>
          <size>9895</size>
          <filedate>1384207105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d1566b4787ca9e640d79c164587cdbec</md5>
          <size>10751</size>
          <filedate>1384207105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>multifield_table 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multifield_table/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta1.tar.gz</download_link>
      <date>1384020504</date>
      <mdhash>16e9982501349703615eb4e1f38a09c0</mdhash>
      <filesize>7880</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>16e9982501349703615eb4e1f38a09c0</md5>
          <size>7880</size>
          <filedate>1384020504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multifield_table-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>196fe4e602697c55946be3afdb643136</md5>
          <size>8385</size>
          <filedate>1384020504</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
