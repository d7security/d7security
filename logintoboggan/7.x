<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>LoginToboggan</title>
  <short_name>logintoboggan</short_name>
  <dc:creator>greg.harvey</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/logintoboggan</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>logintoboggan 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.5.tar.gz</download_link>
      <date>1430501881</date>
      <mdhash>796329de4df50bfe5a764727e978a3e8</mdhash>
      <filesize>33854</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>796329de4df50bfe5a764727e978a3e8</md5>
          <size>33854</size>
          <filedate>1430501881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d3a3f27d9ec63ba5cd64e8f7234bb47</md5>
          <size>44752</size>
          <filedate>1430501881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.4.tar.gz</download_link>
      <date>1404818627</date>
      <mdhash>a4a7c112765e89748429e2483e5626f0</mdhash>
      <filesize>30729</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a4a7c112765e89748429e2483e5626f0</md5>
          <size>30729</size>
          <filedate>1404818627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>98df613235464cc7acc2734229fdae7e</md5>
          <size>40849</size>
          <filedate>1404818627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.3.tar.gz</download_link>
      <date>1320873335</date>
      <mdhash>26865d183114bd579449f6689599f229</mdhash>
      <filesize>30624</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>26865d183114bd579449f6689599f229</md5>
          <size>30624</size>
          <filedate>1320873335</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f9fcbc566587e6a7ed351054c156c9eb</md5>
          <size>40785</size>
          <filedate>1320873335</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.2.tar.gz</download_link>
      <date>1302094316</date>
      <mdhash>deec3c7763a3951a49d46c34ab2af61b</mdhash>
      <filesize>26629</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>deec3c7763a3951a49d46c34ab2af61b</md5>
          <size>26629</size>
          <filedate>1302094316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>50b869c4020c938ffb3c6b28c6daab60</md5>
          <size>33831</size>
          <filedate>1302094316</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.1.tar.gz</download_link>
      <date>1295518337</date>
      <mdhash>092c2c8aa6d0849bdd7f23e9478e1ed7</mdhash>
      <filesize>66677</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>092c2c8aa6d0849bdd7f23e9478e1ed7</md5>
          <size>66677</size>
          <filedate>1295518337</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6da8e42bffc0fda37e26a9adaf02e549</md5>
          <size>92503</size>
          <filedate>1295518337</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0.tar.gz</download_link>
      <date>1294297868</date>
      <mdhash>111fba469050f9b7123adf26d2f3a66a</mdhash>
      <filesize>66527</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>111fba469050f9b7123adf26d2f3a66a</md5>
          <size>66527</size>
          <filedate>1294297868</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>98c37ab92adb7546646aa06ba5539852</md5>
          <size>92397</size>
          <filedate>1294297868</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1281478592</date>
      <mdhash>b5dfe633029b64c0b50ddb4f8783eff0</mdhash>
      <filesize>65422</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b5dfe633029b64c0b50ddb4f8783eff0</md5>
          <size>65422</size>
          <filedate>1281478592</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>cbd59f43a42091c427261ee891fa2e2e</md5>
          <size>87898</size>
          <filedate>1293232720</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1256489441</date>
      <mdhash>b87460c3fd35a7fa4e92ab87594f2e6d</mdhash>
      <filesize>64085</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b87460c3fd35a7fa4e92ab87594f2e6d</md5>
          <size>64085</size>
          <filedate>1256489441</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f1e03b72b9c0acbfdb769cae235302c1</md5>
          <size>85150</size>
          <filedate>1293232721</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1256152889</date>
      <mdhash>1e58d8a72127d8ec333e9a1a194e6ad8</mdhash>
      <filesize>64059</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1e58d8a72127d8ec333e9a1a194e6ad8</md5>
          <size>64059</size>
          <filedate>1256152889</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cadf4500c415996782b1cfb83d889d5a</md5>
          <size>85149</size>
          <filedate>1293232718</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>logintoboggan 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/logintoboggan/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.x-dev.tar.gz</download_link>
      <date>1611836638</date>
      <mdhash>ea9e669040b89d5492762d142f7b9e16</mdhash>
      <filesize>34060</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ea9e669040b89d5492762d142f7b9e16</md5>
          <size>34060</size>
          <filedate>1611836638</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/logintoboggan-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>336d3a7f4b65f57994b76cbabd294d0d</md5>
          <size>45854</size>
          <filedate>1611836638</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
