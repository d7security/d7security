<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Date</title>
  <short_name>date</short_name>
  <dc:creator>vijaycs85</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/date</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>date 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1662589105</date>
      <mdhash>94b93bb5214b637a801e38d8379d95f5</mdhash>
      <filesize>256534</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94b93bb5214b637a801e38d8379d95f5</md5>
          <size>256534</size>
          <filedate>1662589105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>af4969c8272d1397f8d07c34399ee755</md5>
          <size>318339</size>
          <filedate>1662589105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1630097677</date>
      <mdhash>d916d8c6731e56d3e73d029f426b02fc</mdhash>
      <filesize>252955</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d916d8c6731e56d3e73d029f426b02fc</md5>
          <size>252955</size>
          <filedate>1630097677</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>95b64d4a259398421bea8655e6164c6f</md5>
          <size>310185</size>
          <filedate>1630097677</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.14</name>
      <version>7.x-2.14</version>
      <tag>7.x-2.14</tag>
      <version_major>2</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.14.tar.gz</download_link>
      <date>1673711075</date>
      <mdhash>ba6afcc4b52608c08a01486fbfafe9cc</mdhash>
      <filesize>255260</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ba6afcc4b52608c08a01486fbfafe9cc</md5>
          <size>255260</size>
          <filedate>1673711075</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>1bd21795cec7dfb4817aa7db690b891f</md5>
          <size>315647</size>
          <filedate>1673711075</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.13</name>
      <version>7.x-2.13</version>
      <tag>7.x-2.13</tag>
      <version_major>2</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.13.tar.gz</download_link>
      <date>1659633187</date>
      <mdhash>00d63166890cce13902a55968a6f416d</mdhash>
      <filesize>254282</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00d63166890cce13902a55968a6f416d</md5>
          <size>254282</size>
          <filedate>1659633187</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>4931810c0a187d40ec8956397dbfc0e2</md5>
          <size>314560</size>
          <filedate>1659633187</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.12</name>
      <version>7.x-2.12</version>
      <tag>7.x-2.12</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.12.tar.gz</download_link>
      <date>1633627648</date>
      <mdhash>8a7c6ebdc2cb27ff56757463bdf1220a</mdhash>
      <filesize>251038</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8a7c6ebdc2cb27ff56757463bdf1220a</md5>
          <size>251038</size>
          <filedate>1633627648</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf397300d7a6917e87e4d4b39f1bdc52</md5>
          <size>307554</size>
          <filedate>1633627648</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.12-rc1</name>
      <version>7.x-2.12-rc1</version>
      <tag>7.x-2.12-rc1</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.12-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.12-rc1.tar.gz</download_link>
      <date>1630096632</date>
      <mdhash>4c150ae0188524b04c54280d2b6644ed</mdhash>
      <filesize>251056</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.12-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c150ae0188524b04c54280d2b6644ed</md5>
          <size>251056</size>
          <filedate>1630096632</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.12-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ca0431ab7b2f6197bc90ff201264f2e</md5>
          <size>307581</size>
          <filedate>1630096632</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.11.tar.gz</download_link>
      <date>1614952725</date>
      <mdhash>5889b2b273bde51a474ad340971b0a74</mdhash>
      <filesize>251104</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5889b2b273bde51a474ad340971b0a74</md5>
          <size>251104</size>
          <filedate>1614952725</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>093a3828a643943b458041e6c6ee63d2</md5>
          <size>307742</size>
          <filedate>1614952725</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.11-rc1</name>
      <version>7.x-2.11-rc1</version>
      <tag>7.x-2.11-rc1</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.11-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.11-rc1.tar.gz</download_link>
      <date>1601635723</date>
      <mdhash>8b768bf6c9d98b74dcd6acb4d6897b9e</mdhash>
      <filesize>243098</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b768bf6c9d98b74dcd6acb4d6897b9e</md5>
          <size>243098</size>
          <filedate>1601635723</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>79c090d114cb6d6b154e45b5fd3059e5</md5>
          <size>293762</size>
          <filedate>1601635723</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.11-beta3</name>
      <version>7.x-2.11-beta3</version>
      <tag>7.x-2.11-beta3</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.11-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta3.tar.gz</download_link>
      <date>1569201784</date>
      <mdhash>c2bab649c4148ab99a41007055b0ea5a</mdhash>
      <filesize>239744</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c2bab649c4148ab99a41007055b0ea5a</md5>
          <size>239744</size>
          <filedate>1569201784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ca4a7dbdcf2ddf25141de79b9081bdb</md5>
          <size>287268</size>
          <filedate>1569201784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.11-beta2</name>
      <version>7.x-2.11-beta2</version>
      <tag>7.x-2.11-beta2</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.11-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta2.tar.gz</download_link>
      <date>1548172380</date>
      <mdhash>1d2cd1482bee751f4d0cce11cdd5147b</mdhash>
      <filesize>234898</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1d2cd1482bee751f4d0cce11cdd5147b</md5>
          <size>234898</size>
          <filedate>1548172380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>78329c429472618ec6ae53b31985537e</md5>
          <size>281696</size>
          <filedate>1548172380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.11-beta1</name>
      <version>7.x-2.11-beta1</version>
      <tag>7.x-2.11-beta1</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.11-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta1.tar.gz</download_link>
      <date>1540563484</date>
      <mdhash>6f84719e3f73db03fd6815c57a9f2191</mdhash>
      <filesize>235918</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6f84719e3f73db03fd6815c57a9f2191</md5>
          <size>235918</size>
          <filedate>1540563484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.11-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1cb6502bbb25948d95b96856434a5fa5</md5>
          <size>283490</size>
          <filedate>1540563484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.10.tar.gz</download_link>
      <date>1491562083</date>
      <mdhash>2574ab984de79b65f93dbeb48a4e2036</mdhash>
      <filesize>226466</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2574ab984de79b65f93dbeb48a4e2036</md5>
          <size>226466</size>
          <filedate>1491562083</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5577b1d9e1aca65b4635a066d252fc7</md5>
          <size>273344</size>
          <filedate>1491562083</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.10-rc2</name>
      <version>7.x-2.10-rc2</version>
      <tag>7.x-2.10-rc2</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.10-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc2.tar.gz</download_link>
      <date>1490783284</date>
      <mdhash>2e59072e74cf1e180b5f159cea91639c</mdhash>
      <filesize>226485</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2e59072e74cf1e180b5f159cea91639c</md5>
          <size>226485</size>
          <filedate>1490783284</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8ce42989f835811e2405ec3b5fdad7da</md5>
          <size>273382</size>
          <filedate>1490783284</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.10-rc1</name>
      <version>7.x-2.10-rc1</version>
      <tag>7.x-2.10-rc1</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.10-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc1.tar.gz</download_link>
      <date>1461749740</date>
      <mdhash>59079319cdaa7f96843c66a674bd6f96</mdhash>
      <filesize>226185</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>59079319cdaa7f96843c66a674bd6f96</md5>
          <size>226185</size>
          <filedate>1461749740</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c208293f1fecbe2173655197a963b78</md5>
          <size>272875</size>
          <filedate>1461749740</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.10-beta1</name>
      <version>7.x-2.10-beta1</version>
      <tag>7.x-2.10-beta1</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.10-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.10-beta1.tar.gz</download_link>
      <date>1452253139</date>
      <mdhash>b6c7abde2995afb8a014cb590ac131ac</mdhash>
      <filesize>224719</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b6c7abde2995afb8a014cb590ac131ac</md5>
          <size>224719</size>
          <filedate>1452253139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.10-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c924fb468794899fb8618784d627af68</md5>
          <size>270329</size>
          <filedate>1452253139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9.tar.gz</download_link>
      <date>1441727339</date>
      <mdhash>5d76a5e020d3926aa3c516e426678700</mdhash>
      <filesize>224512</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d76a5e020d3926aa3c516e426678700</md5>
          <size>224512</size>
          <filedate>1441727339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>37f01af9001236286c849b27cdc20f3a</md5>
          <size>270158</size>
          <filedate>1441727339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.9-rc1</name>
      <version>7.x-2.9-rc1</version>
      <tag>7.x-2.9-rc1</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9-rc1.tar.gz</download_link>
      <date>1427454181</date>
      <mdhash>5cc051772c39d76bb0a2bfd8a5f7c775</mdhash>
      <filesize>224454</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5cc051772c39d76bb0a2bfd8a5f7c775</md5>
          <size>224454</size>
          <filedate>1427454181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5ce9b5b8d34c4d412f53d143fdf73c06</md5>
          <size>270115</size>
          <filedate>1427454181</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.9-beta2</name>
      <version>7.x-2.9-beta2</version>
      <tag>7.x-2.9-beta2</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta2.tar.gz</download_link>
      <date>1417718873</date>
      <mdhash>e81616832e6ef58be039640a4fe35390</mdhash>
      <filesize>223732</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e81616832e6ef58be039640a4fe35390</md5>
          <size>223732</size>
          <filedate>1417718873</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>943f0dc3fd7a18dfbce6d67e94d0033d</md5>
          <size>268342</size>
          <filedate>1417718873</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.9-beta1</name>
      <version>7.x-2.9-beta1</version>
      <tag>7.x-2.9-beta1</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta1.tar.gz</download_link>
      <date>1415483880</date>
      <mdhash>e03390f28efe81e7e56180651f4bb76a</mdhash>
      <filesize>222344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e03390f28efe81e7e56180651f4bb76a</md5>
          <size>222344</size>
          <filedate>1415483880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2a3239f3526e8f101b2adbcffb1c5fb2</md5>
          <size>267037</size>
          <filedate>1415483880</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.9-alpha2</name>
      <version>7.x-2.9-alpha2</version>
      <tag>7.x-2.9-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha2.tar.gz</download_link>
      <date>1411597754</date>
      <mdhash>2f71a9bc1dc8a13c1607ca2dc74a87b7</mdhash>
      <filesize>218373</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2f71a9bc1dc8a13c1607ca2dc74a87b7</md5>
          <size>218373</size>
          <filedate>1411597754</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f40041521bc83e1cd03154bac3bce1d4</md5>
          <size>262109</size>
          <filedate>1411597754</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.9-alpha1</name>
      <version>7.x-2.9-alpha1</version>
      <tag>7.x-2.9-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.9-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha1.tar.gz</download_link>
      <date>1406829527</date>
      <mdhash>3e6a694c86989c61d9cd5cf4cf582bc5</mdhash>
      <filesize>218628</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3e6a694c86989c61d9cd5cf4cf582bc5</md5>
          <size>218628</size>
          <filedate>1406829527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.9-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3149e699e561ef15ea5d22f4a2a06d97</md5>
          <size>259816</size>
          <filedate>1406829527</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.8.tar.gz</download_link>
      <date>1406653427</date>
      <mdhash>e0d07255a60e3a7716db5902767e08d1</mdhash>
      <filesize>218587</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e0d07255a60e3a7716db5902767e08d1</md5>
          <size>218587</size>
          <filedate>1406653427</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>04a3836beac1cd9f1d68eb7c2495b70c</md5>
          <size>259760</size>
          <filedate>1406653427</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.8-beta2</name>
      <version>7.x-2.8-beta2</version>
      <tag>7.x-2.8-beta2</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.8-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta2.tar.gz</download_link>
      <date>1406317428</date>
      <mdhash>3ba190c30aa3833500af8505c9951121</mdhash>
      <filesize>217910</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ba190c30aa3833500af8505c9951121</md5>
          <size>217910</size>
          <filedate>1406317428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>be3632c5433702be9963f668e57c9372</md5>
          <size>259106</size>
          <filedate>1406317428</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.8-beta1</name>
      <version>7.x-2.8-beta1</version>
      <tag>7.x-2.8-beta1</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.8-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta1.tar.gz</download_link>
      <date>1403366927</date>
      <mdhash>cf89ddee77f567db0157124e1651246d</mdhash>
      <filesize>217411</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cf89ddee77f567db0157124e1651246d</md5>
          <size>217411</size>
          <filedate>1403366927</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.8-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>545d780c5ed75d0acde5d55e235c3852</md5>
          <size>258669</size>
          <filedate>1403366927</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.7.tar.gz</download_link>
      <date>1387659204</date>
      <mdhash>24e11f82623a442305c30cb6887d77f9</mdhash>
      <filesize>216320</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24e11f82623a442305c30cb6887d77f9</md5>
          <size>216320</size>
          <filedate>1387659204</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>16d40a706e7dec3ecae6c9fa0d8668d4</md5>
          <size>257269</size>
          <filedate>1387659204</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.6.tar.gz</download_link>
      <date>1344850024</date>
      <mdhash>cc22fe50289aea79b3b9b240baba0111</mdhash>
      <filesize>213301</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cc22fe50289aea79b3b9b240baba0111</md5>
          <size>213301</size>
          <filedate>1344850024</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>91f1461690ed3ed13b62de1bf92b2258</md5>
          <size>253562</size>
          <filedate>1344850024</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.5.tar.gz</download_link>
      <date>1334835098</date>
      <mdhash>56fa435f5c8831655ddf3f4fffe2b97a</mdhash>
      <filesize>211078</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>56fa435f5c8831655ddf3f4fffe2b97a</md5>
          <size>211078</size>
          <filedate>1334835098</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f91c7f2c48ca6531bb11c6cf03d13d4</md5>
          <size>251465</size>
          <filedate>1334835098</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.4.tar.gz</download_link>
      <date>1334828498</date>
      <mdhash>2c152c82d8cc55e090cba40a46742465</mdhash>
      <filesize>210999</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2c152c82d8cc55e090cba40a46742465</md5>
          <size>210999</size>
          <filedate>1334828498</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>c1656aa04d224d1ad3418f035da91ecb</md5>
          <size>251395</size>
          <filedate>1334828498</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.3.tar.gz</download_link>
      <date>1333273246</date>
      <mdhash>3290ee3891157384b606633dda02fe19</mdhash>
      <filesize>210464</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3290ee3891157384b606633dda02fe19</md5>
          <size>210464</size>
          <filedate>1333273246</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b8e39350fd2a1913ea90f0e0e906b16d</md5>
          <size>250887</size>
          <filedate>1333273246</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.2.tar.gz</download_link>
      <date>1330089942</date>
      <mdhash>652323d741226212057e8f58906c7d6a</mdhash>
      <filesize>207223</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>652323d741226212057e8f58906c7d6a</md5>
          <size>207223</size>
          <filedate>1330089942</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>adf2690ba7bc4cc2bbfc6892f67067a3</md5>
          <size>247380</size>
          <filedate>1330089942</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.1.tar.gz</download_link>
      <date>1329214840</date>
      <mdhash>08eab951b7096b1edcc1cdff52b3c9cc</mdhash>
      <filesize>208349</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>08eab951b7096b1edcc1cdff52b3c9cc</md5>
          <size>208349</size>
          <filedate>1329214840</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>878532d584bf9fbe86d9cd6d3c4b38fb</md5>
          <size>249249</size>
          <filedate>1329214840</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>date 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc2.tar.gz</download_link>
      <date>1328104249</date>
      <mdhash>60e5d591d567e823141650db8290394b</mdhash>
      <filesize>208479</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>60e5d591d567e823141650db8290394b</md5>
          <size>208479</size>
          <filedate>1328104249</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c66399b30711a80eddedb99b2af1bd6</md5>
          <size>249514</size>
          <filedate>1328104250</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc1.tar.gz</download_link>
      <date>1324415138</date>
      <mdhash>553e622be1be5c28b59c088f8b890872</mdhash>
      <filesize>203112</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>553e622be1be5c28b59c088f8b890872</md5>
          <size>203112</size>
          <filedate>1324415138</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ad24ceabf16253f0a3a15007e87529c2</md5>
          <size>243216</size>
          <filedate>1324415138</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-alpha5</name>
      <version>7.x-2.0-alpha5</version>
      <tag>7.x-2.0-alpha5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha5.tar.gz</download_link>
      <date>1322563238</date>
      <mdhash>02cf9eff83b7203b70c311daca9c0e8c</mdhash>
      <filesize>204364</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>02cf9eff83b7203b70c311daca9c0e8c</md5>
          <size>204364</size>
          <filedate>1322563238</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d611ae1139bf7e82a76a411a9980ba1c</md5>
          <size>246879</size>
          <filedate>1322563238</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-alpha4</name>
      <version>7.x-2.0-alpha4</version>
      <tag>7.x-2.0-alpha4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha4.tar.gz</download_link>
      <date>1314097619</date>
      <mdhash>832ce7f175146004c9976b9191d37f45</mdhash>
      <filesize>195013</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>832ce7f175146004c9976b9191d37f45</md5>
          <size>195013</size>
          <filedate>1314097619</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ebdefcee8975d715330e7f62c532b6f5</md5>
          <size>236622</size>
          <filedate>1314097619</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-alpha3</name>
      <version>7.x-2.0-alpha3</version>
      <tag>7.x-2.0-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha3.tar.gz</download_link>
      <date>1303821715</date>
      <mdhash>d7de2d8ece4098fe8ac9ce6e5eb30270</mdhash>
      <filesize>169553</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d7de2d8ece4098fe8ac9ce6e5eb30270</md5>
          <size>169553</size>
          <filedate>1303821715</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f15a8900e4a8c7a26ac28e359f39fae</md5>
          <size>204604</size>
          <filedate>1303821715</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1303818715</date>
      <mdhash>2a2c10cb87eed66b0174283f9fc607c4</mdhash>
      <filesize>169563</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a2c10cb87eed66b0174283f9fc607c4</md5>
          <size>169563</size>
          <filedate>1303818715</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a342f41b721d102bac3afda110d975db</md5>
          <size>204613</size>
          <filedate>1303818715</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1299682267</date>
      <mdhash>c5ab4e67bb59da279c0da9b10ed9725e</mdhash>
      <filesize>166837</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c5ab4e67bb59da279c0da9b10ed9725e</md5>
          <size>166837</size>
          <filedate>1299682267</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>022ef23e80dde4176f0eaa5801838e90</md5>
          <size>201649</size>
          <filedate>1299682267</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1295529407</date>
      <mdhash>0ed3d8939eeb72e7a3bcb537ed4d21ff</mdhash>
      <filesize>268480</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0ed3d8939eeb72e7a3bcb537ed4d21ff</md5>
          <size>268480</size>
          <filedate>1295529407</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9b95f34d30785c81647d631b55efdc3b</md5>
          <size>368779</size>
          <filedate>1295529407</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1295353339</date>
      <mdhash>6534800ffd881bab8721172100039111</mdhash>
      <filesize>268378</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6534800ffd881bab8721172100039111</md5>
          <size>268378</size>
          <filedate>1295353339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c2531e2a324dd5dce191735127699bc2</md5>
          <size>368707</size>
          <filedate>1295353339</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-3.x-dev.tar.gz</download_link>
      <date>1675273385</date>
      <mdhash>4362a51d1690b364ef99e4ac80664ce8</mdhash>
      <filesize>257365</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4362a51d1690b364ef99e4ac80664ce8</md5>
          <size>257365</size>
          <filedate>1675273385</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f2cfcfb9290da209c642841e8c1b62f7</md5>
          <size>319595</size>
          <filedate>1675273385</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-2.x-dev.tar.gz</download_link>
      <date>1675273269</date>
      <mdhash>8b740f41fa051cd478d5cb1b52cd8cab</mdhash>
      <filesize>256003</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b740f41fa051cd478d5cb1b52cd8cab</md5>
          <size>256003</size>
          <filedate>1675273269</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>31d7cb24aff0c2a0bdc4eb9e6f38b851</md5>
          <size>316065</size>
          <filedate>1675273269</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>date-7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/date/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/date-7.x-1.x-dev.tar.gz</download_link>
      <date>1490710682</date>
      <mdhash>fac06f792a558a00f0bc1b49b43469fd</mdhash>
      <filesize>166214</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fac06f792a558a00f0bc1b49b43469fd</md5>
          <size>166214</size>
          <filedate>1490710682</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/date-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c1f7f9030dfd0fb5930cbd69e295b65a</md5>
          <size>202965</size>
          <filedate>1490710682</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
