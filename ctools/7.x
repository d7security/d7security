<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Chaos Tool Suite (ctools)</title>
  <short_name>ctools</short_name>
  <dc:creator>merlinofchaos</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ctools</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ctools 7.x-1.21</name>
      <version>7.x-1.21</version>
      <tag>7.x-1.21</tag>
      <version_major>1</version_major>
      <version_patch>21</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.21</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.21.tar.gz</download_link>
      <date>1675152415</date>
      <mdhash>184c9c2e621b69e049af055a9a309305</mdhash>
      <filesize>451812</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.21.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>184c9c2e621b69e049af055a9a309305</md5>
          <size>451812</size>
          <filedate>1675152415</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.21.zip</url>
          <archive_type>zip</archive_type>
          <md5>c89bc504c0b75bb99295c15aa86e3de2</md5>
          <size>702272</size>
          <filedate>1675152415</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.20</name>
      <version>7.x-1.20</version>
      <tag>7.x-1.20</tag>
      <version_major>1</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.20.tar.gz</download_link>
      <date>1642577344</date>
      <mdhash>f8ccd6de5c3eadc4cbc20a9fe5960ebb</mdhash>
      <filesize>451670</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f8ccd6de5c3eadc4cbc20a9fe5960ebb</md5>
          <size>451670</size>
          <filedate>1642577344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab3ccf6c7b247178d8e3846e2dac2d24</md5>
          <size>702098</size>
          <filedate>1642577344</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.19</name>
      <version>7.x-1.19</version>
      <tag>7.x-1.19</tag>
      <version_major>1</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.19.tar.gz</download_link>
      <date>1611988841</date>
      <mdhash>57a6bb37a3d69c51170212802e728951</mdhash>
      <filesize>451495</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57a6bb37a3d69c51170212802e728951</md5>
          <size>451495</size>
          <filedate>1611988841</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>e08acdf4f5db338267cad120dd34f385</md5>
          <size>701958</size>
          <filedate>1611988841</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.18</name>
      <version>7.x-1.18</version>
      <tag>7.x-1.18</tag>
      <version_major>1</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.18.tar.gz</download_link>
      <date>1611815489</date>
      <mdhash>bbb8d0e7d3d80306c49238870340d5dd</mdhash>
      <filesize>451518</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bbb8d0e7d3d80306c49238870340d5dd</md5>
          <size>451518</size>
          <filedate>1611815489</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>66985b18914a89763257d99d837293f2</md5>
          <size>701964</size>
          <filedate>1611815489</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.17</name>
      <version>7.x-1.17</version>
      <tag>7.x-1.17</tag>
      <version_major>1</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.17.tar.gz</download_link>
      <date>1603490547</date>
      <mdhash>7e80e0c743a1d77607e1af9f094fc7be</mdhash>
      <filesize>447589</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e80e0c743a1d77607e1af9f094fc7be</md5>
          <size>447589</size>
          <filedate>1603490547</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f69d5cd0e178aa9db78f5d883dbd1b3</md5>
          <size>686775</size>
          <filedate>1603490547</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.16</name>
      <version>7.x-1.16</version>
      <tag>7.x-1.16</tag>
      <version_major>1</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.16.tar.gz</download_link>
      <date>1603430411</date>
      <mdhash>468b6758643f2111738b025641fa7b6c</mdhash>
      <filesize>441836</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>468b6758643f2111738b025641fa7b6c</md5>
          <size>441836</size>
          <filedate>1603430411</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f18ff84fe7e8cfa63346e0712ade45a</md5>
          <size>681149</size>
          <filedate>1603430411</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.15</name>
      <version>7.x-1.15</version>
      <tag>7.x-1.15</tag>
      <version_major>1</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.15.tar.gz</download_link>
      <date>1549603684</date>
      <mdhash>a59f7b21f3a8b6218904f0a589e483c3</mdhash>
      <filesize>441881</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a59f7b21f3a8b6218904f0a589e483c3</md5>
          <size>441881</size>
          <filedate>1549603684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>7b134d762c22be1f837e5454f367a9f6</md5>
          <size>681413</size>
          <filedate>1549603684</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.14</name>
      <version>7.x-1.14</version>
      <tag>7.x-1.14</tag>
      <version_major>1</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.14.tar.gz</download_link>
      <date>1519455784</date>
      <mdhash>88dbe403ecfe2fe434f4237e5fd5ec67</mdhash>
      <filesize>441835</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>88dbe403ecfe2fe434f4237e5fd5ec67</md5>
          <size>441835</size>
          <filedate>1519455784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>7bccd396a522d141e141fc6353e71322</md5>
          <size>681504</size>
          <filedate>1519455784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.13.tar.gz</download_link>
      <date>1517704084</date>
      <mdhash>17e4b504d991cc0e98f868d8ab4ace96</mdhash>
      <filesize>443520</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>17e4b504d991cc0e98f868d8ab4ace96</md5>
          <size>443520</size>
          <filedate>1517704084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>f745806d739c4e50c407189658d6c7e7</md5>
          <size>684257</size>
          <filedate>1517704084</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.12.tar.gz</download_link>
      <date>1479787142</date>
      <mdhash>daae0be22830e34c4d952f91557ae5a5</mdhash>
      <filesize>433707</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>daae0be22830e34c4d952f91557ae5a5</md5>
          <size>433707</size>
          <filedate>1479787142</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>019cbf4db742c1d56a08b836d487dd81</md5>
          <size>669807</size>
          <filedate>1479787142</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.11.tar.gz</download_link>
      <date>1476581639</date>
      <mdhash>271d08d18f3c05f51aba1ad77d42f96c</mdhash>
      <filesize>432144</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>271d08d18f3c05f51aba1ad77d42f96c</md5>
          <size>432144</size>
          <filedate>1476581639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>d0260490a46de5c7e6e745b51be16cb0</md5>
          <size>667431</size>
          <filedate>1476581639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.10.tar.gz</download_link>
      <date>1471454080</date>
      <mdhash>38bb47304f6dcd0bcd6b02ed6ee235fc</mdhash>
      <filesize>430198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38bb47304f6dcd0bcd6b02ed6ee235fc</md5>
          <size>430198</size>
          <filedate>1471454080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c49528f51044c6499fe924ca3735258</md5>
          <size>663176</size>
          <filedate>1471454080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.9.tar.gz</download_link>
      <date>1440019139</date>
      <mdhash>bd7b5dac915e8fa3da909379807ef824</mdhash>
      <filesize>429330</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bd7b5dac915e8fa3da909379807ef824</md5>
          <size>429330</size>
          <filedate>1440019139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>df4ebccd37d4782fd426fb1a86fd3b03</md5>
          <size>662151</size>
          <filedate>1440019139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.8.tar.gz</download_link>
      <date>1440015839</date>
      <mdhash>629f8bc414026864fe4a235b62586e0c</mdhash>
      <filesize>428252</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>629f8bc414026864fe4a235b62586e0c</md5>
          <size>428252</size>
          <filedate>1440015839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>07ceed9e0c3c9a94ffee906627d75aad</md5>
          <size>659965</size>
          <filedate>1440015839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.7.tar.gz</download_link>
      <date>1426695781</date>
      <mdhash>81ab507b12875b02b88a0492d1173b99</mdhash>
      <filesize>426463</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81ab507b12875b02b88a0492d1173b99</md5>
          <size>426463</size>
          <filedate>1426695781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>4e47f8c599f0489c5873c193a29127f9</md5>
          <size>658215</size>
          <filedate>1426695781</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.6.tar.gz</download_link>
      <date>1422471481</date>
      <mdhash>e8999d5bd1748ee3dcd4fdc33a24c615</mdhash>
      <filesize>424622</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e8999d5bd1748ee3dcd4fdc33a24c615</md5>
          <size>424622</size>
          <filedate>1422471481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>b131f2090a66bada6a10edb46a60d0c5</md5>
          <size>655884</size>
          <filedate>1422471481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.6-rc1</name>
      <version>7.x-1.6-rc1</version>
      <tag>7.x-1.6-rc1</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.6-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.6-rc1.tar.gz</download_link>
      <date>1418961480</date>
      <mdhash>f3dc80ff7f3e29c73870f26a6ad02ff7</mdhash>
      <filesize>422180</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.6-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3dc80ff7f3e29c73870f26a6ad02ff7</md5>
          <size>422180</size>
          <filedate>1418961480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.6-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>93c4ac7efd7f8850e5657ceeb52c8d84</md5>
          <size>650880</size>
          <filedate>1418961480</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.5.tar.gz</download_link>
      <date>1416423482</date>
      <mdhash>0e90d8baaae78d3c37fe73dbe0d98dac</mdhash>
      <filesize>417508</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0e90d8baaae78d3c37fe73dbe0d98dac</md5>
          <size>417508</size>
          <filedate>1416423482</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3574bf6f27fe7ae35ca24f4202c929ec</md5>
          <size>642742</size>
          <filedate>1416423482</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.4.tar.gz</download_link>
      <date>1392220719</date>
      <mdhash>df1ce5aeb6a9a5c383d944a2a7494f18</mdhash>
      <filesize>415678</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df1ce5aeb6a9a5c383d944a2a7494f18</md5>
          <size>415678</size>
          <filedate>1392220719</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>dbde17e8df9f710d27fdd15d97396b75</md5>
          <size>633201</size>
          <filedate>1392220719</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.3</release_link>
      <download_link/>
      <date>1365013512</date>
      <mdhash>b27a7230f63a7a81db8cc480ab2dba63</mdhash>
      <filesize>413477</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>b27a7230f63a7a81db8cc480ab2dba63</md5>
          <size>413477</size>
          <filedate>1365013512</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>ce26992bba2fbce59b3c7acbb0e47969</md5>
          <size>626973</size>
          <filedate>1365013512</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.2.tar.gz</download_link>
      <date>1345319204</date>
      <mdhash>62fa855ee8aced1e355b09ac6cd54e99</mdhash>
      <filesize>407345</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>62fa855ee8aced1e355b09ac6cd54e99</md5>
          <size>407345</size>
          <filedate>1345319204</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d5ffe2c3301584b41f0ad6b820c047f</md5>
          <size>616151</size>
          <filedate>1345319204</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.1</release_link>
      <download_link/>
      <date>1344463011</date>
      <mdhash>c36cbb2fd7a6eb8b3245c13972380bd7</mdhash>
      <filesize>405567</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>c36cbb2fd7a6eb8b3245c13972380bd7</md5>
          <size>405567</size>
          <filedate>1344463011</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>f56ee3c874a6143f84c8c686f62f2570</md5>
          <size>613923</size>
          <filedate>1344463011</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0</release_link>
      <download_link/>
      <date>1332962446</date>
      <mdhash>642eca364e64b0074e5ad49f86627ac5</mdhash>
      <filesize>402254</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>642eca364e64b0074e5ad49f86627ac5</md5>
          <size>402254</size>
          <filedate>1332962446</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>694ac0d0775200adb41027b725255250</md5>
          <size>608054</size>
          <filedate>1332962446</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc2.tar.gz</download_link>
      <date>1332017742</date>
      <mdhash>791167a47cea014fad9018c93376aa88</mdhash>
      <filesize>396440</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>791167a47cea014fad9018c93376aa88</md5>
          <size>396440</size>
          <filedate>1332017742</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea9ed1cbced00c1bd64d65f7f816c740</md5>
          <size>603037</size>
          <filedate>1332017742</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc1.tar.gz</download_link>
      <date>1311894415</date>
      <mdhash>39bcea671210505409e3148c8eaefc0e</mdhash>
      <filesize>381078</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>39bcea671210505409e3148c8eaefc0e</md5>
          <size>381078</size>
          <filedate>1311894415</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>952d6ff9f89ca1dcc92b3839998c8a71</md5>
          <size>576200</size>
          <filedate>1311894416</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-beta1.tar.gz</download_link>
      <date>1306885315</date>
      <mdhash>96d8154bfab611eb59cbd3774aa94793</mdhash>
      <filesize>378287</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>96d8154bfab611eb59cbd3774aa94793</md5>
          <size>378287</size>
          <filedate>1306885315</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3b696ec73aab7fd194306f6b8954e5e5</md5>
          <size>573954</size>
          <filedate>1306885315</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1301099792</date>
      <mdhash>774f6654c149f192b11b136a06be5544</mdhash>
      <filesize>370763</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>774f6654c149f192b11b136a06be5544</md5>
          <size>370763</size>
          <filedate>1301099792</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0d9b8a47775cb1b6e5cd1f3cb55e27d</md5>
          <size>562274</size>
          <filedate>1301099792</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1300395967</date>
      <mdhash>1909846a7bd5775c99331c6564ffefa8</mdhash>
      <filesize>370238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1909846a7bd5775c99331c6564ffefa8</md5>
          <size>370238</size>
          <filedate>1300395967</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>b5952fe41114acb2afed3f8435aa586d</md5>
          <size>561811</size>
          <filedate>1300395967</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1294276865</date>
      <mdhash>30e0f5cb27949d2cd67b80bb36701d3e</mdhash>
      <filesize>521068</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>30e0f5cb27949d2cd67b80bb36701d3e</md5>
          <size>521068</size>
          <filedate>1294276865</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>620f26a406793b0fd87e05426ad346e8</md5>
          <size>763489</size>
          <filedate>1294276866</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1286838960</date>
      <mdhash>500a55a33f90b2bab865753a0d8c5439</mdhash>
      <filesize>510384</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>500a55a33f90b2bab865753a0d8c5439</md5>
          <size>510384</size>
          <filedate>1286838960</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea48dcae90dcad1ed1f9fa4459592a1e</md5>
          <size>740449</size>
          <filedate>1293230547</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ctools 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ctools/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ctools-7.x-1.x-dev.tar.gz</download_link>
      <date>1678252794</date>
      <mdhash>e3db2ceee069a7e5d89bd065108b8606</mdhash>
      <filesize>451808</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e3db2ceee069a7e5d89bd065108b8606</md5>
          <size>451808</size>
          <filedate>1678252794</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ctools-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f3adeb44c5873384a0a285dbdfc74249</md5>
          <size>702343</size>
          <filedate>1678252794</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
