<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Autocomplete Filters</title>
  <short_name>views_autocomplete_filters</short_name>
  <dc:creator>vasike</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/views_autocomplete_filters</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site search</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_autocomplete_filters 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/views_autocomplete_filters/-/releases/7.x-1.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67463648/packages/generic/views_autocomplete_filters/7.x-1.3/views_autocomplete_filters-7.x-1.3.tar.gz</download_link>
      <date>1740502097</date>
      <mdhash>435d02098d403c66eec0afe419ab4c17</mdhash>
      <filesize>13876</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67463648/packages/generic/views_autocomplete_filters/7.x-1.3/views_autocomplete_filters-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>435d02098d403c66eec0afe419ab4c17</md5>
          <size>13876</size>
          <filedate>1740502097</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67463648/packages/generic/views_autocomplete_filters/7.x-1.3/views_autocomplete_filters-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c15393a3bdafed8b45d93f9a5453d68</md5>
          <size>19970</size>
          <filedate>1740502097</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.2.tar.gz</download_link>
      <date>1426789381</date>
      <mdhash>87be5ad06c3c4c7e65513a72ab39ff97</mdhash>
      <filesize>12489</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>87be5ad06c3c4c7e65513a72ab39ff97</md5>
          <size>12489</size>
          <filedate>1426789381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0632f47df5cc8229e08e0ed997e672d1</md5>
          <size>17478</size>
          <filedate>1426789381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.1.tar.gz</download_link>
      <date>1393674805</date>
      <mdhash>09327d26fa7d31b3c81111d0bc95a977</mdhash>
      <filesize>12066</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>09327d26fa7d31b3c81111d0bc95a977</md5>
          <size>12066</size>
          <filedate>1393674805</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7515ba22e72d729b2d8e5d7aeca4551</md5>
          <size>14460</size>
          <filedate>1393674805</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0.tar.gz</download_link>
      <date>1374920488</date>
      <mdhash>b8940188e4a6e2cfd73cc9729dce866a</mdhash>
      <filesize>11481</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8940188e4a6e2cfd73cc9729dce866a</md5>
          <size>11481</size>
          <filedate>1374920488</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>c47e97c3097ea7424b8b98e062c51a87</md5>
          <size>13862</size>
          <filedate>1374920488</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-rc1.tar.gz</download_link>
      <date>1371288656</date>
      <mdhash>109cdf525dd71b92ae675cb50b74c1fb</mdhash>
      <filesize>11436</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>109cdf525dd71b92ae675cb50b74c1fb</md5>
          <size>11436</size>
          <filedate>1371288656</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>24d13864d68390e7794c472dccc9682a</md5>
          <size>13809</size>
          <filedate>1371288656</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta2.tar.gz</download_link>
      <date>1357634964</date>
      <mdhash>49adb7c90dc4fb3b9a9890c55f373860</mdhash>
      <filesize>11430</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>49adb7c90dc4fb3b9a9890c55f373860</md5>
          <size>11430</size>
          <filedate>1357634964</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>cb1d47faf5db3f4e8891c25beff516d4</md5>
          <size>13796</size>
          <filedate>1357634964</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta1.tar.gz</download_link>
      <date>1345705396</date>
      <mdhash>85622f7a66d44360de92ceff7e436986</mdhash>
      <filesize>11195</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>85622f7a66d44360de92ceff7e436986</md5>
          <size>11195</size>
          <filedate>1345705396</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>68da129698177417708f8ecddfc0995b</md5>
          <size>13553</size>
          <filedate>1345705396</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_autocomplete_filters 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_autocomplete_filters/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.x-dev.tar.gz</download_link>
      <date>1478434139</date>
      <mdhash>9870e55625eb8f1c46462102282fefe6</mdhash>
      <filesize>13615</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9870e55625eb8f1c46462102282fefe6</md5>
          <size>13615</size>
          <filedate>1478434139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_autocomplete_filters-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f5c8c668d86965bbcae130532edcbefc</md5>
          <size>19558</size>
          <filedate>1478434139</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
