<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>CKEditor 4 LTS - WYSIWYG HTML editor</title>
  <short_name>ckeditor_lts</short_name>
  <dc:creator>salmonek</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ckeditor_lts</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ckeditor_lts 7.x-1.26</name>
      <version>7.x-1.26</version>
      <tag>7.x-1.26</tag>
      <version_major>1</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_lts/releases/7.x-1.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.26.tar.gz</download_link>
      <date>1724245764</date>
      <mdhash>325296ff81d807a4cd9c3b97abd6e8ae</mdhash>
      <filesize>206828</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>325296ff81d807a4cd9c3b97abd6e8ae</md5>
          <size>206828</size>
          <filedate>1724245764</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>caa067e5b07b6a3e07938741ff944394</md5>
          <size>266498</size>
          <filedate>1724245764</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_lts 7.x-1.25</name>
      <version>7.x-1.25</version>
      <tag>7.x-1.25</tag>
      <version_major>1</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_lts/releases/7.x-1.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.25.tar.gz</download_link>
      <date>1707309290</date>
      <mdhash>c35f94470c979443c6bb0c6fa51999bc</mdhash>
      <filesize>206764</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c35f94470c979443c6bb0c6fa51999bc</md5>
          <size>206764</size>
          <filedate>1707309290</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>43e600eb0c2201eee404700b14319813</md5>
          <size>266427</size>
          <filedate>1707309290</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_lts 7.x-1.24</name>
      <version>7.x-1.24</version>
      <tag>7.x-1.24</tag>
      <version_major>1</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_lts/releases/7.x-1.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.24.tar.gz</download_link>
      <date>1704371445</date>
      <mdhash>9b3b748d2392003645e01ef5d74dc78b</mdhash>
      <filesize>206623</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9b3b748d2392003645e01ef5d74dc78b</md5>
          <size>206623</size>
          <filedate>1704371445</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_lts-7.x-1.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>14436adc95da9315f5dd1bca76434640</md5>
          <size>266312</size>
          <filedate>1704371445</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
  </releases>
</project>
