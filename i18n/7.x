<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Internationalization</title>
  <short_name>i18n</short_name>
  <dc:creator>jose reyero</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/i18n</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>i18n 7.x-1.35</name>
      <version>7.x-1.35</version>
      <tag>7.x-1.35</tag>
      <version_major>1</version_major>
      <version_patch>35</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.35</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.35.tar.gz</download_link>
      <date>1711304992</date>
      <mdhash>a5f92d29c08f48175a16948c57868da2</mdhash>
      <filesize>164046</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.35.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a5f92d29c08f48175a16948c57868da2</md5>
          <size>164046</size>
          <filedate>1711304992</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.35.zip</url>
          <archive_type>zip</archive_type>
          <md5>e7cafa0ddccf87dc12f61deacfb80b22</md5>
          <size>223616</size>
          <filedate>1711304992</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.34</name>
      <version>7.x-1.34</version>
      <tag>7.x-1.34</tag>
      <version_major>1</version_major>
      <version_patch>34</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.34</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.34.tar.gz</download_link>
      <date>1673540256</date>
      <mdhash>0b200fabcc77578412e2ce509e4373b0</mdhash>
      <filesize>162995</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.34.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b200fabcc77578412e2ce509e4373b0</md5>
          <size>162995</size>
          <filedate>1673540256</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.34.zip</url>
          <archive_type>zip</archive_type>
          <md5>17ed01f670fb2e91e7c44836a403c7ac</md5>
          <size>222320</size>
          <filedate>1673540256</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.33</name>
      <version>7.x-1.33</version>
      <tag>7.x-1.33</tag>
      <version_major>1</version_major>
      <version_patch>33</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.33</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.33.tar.gz</download_link>
      <date>1671097723</date>
      <mdhash>201d8425d0f1789a9ff059ed0c692fd9</mdhash>
      <filesize>162516</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.33.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>201d8425d0f1789a9ff059ed0c692fd9</md5>
          <size>162516</size>
          <filedate>1671097723</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.33.zip</url>
          <archive_type>zip</archive_type>
          <md5>cdd387ba24f7cf70c009e43440aa5c3b</md5>
          <size>221906</size>
          <filedate>1671097723</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.32</name>
      <version>7.x-1.32</version>
      <tag>7.x-1.32</tag>
      <version_major>1</version_major>
      <version_patch>32</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.32</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.32.tar.gz</download_link>
      <date>1670344815</date>
      <mdhash>a10b0988f24ca1acd53fccd07f4dc967</mdhash>
      <filesize>162413</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.32.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a10b0988f24ca1acd53fccd07f4dc967</md5>
          <size>162413</size>
          <filedate>1670344815</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.32.zip</url>
          <archive_type>zip</archive_type>
          <md5>37bb5a99ccb1e56c990f0dca2caa18f3</md5>
          <size>221776</size>
          <filedate>1670344815</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.31</name>
      <version>7.x-1.31</version>
      <tag>7.x-1.31</tag>
      <version_major>1</version_major>
      <version_patch>31</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.31</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.31.tar.gz</download_link>
      <date>1619398282</date>
      <mdhash>ff9067f6e80c3dff1d853f2bddda54de</mdhash>
      <filesize>162301</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.31.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ff9067f6e80c3dff1d853f2bddda54de</md5>
          <size>162301</size>
          <filedate>1619398282</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.31.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba8b71ebd4ab68650e1817e093c08521</md5>
          <size>221631</size>
          <filedate>1619398282</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.30</name>
      <version>7.x-1.30</version>
      <tag>7.x-1.30</tag>
      <version_major>1</version_major>
      <version_patch>30</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.30</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.30.tar.gz</download_link>
      <date>1619395431</date>
      <mdhash>682868dc74bb52ae73686f2fb235d4e1</mdhash>
      <filesize>160892</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.30.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>682868dc74bb52ae73686f2fb235d4e1</md5>
          <size>160892</size>
          <filedate>1619395431</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.30.zip</url>
          <archive_type>zip</archive_type>
          <md5>8599d1bc3215aa377f5bb384b17c685d</md5>
          <size>220270</size>
          <filedate>1619395431</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.29</name>
      <version>7.x-1.29</version>
      <tag>7.x-1.29</tag>
      <version_major>1</version_major>
      <version_patch>29</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.29</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.29.tar.gz</download_link>
      <date>1619299253</date>
      <mdhash>8ad5fca12cb65611e996e4e42331ddc6</mdhash>
      <filesize>160858</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.29.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8ad5fca12cb65611e996e4e42331ddc6</md5>
          <size>160858</size>
          <filedate>1619299253</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.29.zip</url>
          <archive_type>zip</archive_type>
          <md5>4eefc38debe624aacb9098d18b1f2aab</md5>
          <size>220256</size>
          <filedate>1619299253</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.28</name>
      <version>7.x-1.28</version>
      <tag>7.x-1.28</tag>
      <version_major>1</version_major>
      <version_patch>28</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.28</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.28.tar.gz</download_link>
      <date>1619298763</date>
      <mdhash>2cc6c75376240704c3b1966ddf9f3dd6</mdhash>
      <filesize>160911</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.28.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2cc6c75376240704c3b1966ddf9f3dd6</md5>
          <size>160911</size>
          <filedate>1619298763</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.28.zip</url>
          <archive_type>zip</archive_type>
          <md5>c84bb464e2620ea7bdbd184b35fb7dd4</md5>
          <size>220257</size>
          <filedate>1619298763</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.27</name>
      <version>7.x-1.27</version>
      <tag>7.x-1.27</tag>
      <version_major>1</version_major>
      <version_patch>27</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.27</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.27.tar.gz</download_link>
      <date>1592373388</date>
      <mdhash>d6f2e82865617f56f91b4ff90f901f71</mdhash>
      <filesize>160892</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.27.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d6f2e82865617f56f91b4ff90f901f71</md5>
          <size>160892</size>
          <filedate>1592373388</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.27.zip</url>
          <archive_type>zip</archive_type>
          <md5>a3838e8fc508da4d3fa36a8b5aefbf36</md5>
          <size>220257</size>
          <filedate>1592373388</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.26</name>
      <version>7.x-1.26</version>
      <tag>7.x-1.26</tag>
      <version_major>1</version_major>
      <version_patch>26</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.26</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.26.tar.gz</download_link>
      <date>1534531980</date>
      <mdhash>93197aa2b83164c3402fe4bb5e775371</mdhash>
      <filesize>160925</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.26.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>93197aa2b83164c3402fe4bb5e775371</md5>
          <size>160925</size>
          <filedate>1534531980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.26.zip</url>
          <archive_type>zip</archive_type>
          <md5>6ae1fe5752fbc16e25abf2b929f73200</md5>
          <size>220259</size>
          <filedate>1534531980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.25</name>
      <version>7.x-1.25</version>
      <tag>7.x-1.25</tag>
      <version_major>1</version_major>
      <version_patch>25</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.25</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.25.tar.gz</download_link>
      <date>1531342120</date>
      <mdhash>b9e07ae1d7d595372e0c34b18cb62871</mdhash>
      <filesize>160205</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.25.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9e07ae1d7d595372e0c34b18cb62871</md5>
          <size>160205</size>
          <filedate>1531342120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.25.zip</url>
          <archive_type>zip</archive_type>
          <md5>80773f39af0e9c40146228de278e8cff</md5>
          <size>219627</size>
          <filedate>1531342120</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.24</name>
      <version>7.x-1.24</version>
      <tag>7.x-1.24</tag>
      <version_major>1</version_major>
      <version_patch>24</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.24</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.24.tar.gz</download_link>
      <date>1524222484</date>
      <mdhash>0d640506d414d3a95773215dc5f42468</mdhash>
      <filesize>160238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.24.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d640506d414d3a95773215dc5f42468</md5>
          <size>160238</size>
          <filedate>1524222484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.24.zip</url>
          <archive_type>zip</archive_type>
          <md5>1016865da69066448389b6c6658f1e6c</md5>
          <size>219582</size>
          <filedate>1524222484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.22</name>
      <version>7.x-1.22</version>
      <tag>7.x-1.22</tag>
      <version_major>1</version_major>
      <version_patch>22</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.22</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.22.tar.gz</download_link>
      <date>1511689985</date>
      <mdhash>7c3abc1bb63a67acc75b77c020c739ea</mdhash>
      <filesize>159112</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.22.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7c3abc1bb63a67acc75b77c020c739ea</md5>
          <size>159112</size>
          <filedate>1511689985</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.22.zip</url>
          <archive_type>zip</archive_type>
          <md5>23cf50d4530639f6373336aaf381e34f</md5>
          <size>218460</size>
          <filedate>1511689985</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.20</name>
      <version>7.x-1.20</version>
      <tag>7.x-1.20</tag>
      <version_major>1</version_major>
      <version_patch>20</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.20</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.20.tar.gz</download_link>
      <date>1511587385</date>
      <mdhash>3d66579d2d9be345ff1c88f3d9808b82</mdhash>
      <filesize>159100</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.20.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3d66579d2d9be345ff1c88f3d9808b82</md5>
          <size>159100</size>
          <filedate>1511587385</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.20.zip</url>
          <archive_type>zip</archive_type>
          <md5>2904d3fdb91a2bf3ea372f5ada0c84bb</md5>
          <size>218466</size>
          <filedate>1511587385</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.19</name>
      <version>7.x-1.19</version>
      <tag>7.x-1.19</tag>
      <version_major>1</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.19.tar.gz</download_link>
      <date>1511586484</date>
      <mdhash>e4c0c7dd8a7725280470f6fc101a64e7</mdhash>
      <filesize>159082</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4c0c7dd8a7725280470f6fc101a64e7</md5>
          <size>159082</size>
          <filedate>1511586484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>9803208395e529547e98bbd3bf2f63c8</md5>
          <size>218440</size>
          <filedate>1511586484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.18</name>
      <version>7.x-1.18</version>
      <tag>7.x-1.18</tag>
      <version_major>1</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.18.tar.gz</download_link>
      <date>1497704042</date>
      <mdhash>e497647e0f4f8e53bfcfc3ff7cd3b5d7</mdhash>
      <filesize>158805</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e497647e0f4f8e53bfcfc3ff7cd3b5d7</md5>
          <size>158805</size>
          <filedate>1497704042</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>b19dc4707ecb075d510f1ee3a287ca81</md5>
          <size>218131</size>
          <filedate>1497704042</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.17</name>
      <version>7.x-1.17</version>
      <tag>7.x-1.17</tag>
      <version_major>1</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.17.tar.gz</download_link>
      <date>1492537742</date>
      <mdhash>82b1b4a5734383ebfa3e19dbafd12cd7</mdhash>
      <filesize>158762</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>82b1b4a5734383ebfa3e19dbafd12cd7</md5>
          <size>158762</size>
          <filedate>1492537742</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>5af32aa2f65bccd1609b55fd99b921bb</md5>
          <size>218275</size>
          <filedate>1492537742</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.16</name>
      <version>7.x-1.16</version>
      <tag>7.x-1.16</tag>
      <version_major>1</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.16.tar.gz</download_link>
      <date>1492535643</date>
      <mdhash>b322c08e58b0116332617faa8d38cba7</mdhash>
      <filesize>158373</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b322c08e58b0116332617faa8d38cba7</md5>
          <size>158373</size>
          <filedate>1492535643</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>794456ae5fb425f8caecf0a650478404</md5>
          <size>217831</size>
          <filedate>1492535643</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.15</name>
      <version>7.x-1.15</version>
      <tag>7.x-1.15</tag>
      <version_major>1</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.15.tar.gz</download_link>
      <date>1485834788</date>
      <mdhash>43ad0d8abda83139ab231add02e746a3</mdhash>
      <filesize>157783</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>43ad0d8abda83139ab231add02e746a3</md5>
          <size>157783</size>
          <filedate>1485834788</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>4d2ce2389bba9158ed2fc184475f5e52</md5>
          <size>217238</size>
          <filedate>1485834788</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.14</name>
      <version>7.x-1.14</version>
      <tag>7.x-1.14</tag>
      <version_major>1</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.14.tar.gz</download_link>
      <date>1477154940</date>
      <mdhash>51a67e9543afc1b901e7b34f41ed46f5</mdhash>
      <filesize>147525</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51a67e9543afc1b901e7b34f41ed46f5</md5>
          <size>147525</size>
          <filedate>1477154940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>377d159194eb7b8a37ba07eb1fb32889</md5>
          <size>201073</size>
          <filedate>1477154940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.13.tar.gz</download_link>
      <date>1430999882</date>
      <mdhash>9ba72bcc87f3eed9bef6f2eb38ae474d</mdhash>
      <filesize>146305</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9ba72bcc87f3eed9bef6f2eb38ae474d</md5>
          <size>146305</size>
          <filedate>1430999882</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>914e5fdbb71f8215dd34ea43765d79e9</md5>
          <size>199800</size>
          <filedate>1430999882</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.12.tar.gz</download_link>
      <date>1422286980</date>
      <mdhash>c1d7460ef3b7ee18b0e7788d98ba91be</mdhash>
      <filesize>145997</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c1d7460ef3b7ee18b0e7788d98ba91be</md5>
          <size>145997</size>
          <filedate>1422286980</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>63a2344342cec3e3f76b9a8aa44aef85</md5>
          <size>199479</size>
          <filedate>1422286980</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.11.tar.gz</download_link>
      <date>1397666962</date>
      <mdhash>b7949e640740e34be4be15a22281c5cf</mdhash>
      <filesize>144606</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b7949e640740e34be4be15a22281c5cf</md5>
          <size>144606</size>
          <filedate>1397666962</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>c7c8efdf9a81b3d20192e7f7d4c3c99c</md5>
          <size>195869</size>
          <filedate>1397666962</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.10.tar.gz</download_link>
      <date>1377069697</date>
      <mdhash>8ade3f3fc2e740ee3c5eae949e48eedf</mdhash>
      <filesize>142894</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8ade3f3fc2e740ee3c5eae949e48eedf</md5>
          <size>142894</size>
          <filedate>1377069697</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>255767d038edbb27a38b95343c3b9c80</md5>
          <size>193457</size>
          <filedate>1377069697</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.9.tar.gz</download_link>
      <date>1373385355</date>
      <mdhash>eb3b820b05203f790cc4e42ba109ce06</mdhash>
      <filesize>142609</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb3b820b05203f790cc4e42ba109ce06</md5>
          <size>142609</size>
          <filedate>1373385355</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>3419a78b21f9eaf43858dfff2d3f0f52</md5>
          <size>193130</size>
          <filedate>1373385355</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.8.tar.gz</download_link>
      <date>1358075002</date>
      <mdhash>6b2d3d4df7ebe6bc6392699c4c95b5b4</mdhash>
      <filesize>140957</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b2d3d4df7ebe6bc6392699c4c95b5b4</md5>
          <size>140957</size>
          <filedate>1358075002</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>78c8fef2d5f55b89c74ec13c362078ec</md5>
          <size>191109</size>
          <filedate>1358075002</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.7.tar.gz</download_link>
      <date>1341236501</date>
      <mdhash>14a8e0cc7596ed9744b2e2d62f67d6f2</mdhash>
      <filesize>138498</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14a8e0cc7596ed9744b2e2d62f67d6f2</md5>
          <size>138498</size>
          <filedate>1341236501</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>19aa413e87ec32fd6fbea88e15ec3197</md5>
          <size>187426</size>
          <filedate>1341236501</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.6.tar.gz</download_link>
      <date>1340225780</date>
      <mdhash>0eeb8b2d91ecc9ce1e1d6df6614f5350</mdhash>
      <filesize>138494</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0eeb8b2d91ecc9ce1e1d6df6614f5350</md5>
          <size>138494</size>
          <filedate>1340225780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>700707f75e9fdaeaced00b5a3bb30a67</md5>
          <size>187399</size>
          <filedate>1340225780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.5.tar.gz</download_link>
      <date>1335000084</date>
      <mdhash>754960703013ecbfe8d9fdb06f16df58</mdhash>
      <filesize>138516</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>754960703013ecbfe8d9fdb06f16df58</md5>
          <size>138516</size>
          <filedate>1335000084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf82f055dda68475c4b1c27306cc7b8f</md5>
          <size>187718</size>
          <filedate>1335000084</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.4.tar.gz</download_link>
      <date>1328469343</date>
      <mdhash>11b5c3a01c65cb434a2398685923b5f2</mdhash>
      <filesize>134767</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>11b5c3a01c65cb434a2398685923b5f2</md5>
          <size>134767</size>
          <filedate>1328469343</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>8cde2e170ceaf5b79a7ff5b2dee19f1d</md5>
          <size>183064</size>
          <filedate>1328469343</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.3.tar.gz</download_link>
      <date>1326276941</date>
      <mdhash>c1a688a15bc7183d7c2d0a1260822911</mdhash>
      <filesize>133142</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c1a688a15bc7183d7c2d0a1260822911</md5>
          <size>133142</size>
          <filedate>1326276941</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>76e9b82f26adc9f151a150326efb5b86</md5>
          <size>181254</size>
          <filedate>1326276941</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.2.tar.gz</download_link>
      <date>1322474442</date>
      <mdhash>b25c24e442bae244ffaf028a9f085343</mdhash>
      <filesize>129943</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b25c24e442bae244ffaf028a9f085343</md5>
          <size>129943</size>
          <filedate>1322474442</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fe297133793dd7f7c79a88936e8a5c15</md5>
          <size>176376</size>
          <filedate>1322474442</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.1.tar.gz</download_link>
      <date>1318100203</date>
      <mdhash>45c6464b8128732dd3cbf93d949b46c3</mdhash>
      <filesize>129532</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>45c6464b8128732dd3cbf93d949b46c3</md5>
          <size>129532</size>
          <filedate>1318100203</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fa342998be099e75249b0b55a6fcb41b</md5>
          <size>175957</size>
          <filedate>1318100203</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0.tar.gz</download_link>
      <date>1313936520</date>
      <mdhash>ddc6efedc7e412c13adba4bd3444dd7f</mdhash>
      <filesize>126819</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ddc6efedc7e412c13adba4bd3444dd7f</md5>
          <size>126819</size>
          <filedate>1313936520</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>ad0a1b58d37521ee1cf80ddb1bfa0b5b</md5>
          <size>173344</size>
          <filedate>1313936520</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc3.tar.gz</download_link>
      <date>1312832516</date>
      <mdhash>f506797c2f66c1d3621adcabd6689fb5</mdhash>
      <filesize>126457</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f506797c2f66c1d3621adcabd6689fb5</md5>
          <size>126457</size>
          <filedate>1312832516</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f8011b062f46d9abfca0b86b89c6a1f1</md5>
          <size>172744</size>
          <filedate>1312832516</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc2.tar.gz</download_link>
      <date>1311532319</date>
      <mdhash>732737b342324de2ed2c5e1b9e77c24c</mdhash>
      <filesize>125375</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>732737b342324de2ed2c5e1b9e77c24c</md5>
          <size>125375</size>
          <filedate>1311532319</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>88778b5157ed57bc921d7fd1923414a4</md5>
          <size>171189</size>
          <filedate>1311532320</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc1.tar.gz</download_link>
      <date>1310466117</date>
      <mdhash>85ccc65b6e54338fa849591cfa2a6740</mdhash>
      <filesize>124377</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>85ccc65b6e54338fa849591cfa2a6740</md5>
          <size>124377</size>
          <filedate>1310466117</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7465eccad276f699a5d247838c9f26f0</md5>
          <size>169825</size>
          <filedate>1310466117</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta8</name>
      <version>7.x-1.0-beta8</version>
      <tag>7.x-1.0-beta8</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta8.tar.gz</download_link>
      <date>1309858916</date>
      <mdhash>8b891fef746bd5adc216ce158e04e072</mdhash>
      <filesize>122698</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b891fef746bd5adc216ce158e04e072</md5>
          <size>122698</size>
          <filedate>1309858916</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>cdee35139859d3d8ae24b7daa96293c4</md5>
          <size>168079</size>
          <filedate>1309858916</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta7.tar.gz</download_link>
      <date>1307806316</date>
      <mdhash>89027827dafff6d87818c5206774dd65</mdhash>
      <filesize>118216</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89027827dafff6d87818c5206774dd65</md5>
          <size>118216</size>
          <filedate>1307806316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>d73fd00ad5b66b7797d1128ebf365404</md5>
          <size>162893</size>
          <filedate>1307806316</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta6.tar.gz</download_link>
      <date>1306227416</date>
      <mdhash>2180948e651d4d528853e2a732401149</mdhash>
      <filesize>112115</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2180948e651d4d528853e2a732401149</md5>
          <size>112115</size>
          <filedate>1306227416</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>350c3ced2b4e11ca643b49f1523538bf</md5>
          <size>154044</size>
          <filedate>1306227416</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta5.tar.gz</download_link>
      <date>1304158615</date>
      <mdhash>8bcbe4378b330c4d8b6ad521ab10d055</mdhash>
      <filesize>93396</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8bcbe4378b330c4d8b6ad521ab10d055</md5>
          <size>93396</size>
          <filedate>1304158615</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ce02ed5700fd016743f2bfa99cde11b</md5>
          <size>128224</size>
          <filedate>1304158615</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta4.tar.gz</download_link>
      <date>1301683568</date>
      <mdhash>4649fcd7e3be0a36cb3c3a499d528600</mdhash>
      <filesize>93145</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4649fcd7e3be0a36cb3c3a499d528600</md5>
          <size>93145</size>
          <filedate>1301683568</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>374cc80389612c624fc8475cb21d2074</md5>
          <size>127768</size>
          <filedate>1301683568</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta3.tar.gz</download_link>
      <date>1300145467</date>
      <mdhash>af324f4684f56fdfc9728a5755a4ef67</mdhash>
      <filesize>97481</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af324f4684f56fdfc9728a5755a4ef67</md5>
          <size>97481</size>
          <filedate>1300145467</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7289b85ebeb86e8fbc529829d685d7b2</md5>
          <size>132920</size>
          <filedate>1300145468</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta2.tar.gz</download_link>
      <date>1298892072</date>
      <mdhash>108d90910b6aae8f5cf17433838ae26d</mdhash>
      <filesize>97012</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>108d90910b6aae8f5cf17433838ae26d</md5>
          <size>97012</size>
          <filedate>1298892072</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>96b952012b83ebc479612876a687fd0d</md5>
          <size>132491</size>
          <filedate>1298892072</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta1.tar.gz</download_link>
      <date>1298539000</date>
      <mdhash>2c4d61513eeb7aa1c3a1c6710a6cfe36</mdhash>
      <filesize>97944</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2c4d61513eeb7aa1c3a1c6710a6cfe36</md5>
          <size>97944</size>
          <filedate>1298539000</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e0b0a239fce8823c1492a80053a9151a</md5>
          <size>137638</size>
          <filedate>1298539000</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1298138679</date>
      <mdhash>574a1cb4e3a607ef1f56775b2741e32a</mdhash>
      <filesize>91045</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>574a1cb4e3a607ef1f56775b2741e32a</md5>
          <size>91045</size>
          <filedate>1298138679</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d401e277d42d3ea2c30ccb7716f8d52</md5>
          <size>127513</size>
          <filedate>1298138679</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1297683731</date>
      <mdhash>ecc35a3641cc80c278c5f3899867b65f</mdhash>
      <filesize>85227</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ecc35a3641cc80c278c5f3899867b65f</md5>
          <size>85227</size>
          <filedate>1297683731</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>63509c7dd2b7a1a228164930f77b2ef7</md5>
          <size>116702</size>
          <filedate>1297683732</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1294773669</date>
      <mdhash>d4a69a4cdc9dce950fe8f785203888fc</mdhash>
      <filesize>80850</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d4a69a4cdc9dce950fe8f785203888fc</md5>
          <size>80850</size>
          <filedate>1294773669</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e38c10b26a131119056821113f9dd236</md5>
          <size>107123</size>
          <filedate>1294773669</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>i18n 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/i18n/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/i18n-7.x-1.x-dev.tar.gz</download_link>
      <date>1711307668</date>
      <mdhash>72db71cd6d1f13dcddba947a3edee348</mdhash>
      <filesize>164037</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>72db71cd6d1f13dcddba947a3edee348</md5>
          <size>164037</size>
          <filedate>1711307668</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/i18n-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c5b1302b09d44d54902c67c625d6ef52</md5>
          <size>223689</size>
          <filedate>1711307668</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
