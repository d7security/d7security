<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>OAuth2 LoginProvider</title>
  <short_name>oauth2_loginprovider</short_name>
  <dc:creator>dashohoxha</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/oauth2_loginprovider</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>oauth2_loginprovider 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.11.tar.gz</download_link>
      <date>1503581044</date>
      <mdhash>ce9841745c00200d091a2068c9161d96</mdhash>
      <filesize>12648</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce9841745c00200d091a2068c9161d96</md5>
          <size>12648</size>
          <filedate>1503581044</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>c528ee07f1f44094664a0a22b5190284</md5>
          <size>17045</size>
          <filedate>1503581044</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.10.tar.gz</download_link>
      <date>1503524644</date>
      <mdhash>eddf73b6a636783ccb42fceab020b272</mdhash>
      <filesize>12152</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eddf73b6a636783ccb42fceab020b272</md5>
          <size>12152</size>
          <filedate>1503524644</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>b4912f6129067133ea8d36fddeae6f57</md5>
          <size>16154</size>
          <filedate>1503524644</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.9.tar.gz</download_link>
      <date>1479650939</date>
      <mdhash>0b1c8978cef5f2228c2c634bee8d3c0d</mdhash>
      <filesize>12164</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b1c8978cef5f2228c2c634bee8d3c0d</md5>
          <size>12164</size>
          <filedate>1479650939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>3da31c798ef7812de9fb7c78fd0869e5</md5>
          <size>16152</size>
          <filedate>1479650939</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.8.tar.gz</download_link>
      <date>1462490940</date>
      <mdhash>9727f3aac3d9301d54d2b93f09dd0909</mdhash>
      <filesize>12104</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9727f3aac3d9301d54d2b93f09dd0909</md5>
          <size>12104</size>
          <filedate>1462490940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>14bbc59a165386574ae6c52390c12bd7</md5>
          <size>16086</size>
          <filedate>1462490940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.7.tar.gz</download_link>
      <date>1456478302</date>
      <mdhash>69247cfe1d4ceea0c5225fa6f260629f</mdhash>
      <filesize>12076</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69247cfe1d4ceea0c5225fa6f260629f</md5>
          <size>12076</size>
          <filedate>1456478302</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f0c3b3f556c335d48a590dd10791510</md5>
          <size>16046</size>
          <filedate>1456478302</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.6.tar.gz</download_link>
      <date>1442407739</date>
      <mdhash>f69ce48fdbc406f38c652506b6db8358</mdhash>
      <filesize>12149</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f69ce48fdbc406f38c652506b6db8358</md5>
          <size>12149</size>
          <filedate>1442407739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>b97d9eac294be460458fdc2766f8e9c0</md5>
          <size>16143</size>
          <filedate>1442407739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.5.tar.gz</download_link>
      <date>1440865739</date>
      <mdhash>dc724dff8a437884c224686ee18be190</mdhash>
      <filesize>12106</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc724dff8a437884c224686ee18be190</md5>
          <size>12106</size>
          <filedate>1440865739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d5d46aed65e302134cf22700643a3f0f</md5>
          <size>16091</size>
          <filedate>1440865739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.4.tar.gz</download_link>
      <date>1430228582</date>
      <mdhash>82fa7ae6fcde5ddc738aad179dfcb275</mdhash>
      <filesize>12025</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>82fa7ae6fcde5ddc738aad179dfcb275</md5>
          <size>12025</size>
          <filedate>1430228582</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>c0a0a84cbfe82dbb752622b6788da686</md5>
          <size>15986</size>
          <filedate>1430228582</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.3.tar.gz</download_link>
      <date>1426189981</date>
      <mdhash>b93dfb1c30dd8f7c0ddf9abe02bf475b</mdhash>
      <filesize>12018</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b93dfb1c30dd8f7c0ddf9abe02bf475b</md5>
          <size>12018</size>
          <filedate>1426189981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>e187b83bfb13033bfee1e274bb005d5c</md5>
          <size>15982</size>
          <filedate>1426189981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.2.tar.gz</download_link>
      <date>1402235627</date>
      <mdhash>8d28cc40a7f2089da902b9cdab76a065</mdhash>
      <filesize>12000</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8d28cc40a7f2089da902b9cdab76a065</md5>
          <size>12000</size>
          <filedate>1402235627</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3bd785d4570330d06839e95d61d73825</md5>
          <size>15762</size>
          <filedate>1402235627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.0.tar.gz</download_link>
      <date>1395266056</date>
      <mdhash>cd0ca8d0ca64c4306cb8c4e219846ac8</mdhash>
      <filesize>11664</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cd0ca8d0ca64c4306cb8c4e219846ac8</md5>
          <size>11664</size>
          <filedate>1395266056</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>5165aa47c5415980832939ff59c1c7d1</md5>
          <size>15375</size>
          <filedate>1395266056</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>oauth2_loginprovider 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/oauth2_loginprovider/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.x-dev.tar.gz</download_link>
      <date>1727095904</date>
      <mdhash>934e6f1050a25eb2f3a7d55f77de7ed4</mdhash>
      <filesize>12674</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>934e6f1050a25eb2f3a7d55f77de7ed4</md5>
          <size>12674</size>
          <filedate>1727095904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/oauth2_loginprovider-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>72901bf144bdc46eaf01a84b4f2bcde9</md5>
          <size>17056</size>
          <filedate>1727095904</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
