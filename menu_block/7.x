<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Block</title>
  <short_name>menu_block</short_name>
  <dc:creator>johnalbin</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/menu_block</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_block 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.9.tar.gz</download_link>
      <date>1642573784</date>
      <mdhash>42532a7627ee3651699972c71c747ccc</mdhash>
      <filesize>34119</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42532a7627ee3651699972c71c747ccc</md5>
          <size>34119</size>
          <filedate>1642573784</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>22862d35445b0daf2ae6e30519d10f9a</md5>
          <size>42955</size>
          <filedate>1642573784</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.8.tar.gz</download_link>
      <date>1543950480</date>
      <mdhash>836c6dbcb9e19294ec1807a823cb1706</mdhash>
      <filesize>34097</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>836c6dbcb9e19294ec1807a823cb1706</md5>
          <size>34097</size>
          <filedate>1543950480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>560523c8e0257ecd7f25d689ca8cbd9f</md5>
          <size>42925</size>
          <filedate>1543950480</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.7.tar.gz</download_link>
      <date>1435675672</date>
      <mdhash>83e01370ea5d503b47c8f888c10afc17</mdhash>
      <filesize>34548</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83e01370ea5d503b47c8f888c10afc17</md5>
          <size>34548</size>
          <filedate>1435675672</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>0be63d23634c7bb6dd6c17527e8ce272</md5>
          <size>42475</size>
          <filedate>1435675672</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.6.tar.gz</download_link>
      <date>1433189581</date>
      <mdhash>5d8ec2eb1aaff9368fc750c7473b9e6e</mdhash>
      <filesize>34554</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d8ec2eb1aaff9368fc750c7473b9e6e</md5>
          <size>34554</size>
          <filedate>1433189581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>6352a7e816257484504ad62f4fa43e45</md5>
          <size>42473</size>
          <filedate>1433189581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.5.tar.gz</download_link>
      <date>1423092481</date>
      <mdhash>3b526989bc533179bf278ed4f3ee7a11</mdhash>
      <filesize>37280</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3b526989bc533179bf278ed4f3ee7a11</md5>
          <size>37280</size>
          <filedate>1423092481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>b3135287122632ff7dad2107fee092ab</md5>
          <size>45685</size>
          <filedate>1423092481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.4.tar.gz</download_link>
      <date>1399238027</date>
      <mdhash>4cafb670f61ff14e533a25036b2ac556</mdhash>
      <filesize>34992</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4cafb670f61ff14e533a25036b2ac556</md5>
          <size>34992</size>
          <filedate>1399238027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f0ee34460b1568041457eaea4a71e717</md5>
          <size>43878</size>
          <filedate>1399238027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.3.tar.gz</download_link>
      <date>1328286646</date>
      <mdhash>4d307621d347d2245031ca8b64244069</mdhash>
      <filesize>34795</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4d307621d347d2245031ca8b64244069</md5>
          <size>34795</size>
          <filedate>1328286646</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6df73b874385c218659ee41fa944891d</md5>
          <size>42649</size>
          <filedate>1328286646</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.2.tar.gz</download_link>
      <date>1299683174</date>
      <mdhash>8220095bb1c9a1f64134a0d8e4700008</mdhash>
      <filesize>34966</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8220095bb1c9a1f64134a0d8e4700008</md5>
          <size>34966</size>
          <filedate>1299683174</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>065332a0f8b5618671f2cd7a758cefae</md5>
          <size>43665</size>
          <filedate>1299683174</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.1.tar.gz</download_link>
      <date>1296950749</date>
      <mdhash>475ba491a9322bc39d884ae273df2ebd</mdhash>
      <filesize>34325</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>475ba491a9322bc39d884ae273df2ebd</md5>
          <size>34325</size>
          <filedate>1296950749</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e1cd4a49425d2fa82767d6e709a65f2</md5>
          <size>44241</size>
          <filedate>1296950749</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0.tar.gz</download_link>
      <date>1294728069</date>
      <mdhash>028fa357e5ea17820db72bf944c7f321</mdhash>
      <filesize>34553</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>028fa357e5ea17820db72bf944c7f321</md5>
          <size>34553</size>
          <filedate>1294728069</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b3b6ebb8bff9d8ad7522d3d0c10942f8</md5>
          <size>44467</size>
          <filedate>1294728069</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_block 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta4.tar.gz</download_link>
      <date>1285708862</date>
      <mdhash>3a08cc3cc607d96c0c3985eec66079a5</mdhash>
      <filesize>38659</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3a08cc3cc607d96c0c3985eec66079a5</md5>
          <size>38659</size>
          <filedate>1285708862</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9533a1cac7cebf1f0bf72ba8241ac9b0</md5>
          <size>53018</size>
          <filedate>1293232923</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_block 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta3.tar.gz</download_link>
      <date>1285704962</date>
      <mdhash>742f97eccb1be8781c8632a57e62b6c4</mdhash>
      <filesize>38611</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>742f97eccb1be8781c8632a57e62b6c4</md5>
          <size>38611</size>
          <filedate>1285704962</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bdac5bbb3c029a1032707dc7ae089e6d</md5>
          <size>52971</size>
          <filedate>1293232924</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_block 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta2.tar.gz</download_link>
      <date>1271435107</date>
      <mdhash>e930e08d4e52edc43fba75fc0b104455</mdhash>
      <filesize>37374</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e930e08d4e52edc43fba75fc0b104455</md5>
          <size>37374</size>
          <filedate>1271435107</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9bc67c760672775a7856cffa59d63752</md5>
          <size>52080</size>
          <filedate>1293232922</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_block 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta1.tar.gz</download_link>
      <date>1269641717</date>
      <mdhash>1572b2226be88146a6e7c7d7b293c876</mdhash>
      <filesize>35580</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1572b2226be88146a6e7c7d7b293c876</md5>
          <size>35580</size>
          <filedate>1269641717</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6db780196735c936495e769b07665496</md5>
          <size>49316</size>
          <filedate>1293232923</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_block 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-3.x-dev.tar.gz</download_link>
      <date>1444680239</date>
      <mdhash>ec99b56953154b5e1aa55eec4cca3ddd</mdhash>
      <filesize>34541</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec99b56953154b5e1aa55eec4cca3ddd</md5>
          <size>34541</size>
          <filedate>1444680239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4169d9787c1798bfa2521a5d63789aee</md5>
          <size>42480</size>
          <filedate>1444680239</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>menu_block 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_block/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_block-7.x-2.x-dev.tar.gz</download_link>
      <date>1715036731</date>
      <mdhash>5d4e4954ff89b0a09deb0181432bde2d</mdhash>
      <filesize>34229</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d4e4954ff89b0a09deb0181432bde2d</md5>
          <size>34229</size>
          <filedate>1715036731</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_block-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>3f6f9e23f58f6cf07087c6e97541fa29</md5>
          <size>43013</size>
          <filedate>1715036731</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
