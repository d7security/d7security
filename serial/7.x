<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Serial Field</title>
  <short_name>serial</short_name>
  <dc:creator>colan</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/serial</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>serial 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.8.tar.gz</download_link>
      <date>1472678339</date>
      <mdhash>21e681a3db623b954559054c24c0b31e</mdhash>
      <filesize>13030</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>21e681a3db623b954559054c24c0b31e</md5>
          <size>13030</size>
          <filedate>1472678339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>88ed809e7c6f922d10b4994a8e025575</md5>
          <size>15294</size>
          <filedate>1472678339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.7.tar.gz</download_link>
      <date>1461253739</date>
      <mdhash>0b2c66d0384e2c3e4f936cd0f4b5b54b</mdhash>
      <filesize>13017</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b2c66d0384e2c3e4f936cd0f4b5b54b</md5>
          <size>13017</size>
          <filedate>1461253739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>8b1caa62222d0c5a66c01796a3abed95</md5>
          <size>15287</size>
          <filedate>1461253739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.6.tar.gz</download_link>
      <date>1461164340</date>
      <mdhash>05bebf7609dd2a420544eecbfc7adeb8</mdhash>
      <filesize>13029</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>05bebf7609dd2a420544eecbfc7adeb8</md5>
          <size>13029</size>
          <filedate>1461164340</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>b544cdd3792229edc80a129f985adcdb</md5>
          <size>15282</size>
          <filedate>1461164340</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.5.tar.gz</download_link>
      <date>1448542439</date>
      <mdhash>026f366bb005ca4852323573c23f4f3f</mdhash>
      <filesize>10493</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>026f366bb005ca4852323573c23f4f3f</md5>
          <size>10493</size>
          <filedate>1448542439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ef18911b1924a70d2757cccaf3032b16</md5>
          <size>11676</size>
          <filedate>1448542439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.4.tar.gz</download_link>
      <date>1432025881</date>
      <mdhash>3f4a49c08b3363cfb512092fad421233</mdhash>
      <filesize>10492</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3f4a49c08b3363cfb512092fad421233</md5>
          <size>10492</size>
          <filedate>1432025881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>9715c83bb082f3f2c4ded83f18979870</md5>
          <size>11671</size>
          <filedate>1432025881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.3.tar.gz</download_link>
      <date>1381844527</date>
      <mdhash>5a097508ae668ebab03807436a894017</mdhash>
      <filesize>10967</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5a097508ae668ebab03807436a894017</md5>
          <size>10967</size>
          <filedate>1381844527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>abe869829c72fd3bc52e691dc76be1c7</md5>
          <size>12113</size>
          <filedate>1381844527</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.2.tar.gz</download_link>
      <date>1320468935</date>
      <mdhash>7f7e39993a797903e3fbe7036f4b5496</mdhash>
      <filesize>10690</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7f7e39993a797903e3fbe7036f4b5496</md5>
          <size>10690</size>
          <filedate>1320468935</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9f68ad6b4f49abb4182c29f654df48ab</md5>
          <size>11895</size>
          <filedate>1320468935</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.1.tar.gz</download_link>
      <date>1319644838</date>
      <mdhash>c9c4b872784c4a0ea8bb9a479d201327</mdhash>
      <filesize>11188</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c9c4b872784c4a0ea8bb9a479d201327</md5>
          <size>11188</size>
          <filedate>1319644838</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8513103b97344f83bd953e7fef1ea64</md5>
          <size>12862</size>
          <filedate>1319644838</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.0.tar.gz</download_link>
      <date>1296823063</date>
      <mdhash>8496452fc4c6c0cc653e4e9b7eb73b2b</mdhash>
      <filesize>10275</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8496452fc4c6c0cc653e4e9b7eb73b2b</md5>
          <size>10275</size>
          <filedate>1296823063</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>2d059e124600f3f73b8a0739c193ae48</md5>
          <size>12152</size>
          <filedate>1296823063</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>serial 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.0-rc1.tar.gz</download_link>
      <date>1296825435</date>
      <mdhash>ee7a6d2def7c7204773373549b9d1145</mdhash>
      <filesize>10126</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ee7a6d2def7c7204773373549b9d1145</md5>
          <size>10126</size>
          <filedate>1296825435</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>006e06fd72c9a299cfa75fa5295ab4a3</md5>
          <size>12204</size>
          <filedate>1296825435</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>serial 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/serial/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/serial-7.x-1.x-dev.tar.gz</download_link>
      <date>1461616439</date>
      <mdhash>37c2f78e9d5c463beba1644118860a1e</mdhash>
      <filesize>13027</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>37c2f78e9d5c463beba1644118860a1e</md5>
          <size>13027</size>
          <filedate>1461616439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/serial-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf5e4be3cd5edb83805c4d86abcdae8d</md5>
          <size>15298</size>
          <filedate>1461616439</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
