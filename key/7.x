<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Key</title>
  <short_name>key</short_name>
  <dc:creator>crashtest_</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/key</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>key 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.4.tar.gz</download_link>
      <date>1474601039</date>
      <mdhash>43c762d3c8c98ab02b91b659b15cab10</mdhash>
      <filesize>29149</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>43c762d3c8c98ab02b91b659b15cab10</md5>
          <size>29149</size>
          <filedate>1474601039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4a1c436ec327a781d50bf1be7790e91f</md5>
          <size>42953</size>
          <filedate>1474601039</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.3.tar.gz</download_link>
      <date>1474559778</date>
      <mdhash>3daa566a3200b763cfa46b5d8a068887</mdhash>
      <filesize>29153</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3daa566a3200b763cfa46b5d8a068887</md5>
          <size>29153</size>
          <filedate>1474559778</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>23fac75b1c0c03dd2282461247850a8c</md5>
          <size>42954</size>
          <filedate>1474559778</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.2.tar.gz</download_link>
      <date>1456523339</date>
      <mdhash>256759da1fa45300cf275cb9918d0b67</mdhash>
      <filesize>23805</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>256759da1fa45300cf275cb9918d0b67</md5>
          <size>23805</size>
          <filedate>1456523339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>66a5830474fde29f5427966b432ffae1</md5>
          <size>33159</size>
          <filedate>1456523339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.1.tar.gz</download_link>
      <date>1456007039</date>
      <mdhash>6cd475391bc59d7d39d12be34abe9e54</mdhash>
      <filesize>23804</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6cd475391bc59d7d39d12be34abe9e54</md5>
          <size>23804</size>
          <filedate>1456007039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>69d506b32d40ecc6bb33a9f3a19301f8</md5>
          <size>33157</size>
          <filedate>1456007039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.0.tar.gz</download_link>
      <date>1453825439</date>
      <mdhash>27e292ab9657ce3eefd28a5901f1a77f</mdhash>
      <filesize>23344</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>27e292ab9657ce3eefd28a5901f1a77f</md5>
          <size>23344</size>
          <filedate>1453825439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>ffe92b6476658efef4a33e0547485522</md5>
          <size>32057</size>
          <filedate>1453825439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-2.0.tar.gz</download_link>
      <date>1445448539</date>
      <mdhash>92ccf47edf079dbfdb26a41e45b5314c</mdhash>
      <filesize>16573</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92ccf47edf079dbfdb26a41e45b5314c</md5>
          <size>16573</size>
          <filedate>1445448539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>bd4d7ccec91e2b73fd9c8a6c5e63ffd5</md5>
          <size>20662</size>
          <filedate>1445448539</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta7.tar.gz</download_link>
      <date>1440190439</date>
      <mdhash>2109bc4238d15f0ad23f04b489d0cdd7</mdhash>
      <filesize>18640</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2109bc4238d15f0ad23f04b489d0cdd7</md5>
          <size>18640</size>
          <filedate>1440190439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba31d05b19fd5961d121e31e7b9e1498</md5>
          <size>23989</size>
          <filedate>1440190439</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta6.tar.gz</download_link>
      <date>1438018739</date>
      <mdhash>bf58febe0939f48dd90771effa845d19</mdhash>
      <filesize>18603</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf58febe0939f48dd90771effa845d19</md5>
          <size>18603</size>
          <filedate>1438018739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>4cd9c48153be51fccc082649424952e1</md5>
          <size>24014</size>
          <filedate>1438018739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta5.tar.gz</download_link>
      <date>1437174839</date>
      <mdhash>664f7d6a7a6900ce5bcd7d5c2a132151</mdhash>
      <filesize>18575</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>664f7d6a7a6900ce5bcd7d5c2a132151</md5>
          <size>18575</size>
          <filedate>1437174839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>319c59fe17cbf1fecb3a0a0c2d50e9d0</md5>
          <size>23990</size>
          <filedate>1437174839</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta4.tar.gz</download_link>
      <date>1431545281</date>
      <mdhash>8ae4ee0ab229f436db1443867b2359c2</mdhash>
      <filesize>18156</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8ae4ee0ab229f436db1443867b2359c2</md5>
          <size>18156</size>
          <filedate>1431545281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>56c3e4e7e83b40be3d2361e1cde55a17</md5>
          <size>23173</size>
          <filedate>1431545281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta3.tar.gz</download_link>
      <date>1428602281</date>
      <mdhash>905f40b50e83f7ef01baec79160ed881</mdhash>
      <filesize>18424</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>905f40b50e83f7ef01baec79160ed881</md5>
          <size>18424</size>
          <filedate>1428602281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>209d170687f535651c53bc79058c093f</md5>
          <size>27661</size>
          <filedate>1428602281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta2.tar.gz</download_link>
      <date>1426703882</date>
      <mdhash>31d21f906c0c01ccbd02cfc7fc4688e7</mdhash>
      <filesize>16139</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>31d21f906c0c01ccbd02cfc7fc4688e7</md5>
          <size>16139</size>
          <filedate>1426703882</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fda958ff041b1eeb77fcb68ea04ff9a1</md5>
          <size>20568</size>
          <filedate>1426703882</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta1.tar.gz</download_link>
      <date>1424904181</date>
      <mdhash>06f16556b969f0f66291eb846d1618a3</mdhash>
      <filesize>15998</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>06f16556b969f0f66291eb846d1618a3</md5>
          <size>15998</size>
          <filedate>1424904181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba3d8461081f1796851a76b2621c8216</md5>
          <size>20747</size>
          <filedate>1424904181</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-3.x-dev.tar.gz</download_link>
      <date>1474600139</date>
      <mdhash>9927f69d70dd3e6fc98a55802a791ae8</mdhash>
      <filesize>29162</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9927f69d70dd3e6fc98a55802a791ae8</md5>
          <size>29162</size>
          <filedate>1474600139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8c9fdba1180464f6ca5160821b40eaa6</md5>
          <size>42962</size>
          <filedate>1474600139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-2.x-dev.tar.gz</download_link>
      <date>1446161040</date>
      <mdhash>b57c21cdaaa65abf80a328945b3c5279</mdhash>
      <filesize>17184</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b57c21cdaaa65abf80a328945b3c5279</md5>
          <size>17184</size>
          <filedate>1446161040</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>86ca571dca118579dfedaad0e7021f33</md5>
          <size>23050</size>
          <filedate>1446161040</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>key 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/key/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/key-7.x-1.x-dev.tar.gz</download_link>
      <date>1439860441</date>
      <mdhash>5d1bb12aa1f848e062f74c7959252b74</mdhash>
      <filesize>18639</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d1bb12aa1f848e062f74c7959252b74</md5>
          <size>18639</size>
          <filedate>1439860441</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/key-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a07f0fe38c75cce38be2ff187793ce65</md5>
          <size>24002</size>
          <filedate>1439860441</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
