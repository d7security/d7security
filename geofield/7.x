<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Geofield</title>
  <short_name>geofield</short_name>
  <dc:creator>phayes</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/geofield</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>geofield 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.5.tar.gz</download_link>
      <date>1683999477</date>
      <mdhash>c8f31fa02e263ed3e193c3be0de0ec83</mdhash>
      <filesize>60234</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c8f31fa02e263ed3e193c3be0de0ec83</md5>
          <size>60234</size>
          <filedate>1683999477</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>79e2f8c71c9578712d992ea608af3a07</md5>
          <size>85718</size>
          <filedate>1683999477</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.4.tar.gz</download_link>
      <date>1540417981</date>
      <mdhash>7e7ae930fc29f1e540a652d70dc58e1c</mdhash>
      <filesize>60193</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e7ae930fc29f1e540a652d70dc58e1c</md5>
          <size>60193</size>
          <filedate>1540417981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>d3b6f5618ffc5d0f6cc9d6cb68805b66</md5>
          <size>85695</size>
          <filedate>1540417981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.3.tar.gz</download_link>
      <date>1411337631</date>
      <mdhash>94eae6025b93addeb314330c2f867898</mdhash>
      <filesize>57511</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>94eae6025b93addeb314330c2f867898</md5>
          <size>57511</size>
          <filedate>1411337631</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>2e96d01b4c1daecfa804350fb0f547da</md5>
          <size>81355</size>
          <filedate>1411337631</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.2.tar.gz</download_link>
      <date>1411332528</date>
      <mdhash>a37853fbda616fa57c9a6474ed672408</mdhash>
      <filesize>57506</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a37853fbda616fa57c9a6474ed672408</md5>
          <size>57506</size>
          <filedate>1411332528</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4ae85c0a6506777540624e125fe45a8</md5>
          <size>81342</size>
          <filedate>1411332528</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.1.tar.gz</download_link>
      <date>1384708104</date>
      <mdhash>a8b92d879f45c9cc1a562b43fb42e88d</mdhash>
      <filesize>56463</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a8b92d879f45c9cc1a562b43fb42e88d</md5>
          <size>56463</size>
          <filedate>1384708104</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f47cab44136805a18d36652df09741fb</md5>
          <size>78593</size>
          <filedate>1384708104</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.0.tar.gz</download_link>
      <date>1379874471</date>
      <mdhash>d23142ffd9f2b0b7db9304e5cc38e529</mdhash>
      <filesize>56166</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d23142ffd9f2b0b7db9304e5cc38e529</md5>
          <size>56166</size>
          <filedate>1379874471</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>be270d729af90f999d6aa9e76f155c41</md5>
          <size>78634</size>
          <filedate>1379874471</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-beta1.tar.gz</download_link>
      <date>1373168451</date>
      <mdhash>1912d5bc5733011dc4bfcf43f2425ea2</mdhash>
      <filesize>55557</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1912d5bc5733011dc4bfcf43f2425ea2</md5>
          <size>55557</size>
          <filedate>1373168451</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ecc85a5b201908dd48bedc11433e0713</md5>
          <size>77829</size>
          <filedate>1373168452</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1354769552</date>
      <mdhash>281e7e69f442cb16d0af85f08ef19cf6</mdhash>
      <filesize>51871</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>281e7e69f442cb16d0af85f08ef19cf6</md5>
          <size>51871</size>
          <filedate>1354769552</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>67ba37797872819bfce9094e6080649c</md5>
          <size>72488</size>
          <filedate>1354769552</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1352085418</date>
      <mdhash>8a7e714c035a9d9b16b3ec5cf3e3300c</mdhash>
      <filesize>49126</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8a7e714c035a9d9b16b3ec5cf3e3300c</md5>
          <size>49126</size>
          <filedate>1352085418</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>62d9a93d5fa5b55af0ee7431009b890d</md5>
          <size>66745</size>
          <filedate>1352085419</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.2.tar.gz</download_link>
      <date>1372735859</date>
      <mdhash>14fa10d1739da270856181dfb9ed77ef</mdhash>
      <filesize>33370</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>14fa10d1739da270856181dfb9ed77ef</md5>
          <size>33370</size>
          <filedate>1372735859</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e83fdff780c4a792d6a2c1f9254f8715</md5>
          <size>42754</size>
          <filedate>1372735859</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.1.tar.gz</download_link>
      <date>1338941478</date>
      <mdhash>dd2c7e633cd898ce4baea90c11ef7af2</mdhash>
      <filesize>32605</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dd2c7e633cd898ce4baea90c11ef7af2</md5>
          <size>32605</size>
          <filedate>1338941478</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf599560a811cda8bec16326add59472</md5>
          <size>41555</size>
          <filedate>1338941478</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0.tar.gz</download_link>
      <date>1330810243</date>
      <mdhash>4990f12d8be977c26bc43f0afd6f4399</mdhash>
      <filesize>33197</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4990f12d8be977c26bc43f0afd6f4399</md5>
          <size>33197</size>
          <filedate>1330810243</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>76cad963f1970b4629aee3ab16a5c1f1</md5>
          <size>42228</size>
          <filedate>1330810243</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-rc1.tar.gz</download_link>
      <date>1328414750</date>
      <mdhash>159b0bd815568cd8efce5ff7dc331fd6</mdhash>
      <filesize>33107</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>159b0bd815568cd8efce5ff7dc331fd6</md5>
          <size>33107</size>
          <filedate>1328414750</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1581dbb0e7ade8a3a2590d25b9e61789</md5>
          <size>42146</size>
          <filedate>1328414750</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta2.tar.gz</download_link>
      <date>1321549840</date>
      <mdhash>6906ff5bab0e1de849a659c85584d51a</mdhash>
      <filesize>30250</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6906ff5bab0e1de849a659c85584d51a</md5>
          <size>30250</size>
          <filedate>1321549840</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b0a58e65b055c5a7e6aea9072adaaccd</md5>
          <size>38880</size>
          <filedate>1321549840</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta1.tar.gz</download_link>
      <date>1320344131</date>
      <mdhash>f3b4512a2cf1862ff6960b5ca4015153</mdhash>
      <filesize>30261</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3b4512a2cf1862ff6960b5ca4015153</md5>
          <size>30261</size>
          <filedate>1320344131</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c53ba07637c0740816ee6bfa5d5db6b2</md5>
          <size>38920</size>
          <filedate>1320344131</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1315579902</date>
      <mdhash>924462fbf47ae0faefe77f5a4609df63</mdhash>
      <filesize>22615</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>924462fbf47ae0faefe77f5a4609df63</md5>
          <size>22615</size>
          <filedate>1315579902</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>17a0a7fc0d162f7c242e9149f90a387c</md5>
          <size>30318</size>
          <filedate>1315579902</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1314385322</date>
      <mdhash>4f0498b2017577b42b0c581625936dfb</mdhash>
      <filesize>19839</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4f0498b2017577b42b0c581625936dfb</md5>
          <size>19839</size>
          <filedate>1314385322</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf1657582d0cb162480772adeecee5f6</md5>
          <size>25013</size>
          <filedate>1314385322</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1303525015</date>
      <mdhash>4fe13d519c331f6e383bc9a5ef213c9f</mdhash>
      <filesize>18473</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4fe13d519c331f6e383bc9a5ef213c9f</md5>
          <size>18473</size>
          <filedate>1303525015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f57da6667b49fff48b5facc344c15fd9</md5>
          <size>22960</size>
          <filedate>1303525016</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1302668521</date>
      <mdhash>83e1e916ccf5780a07a4dd6dbe87d14f</mdhash>
      <filesize>16496</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83e1e916ccf5780a07a4dd6dbe87d14f</md5>
          <size>16496</size>
          <filedate>1302668521</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>62d3f202523f304a113885fd7ee939c1</md5>
          <size>20702</size>
          <filedate>1302668521</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1300170368</date>
      <mdhash>f8fe4d53d7b87e74ebed946ce524cf92</mdhash>
      <filesize>15927</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f8fe4d53d7b87e74ebed946ce524cf92</md5>
          <size>15927</size>
          <filedate>1300170368</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3146188095248b38b478a7d706da4133</md5>
          <size>20178</size>
          <filedate>1300170368</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-2.x-dev.tar.gz</download_link>
      <date>1698671659</date>
      <mdhash>7581d298fa874a119f09daf85a555099</mdhash>
      <filesize>60246</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7581d298fa874a119f09daf85a555099</md5>
          <size>60246</size>
          <filedate>1698671659</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b714563028ebed8ade109c406085eb1</md5>
          <size>85743</size>
          <filedate>1698671659</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>geofield 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/geofield/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/geofield-7.x-1.x-dev.tar.gz</download_link>
      <date>1380580692</date>
      <mdhash>6d5226dba1aa40df5bdf398eab10a1a8</mdhash>
      <filesize>33475</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6d5226dba1aa40df5bdf398eab10a1a8</md5>
          <size>33475</size>
          <filedate>1380580692</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/geofield-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>da1a1555628f00cded6ad5274eb4422f</md5>
          <size>42860</size>
          <filedate>1380580692</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
