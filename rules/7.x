<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Rules</title>
  <short_name>rules</short_name>
  <dc:creator>fago</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/rules</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Automation</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>rules 7.x-2.14</name>
      <version>7.x-2.14</version>
      <tag>7.x-2.14</tag>
      <version_major>2</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.14.tar.gz</download_link>
      <date>1691009300</date>
      <mdhash>85cd81148805196a9ff798cf9ac76851</mdhash>
      <filesize>195832</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>85cd81148805196a9ff798cf9ac76851</md5>
          <size>195832</size>
          <filedate>1691009300</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>465d43ec4f8d450ede638b4acd2c5db9</md5>
          <size>239741</size>
          <filedate>1691009300</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.13</name>
      <version>7.x-2.13</version>
      <tag>7.x-2.13</tag>
      <version_major>2</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.13.tar.gz</download_link>
      <date>1638479564</date>
      <mdhash>8a1470429b1fef7d29ca558e03dda0e6</mdhash>
      <filesize>195634</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8a1470429b1fef7d29ca558e03dda0e6</md5>
          <size>195634</size>
          <filedate>1638479564</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4adedd0bc98e03b14dd4a4cbfa78ec9</md5>
          <size>239556</size>
          <filedate>1638479564</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.12</name>
      <version>7.x-2.12</version>
      <tag>7.x-2.12</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.12.tar.gz</download_link>
      <date>1548305581</date>
      <mdhash>51d0b9dff4b9b14bafc82f28b103c508</mdhash>
      <filesize>191870</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51d0b9dff4b9b14bafc82f28b103c508</md5>
          <size>191870</size>
          <filedate>1548305581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>b309bfbcc7c9bba9d491d84372575972</md5>
          <size>234200</size>
          <filedate>1548305581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.11.tar.gz</download_link>
      <date>1526653384</date>
      <mdhash>68ea2a0c43464dd93841b89ac34b3d21</mdhash>
      <filesize>187914</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>68ea2a0c43464dd93841b89ac34b3d21</md5>
          <size>187914</size>
          <filedate>1526653384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>4b4d69f0be26520bda9aa6830ee246e8</md5>
          <size>228786</size>
          <filedate>1526653384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.10.tar.gz</download_link>
      <date>1492697942</date>
      <mdhash>b66045ee97acdec07b4808f649e9dcc8</mdhash>
      <filesize>187783</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b66045ee97acdec07b4808f649e9dcc8</md5>
          <size>187783</size>
          <filedate>1492697942</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>40a9b74a61e00f792fce6d073069f600</md5>
          <size>228581</size>
          <filedate>1492697942</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.9.tar.gz</download_link>
      <date>1426527181</date>
      <mdhash>ce4e3363c37cb00f4caf0abd3f38f38a</mdhash>
      <filesize>188880</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ce4e3363c37cb00f4caf0abd3f38f38a</md5>
          <size>188880</size>
          <filedate>1426527181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>6a170f4b2a8d49ea30002e44b6db402e</md5>
          <size>228377</size>
          <filedate>1426527181</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.8.tar.gz</download_link>
      <date>1420733280</date>
      <mdhash>c84d88caa5ba178efa689a1a71d3a144</mdhash>
      <filesize>187857</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c84d88caa5ba178efa689a1a71d3a144</md5>
          <size>187857</size>
          <filedate>1420733280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>254dc7166c94318f65327a993739bc63</md5>
          <size>226701</size>
          <filedate>1420733280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.7.tar.gz</download_link>
      <date>1399041227</date>
      <mdhash>f28b8811918dd16952acca967d09dcc0</mdhash>
      <filesize>186255</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f28b8811918dd16952acca967d09dcc0</md5>
          <size>186255</size>
          <filedate>1399041227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>493ce8c551a025a9886181959a5719ff</md5>
          <size>223084</size>
          <filedate>1399041227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.6.tar.gz</download_link>
      <date>1383063053</date>
      <mdhash>152c1f330f91f83d358b478253a8c042</mdhash>
      <filesize>186022</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>152c1f330f91f83d358b478253a8c042</md5>
          <size>186022</size>
          <filedate>1383063053</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>5cec66aa484a6d23dd2ca1db84ca5b7f</md5>
          <size>222363</size>
          <filedate>1383063053</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.5.tar.gz</download_link>
      <date>1379579047</date>
      <mdhash>5387542c8c5cd2de9682d61fafb57314</mdhash>
      <filesize>185061</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5387542c8c5cd2de9682d61fafb57314</md5>
          <size>185061</size>
          <filedate>1379579047</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>2a1ab75be564f2318ea88d584df8ffe7</md5>
          <size>221245</size>
          <filedate>1379579048</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.4.tar.gz</download_link>
      <date>1379354606</date>
      <mdhash>e19b2c10f96c8a680afe9e4f65db4e5c</mdhash>
      <filesize>184071</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e19b2c10f96c8a680afe9e4f65db4e5c</md5>
          <size>184071</size>
          <filedate>1379354606</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>0f0921e0dfbb530083d218891002946c</md5>
          <size>219839</size>
          <filedate>1379354607</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.3</release_link>
      <download_link/>
      <date>1364401819</date>
      <mdhash>9af9fbdc593158638603832c633432b1</mdhash>
      <filesize>170892</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>9af9fbdc593158638603832c633432b1</md5>
          <size>170892</size>
          <filedate>1364401819</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>29c9b74f15fbc6c45a00992558087745</md5>
          <size>204217</size>
          <filedate>1364401819</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.2.tar.gz</download_link>
      <date>1343980733</date>
      <mdhash>91799bff2cf6690937e056734265458c</mdhash>
      <filesize>168867</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>91799bff2cf6690937e056734265458c</md5>
          <size>168867</size>
          <filedate>1343980733</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>13c16131d6ef02731c3843e64ff60583</md5>
          <size>202120</size>
          <filedate>1343980734</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.1.tar.gz</download_link>
      <date>1331918148</date>
      <mdhash>a7f15c72df435554790d5611a8b1732e</mdhash>
      <filesize>167042</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a7f15c72df435554790d5611a8b1732e</md5>
          <size>167042</size>
          <filedate>1331918148</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc75f7c1c9fd4a6718aff56444f73012</md5>
          <size>200411</size>
          <filedate>1331918148</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0.tar.gz</download_link>
      <date>1318336335</date>
      <mdhash>efe3f31a4d21b9fa8757cd692dc8f641</mdhash>
      <filesize>155077</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>efe3f31a4d21b9fa8757cd692dc8f641</md5>
          <size>155077</size>
          <filedate>1318336335</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>199643162d18d901d8610680de985c4d</md5>
          <size>185543</size>
          <filedate>1318336335</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>rules 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc2.tar.gz</download_link>
      <date>1316339806</date>
      <mdhash>9a9d7de26f666d5af62fc90612551f64</mdhash>
      <filesize>152243</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a9d7de26f666d5af62fc90612551f64</md5>
          <size>152243</size>
          <filedate>1316339806</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c9615634da003486a48a1e6810c7287e</md5>
          <size>182497</size>
          <filedate>1316339806</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc1.tar.gz</download_link>
      <date>1313600220</date>
      <mdhash>0c01319958aed5439bd7e22d5f060ab0</mdhash>
      <filesize>150814</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0c01319958aed5439bd7e22d5f060ab0</md5>
          <size>150814</size>
          <filedate>1313600220</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e8db87016549adba4df29162ab68beaa</md5>
          <size>180965</size>
          <filedate>1313600220</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta3.tar.gz</download_link>
      <date>1312908119</date>
      <mdhash>c0d8f94ec44834355ab2acd02eb01781</mdhash>
      <filesize>146211</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c0d8f94ec44834355ab2acd02eb01781</md5>
          <size>146211</size>
          <filedate>1312908119</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea53922c84235618ac986227d478bf1e</md5>
          <size>176235</size>
          <filedate>1312908119</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta2.tar.gz</download_link>
      <date>1308241620</date>
      <mdhash>fd1bdc36bc612913dfba91e7bdeda00d</mdhash>
      <filesize>139063</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd1bdc36bc612913dfba91e7bdeda00d</md5>
          <size>139063</size>
          <filedate>1308241620</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3dc68367e90e2939c9ffb0493ae49b1d</md5>
          <size>168596</size>
          <filedate>1308241620</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta1.tar.gz</download_link>
      <date>1300896369</date>
      <mdhash>7b9718415365872eca4a2f7b9f50787a</mdhash>
      <filesize>134506</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7b9718415365872eca4a2f7b9f50787a</md5>
          <size>134506</size>
          <filedate>1300896369</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4e5dd72c400db729ade19626b9f3aec</md5>
          <size>162817</size>
          <filedate>1300896369</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-alpha5</name>
      <version>7.x-2.0-alpha5</version>
      <tag>7.x-2.0-alpha5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-alpha5</release_link>
      <download_link/>
      <date>1297877847</date>
      <mdhash>cf14a81dca5d26d4ab392bc14c0c23a6</mdhash>
      <filesize>124733</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>cf14a81dca5d26d4ab392bc14c0c23a6</md5>
          <size>124733</size>
          <filedate>1297877847</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>c48acf5c05d0d151188242df3541c292</md5>
          <size>156397</size>
          <filedate>1297877847</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-alpha4</name>
      <version>7.x-2.0-alpha4</version>
      <tag>7.x-2.0-alpha4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-alpha4</release_link>
      <download_link/>
      <date>1294228281</date>
      <mdhash>3e6905f82d9c9e1ab462ab0c90afe3e9</mdhash>
      <filesize>113547</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>3e6905f82d9c9e1ab462ab0c90afe3e9</md5>
          <size>113547</size>
          <filedate>1294228281</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>709b25d0b78d42f01c3fabf2cd22b890</md5>
          <size>143662</size>
          <filedate>1294228281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-alpha3</name>
      <version>7.x-2.0-alpha3</version>
      <tag>7.x-2.0-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.0-alpha3.tar.gz</download_link>
      <date>1291637070</date>
      <mdhash>92f674a49f40ddd9ffb8de5b0e91425f</mdhash>
      <filesize>108782</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92f674a49f40ddd9ffb8de5b0e91425f</md5>
          <size>108782</size>
          <filedate>1291637070</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5855706f9f38407af6a30363e38b3ff8</md5>
          <size>135222</size>
          <filedate>1293234343</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-alpha2</release_link>
      <download_link/>
      <date>1286798162</date>
      <mdhash>57e3e8298fff1c6f7cf6af8a0a3c3309</mdhash>
      <filesize>106373</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>57e3e8298fff1c6f7cf6af8a0a3c3309</md5>
          <size>106373</size>
          <filedate>1286798162</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>084d7bae3b691c879c1ad2bedba3b181</md5>
          <size>132178</size>
          <filedate>1293234345</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.0-alpha1</release_link>
      <download_link/>
      <date>1283932308</date>
      <mdhash>103e926a3b79276e6a7ea62b12a7f2a1</mdhash>
      <filesize>104650</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>103e926a3b79276e6a7ea62b12a7f2a1</md5>
          <size>104650</size>
          <filedate>1283932308</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>a35c6a59acb7db7d1226ab5bf3bb23fb</md5>
          <size>130242</size>
          <filedate>1293234343</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1286792763</date>
      <mdhash>c49d85d2bb2fb860580a79e3f930b5c1</mdhash>
      <filesize>106374</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c49d85d2bb2fb860580a79e3f930b5c1</md5>
          <size>106374</size>
          <filedate>1286792763</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>705099a5939da248f190f25c9e129d39</md5>
          <size>132180</size>
          <filedate>1293234347</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-2.x-dev.tar.gz</download_link>
      <date>1721451649</date>
      <mdhash>9465a3f01bf2d5a3e1c060a8846e2a9b</mdhash>
      <filesize>197040</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9465a3f01bf2d5a3e1c060a8846e2a9b</md5>
          <size>197040</size>
          <filedate>1721451649</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>57ec86f64d77e64ec8ae9e52f25f9d30</md5>
          <size>241145</size>
          <filedate>1721451649</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>rules 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/rules/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/rules-7.x-1.x-dev.tar.gz</download_link>
      <date>1380626241</date>
      <mdhash>a1a7d855e52429da95024a30a55a635a</mdhash>
      <filesize>7108</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a1a7d855e52429da95024a30a55a635a</md5>
          <size>7108</size>
          <filedate>1380626241</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/rules-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e6b85ec223ed064beb4fbcae718189b5</md5>
          <size>7311</size>
          <filedate>1380626241</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
