<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Automated Logout</title>
  <short_name>autologout</short_name>
  <dc:creator>jrglasgow</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/autologout</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>autologout 7.x-4.6</name>
      <version>7.x-4.6</version>
      <tag>7.x-4.6</tag>
      <version_major>4</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.6.tar.gz</download_link>
      <date>1586345248</date>
      <mdhash>b8b93bdf3fc3ce86cb37be4c3c1295a4</mdhash>
      <filesize>23782</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8b93bdf3fc3ce86cb37be4c3c1295a4</md5>
          <size>23782</size>
          <filedate>1586345248</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>47feed91df59091384ae3eec4d9b478f</md5>
          <size>27941</size>
          <filedate>1586345248</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.5</name>
      <version>7.x-4.5</version>
      <tag>7.x-4.5</tag>
      <version_major>4</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.5.tar.gz</download_link>
      <date>1509524286</date>
      <mdhash>660c162b20ea9319d858dbb2d9e83096</mdhash>
      <filesize>22673</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>660c162b20ea9319d858dbb2d9e83096</md5>
          <size>22673</size>
          <filedate>1509524286</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d37dc54415d1c635cf1585b48cdbdff6</md5>
          <size>26922</size>
          <filedate>1509524286</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.4</name>
      <version>7.x-4.4</version>
      <tag>7.x-4.4</tag>
      <version_major>4</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.4.tar.gz</download_link>
      <date>1441543439</date>
      <mdhash>eb7901f94e74258c563e0e424828ca35</mdhash>
      <filesize>21906</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb7901f94e74258c563e0e424828ca35</md5>
          <size>21906</size>
          <filedate>1441543439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>01880eb805cd7bc65f65659b211d7acd</md5>
          <size>26047</size>
          <filedate>1441543439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.3</name>
      <version>7.x-4.3</version>
      <tag>7.x-4.3</tag>
      <version_major>4</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.3.tar.gz</download_link>
      <date>1383644005</date>
      <mdhash>986edaed9d15480d6e6034be6dfcb569</mdhash>
      <filesize>21618</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>986edaed9d15480d6e6034be6dfcb569</md5>
          <size>21618</size>
          <filedate>1383644005</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>8cd661cf45ee580ead9e33c8cae5d97e</md5>
          <size>25079</size>
          <filedate>1383644005</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.2</name>
      <version>7.x-4.2</version>
      <tag>7.x-4.2</tag>
      <version_major>4</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.2.tar.gz</download_link>
      <date>1370421357</date>
      <mdhash>93b0dff98de195bacbf5c4f954970354</mdhash>
      <filesize>20181</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>93b0dff98de195bacbf5c4f954970354</md5>
          <size>20181</size>
          <filedate>1370421357</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>4942bd6ed6899366c1f26c981528d815</md5>
          <size>23525</size>
          <filedate>1370421358</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.1</name>
      <version>7.x-4.1</version>
      <tag>7.x-4.1</tag>
      <version_major>4</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.1.tar.gz</download_link>
      <date>1369248612</date>
      <mdhash>01b12eaad83757e7b8e370a271f3ce28</mdhash>
      <filesize>17599</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>01b12eaad83757e7b8e370a271f3ce28</md5>
          <size>17599</size>
          <filedate>1369248612</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c30f6d40d17d5ea66dd8cd0fd66bdec0</md5>
          <size>20795</size>
          <filedate>1369248612</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.0.tar.gz</download_link>
      <date>1343655102</date>
      <mdhash>7d0148b7acd3abbf48fe22fe7551c40a</mdhash>
      <filesize>13012</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d0148b7acd3abbf48fe22fe7551c40a</md5>
          <size>13012</size>
          <filedate>1343655102</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>850af2ce238bfd4953a72e7f0d1df2ba</md5>
          <size>15150</size>
          <filedate>1343655102</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autologout 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-2.0-beta1.tar.gz</download_link>
      <date>1335196872</date>
      <mdhash>67408565be01f6d6b1f475f56c89c404</mdhash>
      <filesize>15020</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>67408565be01f6d6b1f475f56c89c404</md5>
          <size>15020</size>
          <filedate>1335196872</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8bd7e62b68cc05d64d27eca3a4cff38</md5>
          <size>17390</size>
          <filedate>1335196872</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autologout 7.x-5.x-dev</name>
      <version>7.x-5.x-dev</version>
      <tag>7.x-5.x</tag>
      <version_major>5</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-5.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-5.x-dev.tar.gz</download_link>
      <date>1449182039</date>
      <mdhash>79dece86a710c81c3ffd05ffbc445274</mdhash>
      <filesize>7127</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-5.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>79dece86a710c81c3ffd05ffbc445274</md5>
          <size>7127</size>
          <filedate>1449182039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-5.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>bac63d4a3793a7dd4457f1babc3c688e</md5>
          <size>7429</size>
          <filedate>1449182039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autologout 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-4.x-dev.tar.gz</download_link>
      <date>1585903886</date>
      <mdhash>e8ed6869ddac68722833a21aecc18646</mdhash>
      <filesize>23784</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e8ed6869ddac68722833a21aecc18646</md5>
          <size>23784</size>
          <filedate>1585903886</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>2761d7911b89238b8527187aa2f74318</md5>
          <size>27947</size>
          <filedate>1585903886</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autologout 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autologout/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autologout-7.x-2.x-dev.tar.gz</download_link>
      <date>1380554876</date>
      <mdhash>5c6edb32e36082c0973c8b41a4c29854</mdhash>
      <filesize>15003</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5c6edb32e36082c0973c8b41a4c29854</md5>
          <size>15003</size>
          <filedate>1380554876</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autologout-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>4025a407b0d303fabebce8da0c49638b</md5>
          <size>17373</size>
          <filedate>1380554876</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
