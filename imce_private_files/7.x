<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>IMCE Private Files</title>
  <short_name>imce_private_files</short_name>
  <dc:creator>ldpm</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/imce_private_files</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>imce_private_files 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce_private_files/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.2.tar.gz</download_link>
      <date>1445958539</date>
      <mdhash>3c2a742177f0f7c70c38fa1c42f3a2c7</mdhash>
      <filesize>11578</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3c2a742177f0f7c70c38fa1c42f3a2c7</md5>
          <size>11578</size>
          <filedate>1445958539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7832708491752ce7d72c3b91fc2bdf09</md5>
          <size>13417</size>
          <filedate>1445958539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce_private_files 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce_private_files/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.1.tar.gz</download_link>
      <date>1442612039</date>
      <mdhash>52c5d67c81f7228102a3f302f818bc27</mdhash>
      <filesize>11467</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>52c5d67c81f7228102a3f302f818bc27</md5>
          <size>11467</size>
          <filedate>1442612039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fdf3182b7d1887cfc783ac13d7e5403b</md5>
          <size>13285</size>
          <filedate>1442612039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce_private_files 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce_private_files/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.0.tar.gz</download_link>
      <date>1436376539</date>
      <mdhash>5a7804c71cdfb2cf25e8453dd88e738c</mdhash>
      <filesize>11458</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5a7804c71cdfb2cf25e8453dd88e738c</md5>
          <size>11458</size>
          <filedate>1436376539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>90d25f23cc455380b0bfe993899b9e3c</md5>
          <size>13264</size>
          <filedate>1436376539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imce_private_files 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imce_private_files/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.x-dev.tar.gz</download_link>
      <date>1445957339</date>
      <mdhash>5619132744b888bc34afad526683321a</mdhash>
      <filesize>11580</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5619132744b888bc34afad526683321a</md5>
          <size>11580</size>
          <filedate>1445957339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imce_private_files-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e04d0834ff84a5c57320ef416f2b6f84</md5>
          <size>13422</size>
          <filedate>1445957339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
