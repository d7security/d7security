<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Search API Database Search</title>
  <short_name>search_api_db</short_name>
  <dc:creator>drunken monkey</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/search_api_db</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site search</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>search_api_db 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.9.tar.gz</download_link>
      <date>1698922242</date>
      <mdhash>eb283022829f9c251b599e2cd505513c</mdhash>
      <filesize>39621</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eb283022829f9c251b599e2cd505513c</md5>
          <size>39621</size>
          <filedate>1698922242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc8e2060f351c86096abd2ce6469c39e</md5>
          <size>42868</size>
          <filedate>1698922242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.8.tar.gz</download_link>
      <date>1610364552</date>
      <mdhash>6cc74bb32ac6b073b98eaa8664c36cc6</mdhash>
      <filesize>39540</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6cc74bb32ac6b073b98eaa8664c36cc6</md5>
          <size>39540</size>
          <filedate>1610364552</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>89815c45c3a1349a4dd70b7fa45fe908</md5>
          <size>42768</size>
          <filedate>1610364552</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.7.tar.gz</download_link>
      <date>1537173480</date>
      <mdhash>31d8feab58593cf7d7fba6ffbffd7170</mdhash>
      <filesize>39012</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>31d8feab58593cf7d7fba6ffbffd7170</md5>
          <size>39012</size>
          <filedate>1537173480</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>46883f820512ea7ec114f19080b7530d</md5>
          <size>42288</size>
          <filedate>1537173480</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.6.tar.gz</download_link>
      <date>1487844783</date>
      <mdhash>f1d282d1993f7b0586952bb927e8c313</mdhash>
      <filesize>38356</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f1d282d1993f7b0586952bb927e8c313</md5>
          <size>38356</size>
          <filedate>1487844783</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>431dd15d927781a8fa85e96a6a534fb8</md5>
          <size>41624</size>
          <filedate>1487844783</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.5.tar.gz</download_link>
      <date>1440962639</date>
      <mdhash>b41d20e189557bcf11926ea0fc80e74c</mdhash>
      <filesize>36795</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b41d20e189557bcf11926ea0fc80e74c</md5>
          <size>36795</size>
          <filedate>1440962639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>2ef93618e6d7e7262e21c9e0111806d7</md5>
          <size>39893</size>
          <filedate>1440962639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.4.tar.gz</download_link>
      <date>1410185628</date>
      <mdhash>ea1c383f27e61d1b4e5266fb4e2ffaf7</mdhash>
      <filesize>35981</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ea1c383f27e61d1b4e5266fb4e2ffaf7</md5>
          <size>35981</size>
          <filedate>1410185628</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>1e2fb42de5433c56f64ed1aea5bbe07c</md5>
          <size>39053</size>
          <filedate>1410185628</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.3.tar.gz</download_link>
      <date>1400837927</date>
      <mdhash>b9b34b4e5822c0bde8ccabe5368b2baa</mdhash>
      <filesize>32934</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b9b34b4e5822c0bde8ccabe5368b2baa</md5>
          <size>32934</size>
          <filedate>1400837927</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>dc25ccaec389a97c839203650c5efcd6</md5>
          <size>35373</size>
          <filedate>1400837927</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.2.tar.gz</download_link>
      <date>1387968804</date>
      <mdhash>1bf9fd4d6b22e991a6904e3da9d15e19</mdhash>
      <filesize>28351</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1bf9fd4d6b22e991a6904e3da9d15e19</md5>
          <size>28351</size>
          <filedate>1387968804</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0d69819a7f5cd8a596101df7edcf46d7</md5>
          <size>30754</size>
          <filedate>1387968804</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.1.tar.gz</download_link>
      <date>1386584304</date>
      <mdhash>78160ac36ece6d9235c424a6ef4f0da4</mdhash>
      <filesize>27812</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>78160ac36ece6d9235c424a6ef4f0da4</md5>
          <size>27812</size>
          <filedate>1386584304</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>359d9053a89fcafc944056a7b69a003d</md5>
          <size>30272</size>
          <filedate>1386584304</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0.tar.gz</download_link>
      <date>1382523933</date>
      <mdhash>47e2770bb7021140475b430203fe48ca</mdhash>
      <filesize>26169</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>47e2770bb7021140475b430203fe48ca</md5>
          <size>26169</size>
          <filedate>1382523933</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>74c7bede27755bfb920b047032fc60ec</md5>
          <size>28856</size>
          <filedate>1382523933</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc3.tar.gz</download_link>
      <date>1380367065</date>
      <mdhash>cac44ff001c787e2beffd9d2fcd977b4</mdhash>
      <filesize>25847</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cac44ff001c787e2beffd9d2fcd977b4</md5>
          <size>25847</size>
          <filedate>1380367065</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>3fe30bd7b75c2a469f04f36f8dd39986</md5>
          <size>28589</size>
          <filedate>1380367066</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc2.tar.gz</download_link>
      <date>1374443183</date>
      <mdhash>288bf257573c788f1d93ed50b0b6cd9d</mdhash>
      <filesize>24728</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>288bf257573c788f1d93ed50b0b6cd9d</md5>
          <size>24728</size>
          <filedate>1374443183</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b79291ee144c9057eab45dde8cd776b9</md5>
          <size>27447</size>
          <filedate>1374443184</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc1.tar.gz</download_link>
      <date>1368805217</date>
      <mdhash>278528dc46fa4361fe76e4f76145e243</mdhash>
      <filesize>20412</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>278528dc46fa4361fe76e4f76145e243</md5>
          <size>20412</size>
          <filedate>1368805217</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>dc6151a17bd28aa0c086fb1181bfa911</md5>
          <size>22586</size>
          <filedate>1368805218</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta4.tar.gz</download_link>
      <date>1351629735</date>
      <mdhash>45b5a4f9219744f9901939fea8559449</mdhash>
      <filesize>19807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>45b5a4f9219744f9901939fea8559449</md5>
          <size>19807</size>
          <filedate>1351629735</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>3e9828c94448f8260b9cc5d84c472628</md5>
          <size>21899</size>
          <filedate>1351629735</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta3.tar.gz</download_link>
      <date>1337790720</date>
      <mdhash>8d1a0ab3c61e94860338b40988be8eb8</mdhash>
      <filesize>19583</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8d1a0ab3c61e94860338b40988be8eb8</md5>
          <size>19583</size>
          <filedate>1337790720</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>41300037fd1eda9b040c151619dba1eb</md5>
          <size>21670</size>
          <filedate>1337790720</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta2.tar.gz</download_link>
      <date>1320944435</date>
      <mdhash>7ca0fa2f9cc7f73ce3b923511539ff5e</mdhash>
      <filesize>19670</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7ca0fa2f9cc7f73ce3b923511539ff5e</md5>
          <size>19670</size>
          <filedate>1320944435</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>63ed468b485d68d0ef7fc06f5a6b7b35</md5>
          <size>21677</size>
          <filedate>1320944435</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta1.tar.gz</download_link>
      <date>1315339623</date>
      <mdhash>4abb5dd2d2676c44506114a2cf1f8f82</mdhash>
      <filesize>18426</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4abb5dd2d2676c44506114a2cf1f8f82</md5>
          <size>18426</size>
          <filedate>1315339623</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa656c1ad82bb1fccd1d523a155fc026</md5>
          <size>20344</size>
          <filedate>1315339624</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>search_api_db 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/search_api_db/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.x-dev.tar.gz</download_link>
      <date>1698922182</date>
      <mdhash>e6a65bcf49c0472970d2415308ac4e92</mdhash>
      <filesize>39641</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e6a65bcf49c0472970d2415308ac4e92</md5>
          <size>39641</size>
          <filedate>1698922182</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/search_api_db-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4dfe6db4fd9525dbcb5f123de97070c</md5>
          <size>42890</size>
          <filedate>1698922182</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
