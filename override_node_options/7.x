<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Override Node Options</title>
  <short_name>override_node_options</short_name>
  <dc:creator>opdavies</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/override_node_options</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>override_node_options 7.x-1.15</name>
      <version>7.x-1.15</version>
      <tag>7.x-1.15</tag>
      <version_major>1</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/override_node_options/releases/7.x-1.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.15.tar.gz</download_link>
      <date>1688501526</date>
      <mdhash>5577098db85b79acd1d038b035a32ce3</mdhash>
      <filesize>11793</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5577098db85b79acd1d038b035a32ce3</md5>
          <size>11793</size>
          <filedate>1688501526</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>238ecbc65e92f7e128a778f260639249</md5>
          <size>14415</size>
          <filedate>1688501526</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>override_node_options 7.x-1.14</name>
      <version>7.x-1.14</version>
      <tag>7.x-1.14</tag>
      <version_major>1</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/override_node_options/releases/7.x-1.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.14.tar.gz</download_link>
      <date>1522482185</date>
      <mdhash>76baec6e4fe7147c03b327572d1114ae</mdhash>
      <filesize>11495</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>76baec6e4fe7147c03b327572d1114ae</md5>
          <size>11495</size>
          <filedate>1522482185</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>53ba4e37e5400997a7e877fef2841c1b</md5>
          <size>13546</size>
          <filedate>1522482185</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>override_node_options 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/override_node_options/releases/7.x-1.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.13.tar.gz</download_link>
      <date>1411157928</date>
      <mdhash>2e14b3fbca8caec3742af39066391d63</mdhash>
      <filesize>9591</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2e14b3fbca8caec3742af39066391d63</md5>
          <size>9591</size>
          <filedate>1411157928</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d492e1967e013b832f866813659d92f</md5>
          <size>11227</size>
          <filedate>1411157928</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>override_node_options 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/override_node_options/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.12.tar.gz</download_link>
      <date>1304695316</date>
      <mdhash>619eb624b1903aca5b5fd4c3099796fc</mdhash>
      <filesize>8280</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>619eb624b1903aca5b5fd4c3099796fc</md5>
          <size>8280</size>
          <filedate>1304695316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>3c37b25f6d7ca51e914dd1468e419794</md5>
          <size>9838</size>
          <filedate>1304695316</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>override_node_options 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/override_node_options/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.x-dev.tar.gz</download_link>
      <date>1688501019</date>
      <mdhash>44ddd84acff6896e73fda84c86e8fa62</mdhash>
      <filesize>11797</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>44ddd84acff6896e73fda84c86e8fa62</md5>
          <size>11797</size>
          <filedate>1688501019</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/override_node_options-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a419195debc2ef7468a171be79b29d45</md5>
          <size>14421</size>
          <filedate>1688501019</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
