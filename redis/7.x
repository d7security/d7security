<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Redis</title>
  <short_name>redis</short_name>
  <dc:creator>pounard</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/redis</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>redis 7.x-3.19</name>
      <version>7.x-3.19</version>
      <tag>7.x-3.19</tag>
      <version_major>3</version_major>
      <version_patch>19</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.19</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.19.tar.gz</download_link>
      <date>1654035572</date>
      <mdhash>ff893e65bbc484ff5c5b73159bb891bc</mdhash>
      <filesize>51198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.19.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ff893e65bbc484ff5c5b73159bb891bc</md5>
          <size>51198</size>
          <filedate>1654035572</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.19.zip</url>
          <archive_type>zip</archive_type>
          <md5>922555f3c4d42d6d8fe0d6a28593d244</md5>
          <size>85072</size>
          <filedate>1654035572</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.18</name>
      <version>7.x-3.18</version>
      <tag>7.x-3.18</tag>
      <version_major>3</version_major>
      <version_patch>18</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.18</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.18.tar.gz</download_link>
      <date>1572430084</date>
      <mdhash>a79e2dc95334585efb24a46d5e6f66c1</mdhash>
      <filesize>51140</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.18.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a79e2dc95334585efb24a46d5e6f66c1</md5>
          <size>51140</size>
          <filedate>1572430084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.18.zip</url>
          <archive_type>zip</archive_type>
          <md5>0f0a677986ba1a940da41b3bbb4c2aec</md5>
          <size>84980</size>
          <filedate>1572430084</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.17</name>
      <version>7.x-3.17</version>
      <tag>7.x-3.17</tag>
      <version_major>3</version_major>
      <version_patch>17</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.17</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.17.tar.gz</download_link>
      <date>1513939085</date>
      <mdhash>6fa131d0e4d20991db2755492effa729</mdhash>
      <filesize>51125</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.17.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6fa131d0e4d20991db2755492effa729</md5>
          <size>51125</size>
          <filedate>1513939085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.17.zip</url>
          <archive_type>zip</archive_type>
          <md5>33f22bc2103635a396d8dcc2aa457869</md5>
          <size>84968</size>
          <filedate>1513939085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.15</name>
      <version>7.x-3.15</version>
      <tag>7.x-3.15</tag>
      <version_major>3</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.15.tar.gz</download_link>
      <date>1482355383</date>
      <mdhash>4c6f4e3cfe91013171636efb65909e10</mdhash>
      <filesize>51100</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c6f4e3cfe91013171636efb65909e10</md5>
          <size>51100</size>
          <filedate>1482355383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>4a1a60103387abdd383f2cd04d35fccf</md5>
          <size>84931</size>
          <filedate>1482355383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.14</name>
      <version>7.x-3.14</version>
      <tag>7.x-3.14</tag>
      <version_major>3</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.14.tar.gz</download_link>
      <date>1482342482</date>
      <mdhash>2b83bc001d23d03bb7c8c6722789e563</mdhash>
      <filesize>50681</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2b83bc001d23d03bb7c8c6722789e563</md5>
          <size>50681</size>
          <filedate>1482342482</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>5927b788c07257bccc58bbb74d7cadfb</md5>
          <size>84572</size>
          <filedate>1482342482</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.13</name>
      <version>7.x-3.13</version>
      <tag>7.x-3.13</tag>
      <version_major>3</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.13.tar.gz</download_link>
      <date>1479996780</date>
      <mdhash>9f82ccfe851e89cae7217ce4dc7ae1c0</mdhash>
      <filesize>50446</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9f82ccfe851e89cae7217ce4dc7ae1c0</md5>
          <size>50446</size>
          <filedate>1479996780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c4eebd3acb95f4f9a812354e939a628</md5>
          <size>84257</size>
          <filedate>1479996780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.12</name>
      <version>7.x-3.12</version>
      <tag>7.x-3.12</tag>
      <version_major>3</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.12.tar.gz</download_link>
      <date>1460129339</date>
      <mdhash>becc6aa545b3d2027c9fe9ada758fca0</mdhash>
      <filesize>49495</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>becc6aa545b3d2027c9fe9ada758fca0</md5>
          <size>49495</size>
          <filedate>1460129339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>ddf164ade91037ce002c51b319374af3</md5>
          <size>79617</size>
          <filedate>1460129339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.11</name>
      <version>7.x-3.11</version>
      <tag>7.x-3.11</tag>
      <version_major>3</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.11.tar.gz</download_link>
      <date>1441010943</date>
      <mdhash>25f5111c991a44e42a32695979c51c61</mdhash>
      <filesize>49253</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>25f5111c991a44e42a32695979c51c61</md5>
          <size>49253</size>
          <filedate>1441010943</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>9762f55c36ab95cfb1efa12da1ca78a8</md5>
          <size>79220</size>
          <filedate>1441010943</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.10</name>
      <version>7.x-3.10</version>
      <tag>7.x-3.10</tag>
      <version_major>3</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.10.tar.gz</download_link>
      <date>1440336239</date>
      <mdhash>20a3730f298eeca74ebc8b02abd27b1a</mdhash>
      <filesize>49085</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>20a3730f298eeca74ebc8b02abd27b1a</md5>
          <size>49085</size>
          <filedate>1440336239</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>07cae6df4c6999a70ded56b3db97172b</md5>
          <size>79045</size>
          <filedate>1440336239</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.9</name>
      <version>7.x-3.9</version>
      <tag>7.x-3.9</tag>
      <version_major>3</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.9.tar.gz</download_link>
      <date>1439203739</date>
      <mdhash>51df264b22cf7baac58bfb65c2c438ae</mdhash>
      <filesize>48666</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51df264b22cf7baac58bfb65c2c438ae</md5>
          <size>48666</size>
          <filedate>1439203739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>33dba88d2c9cfba77b045f126988525f</md5>
          <size>78688</size>
          <filedate>1439203739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.8</name>
      <version>7.x-3.8</version>
      <tag>7.x-3.8</tag>
      <version_major>3</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.8.tar.gz</download_link>
      <date>1435315686</date>
      <mdhash>6da050f626da9d125b0e98c58ffa7731</mdhash>
      <filesize>47921</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6da050f626da9d125b0e98c58ffa7731</md5>
          <size>47921</size>
          <filedate>1435315686</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f361b49be65d8cae6d95307acb962f0</md5>
          <size>76368</size>
          <filedate>1435315686</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.7</name>
      <version>7.x-3.7</version>
      <tag>7.x-3.7</tag>
      <version_major>3</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.7.tar.gz</download_link>
      <date>1431003781</date>
      <mdhash>b70366e2311dbb3f0f62d59f9586ee45</mdhash>
      <filesize>47846</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b70366e2311dbb3f0f62d59f9586ee45</md5>
          <size>47846</size>
          <filedate>1431003781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>305e3be2658853d13c881e10e4fb4bc4</md5>
          <size>76284</size>
          <filedate>1431003781</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.6.tar.gz</download_link>
      <date>1430832481</date>
      <mdhash>642f5aa8430c293079158850575294e5</mdhash>
      <filesize>46859</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>642f5aa8430c293079158850575294e5</md5>
          <size>46859</size>
          <filedate>1430832481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>7bc8fe826ca57819f3da9e68882e262f</md5>
          <size>72133</size>
          <filedate>1430832481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.5.tar.gz</download_link>
      <date>1430828281</date>
      <mdhash>bee7672070e11b2fb5f914b8190d93c7</mdhash>
      <filesize>46844</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bee7672070e11b2fb5f914b8190d93c7</md5>
          <size>46844</size>
          <filedate>1430828281</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>3018ab791f6c803a1a7dce34136b9fec</md5>
          <size>72102</size>
          <filedate>1430828281</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.4.tar.gz</download_link>
      <date>1430158681</date>
      <mdhash>0fd342a5b5d4dad20b85ed989eb29875</mdhash>
      <filesize>46368</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0fd342a5b5d4dad20b85ed989eb29875</md5>
          <size>46368</size>
          <filedate>1430158681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>26a3744068fc5b27b4efcc7a721eb652</md5>
          <size>71262</size>
          <filedate>1430158681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.3.tar.gz</download_link>
      <date>1428422881</date>
      <mdhash>6617a8e8cfdb4a735061908cf1856096</mdhash>
      <filesize>42995</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6617a8e8cfdb4a735061908cf1856096</md5>
          <size>42995</size>
          <filedate>1428422881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>52aa8e40f28abb1d086212f12a4f9a52</md5>
          <size>64319</size>
          <filedate>1428422881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.2.tar.gz</download_link>
      <date>1425393481</date>
      <mdhash>591fc4c99ddaea3a19546bd8167ac1d1</mdhash>
      <filesize>42743</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>591fc4c99ddaea3a19546bd8167ac1d1</md5>
          <size>42743</size>
          <filedate>1425393481</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8085d0d90d70fd5e11f5807484994ec</md5>
          <size>64073</size>
          <filedate>1425393481</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.1.tar.gz</download_link>
      <date>1425297781</date>
      <mdhash>85be6a0bebc0a566a3ddd7be6e32ec16</mdhash>
      <filesize>42744</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>85be6a0bebc0a566a3ddd7be6e32ec16</md5>
          <size>42744</size>
          <filedate>1425297781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>792e58962a226056f150f3c7197536e1</md5>
          <size>64078</size>
          <filedate>1425297781</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.0.tar.gz</download_link>
      <date>1424624581</date>
      <mdhash>ef6e70546cbdbc9d266dafaad6343cfe</mdhash>
      <filesize>42196</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef6e70546cbdbc9d266dafaad6343cfe</md5>
          <size>42196</size>
          <filedate>1424624581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>e12af8b8dfe631cc8d214cc3e4a7f732</md5>
          <size>63400</size>
          <filedate>1424624581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.15</name>
      <version>7.x-2.15</version>
      <tag>7.x-2.15</tag>
      <version_major>2</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.15.tar.gz</download_link>
      <date>1436175839</date>
      <mdhash>69d1723b084cfee08dfde251ad9e6d06</mdhash>
      <filesize>43540</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69d1723b084cfee08dfde251ad9e6d06</md5>
          <size>43540</size>
          <filedate>1436175839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>c0e5f9d10448a8f09ef450257214ff1e</md5>
          <size>68615</size>
          <filedate>1436175839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.14</name>
      <version>7.x-2.14</version>
      <tag>7.x-2.14</tag>
      <version_major>2</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.14.tar.gz</download_link>
      <date>1435315686</date>
      <mdhash>c03de36b3988e2bedc6423f752b5c129</mdhash>
      <filesize>43542</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c03de36b3988e2bedc6423f752b5c129</md5>
          <size>43542</size>
          <filedate>1435315686</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>adfe66a391edab9153f4d501c56d9728</md5>
          <size>68610</size>
          <filedate>1435315686</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.13</name>
      <version>7.x-2.13</version>
      <tag>7.x-2.13</tag>
      <version_major>2</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.13.tar.gz</download_link>
      <date>1430932681</date>
      <mdhash>9a372012369e6e701eee4291d92fadb4</mdhash>
      <filesize>42769</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a372012369e6e701eee4291d92fadb4</md5>
          <size>42769</size>
          <filedate>1430932681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>1da0fb0ab669000da37ed4a508187569</md5>
          <size>66510</size>
          <filedate>1430932681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.12</name>
      <version>7.x-2.12</version>
      <tag>7.x-2.12</tag>
      <version_major>2</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.12.tar.gz</download_link>
      <date>1410458928</date>
      <mdhash>d908915c4f24b17bf69d36d0ad9f820c</mdhash>
      <filesize>40917</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d908915c4f24b17bf69d36d0ad9f820c</md5>
          <size>40917</size>
          <filedate>1410458928</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>a14ce7d0f260342513ae7cdb4ae2bad7</md5>
          <size>62055</size>
          <filedate>1410458928</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.11.tar.gz</download_link>
      <date>1404381529</date>
      <mdhash>7853172198ebef220b50c37b0e3050ae</mdhash>
      <filesize>40312</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7853172198ebef220b50c37b0e3050ae</md5>
          <size>40312</size>
          <filedate>1404381529</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f84ecca9a3f722a16089db97c146c29</md5>
          <size>60285</size>
          <filedate>1404381529</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.10.tar.gz</download_link>
      <date>1400849928</date>
      <mdhash>b8d149caf80df6fa9adc6a832e29d89d</mdhash>
      <filesize>37051</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8d149caf80df6fa9adc6a832e29d89d</md5>
          <size>37051</size>
          <filedate>1400849928</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>14bffe06b3b1a12f96838d157567ec66</md5>
          <size>54566</size>
          <filedate>1400849928</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.9.tar.gz</download_link>
      <date>1400846327</date>
      <mdhash>af63f1eb9ca0012c58a6c75aee9330c2</mdhash>
      <filesize>37009</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>af63f1eb9ca0012c58a6c75aee9330c2</md5>
          <size>37009</size>
          <filedate>1400846327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>49528c3eb306ed3cf8355240597c1a89</md5>
          <size>54509</size>
          <filedate>1400846327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.8.tar.gz</download_link>
      <date>1400831327</date>
      <mdhash>d6e976ef50f002994c256e3d27b1f49a</mdhash>
      <filesize>36946</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d6e976ef50f002994c256e3d27b1f49a</md5>
          <size>36946</size>
          <filedate>1400831327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>670553d7f65fa463098a937fe07e884a</md5>
          <size>53426</size>
          <filedate>1400831327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.7.tar.gz</download_link>
      <date>1400797727</date>
      <mdhash>8df23753184ada736ce088a4a79d001d</mdhash>
      <filesize>36508</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8df23753184ada736ce088a4a79d001d</md5>
          <size>36508</size>
          <filedate>1400797727</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>16cc62cd8a612e73605b1360b87a289b</md5>
          <size>52900</size>
          <filedate>1400797727</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.6.tar.gz</download_link>
      <date>1385558304</date>
      <mdhash>3eece93e007f2f9bd35cc39835c2e5ca</mdhash>
      <filesize>28945</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3eece93e007f2f9bd35cc39835c2e5ca</md5>
          <size>28945</size>
          <filedate>1385558304</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>556474434c4aef76224105c003bb56aa</md5>
          <size>43503</size>
          <filedate>1385558304</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.5.tar.gz</download_link>
      <date>1384264104</date>
      <mdhash>80e5a275c9815149b3fa8fd362adb810</mdhash>
      <filesize>27167</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>80e5a275c9815149b3fa8fd362adb810</md5>
          <size>27167</size>
          <filedate>1384264104</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d66d41b66e76defb96cbb821f8fa2ae1</md5>
          <size>41486</size>
          <filedate>1384264104</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.4.tar.gz</download_link>
      <date>1384260805</date>
      <mdhash>528b6c4f728108cb8d43520afbd0f8c7</mdhash>
      <filesize>26880</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>528b6c4f728108cb8d43520afbd0f8c7</md5>
          <size>26880</size>
          <filedate>1384260805</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>77b8a7e4a08c4d9ab7005b6972bdca13</md5>
          <size>40881</size>
          <filedate>1384260805</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.3.tar.gz</download_link>
      <date>1384034904</date>
      <mdhash>9446de7c19f99f88d28da4c552eb8313</mdhash>
      <filesize>26311</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9446de7c19f99f88d28da4c552eb8313</md5>
          <size>26311</size>
          <filedate>1384034904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>cee9f7ac9cde1c833d9de10d931718b7</md5>
          <size>38991</size>
          <filedate>1384034904</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.2.tar.gz</download_link>
      <date>1384034305</date>
      <mdhash>1838f6d36d4ef27356c43a20ce313a1d</mdhash>
      <filesize>26256</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1838f6d36d4ef27356c43a20ce313a1d</md5>
          <size>26256</size>
          <filedate>1384034305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b1cb47bd5a5143d3606bd09d30ed9615</md5>
          <size>38941</size>
          <filedate>1384034305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.1.tar.gz</download_link>
      <date>1382688626</date>
      <mdhash>e77f0986190f80f434604e99ce2b4af1</mdhash>
      <filesize>23381</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e77f0986190f80f434604e99ce2b4af1</md5>
          <size>23381</size>
          <filedate>1382688626</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>459afcbc63b15fdf8bb3adb8715dd5a1</md5>
          <size>34583</size>
          <filedate>1382688627</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0.tar.gz</download_link>
      <date>1382428615</date>
      <mdhash>e36a4d66b2e40303663c2018c5fb7dbe</mdhash>
      <filesize>23188</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e36a4d66b2e40303663c2018c5fb7dbe</md5>
          <size>23188</size>
          <filedate>1382428615</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>eeae2e8dc816727531dce7cec3ca95fe</md5>
          <size>34444</size>
          <filedate>1382428615</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>redis 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta4.tar.gz</download_link>
      <date>1358339303</date>
      <mdhash>dc46a65925902f8c6d17fab38296acea</mdhash>
      <filesize>22847</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dc46a65925902f8c6d17fab38296acea</md5>
          <size>22847</size>
          <filedate>1358339303</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>6839ce9c7f12e2449a786a07962c4175</md5>
          <size>34116</size>
          <filedate>1358339303</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta3.tar.gz</download_link>
      <date>1358338717</date>
      <mdhash>8694ac7f41a8c0ab80ee39a6274ebd69</mdhash>
      <filesize>22838</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8694ac7f41a8c0ab80ee39a6274ebd69</md5>
          <size>22838</size>
          <filedate>1358338717</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f62401ec50ef764acad755916dc8292</md5>
          <size>34128</size>
          <filedate>1358338717</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta2.tar.gz</download_link>
      <date>1356010140</date>
      <mdhash>c76a476a67c44b891787a7c560b9fd37</mdhash>
      <filesize>22840</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c76a476a67c44b891787a7c560b9fd37</md5>
          <size>22840</size>
          <filedate>1356010140</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ebc76fccff3cd5ecfa07a7ae73e7f371</md5>
          <size>34115</size>
          <filedate>1356010140</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta1.tar.gz</download_link>
      <date>1352735294</date>
      <mdhash>e9831477e812399c98050c2a38a77c89</mdhash>
      <filesize>22163</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e9831477e812399c98050c2a38a77c89</md5>
          <size>22163</size>
          <filedate>1352735294</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f1b2849fcb29b0a6a15f9cfe75308872</md5>
          <size>33299</size>
          <filedate>1352735294</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha11</name>
      <version>7.x-2.0-alpha11</version>
      <tag>7.x-2.0-alpha11</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha11</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha11.tar.gz</download_link>
      <date>1347468874</date>
      <mdhash>ff4be68d36c7274938c8ac73d6a10fe3</mdhash>
      <filesize>20915</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ff4be68d36c7274938c8ac73d6a10fe3</md5>
          <size>20915</size>
          <filedate>1347468874</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha11.zip</url>
          <archive_type>zip</archive_type>
          <md5>f426edc76ac9d32bf0a4693a17bd91c4</md5>
          <size>31838</size>
          <filedate>1347468874</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha10</name>
      <version>7.x-2.0-alpha10</version>
      <tag>7.x-2.0-alpha10</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha10</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha10.tar.gz</download_link>
      <date>1346860040</date>
      <mdhash>6662960efbabb50773949ed3f9585cfc</mdhash>
      <filesize>19690</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6662960efbabb50773949ed3f9585cfc</md5>
          <size>19690</size>
          <filedate>1346860040</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha10.zip</url>
          <archive_type>zip</archive_type>
          <md5>b8e189a49dbb03cdedb01104d5dc2301</md5>
          <size>29902</size>
          <filedate>1346860040</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha9</name>
      <version>7.x-2.0-alpha9</version>
      <tag>7.x-2.0-alpha9</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha9</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha9.tar.gz</download_link>
      <date>1336395092</date>
      <mdhash>cac25d9a4ec0896b9072caa27c28112a</mdhash>
      <filesize>19694</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cac25d9a4ec0896b9072caa27c28112a</md5>
          <size>19694</size>
          <filedate>1336395092</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha9.zip</url>
          <archive_type>zip</archive_type>
          <md5>5626f7010d360c16d9c39025dc43d94b</md5>
          <size>29901</size>
          <filedate>1336395092</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha8</name>
      <version>7.x-2.0-alpha8</version>
      <tag>7.x-2.0-alpha8</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha8.tar.gz</download_link>
      <date>1333732269</date>
      <mdhash>c411568694e8072f8f9a886d30a673f2</mdhash>
      <filesize>19630</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c411568694e8072f8f9a886d30a673f2</md5>
          <size>19630</size>
          <filedate>1333732269</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha8.zip</url>
          <archive_type>zip</archive_type>
          <md5>66d80b4ec6269d9af83d17dc5d5e2967</md5>
          <size>29693</size>
          <filedate>1333732269</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha7</name>
      <version>7.x-2.0-alpha7</version>
      <tag>7.x-2.0-alpha7</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha7.tar.gz</download_link>
      <date>1331841943</date>
      <mdhash>cbb39016cfe52c14f203aa8929c8ee40</mdhash>
      <filesize>19635</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cbb39016cfe52c14f203aa8929c8ee40</md5>
          <size>19635</size>
          <filedate>1331841943</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>3e98d56c5a7bc0f55e3dce0228199342</md5>
          <size>29721</size>
          <filedate>1331841943</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha6</name>
      <version>7.x-2.0-alpha6</version>
      <tag>7.x-2.0-alpha6</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha6.tar.gz</download_link>
      <date>1331470543</date>
      <mdhash>31efba94d5c00af2a110f9a7533a2afe</mdhash>
      <filesize>19514</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>31efba94d5c00af2a110f9a7533a2afe</md5>
          <size>19514</size>
          <filedate>1331470543</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>22295b210dcfa3b5ddb0676fcca287b7</md5>
          <size>29582</size>
          <filedate>1331470543</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha5</name>
      <version>7.x-2.0-alpha5</version>
      <tag>7.x-2.0-alpha5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha5.tar.gz</download_link>
      <date>1329565842</date>
      <mdhash>36088fb57f5b0d5f1c8656b7f941b5ed</mdhash>
      <filesize>19473</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>36088fb57f5b0d5f1c8656b7f941b5ed</md5>
          <size>19473</size>
          <filedate>1329565842</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>6329d9dbd6abb8b7a5be893fa1fb2e29</md5>
          <size>29555</size>
          <filedate>1329565842</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha4</name>
      <version>7.x-2.0-alpha4</version>
      <tag>7.x-2.0-alpha4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha4.tar.gz</download_link>
      <date>1328129746</date>
      <mdhash>00e3f4ac849654337f8574546263f7e9</mdhash>
      <filesize>19188</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00e3f4ac849654337f8574546263f7e9</md5>
          <size>19188</size>
          <filedate>1328129746</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d805cef45398c3a9df7b0beefe59ecf</md5>
          <size>29270</size>
          <filedate>1328129746</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha3</name>
      <version>7.x-2.0-alpha3</version>
      <tag>7.x-2.0-alpha3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha3.tar.gz</download_link>
      <date>1325872844</date>
      <mdhash>12a3799e040fec69d62b9fcba4c702c7</mdhash>
      <filesize>19594</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>12a3799e040fec69d62b9fcba4c702c7</md5>
          <size>19594</size>
          <filedate>1325872844</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea7031ffbcf9a381b2141ff507b4a50f</md5>
          <size>30008</size>
          <filedate>1325872844</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1319820935</date>
      <mdhash>08db1eac49cd4accc2b1b2e3658d2ed9</mdhash>
      <filesize>19577</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>08db1eac49cd4accc2b1b2e3658d2ed9</md5>
          <size>19577</size>
          <filedate>1319820935</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>bc7848b739d5ea978b0c889d7b8b0cda</md5>
          <size>29960</size>
          <filedate>1319820935</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1313768520</date>
      <mdhash>f0f06953735d3e1c0b643bd7ce161a17</mdhash>
      <filesize>16909</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0f06953735d3e1c0b643bd7ce161a17</md5>
          <size>16909</size>
          <filedate>1313768520</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ab8b18de1f21ab50c634950c1bc0a643</md5>
          <size>27046</size>
          <filedate>1313768520</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha7</name>
      <version>7.x-1.0-alpha7</version>
      <tag>7.x-1.0-alpha7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha7.tar.gz</download_link>
      <date>1333732266</date>
      <mdhash>d457b194acddb459e60df8590dc85e98</mdhash>
      <filesize>18091</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d457b194acddb459e60df8590dc85e98</md5>
          <size>18091</size>
          <filedate>1333732266</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha7.zip</url>
          <archive_type>zip</archive_type>
          <md5>340f52a5acbfba8abbb3311e1844474d</md5>
          <size>28640</size>
          <filedate>1333732266</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha6</name>
      <version>7.x-1.0-alpha6</version>
      <tag>7.x-1.0-alpha6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha6.tar.gz</download_link>
      <date>1313584323</date>
      <mdhash>de4f56dbad4d043c384d9e213540e755</mdhash>
      <filesize>16658</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>de4f56dbad4d043c384d9e213540e755</md5>
          <size>16658</size>
          <filedate>1313584323</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>001bddc516bf22a439b0a4d494404c04</md5>
          <size>27551</size>
          <filedate>1313584323</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1310826119</date>
      <mdhash>2b00646f699233c9eab3b7d560fdc127</mdhash>
      <filesize>15863</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2b00646f699233c9eab3b7d560fdc127</md5>
          <size>15863</size>
          <filedate>1310826119</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>0798427677732f937b770e564c608128</md5>
          <size>24357</size>
          <filedate>1310826119</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1310815658</date>
      <mdhash>f1f23593e5c498d37eb0ebd39ecb641e</mdhash>
      <filesize>14149</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f1f23593e5c498d37eb0ebd39ecb641e</md5>
          <size>14149</size>
          <filedate>1310815658</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4e3e263e2630f2ac18ee860cfc8f56de</md5>
          <size>22077</size>
          <filedate>1310815658</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1314633721</date>
      <mdhash>69a380d88043443ca8deab5c054d1516</mdhash>
      <filesize>9929</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69a380d88043443ca8deab5c054d1516</md5>
          <size>9929</size>
          <filedate>1314633721</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>393d4e97279fff7c5e987b7ae1da6385</md5>
          <size>12601</size>
          <filedate>1314633721</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1310423520</date>
      <mdhash>9a9a8f268e7a892176b1d893d8ea0677</mdhash>
      <filesize>9932</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a9a8f268e7a892176b1d893d8ea0677</md5>
          <size>9932</size>
          <filedate>1310423520</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>245e362d5f376770a4dbad0bc732c3ad</md5>
          <size>12604</size>
          <filedate>1310423520</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>redis 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/redis/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/redis-7.x-3.x-dev.tar.gz</download_link>
      <date>1654035237</date>
      <mdhash>062bed29ddfd40ad93c69f44df7998af</mdhash>
      <filesize>51203</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>062bed29ddfd40ad93c69f44df7998af</md5>
          <size>51203</size>
          <filedate>1654035237</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/redis-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa65bffe5356148dcce350a142cd16bf</md5>
          <size>85077</size>
          <filedate>1654035237</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
