<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>CAPTCHA</title>
  <short_name>captcha</short_name>
  <dc:creator>wundo</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/captcha</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>captcha 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.7.tar.gz</download_link>
      <date>1582293278</date>
      <mdhash>822e7a0a71554beca49ee51893fecc5c</mdhash>
      <filesize>106017</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>822e7a0a71554beca49ee51893fecc5c</md5>
          <size>106017</size>
          <filedate>1582293278</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d0f63a1d32ff389299d7f5a91e4353b</md5>
          <size>115651</size>
          <filedate>1582293278</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.6.tar.gz</download_link>
      <date>1570300085</date>
      <mdhash>caaa291bb6e6e565f4a7325ad7be2fd4</mdhash>
      <filesize>105939</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>caaa291bb6e6e565f4a7325ad7be2fd4</md5>
          <size>105939</size>
          <filedate>1570300085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>21f28e330369a79e2da5a7ce526b90c1</md5>
          <size>115571</size>
          <filedate>1570300085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.5.tar.gz</download_link>
      <date>1504724039</date>
      <mdhash>66d00fbac52ff5734d91a1dfb50117b0</mdhash>
      <filesize>105271</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66d00fbac52ff5734d91a1dfb50117b0</md5>
          <size>105271</size>
          <filedate>1504724039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>fbfdfe22db5e52d6866274079241809f</md5>
          <size>114913</size>
          <filedate>1504724039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.4.tar.gz</download_link>
      <date>1487198284</date>
      <mdhash>e7ec5b8d8e4f1ca80ede790bd2d58a4b</mdhash>
      <filesize>105217</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e7ec5b8d8e4f1ca80ede790bd2d58a4b</md5>
          <size>105217</size>
          <filedate>1487198284</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>d2d331635fd69c7b98888eea7b79bbf0</md5>
          <size>114854</size>
          <filedate>1487198284</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.3.tar.gz</download_link>
      <date>1427464081</date>
      <mdhash>b180586b75ef0ec8a0d72caccf95043e</mdhash>
      <filesize>102964</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b180586b75ef0ec8a0d72caccf95043e</md5>
          <size>102964</size>
          <filedate>1427464081</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>600b1cc15982006a035938b11252788a</md5>
          <size>113769</size>
          <filedate>1427464081</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.2.tar.gz</download_link>
      <date>1417219364</date>
      <mdhash>496b2bdf14a5ace07cd5e6482bcd96d8</mdhash>
      <filesize>101968</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>496b2bdf14a5ace07cd5e6482bcd96d8</md5>
          <size>101968</size>
          <filedate>1417219364</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>398061d4992f1320bb90e5aa1b4f291c</md5>
          <size>112789</size>
          <filedate>1417219364</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.1.tar.gz</download_link>
      <date>1404666827</date>
      <mdhash>4cd343e772dd995c8dacbd5f3b675bc0</mdhash>
      <filesize>103263</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4cd343e772dd995c8dacbd5f3b675bc0</md5>
          <size>103263</size>
          <filedate>1404666827</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2056ec99fb2d89462b4bc947ecfc6d3f</md5>
          <size>112271</size>
          <filedate>1404666827</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0.tar.gz</download_link>
      <date>1372203951</date>
      <mdhash>beb91d544cbf474187d8e5f4b2e162c8</mdhash>
      <filesize>103311</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>beb91d544cbf474187d8e5f4b2e162c8</md5>
          <size>103311</size>
          <filedate>1372203951</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7a0fbbf9423fe6a4470e3dbd2ec2824d</md5>
          <size>111884</size>
          <filedate>1372203951</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>captcha 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta2.tar.gz</download_link>
      <date>1325504137</date>
      <mdhash>01c8c169a08574f1bdddee144d51f510</mdhash>
      <filesize>103503</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>01c8c169a08574f1bdddee144d51f510</md5>
          <size>103503</size>
          <filedate>1325504137</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ace6455adc258c2272dd7eb86f42332a</md5>
          <size>111620</size>
          <filedate>1325504137</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>captcha 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta1.tar.gz</download_link>
      <date>1321806037</date>
      <mdhash>9a1175ab8e61a573743be327370fe04c</mdhash>
      <filesize>102453</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9a1175ab8e61a573743be327370fe04c</md5>
          <size>102453</size>
          <filedate>1321806037</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>2e1143e077f8944462d10247039e6bef</md5>
          <size>110914</size>
          <filedate>1321806037</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>captcha 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1303939314</date>
      <mdhash>1632ad245cc4651861ab15c61eb8b0d2</mdhash>
      <filesize>101733</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1632ad245cc4651861ab15c61eb8b0d2</md5>
          <size>101733</size>
          <filedate>1303939314</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5752dfd25af2feb8d6d82e7c976e72e4</md5>
          <size>110124</size>
          <filedate>1303939314</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>captcha 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1293755549</date>
      <mdhash>81bf574c74befd42fd493ceb1dce1c8e</mdhash>
      <filesize>171605</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>81bf574c74befd42fd493ceb1dce1c8e</md5>
          <size>171605</size>
          <filedate>1293755549</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>a202dc625c29c84ee1f3e7574421dbcb</md5>
          <size>219005</size>
          <filedate>1293755549</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>captcha 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1290295539</date>
      <mdhash>db71e36785b8339f60da3acb89caed40</mdhash>
      <filesize>167422</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>db71e36785b8339f60da3acb89caed40</md5>
          <size>167422</size>
          <filedate>1290295539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7dfdde46fcd1a0c33185f3131d435187</md5>
          <size>214713</size>
          <filedate>1293230120</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>captcha 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/captcha/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/captcha-7.x-1.x-dev.tar.gz</download_link>
      <date>1669041565</date>
      <mdhash>b720fd5d327ec7f4294c4ba6b18f962d</mdhash>
      <filesize>106018</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b720fd5d327ec7f4294c4ba6b18f962d</md5>
          <size>106018</size>
          <filedate>1669041565</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/captcha-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>69a45de15242d2c9c19e3b0d608f30e7</md5>
          <size>115669</size>
          <filedate>1669041565</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
