<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Charts</title>
  <short_name>charts</short_name>
  <dc:creator>quicksketch</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/charts</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>charts 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.1.tar.gz</download_link>
      <date>1534294684</date>
      <mdhash>56d5d93ccd01d93b76544ba17c2895dc</mdhash>
      <filesize>34690</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>56d5d93ccd01d93b76544ba17c2895dc</md5>
          <size>34690</size>
          <filedate>1534294684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d26fc646ad5b41c3b7715798bb4ab601</md5>
          <size>45450</size>
          <filedate>1534294684</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>charts 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0.tar.gz</download_link>
      <date>1520494684</date>
      <mdhash>0cc503e1e94628579ff65e02eceeb1a5</mdhash>
      <filesize>34613</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0cc503e1e94628579ff65e02eceeb1a5</md5>
          <size>34613</size>
          <filedate>1520494684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>aa6eb1a0e7cc867e6b0986d788c5d512</md5>
          <size>45369</size>
          <filedate>1520494684</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>charts 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-rc1.tar.gz</download_link>
      <date>1395123855</date>
      <mdhash>0d0cf25f653a8942cc472bd79de0cfeb</mdhash>
      <filesize>33685</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d0cf25f653a8942cc472bd79de0cfeb</md5>
          <size>33685</size>
          <filedate>1395123855</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>275a42e0dd1b3d031ec96d495a86aae7</md5>
          <size>43547</size>
          <filedate>1395123855</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.0-beta5</name>
      <version>7.x-2.0-beta5</version>
      <tag>7.x-2.0-beta5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta5.tar.gz</download_link>
      <date>1375159272</date>
      <mdhash>4d2b1a6b5f470af4fc321633795b1d34</mdhash>
      <filesize>33156</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4d2b1a6b5f470af4fc321633795b1d34</md5>
          <size>33156</size>
          <filedate>1375159272</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>0f32f1ce565263747162c66bf8c7db7c</md5>
          <size>42939</size>
          <filedate>1375159272</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta4.tar.gz</download_link>
      <date>1375074368</date>
      <mdhash>f0911433c7b86796675daa7c6bfe149d</mdhash>
      <filesize>32867</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f0911433c7b86796675daa7c6bfe149d</md5>
          <size>32867</size>
          <filedate>1375074368</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>dee6b78e50978b6d2a563abdc3b93f6b</md5>
          <size>42642</size>
          <filedate>1375074368</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta3.tar.gz</download_link>
      <date>1374970570</date>
      <mdhash>38edd418403638ce37446cb3700f0733</mdhash>
      <filesize>29872</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>38edd418403638ce37446cb3700f0733</md5>
          <size>29872</size>
          <filedate>1374970570</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5d134c6c89b45f4e193052e1429892bb</md5>
          <size>38814</size>
          <filedate>1374970570</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta2.tar.gz</download_link>
      <date>1374826568</date>
      <mdhash>ef7759be705f07ea75d046d87cced586</mdhash>
      <filesize>28528</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef7759be705f07ea75d046d87cced586</md5>
          <size>28528</size>
          <filedate>1374826568</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d27c65e1e2b975e4e861bbaa1fc97d90</md5>
          <size>37104</size>
          <filedate>1374826568</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta1.tar.gz</download_link>
      <date>1374799567</date>
      <mdhash>13c2b31aa321f4dd89219bdfa37ecfe4</mdhash>
      <filesize>27527</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>13c2b31aa321f4dd89219bdfa37ecfe4</md5>
          <size>27527</size>
          <filedate>1374799567</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>43c1998618ef68d523141aaa6e618a9c</md5>
          <size>36085</size>
          <filedate>1374799567</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-2.x-dev.tar.gz</download_link>
      <date>1598405959</date>
      <mdhash>4a23608478a1137af4e99042ef90a2a9</mdhash>
      <filesize>35247</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a23608478a1137af4e99042ef90a2a9</md5>
          <size>35247</size>
          <filedate>1598405959</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9ec648b612e564b8f5c703a8ad4ed3ba</md5>
          <size>45992</size>
          <filedate>1598405959</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>charts 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/charts/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/charts-7.x-1.x-dev.tar.gz</download_link>
      <date>1380558166</date>
      <mdhash>9e59f5791d65f7956f6e765e455e57ce</mdhash>
      <filesize>22619</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9e59f5791d65f7956f6e765e455e57ce</md5>
          <size>22619</size>
          <filedate>1380558166</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/charts-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f40385f9f5c63a8d3557ce55465e332</md5>
          <size>33241</size>
          <filedate>1380558167</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
