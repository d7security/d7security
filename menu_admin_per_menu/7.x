<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Menu Admin per Menu</title>
  <short_name>menu_admin_per_menu</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/menu_admin_per_menu</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>menu_admin_per_menu 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_admin_per_menu/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.1.tar.gz</download_link>
      <date>1439473439</date>
      <mdhash>d6a7a593bdbb616dd0a47c78581d33b5</mdhash>
      <filesize>9080</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d6a7a593bdbb616dd0a47c78581d33b5</md5>
          <size>9080</size>
          <filedate>1439473439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>568fa3a8d041862e1fa961410b05a9c2</md5>
          <size>10321</size>
          <filedate>1439473439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_admin_per_menu 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_admin_per_menu/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0.tar.gz</download_link>
      <date>1295735798</date>
      <mdhash>5fea23f717c1dbd694656ec886e8f32d</mdhash>
      <filesize>7781</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5fea23f717c1dbd694656ec886e8f32d</md5>
          <size>7781</size>
          <filedate>1295735798</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d87f68d362c5ea14e0194f8faea384d</md5>
          <size>8840</size>
          <filedate>1295735798</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>menu_admin_per_menu 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/menu_admin_per_menu/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0-beta1.tar.gz</download_link>
      <date>1288719059</date>
      <mdhash>cd422db4e3ffb103a388c2c2b008a18d</mdhash>
      <filesize>7759</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cd422db4e3ffb103a388c2c2b008a18d</md5>
          <size>7759</size>
          <filedate>1288719059</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/menu_admin_per_menu-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a364af83ebdad6ec71bab6a6d2092deb</md5>
          <size>8755</size>
          <filedate>1293232920</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
