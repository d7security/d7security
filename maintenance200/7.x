<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Maintenance 200</title>
  <short_name>maintenance200</short_name>
  <dc:creator>Barrett</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/maintenance200</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>maintenance200 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/maintenance200/-/releases/7.x-1.2</release_link>
      <download_link>https://gitlab.com/api/v4/projects/59495708/packages/generic/maintenance200/7.x-1.2/maintenance200-7.x-1.2.tar.gz</download_link>
      <date>1720519571</date>
      <mdhash>2664b52540dd72f73202ec07b85c3424</mdhash>
      <filesize>9208</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/59495708/packages/generic/maintenance200/7.x-1.2/maintenance200-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2664b52540dd72f73202ec07b85c3424</md5>
          <size>9208</size>
          <filedate>1720519571</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/59495708/packages/generic/maintenance200/7.x-1.2/maintenance200-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2cc6b5748ee034c874332dac42c6bc8e</md5>
          <size>10623</size>
          <filedate>1720519571</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>maintenance200 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/maintenance200/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.1.tar.gz</download_link>
      <date>1425773217</date>
      <mdhash>4c4b15d1808f7bc44a2ae97efb1839ab</mdhash>
      <filesize>8581</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4c4b15d1808f7bc44a2ae97efb1839ab</md5>
          <size>8581</size>
          <filedate>1425773217</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a899c10aa29e820e6746740a67f2f39b</md5>
          <size>9824</size>
          <filedate>1425773217</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>maintenance200 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/maintenance200/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.0.tar.gz</download_link>
      <date>1342824720</date>
      <mdhash>0316bb30ddb8081c54b572bb6e037a65</mdhash>
      <filesize>8553</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0316bb30ddb8081c54b572bb6e037a65</md5>
          <size>8553</size>
          <filedate>1342824720</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>634f48d78859744e1d8fa76a9651fc7b</md5>
          <size>9644</size>
          <filedate>1342824720</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>maintenance200 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/maintenance200/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.x-dev.tar.gz</download_link>
      <date>1425773217</date>
      <mdhash>87740da30c622009988db0e4c3d7f46c</mdhash>
      <filesize>8582</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>87740da30c622009988db0e4c3d7f46c</md5>
          <size>8582</size>
          <filedate>1425773217</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/maintenance200-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>bb83a4e52408ee4c185d804088fa8b79</md5>
          <size>9828</size>
          <filedate>1425773217</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
