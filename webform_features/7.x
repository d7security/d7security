<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Webform Features</title>
  <short_name>webform_features</short_name>
  <dc:creator>duaelfr</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/webform_features</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>webform_features 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0.tar.gz</download_link>
      <date>1486224482</date>
      <mdhash>88230f08708d13ee7a02ecbbb9a724df</mdhash>
      <filesize>12012</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>88230f08708d13ee7a02ecbbb9a724df</md5>
          <size>12012</size>
          <filedate>1486224482</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>f6fa5d86d08d66ab9e6da0eeab63362c</md5>
          <size>13691</size>
          <filedate>1486224482</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>webform_features 7.x-4.0-unstable1</name>
      <version>7.x-4.0-unstable1</version>
      <tag>7.x-4.0-unstable1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>unstable1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-4.0-unstable1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0-unstable1.tar.gz</download_link>
      <date>1381308192</date>
      <mdhash>4a84a5fb1a7cde687b7b5cd37c2424a3</mdhash>
      <filesize>11363</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0-unstable1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a84a5fb1a7cde687b7b5cd37c2424a3</md5>
          <size>11363</size>
          <filedate>1381308192</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.0-unstable1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7e79bb3f877f1b8c8884557b15e297a9</md5>
          <size>12811</size>
          <filedate>1381308193</filedate>
        </file>
      </files>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-3.0-beta3</name>
      <version>7.x-3.0-beta3</version>
      <tag>7.x-3.0-beta3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-3.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta3.tar.gz</download_link>
      <date>1389773906</date>
      <mdhash>3936c6a3c7121a98f6673598f32f397b</mdhash>
      <filesize>11731</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3936c6a3c7121a98f6673598f32f397b</md5>
          <size>11731</size>
          <filedate>1389773906</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c59221d70b30dfef6e3ce03e32672661</md5>
          <size>13269</size>
          <filedate>1389773906</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta2.tar.gz</download_link>
      <date>1382690128</date>
      <mdhash>276ecb1e0f6853a6cabc93375d73f91c</mdhash>
      <filesize>11388</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>276ecb1e0f6853a6cabc93375d73f91c</md5>
          <size>11388</size>
          <filedate>1382690128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1006dc8b8e23d69c760a81e9024b27fe</md5>
          <size>12831</size>
          <filedate>1382690129</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta1.tar.gz</download_link>
      <date>1381307473</date>
      <mdhash>10091db1a71b39129f9c3fc56feb9384</mdhash>
      <filesize>11360</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>10091db1a71b39129f9c3fc56feb9384</md5>
          <size>11360</size>
          <filedate>1381307473</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f501c08a47025aa09a6f974a43c42fdd</md5>
          <size>12809</size>
          <filedate>1381307474</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1381307479</date>
      <mdhash>d632e0ada9f682d1fffbd8e16c3e54e4</mdhash>
      <filesize>11290</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d632e0ada9f682d1fffbd8e16c3e54e4</md5>
          <size>11290</size>
          <filedate>1381307479</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>601c5bd2df90a6bc8a41c1d9ca9fa575</md5>
          <size>12732</size>
          <filedate>1381307479</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-4.x-dev.tar.gz</download_link>
      <date>1487020983</date>
      <mdhash>83419dde5ecc45e2c405277cd1a379b5</mdhash>
      <filesize>11958</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83419dde5ecc45e2c405277cd1a379b5</md5>
          <size>11958</size>
          <filedate>1487020983</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf1dc60420b199758801c6f420e9fe3d</md5>
          <size>13652</size>
          <filedate>1487020983</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>webform_features 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_features/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_features-7.x-3.x-dev.tar.gz</download_link>
      <date>1474735439</date>
      <mdhash>9754447bffca1720792d91f9cfb5b18f</mdhash>
      <filesize>11753</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9754447bffca1720792d91f9cfb5b18f</md5>
          <size>11753</size>
          <filedate>1474735439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_features-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>69b175e8c17e7306615372df77d009c5</md5>
          <size>13422</size>
          <filedate>1474735439</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
