<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>ImageField Focus</title>
  <short_name>imagefield_focus</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/imagefield_focus</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>imagefield_focus 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagefield_focus/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.0.tar.gz</download_link>
      <date>1320601832</date>
      <mdhash>ac639632f4c87074cb313bc81c15b6d8</mdhash>
      <filesize>16795</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac639632f4c87074cb313bc81c15b6d8</md5>
          <size>16795</size>
          <filedate>1320601832</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>16dabf66d812a090f3c0d24b8eb58c59</md5>
          <size>19672</size>
          <filedate>1320601832</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>imagefield_focus 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/imagefield_focus/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.x-dev.tar.gz</download_link>
      <date>1451916539</date>
      <mdhash>5c0278ee69b7aa44400b574ee59c65bc</mdhash>
      <filesize>16726</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5c0278ee69b7aa44400b574ee59c65bc</md5>
          <size>16726</size>
          <filedate>1451916539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/imagefield_focus-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd33de8a78afbe3898fc73866340ebed</md5>
          <size>19966</size>
          <filedate>1451916539</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
