<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Phone</title>
  <short_name>phone</short_name>
  <dc:creator>cweagans</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/phone</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking new maintainer</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>phone 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/phone/-/releases/7.x-1.0</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67671751/packages/generic/phone/7.x-1.0/phone-7.x-1.0.tar.gz</download_link>
      <date>1741088421</date>
      <mdhash>7adec90abe1694e7bb1587da688ac6f1</mdhash>
      <filesize>41279</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67671751/packages/generic/phone/7.x-1.0/phone-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7adec90abe1694e7bb1587da688ac6f1</md5>
          <size>41279</size>
          <filedate>1741088421</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67671751/packages/generic/phone/7.x-1.0/phone-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>4c4a81ee6b483eefecc3ca3e2fd450c4</md5>
          <size>75195</size>
          <filedate>1741088421</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>phone 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/phone/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/phone-7.x-1.0-beta1.tar.gz</download_link>
      <date>1389732205</date>
      <mdhash>fa78af4bd9acb3ac91bd7bede3df6392</mdhash>
      <filesize>38320</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa78af4bd9acb3ac91bd7bede3df6392</md5>
          <size>38320</size>
          <filedate>1389732205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0910657b01a8299bc2d05ae30af698e8</md5>
          <size>70524</size>
          <filedate>1389732205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>phone 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/phone/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/phone-7.x-2.x-dev.tar.gz</download_link>
      <date>1380622978</date>
      <mdhash>fd998b363952fa68fe6e4b7986f22add</mdhash>
      <filesize>38340</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fd998b363952fa68fe6e4b7986f22add</md5>
          <size>38340</size>
          <filedate>1380622978</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>14e43904d6fd7a73c7d32a3860cc6b7a</md5>
          <size>47074</size>
          <filedate>1380622979</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>phone 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/phone/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/phone-7.x-1.x-dev.tar.gz</download_link>
      <date>1389732205</date>
      <mdhash>b754f732b31e088fd002ea07052c292d</mdhash>
      <filesize>38327</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b754f732b31e088fd002ea07052c292d</md5>
          <size>38327</size>
          <filedate>1389732205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/phone-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>41b3888ec362e67a93ccf7ec27adb69a</md5>
          <size>70530</size>
          <filedate>1389732205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
