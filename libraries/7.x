<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Libraries API</title>
  <short_name>libraries</short_name>
  <dc:creator>sun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/libraries</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>libraries 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.5.tar.gz</download_link>
      <date>1538770681</date>
      <mdhash>a0de3b07871e4c50a266df9543690386</mdhash>
      <filesize>44028</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a0de3b07871e4c50a266df9543690386</md5>
          <size>44028</size>
          <filedate>1538770681</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ae79b6d779d28806a60d8e4988ef33a3</md5>
          <size>61781</size>
          <filedate>1538770681</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.4.tar.gz</download_link>
      <date>1536581580</date>
      <mdhash>5da160f836d5d6e06ac424beedff4710</mdhash>
      <filesize>43573</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5da160f836d5d6e06ac424beedff4710</md5>
          <size>43573</size>
          <filedate>1536581580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>7ad46ec39da7b969413cfb91738c40f6</md5>
          <size>61300</size>
          <filedate>1536581580</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.3.tar.gz</download_link>
      <date>1463077439</date>
      <mdhash>294d3e4096c513321159b908cfd7c2be</mdhash>
      <filesize>41863</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>294d3e4096c513321159b908cfd7c2be</md5>
          <size>41863</size>
          <filedate>1463077439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5fef9a8cef52152e2b281181cca6014b</md5>
          <size>58061</size>
          <filedate>1463077439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.2.tar.gz</download_link>
      <date>1391965705</date>
      <mdhash>629663be1ddbbc79531ab98d7d3761dc</mdhash>
      <filesize>33326</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>629663be1ddbbc79531ab98d7d3761dc</md5>
          <size>33326</size>
          <filedate>1391965705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>217bad5a2ce6472186ad5dde0ddc9273</md5>
          <size>46858</size>
          <filedate>1391965705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.1.tar.gz</download_link>
      <date>1362848412</date>
      <mdhash>e240bf43e3a4dd2d595bf24ac34d46bd</mdhash>
      <filesize>30501</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e240bf43e3a4dd2d595bf24ac34d46bd</md5>
          <size>30501</size>
          <filedate>1362848412</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7b9e5c1212e01f2f1de9ceff464abafa</md5>
          <size>40286</size>
          <filedate>1362848412</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.0.tar.gz</download_link>
      <date>1343561873</date>
      <mdhash>bb4707fe9c9c67401c9baa514fbab948</mdhash>
      <filesize>30229</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb4707fe9c9c67401c9baa514fbab948</md5>
          <size>30229</size>
          <filedate>1343561873</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>78a894f4441bf0f79d90dbf2281253b7</md5>
          <size>39993</size>
          <filedate>1343561873</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1323950504</date>
      <mdhash>150a5e24e7f1e849e054ba06921a0311</mdhash>
      <filesize>29501</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>150a5e24e7f1e849e054ba06921a0311</md5>
          <size>29501</size>
          <filedate>1323950504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc6850aae09ae4f1f1c1ad1d95a4d153</md5>
          <size>39184</size>
          <filedate>1323950504</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>libraries 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1317479207</date>
      <mdhash>8e3529be946d9ad0a4b387756cb91a5d</mdhash>
      <filesize>28459</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8e3529be946d9ad0a4b387756cb91a5d</md5>
          <size>28459</size>
          <filedate>1317479207</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b670c741ce95f99fdb0cd0d87573a192</md5>
          <size>38091</size>
          <filedate>1317479207</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>libraries 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-1.0.tar.gz</download_link>
      <date>1296096156</date>
      <mdhash>e395a4229adb94f48de0764654e83967</mdhash>
      <filesize>7921</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e395a4229adb94f48de0764654e83967</md5>
          <size>7921</size>
          <filedate>1296096156</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e2d68b9d64906082c95ffb9da335d60</md5>
          <size>8996</size>
          <filedate>1296096156</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>libraries 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-3.x-dev.tar.gz</download_link>
      <date>1538429880</date>
      <mdhash>c000898ad0bfdf1253463fb580429aa4</mdhash>
      <filesize>33591</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c000898ad0bfdf1253463fb580429aa4</md5>
          <size>33591</size>
          <filedate>1538429880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>32ec5263d05dcda64ce77aff43a94cb7</md5>
          <size>47495</size>
          <filedate>1538429880</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>libraries 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-2.x-dev.tar.gz</download_link>
      <date>1538770380</date>
      <mdhash>801a6d0cb1d0afe1878e73052e838eb2</mdhash>
      <filesize>44038</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>801a6d0cb1d0afe1878e73052e838eb2</md5>
          <size>44038</size>
          <filedate>1538770380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a0086f3b6e584464a8c50ccbe358780b</md5>
          <size>61807</size>
          <filedate>1538770380</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>libraries 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/libraries/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/libraries-7.x-1.x-dev.tar.gz</download_link>
      <date>1406512127</date>
      <mdhash>84247d751f028c55371865aef18fbd11</mdhash>
      <filesize>9041</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>84247d751f028c55371865aef18fbd11</md5>
          <size>9041</size>
          <filedate>1406512127</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/libraries-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>0ef1a919983db0a2a7107af893422188</md5>
          <size>9982</size>
          <filedate>1406512127</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
