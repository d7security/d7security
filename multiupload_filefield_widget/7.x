<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Multiupload Filefield Widget</title>
  <short_name>multiupload_filefield_widget</short_name>
  <dc:creator>czigor</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/multiupload_filefield_widget</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>multiupload_filefield_widget 7.x-1.16</name>
      <version>7.x-1.16</version>
      <tag>7.x-1.16</tag>
      <version_major>1</version_major>
      <version_patch>16</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.16</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.16.tar.gz</download_link>
      <date>1680913878</date>
      <mdhash>abb9862dc0e4111bf452ba5f1e33d789</mdhash>
      <filesize>16266</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.16.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>abb9862dc0e4111bf452ba5f1e33d789</md5>
          <size>16266</size>
          <filedate>1680913878</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.16.zip</url>
          <archive_type>zip</archive_type>
          <md5>f232d4afcb3952a6942febf844132987</md5>
          <size>18875</size>
          <filedate>1680913878</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.15</name>
      <version>7.x-1.15</version>
      <tag>7.x-1.15</tag>
      <version_major>1</version_major>
      <version_patch>15</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.15</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.15.tar.gz</download_link>
      <date>1680700673</date>
      <mdhash>e8962d514d4890826d30879fb8fe0514</mdhash>
      <filesize>16266</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.15.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e8962d514d4890826d30879fb8fe0514</md5>
          <size>16266</size>
          <filedate>1680700673</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.15.zip</url>
          <archive_type>zip</archive_type>
          <md5>a133b6b4ec8e3b68e7608d96b64a5d4f</md5>
          <size>18868</size>
          <filedate>1680700673</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.14</name>
      <version>7.x-1.14</version>
      <tag>7.x-1.14</tag>
      <version_major>1</version_major>
      <version_patch>14</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.14</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.14.tar.gz</download_link>
      <date>1677479391</date>
      <mdhash>d17293864507ee9ad0ce5a52a038e8ed</mdhash>
      <filesize>16201</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.14.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d17293864507ee9ad0ce5a52a038e8ed</md5>
          <size>16201</size>
          <filedate>1677479391</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.14.zip</url>
          <archive_type>zip</archive_type>
          <md5>dd8a27158cc681f418d0e0e1bb675039</md5>
          <size>18804</size>
          <filedate>1677479391</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.13</name>
      <version>7.x-1.13</version>
      <tag>7.x-1.13</tag>
      <version_major>1</version_major>
      <version_patch>13</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.13</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.13.tar.gz</download_link>
      <date>1388873904</date>
      <mdhash>3de61c692a6e91ac5871d08402ad7a68</mdhash>
      <filesize>15895</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.13.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3de61c692a6e91ac5871d08402ad7a68</md5>
          <size>15895</size>
          <filedate>1388873904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.13.zip</url>
          <archive_type>zip</archive_type>
          <md5>36c62f8451b5852a3d12545f5cbfee63</md5>
          <size>18389</size>
          <filedate>1388873904</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.12</name>
      <version>7.x-1.12</version>
      <tag>7.x-1.12</tag>
      <version_major>1</version_major>
      <version_patch>12</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.12</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.12.tar.gz</download_link>
      <date>1385379805</date>
      <mdhash>5d4540108a502fc83121c42cf9cb5575</mdhash>
      <filesize>15870</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.12.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d4540108a502fc83121c42cf9cb5575</md5>
          <size>15870</size>
          <filedate>1385379805</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.12.zip</url>
          <archive_type>zip</archive_type>
          <md5>d33ee73b73515ddd2f51cc88bb41552d</md5>
          <size>18353</size>
          <filedate>1385379805</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.11</name>
      <version>7.x-1.11</version>
      <tag>7.x-1.11</tag>
      <version_major>1</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.11.tar.gz</download_link>
      <date>1351419418</date>
      <mdhash>8f77e37de15185dc2d9603a483051447</mdhash>
      <filesize>15821</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8f77e37de15185dc2d9603a483051447</md5>
          <size>15821</size>
          <filedate>1351419418</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>479cc6739d3f65bd63108558cb82b614</md5>
          <size>18344</size>
          <filedate>1351419418</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.0.tar.gz</download_link>
      <date>1341525109</date>
      <mdhash>2ecf7f05ad65c77c080e98e6b9c490b1</mdhash>
      <filesize>15796</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2ecf7f05ad65c77c080e98e6b9c490b1</md5>
          <size>15796</size>
          <filedate>1341525109</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9493f1c6fe48a1254954a4ffee2608c4</md5>
          <size>18260</size>
          <filedate>1341525109</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>multiupload_filefield_widget 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/multiupload_filefield_widget/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.x-dev.tar.gz</download_link>
      <date>1680913710</date>
      <mdhash>29bd0dd06e761530b76219e04a988fdf</mdhash>
      <filesize>16268</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>29bd0dd06e761530b76219e04a988fdf</md5>
          <size>16268</size>
          <filedate>1680913710</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/multiupload_filefield_widget-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5727d46f6bfcbd5ba6cf3ca86a4e0985</md5>
          <size>18881</size>
          <filedate>1680913710</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
