<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>CKEditor Link</title>
  <short_name>ckeditor_link</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ckeditor_link</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ckeditor_link 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.4.tar.gz</download_link>
      <date>1454115839</date>
      <mdhash>1187db908e6430c318a7e593099a4aaa</mdhash>
      <filesize>16347</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1187db908e6430c318a7e593099a4aaa</md5>
          <size>16347</size>
          <filedate>1454115839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>2137c32a7d4f4115d4412e78ded6acad</md5>
          <size>23495</size>
          <filedate>1454115839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.3.tar.gz</download_link>
      <date>1358413212</date>
      <mdhash>de4716347ada82e024fe44988dacc27e</mdhash>
      <filesize>15989</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>de4716347ada82e024fe44988dacc27e</md5>
          <size>15989</size>
          <filedate>1358413212</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>64909595da3ed2937c7d0f22b18a2ae6</md5>
          <size>22869</size>
          <filedate>1358413212</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.2.tar.gz</download_link>
      <date>1325952038</date>
      <mdhash>e070cfbd93c8f9511d1241a6f1628cd2</mdhash>
      <filesize>15929</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e070cfbd93c8f9511d1241a6f1628cd2</md5>
          <size>15929</size>
          <filedate>1325952038</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc316291aa572c2e25d8ed50e1b29fdf</md5>
          <size>23125</size>
          <filedate>1325952038</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.1.tar.gz</download_link>
      <date>1316266301</date>
      <mdhash>7e5611f21459876fc60b9001be311db7</mdhash>
      <filesize>14698</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7e5611f21459876fc60b9001be311db7</md5>
          <size>14698</size>
          <filedate>1316266301</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3d66cdd190cdcbc3388bb9a758e366d8</md5>
          <size>21763</size>
          <filedate>1316266301</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0.tar.gz</download_link>
      <date>1313249515</date>
      <mdhash>92f1f2f2ce3c13b7b3512f787c2cc378</mdhash>
      <filesize>14576</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92f1f2f2ce3c13b7b3512f787c2cc378</md5>
          <size>14576</size>
          <filedate>1313249515</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9e972f664035bf35834a842028e1786e</md5>
          <size>21281</size>
          <filedate>1313249515</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0-rc1.tar.gz</download_link>
      <date>1309990316</date>
      <mdhash>84da6d967d5383f4e068af45b237008e</mdhash>
      <filesize>14359</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>84da6d967d5383f4e068af45b237008e</md5>
          <size>14359</size>
          <filedate>1309990316</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fe9e72acbded437ca6182ba499646650</md5>
          <size>21036</size>
          <filedate>1309990316</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-1.2-beta1</name>
      <version>7.x-1.2-beta1</version>
      <tag>7.x-1.2-beta1</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-1.2-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-1.2-beta1.tar.gz</download_link>
      <date>1293646879</date>
      <mdhash>75bbfeb5ca882f5a0ba9de778433e43d</mdhash>
      <filesize>9580</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-1.2-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>75bbfeb5ca882f5a0ba9de778433e43d</md5>
          <size>9580</size>
          <filedate>1293646879</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-1.2-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb5a96486a1e3c34c104f4a4daecc7bf</md5>
          <size>11731</size>
          <filedate>1293646879</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ckeditor_link 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ckeditor_link/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.x-dev.tar.gz</download_link>
      <date>1660835961</date>
      <mdhash>4cd378f9f363745b03d53ee05fabb5c0</mdhash>
      <filesize>16261</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4cd378f9f363745b03d53ee05fabb5c0</md5>
          <size>16261</size>
          <filedate>1660835961</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ckeditor_link-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>f133c25974cb112e34cf60b5a87983ed</md5>
          <size>23509</size>
          <filedate>1660835961</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
