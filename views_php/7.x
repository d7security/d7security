<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views PHP</title>
  <short_name>views_php</short_name>
  <dc:creator>casey</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_php</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_php 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.1.tar.gz</download_link>
      <date>1643036073</date>
      <mdhash>3ad8f66b967daec25903c5b57ede3d15</mdhash>
      <filesize>13915</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3ad8f66b967daec25903c5b57ede3d15</md5>
          <size>13915</size>
          <filedate>1643036073</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0ea8b521a85bc1266faca88aa6e21bae</md5>
          <size>20765</size>
          <filedate>1643036073</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_php 7.x-1.1-rc1</name>
      <version>7.x-1.1-rc1</version>
      <tag>7.x-1.1-rc1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.1-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.1-rc1.tar.gz</download_link>
      <date>1641313610</date>
      <mdhash>6c2b4ee477b042a75a50673645414b16</mdhash>
      <filesize>13922</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.1-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6c2b4ee477b042a75a50673645414b16</md5>
          <size>13922</size>
          <filedate>1641313610</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.1-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>919d98281bc7c6f4529ed4c3dd733211</md5>
          <size>20767</size>
          <filedate>1641313610</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0.tar.gz</download_link>
      <date>1616602103</date>
      <mdhash>427f3a3b004dc55b292c8cb12870d4f4</mdhash>
      <filesize>13787</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>427f3a3b004dc55b292c8cb12870d4f4</md5>
          <size>13787</size>
          <filedate>1616602103</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>613a023531f3a07ebb1f3b71689d8dd1</md5>
          <size>20618</size>
          <filedate>1616602103</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_php 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-rc1.tar.gz</download_link>
      <date>1615388912</date>
      <mdhash>73d04c5e1abfd33308e36d257fdcff10</mdhash>
      <filesize>13797</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>73d04c5e1abfd33308e36d257fdcff10</md5>
          <size>13797</size>
          <filedate>1615388912</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9be9cddd39d1039e5d22f89c22e9472e</md5>
          <size>20622</size>
          <filedate>1615388912</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-beta1.tar.gz</download_link>
      <date>1614086093</date>
      <mdhash>f87ede53efabd7b324f6cfe764796b86</mdhash>
      <filesize>13800</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f87ede53efabd7b324f6cfe764796b86</md5>
          <size>13800</size>
          <filedate>1614086093</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b971ebee1cd2f822c09697a5107dbe3a</md5>
          <size>20629</size>
          <filedate>1614086093</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1447385940</date>
      <mdhash>5ab8022628da16abff960aba32c7bda3</mdhash>
      <filesize>13790</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ab8022628da16abff960aba32c7bda3</md5>
          <size>13790</size>
          <filedate>1447385940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>1e04bb22d310f4bfe0fa59cedcf00563</md5>
          <size>20553</size>
          <filedate>1447385940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1447294139</date>
      <mdhash>3627cbe05ba535f7028b6e5673b7c7ed</mdhash>
      <filesize>13772</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3627cbe05ba535f7028b6e5673b7c7ed</md5>
          <size>13772</size>
          <filedate>1447294139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c4874455675800f89eb2a9dc4d0f08b</md5>
          <size>20534</size>
          <filedate>1447294139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1390577312</date>
      <mdhash>aeec87fc772e1c8573911db6c76152bd</mdhash>
      <filesize>13750</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aeec87fc772e1c8573911db6c76152bd</md5>
          <size>13750</size>
          <filedate>1390577312</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f32ee82f573350cbb7b291eb2240f50</md5>
          <size>20168</size>
          <filedate>1390577312</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-2.x-dev.tar.gz</download_link>
      <date>1618675942</date>
      <mdhash>e58c208317abdca49259a32d445beedb</mdhash>
      <filesize>14330</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e58c208317abdca49259a32d445beedb</md5>
          <size>14330</size>
          <filedate>1618675942</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>01a33b445c0d6decef504775b3811255</md5>
          <size>21135</size>
          <filedate>1618675942</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_php 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_php/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_php-7.x-1.x-dev.tar.gz</download_link>
      <date>1639600528</date>
      <mdhash>64693d782ba4a8ea94e4fddf6780a754</mdhash>
      <filesize>13918</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>64693d782ba4a8ea94e4fddf6780a754</md5>
          <size>13918</size>
          <filedate>1639600528</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_php-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d7d49f25378218d89ad7a6bbc4e5373</md5>
          <size>20770</size>
          <filedate>1639600528</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
