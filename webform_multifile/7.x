<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Webform Multiple File Upload</title>
  <short_name>webform_multifile</short_name>
  <dc:creator>Unsupported Projects</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/webform_multifile</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Unsupported</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>webform_multifile 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/webform_multifile/-/releases/7.x-1.7</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66212224/packages/generic/webform_multifile/7.x-1.7/webform_multifile-7.x-1.7.tar.gz</download_link>
      <date>1737033206</date>
      <mdhash>b89a5667c7dd27cbe6f8ec0d39051593</mdhash>
      <filesize>21944</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66212224/packages/generic/webform_multifile/7.x-1.7/webform_multifile-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b89a5667c7dd27cbe6f8ec0d39051593</md5>
          <size>21944</size>
          <filedate>1737033206</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66212224/packages/generic/webform_multifile/7.x-1.7/webform_multifile-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>9f05c81265716725ae075876a47bd654</md5>
          <size>24582</size>
          <filedate>1737033206</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.6.tar.gz</download_link>
      <date>1494424085</date>
      <mdhash>b7c0597846131902d11c2e86341a665c</mdhash>
      <filesize>101907</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b7c0597846131902d11c2e86341a665c</md5>
          <size>101907</size>
          <filedate>1494424085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>19e80e1de5afc0ea626bf89502c6394d</md5>
          <size>109465</size>
          <filedate>1494424085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.5.tar.gz</download_link>
      <date>1475735039</date>
      <mdhash>d1e34a22921b35b556cc0e50f3c454b2</mdhash>
      <filesize>102649</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d1e34a22921b35b556cc0e50f3c454b2</md5>
          <size>102649</size>
          <filedate>1475735039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e1cf89951fc6d72d3b88c7bb359da3f</md5>
          <size>109324</size>
          <filedate>1475735039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.4.tar.gz</download_link>
      <date>1468396439</date>
      <mdhash>a86f04408be44db5a46b6bc4b0a2ba5a</mdhash>
      <filesize>102525</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a86f04408be44db5a46b6bc4b0a2ba5a</md5>
          <size>102525</size>
          <filedate>1468396439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>0a43f89a191e1caea78f3047db90994a</md5>
          <size>109170</size>
          <filedate>1468396439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.3.tar.gz</download_link>
      <date>1427275981</date>
      <mdhash>c4ba15c41762fbc19da0813b349a6582</mdhash>
      <filesize>100963</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c4ba15c41762fbc19da0813b349a6582</md5>
          <size>100963</size>
          <filedate>1427275981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a136eaa694ab2b0e104af4b17d2e81e3</md5>
          <size>107282</size>
          <filedate>1427275981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.2.tar.gz</download_link>
      <date>1340091161</date>
      <mdhash>e8e585f8a84dcbefa1b8cbad86818bac</mdhash>
      <filesize>100345</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e8e585f8a84dcbefa1b8cbad86818bac</md5>
          <size>100345</size>
          <filedate>1340091161</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9f2f359de5a990b6d76f4cba6c45b4df</md5>
          <size>106820</size>
          <filedate>1340091161</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
    <release>
      <name>webform_multifile 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/webform_multifile/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.x-dev.tar.gz</download_link>
      <date>1494424085</date>
      <mdhash>932c4ed76f4d6f1fd5eb36abb7256b4b</mdhash>
      <filesize>101911</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>932c4ed76f4d6f1fd5eb36abb7256b4b</md5>
          <size>101911</size>
          <filedate>1494424085</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/webform_multifile-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>1493fae848f9b689d88271abd3e363c0</md5>
          <size>109471</size>
          <filedate>1494424085</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has been unsupported by the Drupal Security Team</security>
    </release>
  </releases>
</project>
