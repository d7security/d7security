<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Node Revision Delete</title>
  <short_name>node_revision_delete</short_name>
  <dc:creator>kaushalkishorejaiswal</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/node_revision_delete</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>node_revision_delete 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/node_revision_delete/-/releases/7.x-3.3</release_link>
      <download_link>https://gitlab.com/api/v4/projects/67457596/packages/generic/node_revision_delete/7.x-3.3/node_revision_delete-7.x-3.3.tar.gz</download_link>
      <date>1740492294</date>
      <mdhash>5b76a9e78c674d4f9790d5f2c261edbf</mdhash>
      <filesize>28895</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/67457596/packages/generic/node_revision_delete/7.x-3.3/node_revision_delete-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5b76a9e78c674d4f9790d5f2c261edbf</md5>
          <size>28895</size>
          <filedate>1740492294</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/67457596/packages/generic/node_revision_delete/7.x-3.3/node_revision_delete-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>01412f113cc6b4fd32863c666595ea6e</md5>
          <size>37810</size>
          <filedate>1740492294</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.2.tar.gz</download_link>
      <date>1600878137</date>
      <mdhash>512a183d16eab5532d8a6d5a1ba208d6</mdhash>
      <filesize>28410</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>512a183d16eab5532d8a6d5a1ba208d6</md5>
          <size>28410</size>
          <filedate>1600878137</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>37cf33b24f0079760956bdef5664a6fa</md5>
          <size>41379</size>
          <filedate>1600878137</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.1.tar.gz</download_link>
      <date>1583947066</date>
      <mdhash>6b5c61b253d459376622e9f06f1066f4</mdhash>
      <filesize>28461</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b5c61b253d459376622e9f06f1066f4</md5>
          <size>28461</size>
          <filedate>1583947066</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0cf7ed708376e88f641fd711de0eb8d3</md5>
          <size>41461</size>
          <filedate>1583947066</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0.tar.gz</download_link>
      <date>1582666778</date>
      <mdhash>8c147e1f835ee876c941fec2a76e378f</mdhash>
      <filesize>28036</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8c147e1f835ee876c941fec2a76e378f</md5>
          <size>28036</size>
          <filedate>1582666778</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>1ea862240b31f7d150bcf5be696e5071</md5>
          <size>37569</size>
          <filedate>1582666778</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-rc2</name>
      <version>7.x-3.0-rc2</version>
      <tag>7.x-3.0-rc2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc2.tar.gz</download_link>
      <date>1576187885</date>
      <mdhash>95f6fa97069ae0a287642b5885481202</mdhash>
      <filesize>27622</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>95f6fa97069ae0a287642b5885481202</md5>
          <size>27622</size>
          <filedate>1576187885</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>61165968c964f48a8533332128b82eab</md5>
          <size>37030</size>
          <filedate>1576187885</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc1.tar.gz</download_link>
      <date>1576018380</date>
      <mdhash>5ae8310db033c95b693ea90e134ab639</mdhash>
      <filesize>27576</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ae8310db033c95b693ea90e134ab639</md5>
          <size>27576</size>
          <filedate>1576018380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3ea19e98f1e9f504cc212ba64cdd090c</md5>
          <size>36983</size>
          <filedate>1576018380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-beta2</name>
      <version>7.x-3.0-beta2</version>
      <tag>7.x-3.0-beta2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta2.tar.gz</download_link>
      <date>1572380583</date>
      <mdhash>f4bcd49b2b173c537ef7d4cbb2fac93f</mdhash>
      <filesize>27203</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f4bcd49b2b173c537ef7d4cbb2fac93f</md5>
          <size>27203</size>
          <filedate>1572380583</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>e0613e1c16dbbd0975d6a9c78d15e3fc</md5>
          <size>36560</size>
          <filedate>1572380583</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-beta1</name>
      <version>7.x-3.0-beta1</version>
      <tag>7.x-3.0-beta1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta1.tar.gz</download_link>
      <date>1502387343</date>
      <mdhash>da83b181484e6d0eea41e24b891a925a</mdhash>
      <filesize>23255</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>da83b181484e6d0eea41e24b891a925a</md5>
          <size>23255</size>
          <filedate>1502387343</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6246f183728f6239c1821fa5e79d1a22</md5>
          <size>28579</size>
          <filedate>1502387343</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1500335043</date>
      <mdhash>4e4b6c7938da1d825c26585e8af47fbe</mdhash>
      <filesize>21430</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e4b6c7938da1d825c26585e8af47fbe</md5>
          <size>21430</size>
          <filedate>1500335043</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7890540e241fa63c19a1428e53a5b034</md5>
          <size>26535</size>
          <filedate>1500335043</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1497977042</date>
      <mdhash>09afbe56f825da1e6d7227b7e3161e89</mdhash>
      <filesize>19496</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>09afbe56f825da1e6d7227b7e3161e89</md5>
          <size>19496</size>
          <filedate>1497977042</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7f6bc218dbcdd293aad72ca7ace5b5eb</md5>
          <size>24340</size>
          <filedate>1497977042</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.7.tar.gz</download_link>
      <date>1495747083</date>
      <mdhash>74d1e7976022cd820e6f639d37c8c0af</mdhash>
      <filesize>16028</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>74d1e7976022cd820e6f639d37c8c0af</md5>
          <size>16028</size>
          <filedate>1495747083</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>38080fc3f91faa2c5ef4a51643fcb473</md5>
          <size>20539</size>
          <filedate>1495747083</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.6.tar.gz</download_link>
      <date>1409588932</date>
      <mdhash>630ba920861155b445efb4089e4c9fd8</mdhash>
      <filesize>12480</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>630ba920861155b445efb4089e4c9fd8</md5>
          <size>12480</size>
          <filedate>1409588932</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>3a9c900371ff142d894d4f731b353e6f</md5>
          <size>14807</size>
          <filedate>1409588932</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.5.tar.gz</download_link>
      <date>1409583232</date>
      <mdhash>19bc643fedde64272991ea3b219529e0</mdhash>
      <filesize>12362</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>19bc643fedde64272991ea3b219529e0</md5>
          <size>12362</size>
          <filedate>1409583232</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>2d4df287d3f4f2010c8d339a287791b2</md5>
          <size>14687</size>
          <filedate>1409583232</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.4.tar.gz</download_link>
      <date>1400084027</date>
      <mdhash>b0b98c98ec2f4676a940f1fd8751520a</mdhash>
      <filesize>11528</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b0b98c98ec2f4676a940f1fd8751520a</md5>
          <size>11528</size>
          <filedate>1400084027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>31bd4c5119c55a7a5a03932d67892e9d</md5>
          <size>13252</size>
          <filedate>1400084027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.3.tar.gz</download_link>
      <date>1400075027</date>
      <mdhash>2c6e1a78c0c2408b7914c0210473e2b4</mdhash>
      <filesize>11529</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2c6e1a78c0c2408b7914c0210473e2b4</md5>
          <size>11529</size>
          <filedate>1400075027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7999e0361ab2e71d89cbcac8b7342ebe</md5>
          <size>13252</size>
          <filedate>1400075027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.2.tar.gz</download_link>
      <date>1398901427</date>
      <mdhash>a03a3f71b6f4008f35180fec1ae33b63</mdhash>
      <filesize>10783</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a03a3f71b6f4008f35180fec1ae33b63</md5>
          <size>10783</size>
          <filedate>1398901427</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f6422e751907808b2f806f8119f048e</md5>
          <size>12407</size>
          <filedate>1398901427</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.1.tar.gz</download_link>
      <date>1398773327</date>
      <mdhash>52c630f770a3ec3fae1cfc993e0d3415</mdhash>
      <filesize>10824</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>52c630f770a3ec3fae1cfc993e0d3415</md5>
          <size>10824</size>
          <filedate>1398773327</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>24dd5f8b667e470c4b1c305c234cf3d9</md5>
          <size>12436</size>
          <filedate>1398773327</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.0.tar.gz</download_link>
      <date>1398769727</date>
      <mdhash>f911a8da1d855d51ae4c1a3c48440538</mdhash>
      <filesize>10812</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f911a8da1d855d51ae4c1a3c48440538</md5>
          <size>10812</size>
          <filedate>1398769727</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>95c39d1cc96e1e70585f3c204583e7d2</md5>
          <size>12423</size>
          <filedate>1398769727</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-1.2-unstable1</name>
      <version>7.x-1.2-unstable1</version>
      <tag>7.x-1.2-unstable1</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <version_extra>unstable1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-1.2-unstable1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.2-unstable1.tar.gz</download_link>
      <date>1350932510</date>
      <mdhash>1d38aee299738e5cca73d1f01b1d6ff5</mdhash>
      <filesize>9712</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.2-unstable1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1d38aee299738e5cca73d1f01b1d6ff5</md5>
          <size>9712</size>
          <filedate>1350932510</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.2-unstable1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ba2c5c9d4560d8616280f2cdb594024a</md5>
          <size>10837</size>
          <filedate>1350932510</filedate>
        </file>
      </files>
      <security>Unstable releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.1.tar.gz</download_link>
      <date>1347551592</date>
      <mdhash>d09f39c54914beab459f8032fe46fa66</mdhash>
      <filesize>9705</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d09f39c54914beab459f8032fe46fa66</md5>
          <size>9705</size>
          <filedate>1347551592</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>159b4524a061fee419ac07cd82068c35</md5>
          <size>10830</size>
          <filedate>1347551592</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.0.tar.gz</download_link>
      <date>1347551593</date>
      <mdhash>17844cfb8cad452bcf6bb624fd4c5211</mdhash>
      <filesize>9705</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>17844cfb8cad452bcf6bb624fd4c5211</md5>
          <size>9705</size>
          <filedate>1347551593</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>59b856d387685909923c1113862f39e7</md5>
          <size>10830</size>
          <filedate>1347551593</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.x-dev.tar.gz</download_link>
      <date>1655087898</date>
      <mdhash>f8ae5c3065ce80df2b41e2359e92551f</mdhash>
      <filesize>28013</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f8ae5c3065ce80df2b41e2359e92551f</md5>
          <size>28013</size>
          <filedate>1655087898</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>e3bcb94defe2faeab2b53f781115611d</md5>
          <size>37557</size>
          <filedate>1655087898</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.x-dev.tar.gz</download_link>
      <date>1559310491</date>
      <mdhash>8457f5080421fa2d35a54d311b852f9c</mdhash>
      <filesize>16208</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8457f5080421fa2d35a54d311b852f9c</md5>
          <size>16208</size>
          <filedate>1559310491</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>b1c8ff87bc0aee51734d0f43bff43a9e</md5>
          <size>20727</size>
          <filedate>1559310491</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_revision_delete 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_revision_delete/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.x-dev.tar.gz</download_link>
      <date>1380594364</date>
      <mdhash>8761422a34d4b2c6736916ab818e7373</mdhash>
      <filesize>9715</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8761422a34d4b2c6736916ab818e7373</md5>
          <size>9715</size>
          <filedate>1380594364</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_revision_delete-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>251fe2315625765669742488006b0d7f</md5>
          <size>10836</size>
          <filedate>1380594365</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
