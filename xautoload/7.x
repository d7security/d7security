<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>X Autoload</title>
  <short_name>xautoload</short_name>
  <dc:creator>donquixote</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/xautoload</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Integrations</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>xautoload 7.x-5.9</name>
      <version>7.x-5.9</version>
      <tag>7.x-5.9</tag>
      <version_major>5</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.9.tar.gz</download_link>
      <date>1678492491</date>
      <mdhash>308172904745394a05bd6e292db55394</mdhash>
      <filesize>86099</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>308172904745394a05bd6e292db55394</md5>
          <size>86099</size>
          <filedate>1678492491</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>07193cb17cb5d3efbcc602efa1a0d2ad</md5>
          <size>182091</size>
          <filedate>1678492491</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.8</name>
      <version>7.x-5.8</version>
      <tag>7.x-5.8</tag>
      <version_major>5</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.8.tar.gz</download_link>
      <date>1591465422</date>
      <mdhash>51114ffe4c8f7ab830f45f32e07642a0</mdhash>
      <filesize>83371</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>51114ffe4c8f7ab830f45f32e07642a0</md5>
          <size>83371</size>
          <filedate>1591465422</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>59aeb6e49fde1cfa0752e85af0511162</md5>
          <size>179124</size>
          <filedate>1591465422</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.7</name>
      <version>7.x-5.7</version>
      <tag>7.x-5.7</tag>
      <version_major>5</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.7.tar.gz</download_link>
      <date>1461353639</date>
      <mdhash>774c1ee80265b111e966bfdf3a850e64</mdhash>
      <filesize>84716</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>774c1ee80265b111e966bfdf3a850e64</md5>
          <size>84716</size>
          <filedate>1461353639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>7144b8fac9afaa480d500fa81e15c426</md5>
          <size>179043</size>
          <filedate>1461353639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.6</name>
      <version>7.x-5.6</version>
      <tag>7.x-5.6</tag>
      <version_major>5</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.6.tar.gz</download_link>
      <date>1457203139</date>
      <mdhash>0d1fa4bcd80391d7573f1ab2c4b17b30</mdhash>
      <filesize>84672</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0d1fa4bcd80391d7573f1ab2c4b17b30</md5>
          <size>84672</size>
          <filedate>1457203139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>2759331568dd484a64354515e128a1c9</md5>
          <size>179011</size>
          <filedate>1457203139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.5</name>
      <version>7.x-5.5</version>
      <tag>7.x-5.5</tag>
      <version_major>5</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.5.tar.gz</download_link>
      <date>1442068739</date>
      <mdhash>4b38adafaf2923870c486ef87573ab7d</mdhash>
      <filesize>84700</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4b38adafaf2923870c486ef87573ab7d</md5>
          <size>84700</size>
          <filedate>1442068739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7648160533a0f6798dbbc79065feadd</md5>
          <size>179024</size>
          <filedate>1442068739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.4</name>
      <version>7.x-5.4</version>
      <tag>7.x-5.4</tag>
      <version_major>5</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.4.tar.gz</download_link>
      <date>1442018339</date>
      <mdhash>35d132bac954b19f043b1f415faaad0f</mdhash>
      <filesize>84250</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35d132bac954b19f043b1f415faaad0f</md5>
          <size>84250</size>
          <filedate>1442018339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>a80da185ba10aaeaf9da2234930d7674</md5>
          <size>177791</size>
          <filedate>1442018339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.2</name>
      <version>7.x-5.2</version>
      <tag>7.x-5.2</tag>
      <version_major>5</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.2.tar.gz</download_link>
      <date>1436865839</date>
      <mdhash>c1ecd560767bd5b69dad326766cd67f3</mdhash>
      <filesize>82849</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c1ecd560767bd5b69dad326766cd67f3</md5>
          <size>82849</size>
          <filedate>1436865839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6bcbaf49e826543341d033879aae8485</md5>
          <size>174555</size>
          <filedate>1436865839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.1</name>
      <version>7.x-5.1</version>
      <tag>7.x-5.1</tag>
      <version_major>5</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.1.tar.gz</download_link>
      <date>1416520981</date>
      <mdhash>6c3dc7e6241184f3ec5b08916480312d</mdhash>
      <filesize>82681</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6c3dc7e6241184f3ec5b08916480312d</md5>
          <size>82681</size>
          <filedate>1416520981</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a6020844c1a71caf46993fcaec4748d</md5>
          <size>174354</size>
          <filedate>1416520981</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.0</name>
      <version>7.x-5.0</version>
      <tag>7.x-5.0</tag>
      <version_major>5</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0.tar.gz</download_link>
      <date>1410310128</date>
      <mdhash>eda0a234b13acfeb8a1d098dd60618b7</mdhash>
      <filesize>82535</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>eda0a234b13acfeb8a1d098dd60618b7</md5>
          <size>82535</size>
          <filedate>1410310128</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>366c6bf12d9453d7087cf3e79941c16f</md5>
          <size>174099</size>
          <filedate>1410310128</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-5.0-beta3</name>
      <version>7.x-5.0-beta3</version>
      <tag>7.x-5.0-beta3</tag>
      <version_major>5</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta3.tar.gz</download_link>
      <date>1403975027</date>
      <mdhash>95e027ef86a587ae0e5fdf9d74b53d71</mdhash>
      <filesize>82467</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>95e027ef86a587ae0e5fdf9d74b53d71</md5>
          <size>82467</size>
          <filedate>1403975027</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>45f0895644d051505bc566f6f2ca0406</md5>
          <size>169379</size>
          <filedate>1403975027</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-5.0-beta2</name>
      <version>7.x-5.0-beta2</version>
      <tag>7.x-5.0-beta2</tag>
      <version_major>5</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta2.tar.gz</download_link>
      <date>1402947227</date>
      <mdhash>0f12238fd2b2b7986711aeea03988620</mdhash>
      <filesize>81751</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0f12238fd2b2b7986711aeea03988620</md5>
          <size>81751</size>
          <filedate>1402947227</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b3d551e8a882ce455f013239aae0c00</md5>
          <size>166128</size>
          <filedate>1402947227</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-5.0-beta1</name>
      <version>7.x-5.0-beta1</version>
      <tag>7.x-5.0-beta1</tag>
      <version_major>5</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta1.tar.gz</download_link>
      <date>1400643827</date>
      <mdhash>06a498e27b46922bf5bb93661a13987f</mdhash>
      <filesize>74002</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>06a498e27b46922bf5bb93661a13987f</md5>
          <size>74002</size>
          <filedate>1400643827</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>0a581b0fc2c68cf5eaec0921b74d179d</md5>
          <size>144685</size>
          <filedate>1400643827</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-4.5</name>
      <version>7.x-4.5</version>
      <tag>7.x-4.5</tag>
      <version_major>4</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.5.tar.gz</download_link>
      <date>1396624750</date>
      <mdhash>9672312ea488268106553ef4d624cd0e</mdhash>
      <filesize>48670</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9672312ea488268106553ef4d624cd0e</md5>
          <size>48670</size>
          <filedate>1396624750</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>90f2b0465134a6cbb76f00fe41bd42f6</md5>
          <size>97958</size>
          <filedate>1396624750</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.4</name>
      <version>7.x-4.4</version>
      <tag>7.x-4.4</tag>
      <version_major>4</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.4.tar.gz</download_link>
      <date>1396307045</date>
      <mdhash>8f5073248070d296902dda7bdecf653e</mdhash>
      <filesize>48667</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8f5073248070d296902dda7bdecf653e</md5>
          <size>48667</size>
          <filedate>1396307045</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>b50eb9836a6f88e9c8b0fed306812d63</md5>
          <size>97879</size>
          <filedate>1396307045</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.3</name>
      <version>7.x-4.3</version>
      <tag>7.x-4.3</tag>
      <version_major>4</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.3.tar.gz</download_link>
      <date>1395418757</date>
      <mdhash>1e140a1c74108b1b0c18dbf08c6ad6f7</mdhash>
      <filesize>48549</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1e140a1c74108b1b0c18dbf08c6ad6f7</md5>
          <size>48549</size>
          <filedate>1395418757</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>f2f9b3aa267ce5e0f14bcfa02878570e</md5>
          <size>97854</size>
          <filedate>1395418757</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.2</name>
      <version>7.x-4.2</version>
      <tag>7.x-4.2</tag>
      <version_major>4</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.2.tar.gz</download_link>
      <date>1393171105</date>
      <mdhash>e357745dc9d26d6dd1d3cb8e76ac6241</mdhash>
      <filesize>48072</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e357745dc9d26d6dd1d3cb8e76ac6241</md5>
          <size>48072</size>
          <filedate>1393171105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc24cd7683ba318263055a93d8d16b70</md5>
          <size>96858</size>
          <filedate>1393171105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.1</name>
      <version>7.x-4.1</version>
      <tag>7.x-4.1</tag>
      <version_major>4</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.1.tar.gz</download_link>
      <date>1392049105</date>
      <mdhash>e22557c26e65ffb6ab3142f3614d33d5</mdhash>
      <filesize>47985</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e22557c26e65ffb6ab3142f3614d33d5</md5>
          <size>47985</size>
          <filedate>1392049105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>73ca1256f4df89e69ba808a1e011943e</md5>
          <size>96751</size>
          <filedate>1392049105</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.0</name>
      <version>7.x-4.0</version>
      <tag>7.x-4.0</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0.tar.gz</download_link>
      <date>1392049105</date>
      <mdhash>62816ce81c0a371ad0eb85284a010071</mdhash>
      <filesize>47982</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>62816ce81c0a371ad0eb85284a010071</md5>
          <size>47982</size>
          <filedate>1392049105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>df128da79ed74c36bb417e1bd96f84e9</md5>
          <size>96750</size>
          <filedate>1392049105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-4.0-alpha4</name>
      <version>7.x-4.0-alpha4</version>
      <tag>7.x-4.0-alpha4</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha4.tar.gz</download_link>
      <date>1388251105</date>
      <mdhash>47f6f52c4f329615d295c7a41c0a5648</mdhash>
      <filesize>47477</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>47f6f52c4f329615d295c7a41c0a5648</md5>
          <size>47477</size>
          <filedate>1388251105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>51dcb59af08e6e405e96b0db3b1b9045</md5>
          <size>96240</size>
          <filedate>1388251105</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-4.0-alpha3</name>
      <version>7.x-4.0-alpha3</version>
      <tag>7.x-4.0-alpha3</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha3.tar.gz</download_link>
      <date>1387897105</date>
      <mdhash>a3f7303f93f7c7ef64a9b3c3526b5d71</mdhash>
      <filesize>47878</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a3f7303f93f7c7ef64a9b3c3526b5d71</md5>
          <size>47878</size>
          <filedate>1387897105</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>e55976947c32fea2c6a162e1737e8a7c</md5>
          <size>97001</size>
          <filedate>1387897105</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-4.0-alpha2</name>
      <version>7.x-4.0-alpha2</version>
      <tag>7.x-4.0-alpha2</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha2.tar.gz</download_link>
      <date>1387892006</date>
      <mdhash>5e6b95a6daf3e97059778374b33fda10</mdhash>
      <filesize>47659</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5e6b95a6daf3e97059778374b33fda10</md5>
          <size>47659</size>
          <filedate>1387892006</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>ee5808976862f50f52e6f5dae80b2ecd</md5>
          <size>96212</size>
          <filedate>1387892006</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-4.0-alpha1</name>
      <version>7.x-4.0-alpha1</version>
      <tag>7.x-4.0-alpha1</tag>
      <version_major>4</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha1.tar.gz</download_link>
      <date>1387816104</date>
      <mdhash>b5a3019a1298169afa77d0a688ddfb79</mdhash>
      <filesize>46689</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b5a3019a1298169afa77d0a688ddfb79</md5>
          <size>46689</size>
          <filedate>1387816104</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7bfdaf196d958daaa7a99b60fb1d2af</md5>
          <size>94075</size>
          <filedate>1387816104</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.6</name>
      <version>7.x-3.6</version>
      <tag>7.x-3.6</tag>
      <version_major>3</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.6.tar.gz</download_link>
      <date>1391769505</date>
      <mdhash>df2e6de3132bf631bb8a02ad269f5ed8</mdhash>
      <filesize>31710</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>df2e6de3132bf631bb8a02ad269f5ed8</md5>
          <size>31710</size>
          <filedate>1391769505</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>14dd2f2cb5b3d12f96d3ef6c0e382f22</md5>
          <size>59257</size>
          <filedate>1391769505</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.5</name>
      <version>7.x-3.5</version>
      <tag>7.x-3.5</tag>
      <version_major>3</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.5.tar.gz</download_link>
      <date>1388245404</date>
      <mdhash>a8d40fa686cc5c1def2fdbb144cb6af0</mdhash>
      <filesize>31682</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a8d40fa686cc5c1def2fdbb144cb6af0</md5>
          <size>31682</size>
          <filedate>1388245404</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>c4cbfb99cc4b75b071a3a091cadce0f3</md5>
          <size>59210</size>
          <filedate>1388245404</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.4.tar.gz</download_link>
      <date>1387815805</date>
      <mdhash>afed98dc3984fd41b95207989a2d7821</mdhash>
      <filesize>31520</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>afed98dc3984fd41b95207989a2d7821</md5>
          <size>31520</size>
          <filedate>1387815805</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>35c2d7da79263f1376ba7454d46c97e5</md5>
          <size>59078</size>
          <filedate>1387815805</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.3.tar.gz</download_link>
      <date>1380217149</date>
      <mdhash>9389fb89704cbf424d3326a48b30bbf1</mdhash>
      <filesize>31771</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9389fb89704cbf424d3326a48b30bbf1</md5>
          <size>31771</size>
          <filedate>1380217149</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a5d99a9fe0f72c2db5eb1adc0266e7a8</md5>
          <size>59065</size>
          <filedate>1380217149</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.2.tar.gz</download_link>
      <date>1374942429</date>
      <mdhash>69a5cbeb213126486eb5c96cb7aa9b01</mdhash>
      <filesize>31242</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>69a5cbeb213126486eb5c96cb7aa9b01</md5>
          <size>31242</size>
          <filedate>1374942429</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f9fa9ea33f860a872f5290478c33b41</md5>
          <size>58338</size>
          <filedate>1374942429</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.1.tar.gz</download_link>
      <date>1368657915</date>
      <mdhash>65c6dcaa552ab131e86d41992d656b9a</mdhash>
      <filesize>31199</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>65c6dcaa552ab131e86d41992d656b9a</md5>
          <size>31199</size>
          <filedate>1368657915</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b40f2ccf37dc146f79cbdf3848804abe</md5>
          <size>58321</size>
          <filedate>1368657915</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0.tar.gz</download_link>
      <date>1366631058</date>
      <mdhash>42c0fc0debcf86f02bb2880fdaacd0f1</mdhash>
      <filesize>31192</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42c0fc0debcf86f02bb2880fdaacd0f1</md5>
          <size>31192</size>
          <filedate>1366631058</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b03fdcba4ec12994258bca5b5323d30c</md5>
          <size>58711</size>
          <filedate>1366631058</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha6</name>
      <version>7.x-3.0-alpha6</version>
      <tag>7.x-3.0-alpha6</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha6.tar.gz</download_link>
      <date>1360776952</date>
      <mdhash>0420820e19a7b9f6336da7795d0a054f</mdhash>
      <filesize>25165</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0420820e19a7b9f6336da7795d0a054f</md5>
          <size>25165</size>
          <filedate>1360776952</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha6.zip</url>
          <archive_type>zip</archive_type>
          <md5>52cc89238fd0b36d44b218850bc3913f</md5>
          <size>43718</size>
          <filedate>1360776952</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha5</name>
      <version>7.x-3.0-alpha5</version>
      <tag>7.x-3.0-alpha5</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha5.tar.gz</download_link>
      <date>1360698958</date>
      <mdhash>13b8dfff0a18a681584f4c3167e0c273</mdhash>
      <filesize>24973</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>13b8dfff0a18a681584f4c3167e0c273</md5>
          <size>24973</size>
          <filedate>1360698958</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>c3162d5b90810f79c2e9dc2b65201ef5</md5>
          <size>43423</size>
          <filedate>1360698958</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha4</name>
      <version>7.x-3.0-alpha4</version>
      <tag>7.x-3.0-alpha4</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha4.tar.gz</download_link>
      <date>1360698624</date>
      <mdhash>dfa1878e08134d967857e64b6c1b2660</mdhash>
      <filesize>25034</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dfa1878e08134d967857e64b6c1b2660</md5>
          <size>25034</size>
          <filedate>1360698624</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>30672a543f2fd56a9e5fbe788b9ff8de</md5>
          <size>43501</size>
          <filedate>1360698624</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha3</name>
      <version>7.x-3.0-alpha3</version>
      <tag>7.x-3.0-alpha3</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha3.tar.gz</download_link>
      <date>1360563254</date>
      <mdhash>24999f659cc176f9cebf07477453066e</mdhash>
      <filesize>24423</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24999f659cc176f9cebf07477453066e</md5>
          <size>24423</size>
          <filedate>1360563254</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f533670f264794775edde08ecce7f25</md5>
          <size>42081</size>
          <filedate>1360563254</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha2</name>
      <version>7.x-3.0-alpha2</version>
      <tag>7.x-3.0-alpha2</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha2.tar.gz</download_link>
      <date>1360562117</date>
      <mdhash>679c494e861387c165a990793639ee1b</mdhash>
      <filesize>24417</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>679c494e861387c165a990793639ee1b</md5>
          <size>24417</size>
          <filedate>1360562117</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb7c6848249c313fe886d75c52b02cd8</md5>
          <size>42072</size>
          <filedate>1360562117</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.0-alpha1</name>
      <version>7.x-3.0-alpha1</version>
      <tag>7.x-3.0-alpha1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha1.tar.gz</download_link>
      <date>1360557326</date>
      <mdhash>98ff7b9e59c28bb5c4ca004a2ea77eb4</mdhash>
      <filesize>24266</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>98ff7b9e59c28bb5c4ca004a2ea77eb4</md5>
          <size>24266</size>
          <filedate>1360557326</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>dc28efd8d42b3a105539a43607caf9c7</md5>
          <size>41849</size>
          <filedate>1360557326</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.7.tar.gz</download_link>
      <date>1352817214</date>
      <mdhash>35ceb6acc0e8862a4e2432ef25750dd3</mdhash>
      <filesize>21958</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>35ceb6acc0e8862a4e2432ef25750dd3</md5>
          <size>21958</size>
          <filedate>1352817214</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>a4e171c586d3a75f7820a7346c7da717</md5>
          <size>36972</size>
          <filedate>1352817214</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.6.tar.gz</download_link>
      <date>1352251344</date>
      <mdhash>d505f7680833c494b292cb601aa3c9ea</mdhash>
      <filesize>21951</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d505f7680833c494b292cb601aa3c9ea</md5>
          <size>21951</size>
          <filedate>1352251344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>d087e12fe9f9ec01ae7358c0208b67d2</md5>
          <size>36959</size>
          <filedate>1352251344</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.5.tar.gz</download_link>
      <date>1351827753</date>
      <mdhash>fb8fd3c4012ab1e7ee74b05a5798c83e</mdhash>
      <filesize>21952</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fb8fd3c4012ab1e7ee74b05a5798c83e</md5>
          <size>21952</size>
          <filedate>1351827753</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>7909ea6f3c49644c17f4f74062ea28bd</md5>
          <size>36967</size>
          <filedate>1351827753</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.4.tar.gz</download_link>
      <date>1351529542</date>
      <mdhash>d34546fa0d54140940a8945ac74982fe</mdhash>
      <filesize>21590</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d34546fa0d54140940a8945ac74982fe</md5>
          <size>21590</size>
          <filedate>1351529542</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ce676f675368a4132c8d11f43a07951d</md5>
          <size>36452</size>
          <filedate>1351529542</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.3.tar.gz</download_link>
      <date>1349770624</date>
      <mdhash>39245dec2cb440487e69a51a26675964</mdhash>
      <filesize>17781</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>39245dec2cb440487e69a51a26675964</md5>
          <size>17781</size>
          <filedate>1349770624</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>841f98891c779825fe83d277e088b8ce</md5>
          <size>28603</size>
          <filedate>1349770624</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.2.tar.gz</download_link>
      <date>1340891241</date>
      <mdhash>4117078179b1517c9ed4dd33098a0a6d</mdhash>
      <filesize>17785</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4117078179b1517c9ed4dd33098a0a6d</md5>
          <size>17785</size>
          <filedate>1340891241</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>157458128b7461559fbe3c19d2093b69</md5>
          <size>28602</size>
          <filedate>1340891241</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.1.tar.gz</download_link>
      <date>1339690066</date>
      <mdhash>f629260cca751f3f33c1dd2c338651ae</mdhash>
      <filesize>17832</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f629260cca751f3f33c1dd2c338651ae</md5>
          <size>17832</size>
          <filedate>1339690066</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc3fdc2b4b5539c97e609d1dafa5537f</md5>
          <size>28647</size>
          <filedate>1339690066</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0.tar.gz</download_link>
      <date>1339451537</date>
      <mdhash>ef262fce3ab40f338c0b94fcd89ca1aa</mdhash>
      <filesize>17707</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ef262fce3ab40f338c0b94fcd89ca1aa</md5>
          <size>17707</size>
          <filedate>1339451537</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>aca901e8a0afe8ea507dc7a6cf342812</md5>
          <size>28527</size>
          <filedate>1339451537</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>xautoload 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1332795946</date>
      <mdhash>c9c8fb2b3e442ab1d938b4e9119436e5</mdhash>
      <filesize>17048</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c9c8fb2b3e442ab1d938b4e9119436e5</md5>
          <size>17048</size>
          <filedate>1332795946</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>274030340388cfe7ad26bba8cba01040</md5>
          <size>27448</size>
          <filedate>1332795947</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1331315444</date>
      <mdhash>b2cd23fc0fd947597e9de3d497c4b95c</mdhash>
      <filesize>16220</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b2cd23fc0fd947597e9de3d497c4b95c</md5>
          <size>16220</size>
          <filedate>1331315444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c04ec6ffd2ea4f3b69cbcd9ecb2e501e</md5>
          <size>25654</size>
          <filedate>1331315444</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1328901948</date>
      <mdhash>9b348f9ae5dd72202a8dbf2f75233a9c</mdhash>
      <filesize>9567</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9b348f9ae5dd72202a8dbf2f75233a9c</md5>
          <size>9567</size>
          <filedate>1328901948</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cc99ecb57cf45ada3539d189e08ce732</md5>
          <size>11332</size>
          <filedate>1328901948</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-5.x-dev</name>
      <version>7.x-5.x-dev</version>
      <tag>7.x-5.x</tag>
      <version_major>5</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-5.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-5.x-dev.tar.gz</download_link>
      <date>1678492216</date>
      <mdhash>15ee34fc9aad43129959047e35d1a101</mdhash>
      <filesize>86104</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>15ee34fc9aad43129959047e35d1a101</md5>
          <size>86104</size>
          <filedate>1678492216</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-5.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>91875f7c8e9320b1c599ff4636e553fb</md5>
          <size>182122</size>
          <filedate>1678492216</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-4.x-dev</name>
      <version>7.x-4.x-dev</version>
      <tag>7.x-4.x</tag>
      <version_major>4</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-4.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-4.x-dev.tar.gz</download_link>
      <date>1396609150</date>
      <mdhash>ccfd74e9d99a0bf3cfe1c1cd5dacbfa7</mdhash>
      <filesize>48690</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ccfd74e9d99a0bf3cfe1c1cd5dacbfa7</md5>
          <size>48690</size>
          <filedate>1396609150</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-4.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a6c64bfdb0d2dc397f8822fce3c4d4b5</md5>
          <size>97984</size>
          <filedate>1396609150</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-3.x-dev.tar.gz</download_link>
      <date>1385896405</date>
      <mdhash>0b265a85552f4c202228e186894e77f4</mdhash>
      <filesize>38277</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0b265a85552f4c202228e186894e77f4</md5>
          <size>38277</size>
          <filedate>1385896405</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>71f7b08c7d251ba600bcf8c43b684e21</md5>
          <size>76345</size>
          <filedate>1385896405</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-2.x-dev.tar.gz</download_link>
      <date>1382797437</date>
      <mdhash>bb5a021bba7548797be703c283ddccef</mdhash>
      <filesize>24807</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb5a021bba7548797be703c283ddccef</md5>
          <size>24807</size>
          <filedate>1382797437</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8d8a49e1e14b278c824d543f35ee0a3e</md5>
          <size>41249</size>
          <filedate>1382797437</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>xautoload 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/xautoload/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/xautoload-7.x-1.x-dev.tar.gz</download_link>
      <date>1382797436</date>
      <mdhash>6a2f865ef2beb82a32507dd3c1489f11</mdhash>
      <filesize>9576</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6a2f865ef2beb82a32507dd3c1489f11</md5>
          <size>9576</size>
          <filedate>1382797436</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/xautoload-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>550e79361abc920101738f7d84a1ab5c</md5>
          <size>11338</size>
          <filedate>1382797436</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
