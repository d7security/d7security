<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Node Option Premium</title>
  <short_name>nopremium</short_name>
  <dc:creator>anrikun</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/nopremium</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Access control</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>nopremium 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/nopremium/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/nopremium-7.x-1.1.tar.gz</download_link>
      <date>1301749569</date>
      <mdhash>5cf9035ab603562a6ac5541c3ff63179</mdhash>
      <filesize>10287</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5cf9035ab603562a6ac5541c3ff63179</md5>
          <size>10287</size>
          <filedate>1301749569</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6216b5adf7df92937f5ba4bccc30d366</md5>
          <size>12599</size>
          <filedate>1301749569</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>nopremium 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/nopremium/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/nopremium-7.x-1.0.tar.gz</download_link>
      <date>1301350268</date>
      <mdhash>e17204aadc2ea8548121ec955a978faf</mdhash>
      <filesize>9874</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e17204aadc2ea8548121ec955a978faf</md5>
          <size>9874</size>
          <filedate>1301350268</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>2abe0ebfb58a578cbb1ea32bbf5cf241</md5>
          <size>11719</size>
          <filedate>1301350268</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>nopremium 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/nopremium/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/nopremium-7.x-1.x-dev.tar.gz</download_link>
      <date>1380594482</date>
      <mdhash>cb46098a18b8b39d4b6a0feb5c875190</mdhash>
      <filesize>12607</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cb46098a18b8b39d4b6a0feb5c875190</md5>
          <size>12607</size>
          <filedate>1380594482</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/nopremium-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ea7231a3421ed7d843e77a7d67e41666</md5>
          <size>14957</size>
          <filedate>1380594483</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
