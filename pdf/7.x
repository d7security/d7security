<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>PDF</title>
  <short_name>pdf</short_name>
  <dc:creator>shenzhuxi</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/pdf</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>pdf 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.9.tar.gz</download_link>
      <date>1510072384</date>
      <mdhash>c5197495d5cc3335993b86b3edb64994</mdhash>
      <filesize>12670</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c5197495d5cc3335993b86b3edb64994</md5>
          <size>12670</size>
          <filedate>1510072384</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>ae44b7e769a8f1e962c179faf7c9acdc</md5>
          <size>15149</size>
          <filedate>1510072384</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.8.tar.gz</download_link>
      <date>1461686433</date>
      <mdhash>f3f02c422c8442f73887318f7505fced</mdhash>
      <filesize>12769</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3f02c422c8442f73887318f7505fced</md5>
          <size>12769</size>
          <filedate>1461686433</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>18f6a0df81fef036002aac9789317146</md5>
          <size>15103</size>
          <filedate>1461686433</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.7.tar.gz</download_link>
      <date>1449230939</date>
      <mdhash>d65425dfa970d12f5bd801593ec4c28e</mdhash>
      <filesize>12723</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d65425dfa970d12f5bd801593ec4c28e</md5>
          <size>12723</size>
          <filedate>1449230939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>743ac6dede77db464e3537bc9a1b8ca8</md5>
          <size>15064</size>
          <filedate>1449230939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.6.tar.gz</download_link>
      <date>1403884428</date>
      <mdhash>e4232faa38357f2f71716a62692b4378</mdhash>
      <filesize>12440</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4232faa38357f2f71716a62692b4378</md5>
          <size>12440</size>
          <filedate>1403884428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>67bd8c58e242449935056218c9fe4596</md5>
          <size>14595</size>
          <filedate>1403884428</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.5.tar.gz</download_link>
      <date>1392051205</date>
      <mdhash>2c80304355247f97ad52fb197388bd05</mdhash>
      <filesize>11476</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2c80304355247f97ad52fb197388bd05</md5>
          <size>11476</size>
          <filedate>1392051205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>b19c59a56b6883fdce73e75ead127e31</md5>
          <size>13295</size>
          <filedate>1392051205</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.4.tar.gz</download_link>
      <date>1376676433</date>
      <mdhash>731d3fd860fb3d64b2429fa871b674ec</mdhash>
      <filesize>16609</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>731d3fd860fb3d64b2429fa871b674ec</md5>
          <size>16609</size>
          <filedate>1376676433</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>e7addcee19b2a9052e2fb19a33f00252</md5>
          <size>19239</size>
          <filedate>1376676434</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.3.tar.gz</download_link>
      <date>1373642468</date>
      <mdhash>249ba9fd02e305bb916dee37179c2798</mdhash>
      <filesize>10402</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>249ba9fd02e305bb916dee37179c2798</md5>
          <size>10402</size>
          <filedate>1373642468</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>2f0680c7f7df7c0c445a39362fc0a439</md5>
          <size>12230</size>
          <filedate>1373642468</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.2.tar.gz</download_link>
      <date>1356009527</date>
      <mdhash>5393f2b6066ba8bf29508a0cecbe6b1e</mdhash>
      <filesize>9955</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5393f2b6066ba8bf29508a0cecbe6b1e</md5>
          <size>9955</size>
          <filedate>1356009527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d03dbfeedc9e3d6458733677525fd7e</md5>
          <size>11131</size>
          <filedate>1356009527</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.1.tar.gz</download_link>
      <date>1352395379</date>
      <mdhash>4e8516a4c8012e97cea8838d10bdec43</mdhash>
      <filesize>9921</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4e8516a4c8012e97cea8838d10bdec43</md5>
          <size>9921</size>
          <filedate>1352395379</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3a88c4b5f1bb47d00494b9791f19eae5</md5>
          <size>11096</size>
          <filedate>1352395379</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.0.tar.gz</download_link>
      <date>1343743381</date>
      <mdhash>5491390822e55cb70d1cadc465783c21</mdhash>
      <filesize>8755</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5491390822e55cb70d1cadc465783c21</md5>
          <size>8755</size>
          <filedate>1343743381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f6e0f7cd658eb0a089608f3418f8733</md5>
          <size>9727</size>
          <filedate>1343743381</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>pdf 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-beta1.tar.gz</download_link>
      <date>1337157686</date>
      <mdhash>e160c356eac944a039d603b9b526054e</mdhash>
      <filesize>8821</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e160c356eac944a039d603b9b526054e</md5>
          <size>8821</size>
          <filedate>1337157686</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7bdffa11c8a3f2328c66a4a8f65b8ae</md5>
          <size>9824</size>
          <filedate>1337157686</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>pdf 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1376663259</date>
      <mdhash>57e295cd11c730e8d708a371c6543040</mdhash>
      <filesize>10165</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>57e295cd11c730e8d708a371c6543040</md5>
          <size>10165</size>
          <filedate>1376663259</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>5e53836a8d0795a48b08d756f2d300f2</md5>
          <size>11507</size>
          <filedate>1376663259</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>pdf 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1330334445</date>
      <mdhash>177a29fe09987528cd21f67ee6392994</mdhash>
      <filesize>20852</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>177a29fe09987528cd21f67ee6392994</md5>
          <size>20852</size>
          <filedate>1330334445</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9ff4d76b4c9dc87e90aa9802c0c8307d</md5>
          <size>21961</size>
          <filedate>1330334445</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>pdf 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1329162345</date>
      <mdhash>39412165bf3bb5fa9033eef96e933888</mdhash>
      <filesize>20718</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>39412165bf3bb5fa9033eef96e933888</md5>
          <size>20718</size>
          <filedate>1329162345</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fe97a98659531d84ae84c87546e7a23e</md5>
          <size>21820</size>
          <filedate>1329162345</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>pdf 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/pdf/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/pdf-7.x-1.x-dev.tar.gz</download_link>
      <date>1660727277</date>
      <mdhash>e76549daf852e4b606ee95adf64e9979</mdhash>
      <filesize>12862</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e76549daf852e4b606ee95adf64e9979</md5>
          <size>12862</size>
          <filedate>1660727277</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/pdf-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>278745e41df88b2cd7fea5743767b392</md5>
          <size>15385</size>
          <filedate>1660727277</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
