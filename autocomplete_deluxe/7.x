<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Autocomplete Deluxe</title>
  <short_name>autocomplete_deluxe</short_name>
  <dc:creator>sepgil</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/autocomplete_deluxe</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration Tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content Editing Experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site Structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>autocomplete_deluxe 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/autocomplete_deluxe/-/releases/7.x-2.4</release_link>
      <download_link>https://gitlab.com/api/v4/projects/61828237/packages/generic/autocomplete_deluxe/7.x-2.4/autocomplete_deluxe-7.x-2.4.tar.gz</download_link>
      <date>1726755494</date>
      <mdhash>b0490092664c8430e7526819789ba9da</mdhash>
      <filesize>22220</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/61828237/packages/generic/autocomplete_deluxe/7.x-2.4/autocomplete_deluxe-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b0490092664c8430e7526819789ba9da</md5>
          <size>22220</size>
          <filedate>1726755494</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/61828237/packages/generic/autocomplete_deluxe/7.x-2.4/autocomplete_deluxe-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f207658429e284dd0cf02bfd45d1702c</md5>
          <size>25525</size>
          <filedate>1726755494</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.3.tar.gz</download_link>
      <date>1501005543</date>
      <mdhash>1768ca9b400fae124e490cc966c8a80c</mdhash>
      <filesize>19688</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1768ca9b400fae124e490cc966c8a80c</md5>
          <size>19688</size>
          <filedate>1501005543</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>d35b168fecbcc9c2d7bcd674ee0afcef</md5>
          <size>22453</size>
          <filedate>1501005543</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.2.tar.gz</download_link>
      <date>1484128684</date>
      <mdhash>efa6b29ac7a9d414f4b1cdf2177220a1</mdhash>
      <filesize>19689</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>efa6b29ac7a9d414f4b1cdf2177220a1</md5>
          <size>19689</size>
          <filedate>1484128684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2509a2202ff3bc680025acff504e5e38</md5>
          <size>22398</size>
          <filedate>1484128684</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.1.tar.gz</download_link>
      <date>1426503181</date>
      <mdhash>2b50fb6b843ad247ca34f61c383c804e</mdhash>
      <filesize>19558</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2b50fb6b843ad247ca34f61c383c804e</md5>
          <size>19558</size>
          <filedate>1426503181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>de5b0c177f6f8760a3de019a7a7d890f</md5>
          <size>22080</size>
          <filedate>1426503181</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0.tar.gz</download_link>
      <date>1424856781</date>
      <mdhash>4656cfb2135855d5df1684945ee6e2d5</mdhash>
      <filesize>19569</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4656cfb2135855d5df1684945ee6e2d5</md5>
          <size>19569</size>
          <filedate>1424856781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>148250714d636a9a14d2e82a4357cbb2</md5>
          <size>22103</size>
          <filedate>1424856781</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta3.tar.gz</download_link>
      <date>1375695670</date>
      <mdhash>8a6a0a63fc657128c896064c07c1a937</mdhash>
      <filesize>19236</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8a6a0a63fc657128c896064c07c1a937</md5>
          <size>19236</size>
          <filedate>1375695670</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf471bba84010127c49d99d607522e71</md5>
          <size>21225</size>
          <filedate>1375695670</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta2.tar.gz</download_link>
      <date>1345195365</date>
      <mdhash>83130756d78ab20234c7ab0455f1423d</mdhash>
      <filesize>16437</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>83130756d78ab20234c7ab0455f1423d</md5>
          <size>16437</size>
          <filedate>1345195365</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>f6fa46e09bb5701e54a2f71e9bf0a6af</md5>
          <size>18409</size>
          <filedate>1345195365</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta1.tar.gz</download_link>
      <date>1344930453</date>
      <mdhash>4eedb2fe1be5920295b09253b2030579</mdhash>
      <filesize>16292</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4eedb2fe1be5920295b09253b2030579</md5>
          <size>16292</size>
          <filedate>1344930453</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d2e816a5749af522f288af542d38fe4</md5>
          <size>18260</size>
          <filedate>1344930453</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0.tar.gz</download_link>
      <date>1426502881</date>
      <mdhash>ebdca1fea0846de058083a5f3bb21118</mdhash>
      <filesize>19573</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ebdca1fea0846de058083a5f3bb21118</md5>
          <size>19573</size>
          <filedate>1426502881</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>260467eef5d1586078727f8240f6c268</md5>
          <size>22102</size>
          <filedate>1426502881</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta7</name>
      <version>7.x-1.0-beta7</version>
      <tag>7.x-1.0-beta7</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta7.tar.gz</download_link>
      <date>1340194874</date>
      <mdhash>5806bb92ff3abc9cdfd01ece725e124e</mdhash>
      <filesize>17305</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5806bb92ff3abc9cdfd01ece725e124e</md5>
          <size>17305</size>
          <filedate>1340194874</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>70d65631eb1c160636ea08c55a4864f6</md5>
          <size>19239</size>
          <filedate>1340194874</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta6.tar.gz</download_link>
      <date>1331561737</date>
      <mdhash>bac8c3f1cbf519da4c6ea010892f192f</mdhash>
      <filesize>17238</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bac8c3f1cbf519da4c6ea010892f192f</md5>
          <size>17238</size>
          <filedate>1331561737</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>9cfbdf4add4900bdc1cd4e94178b8d89</md5>
          <size>19163</size>
          <filedate>1331561737</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta5.tar.gz</download_link>
      <date>1312796515</date>
      <mdhash>2108ceaa977b5602e8b08e10069c8283</mdhash>
      <filesize>15955</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2108ceaa977b5602e8b08e10069c8283</md5>
          <size>15955</size>
          <filedate>1312796515</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>272048d48adb284f10ea07d51ddad6b8</md5>
          <size>17905</size>
          <filedate>1312796515</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta4.tar.gz</download_link>
      <date>1311854527</date>
      <mdhash>4504a62b89beae11b60d9d67af27242d</mdhash>
      <filesize>15955</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4504a62b89beae11b60d9d67af27242d</md5>
          <size>15955</size>
          <filedate>1311854527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>4ff58787e5ea2d571f230792448984a9</md5>
          <size>17901</size>
          <filedate>1311854527</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta3.tar.gz</download_link>
      <date>1311764815</date>
      <mdhash>9f5252dff2a79350774c58b5010d05d7</mdhash>
      <filesize>15953</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9f5252dff2a79350774c58b5010d05d7</md5>
          <size>15953</size>
          <filedate>1311764815</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>346692f75f553292e3c79f19a1a14e39</md5>
          <size>17895</size>
          <filedate>1311764815</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta2.tar.gz</download_link>
      <date>1308145014</date>
      <mdhash>f06b0d015188be88534aa011e2d5a07d</mdhash>
      <filesize>14256</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f06b0d015188be88534aa011e2d5a07d</md5>
          <size>14256</size>
          <filedate>1308145014</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>bea0d3813d8436639b511f00953bd5e7</md5>
          <size>16176</size>
          <filedate>1308145014</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta1.tar.gz</download_link>
      <date>1302190015</date>
      <mdhash>d0e78654eaaba3ac14e889a56dc402fb</mdhash>
      <filesize>14469</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d0e78654eaaba3ac14e889a56dc402fb</md5>
          <size>14469</size>
          <filedate>1302190015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>87fffb8ce20d16760f1e3da9ac270b96</md5>
          <size>16407</size>
          <filedate>1302190015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1293117069</date>
      <mdhash>aea313862f2b65116dfd582149b8f870</mdhash>
      <filesize>12069</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>aea313862f2b65116dfd582149b8f870</md5>
          <size>12069</size>
          <filedate>1293117069</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3f5cef8514ca47bcbdea4fa2ef0dc9d6</md5>
          <size>13543</size>
          <filedate>1293229823</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.x-dev.tar.gz</download_link>
      <date>1566315490</date>
      <mdhash>f688838658882f77fa56404b4760d131</mdhash>
      <filesize>22421</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f688838658882f77fa56404b4760d131</md5>
          <size>22421</size>
          <filedate>1566315490</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9bc0570c5f6ca45c9a04fbf432544552</md5>
          <size>25270</size>
          <filedate>1566315490</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>autocomplete_deluxe 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_deluxe/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.x-dev.tar.gz</download_link>
      <date>1380554808</date>
      <mdhash>dd6237e5ae9b76a2dde10a1104439a0a</mdhash>
      <filesize>17319</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dd6237e5ae9b76a2dde10a1104439a0a</md5>
          <size>17319</size>
          <filedate>1380554808</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_deluxe-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>c19794b9f44c9cc07da681a957e4b6c8</md5>
          <size>19244</size>
          <filedate>1380554808</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
