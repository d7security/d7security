<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Queue UI</title>
  <short_name>queue_ui</short_name>
  <dc:creator>coltrane</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/queue_ui</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>queue_ui 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_ui/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0.tar.gz</download_link>
      <date>1655473477</date>
      <mdhash>543f7c1a7de4efadad6a7f19831cfb68</mdhash>
      <filesize>13722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>543f7c1a7de4efadad6a7f19831cfb68</md5>
          <size>13722</size>
          <filedate>1655473477</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>827856ba149a07bda1e3ceb23f477862</md5>
          <size>16582</size>
          <filedate>1655473477</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>queue_ui 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_ui/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc2.tar.gz</download_link>
      <date>1602836947</date>
      <mdhash>76df010e969090f164e3e2be55b73c32</mdhash>
      <filesize>13720</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>76df010e969090f164e3e2be55b73c32</md5>
          <size>13720</size>
          <filedate>1602836947</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2efa160450ef225adfd3f10b279d6e3b</md5>
          <size>16585</size>
          <filedate>1602836947</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_ui 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_ui/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc1.tar.gz</download_link>
      <date>1372418751</date>
      <mdhash>fafea15e474bcf2d78aca73db9c0cb57</mdhash>
      <filesize>13298</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fafea15e474bcf2d78aca73db9c0cb57</md5>
          <size>13298</size>
          <filedate>1372418751</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c47382dc14058813d143c6d5a74eb66e</md5>
          <size>15681</size>
          <filedate>1372418751</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_ui 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_ui/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.x-dev.tar.gz</download_link>
      <date>1601649787</date>
      <mdhash>d7bbc4403097791ea36eb3a70a809974</mdhash>
      <filesize>13722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d7bbc4403097791ea36eb3a70a809974</md5>
          <size>13722</size>
          <filedate>1601649787</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>cdd76e4eed87e502fd8a9e07e460669a</md5>
          <size>16591</size>
          <filedate>1601649787</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>queue_ui 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/queue_ui/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/queue_ui-7.x-1.x-dev.tar.gz</download_link>
      <date>1380624568</date>
      <mdhash>c6bf730066b3dc36ee14e0539eb4aee5</mdhash>
      <filesize>10220</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c6bf730066b3dc36ee14e0539eb4aee5</md5>
          <size>10220</size>
          <filedate>1380624568</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/queue_ui-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>658ff0ebc04ee3f2857d7b9afc620f02</md5>
          <size>11135</size>
          <filedate>1380624568</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
