<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Features Extra</title>
  <short_name>features_extra</short_name>
  <dc:creator>makara</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/features_extra</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>features_extra 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/features_extra/-/releases/7.x-1.2</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.2/features_extra-7.x-1.2.tar.gz</download_link>
      <date>1738930575</date>
      <mdhash>f1f058c761173e76153dc0ffaa699f8d</mdhash>
      <filesize>24314</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.2/features_extra-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f1f058c761173e76153dc0ffaa699f8d</md5>
          <size>24314</size>
          <filedate>1738930575</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.2/features_extra-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0fab0bd61e844cc1d6c69726d326f365</md5>
          <size>33213</size>
          <filedate>1738930575</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>features_extra 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/features_extra/-/releases/7.x-1.1</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.1/features_extra-7.x-1.1.tar.gz</download_link>
      <date>1736940765</date>
      <mdhash>2b87b4fcf0725ce755a7988f571552ae</mdhash>
      <filesize>24309</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.1/features_extra-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2b87b4fcf0725ce755a7988f571552ae</md5>
          <size>24309</size>
          <filedate>1736940765</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66135317/packages/generic/features_extra/7.x-1.1/features_extra-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4128c57fdb32caa56e84542f34df95cc</md5>
          <size>33212</size>
          <filedate>1736940765</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>features_extra 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_extra/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0.tar.gz</download_link>
      <date>1439376839</date>
      <mdhash>ecfa56304e61cc67d7c683ebbab36035</mdhash>
      <filesize>22992</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ecfa56304e61cc67d7c683ebbab36035</md5>
          <size>22992</size>
          <filedate>1439376839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f61d205aa36f06b1a9d665a3166758a</md5>
          <size>30302</size>
          <filedate>1439376839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features_extra 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_extra/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-beta1.tar.gz</download_link>
      <date>1366049112</date>
      <mdhash>6cf022b7a3ca1149815879280981cb3d</mdhash>
      <filesize>20850</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6cf022b7a3ca1149815879280981cb3d</md5>
          <size>20850</size>
          <filedate>1366049112</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d7f4600f20d02f396314eefe0a213cc8</md5>
          <size>26849</size>
          <filedate>1366049112</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features_extra 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_extra/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1360912912</date>
      <mdhash>7d2cf4bedafd1d7031afa810acad843f</mdhash>
      <filesize>18035</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d2cf4bedafd1d7031afa810acad843f</md5>
          <size>18035</size>
          <filedate>1360912912</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>7174a2cf342b3342d2f1ecb9e302aa86</md5>
          <size>22455</size>
          <filedate>1360912912</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features_extra 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_extra/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1348854967</date>
      <mdhash>bf90e3255158d4b5f0980c89926d0f91</mdhash>
      <filesize>13497</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf90e3255158d4b5f0980c89926d0f91</md5>
          <size>13497</size>
          <filedate>1348854967</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6fe25786584f3c0812115e64a0f75594</md5>
          <size>16475</size>
          <filedate>1348854967</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features_extra 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_extra/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_extra-7.x-1.x-dev.tar.gz</download_link>
      <date>1647992007</date>
      <mdhash>e890eda3b708fecf9cf867b70e6cff8a</mdhash>
      <filesize>23998</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e890eda3b708fecf9cf867b70e6cff8a</md5>
          <size>23998</size>
          <filedate>1647992007</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_extra-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc8a862a0327a84705d9d07f536b7301</md5>
          <size>32901</size>
          <filedate>1647992007</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
