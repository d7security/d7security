<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Ultimate Cron</title>
  <short_name>ultimate_cron</short_name>
  <dc:creator>gielfeldt</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/ultimate_cron</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Performance</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>ultimate_cron 7.x-2.11</name>
      <version>7.x-2.11</version>
      <tag>7.x-2.11</tag>
      <version_major>2</version_major>
      <version_patch>11</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.11</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.11.tar.gz</download_link>
      <date>1732519697</date>
      <mdhash>c70ac84db1f8084417baf2203bcb3567</mdhash>
      <filesize>110020</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.11.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c70ac84db1f8084417baf2203bcb3567</md5>
          <size>110020</size>
          <filedate>1732519697</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.11.zip</url>
          <archive_type>zip</archive_type>
          <md5>ffd5cc01f9d51d8577e37321a07fd2d0</md5>
          <size>145076</size>
          <filedate>1732519697</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.10</name>
      <version>7.x-2.10</version>
      <tag>7.x-2.10</tag>
      <version_major>2</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.10.tar.gz</download_link>
      <date>1732284071</date>
      <mdhash>30df4454bd0191e96bf5df0088c88739</mdhash>
      <filesize>109952</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>30df4454bd0191e96bf5df0088c88739</md5>
          <size>109952</size>
          <filedate>1732284071</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>cfbed1ad31bec82f4c0537310fbecaa8</md5>
          <size>145006</size>
          <filedate>1732284071</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.9</name>
      <version>7.x-2.9</version>
      <tag>7.x-2.9</tag>
      <version_major>2</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.9.tar.gz</download_link>
      <date>1666561347</date>
      <mdhash>c3d2a81aa216ccf46b273f2a149ee67f</mdhash>
      <filesize>109941</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c3d2a81aa216ccf46b273f2a149ee67f</md5>
          <size>109941</size>
          <filedate>1666561347</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>50b5f83451f4cb5545a1e434a112830e</md5>
          <size>144991</size>
          <filedate>1666561347</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.8</name>
      <version>7.x-2.8</version>
      <tag>7.x-2.8</tag>
      <version_major>2</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.8.tar.gz</download_link>
      <date>1531690121</date>
      <mdhash>18609546f3a616d7da1ef8f674aee4fe</mdhash>
      <filesize>109870</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>18609546f3a616d7da1ef8f674aee4fe</md5>
          <size>109870</size>
          <filedate>1531690121</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>d5ad71b78bd42c94d12f76177ea88a42</md5>
          <size>144903</size>
          <filedate>1531690121</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.7</name>
      <version>7.x-2.7</version>
      <tag>7.x-2.7</tag>
      <version_major>2</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.7.tar.gz</download_link>
      <date>1524917284</date>
      <mdhash>c3074f4a29ee5f42b3adc5fb8b309679</mdhash>
      <filesize>109727</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c3074f4a29ee5f42b3adc5fb8b309679</md5>
          <size>109727</size>
          <filedate>1524917284</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>1f1e4f6a22e559fb87a5cf3698c7fc01</md5>
          <size>144794</size>
          <filedate>1524917284</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.6.tar.gz</download_link>
      <date>1522358884</date>
      <mdhash>59433dedb64e12ad673b0fdc92571848</mdhash>
      <filesize>109352</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>59433dedb64e12ad673b0fdc92571848</md5>
          <size>109352</size>
          <filedate>1522358884</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>5813948494f43483e1579cdaef7adbb4</md5>
          <size>144380</size>
          <filedate>1522358884</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.5.tar.gz</download_link>
      <date>1501853344</date>
      <mdhash>03ea90202026c256b848b0e13b3a5088</mdhash>
      <filesize>109297</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>03ea90202026c256b848b0e13b3a5088</md5>
          <size>109297</size>
          <filedate>1501853344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>36c0aac458a124254b680eb7b6221ac1</md5>
          <size>144284</size>
          <filedate>1501853344</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.4.tar.gz</download_link>
      <date>1494969485</date>
      <mdhash>e0a045ef90bfa98cc286c199b8d1dbba</mdhash>
      <filesize>109129</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e0a045ef90bfa98cc286c199b8d1dbba</md5>
          <size>109129</size>
          <filedate>1494969485</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>a55d10780cc35cef75972dd101694937</md5>
          <size>144142</size>
          <filedate>1494969485</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.3.tar.gz</download_link>
      <date>1485097383</date>
      <mdhash>09a29fbc1517791ba1cbf4071768b5af</mdhash>
      <filesize>109062</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>09a29fbc1517791ba1cbf4071768b5af</md5>
          <size>109062</size>
          <filedate>1485097383</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>1e2e178df93bc9103bf0616dbe016207</md5>
          <size>144078</size>
          <filedate>1485097383</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.2.tar.gz</download_link>
      <date>1483564749</date>
      <mdhash>6b2fad30ddb2a886b307f2b31fb4134c</mdhash>
      <filesize>109049</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6b2fad30ddb2a886b307f2b31fb4134c</md5>
          <size>109049</size>
          <filedate>1483564749</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>84f07704619c60435cf25541ffd6d506</md5>
          <size>144052</size>
          <filedate>1483564749</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.1.tar.gz</download_link>
      <date>1482173884</date>
      <mdhash>d1eea04c40b523019953c5a03cab1706</mdhash>
      <filesize>108943</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d1eea04c40b523019953c5a03cab1706</md5>
          <size>108943</size>
          <filedate>1482173884</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ec82342d69d5f9116b9cf8a268f33271</md5>
          <size>143945</size>
          <filedate>1482173884</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0.tar.gz</download_link>
      <date>1472716139</date>
      <mdhash>efa719445c798ebe65b563827c4a418a</mdhash>
      <filesize>112632</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>efa719445c798ebe65b563827c4a418a</md5>
          <size>112632</size>
          <filedate>1472716139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b4dd755102fa9698696251c27f849d9</md5>
          <size>143810</size>
          <filedate>1472716139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-rc4</name>
      <version>7.x-2.0-rc4</version>
      <tag>7.x-2.0-rc4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc4.tar.gz</download_link>
      <date>1471859639</date>
      <mdhash>00bc42f43fdaf440e88d583fa47e29cc</mdhash>
      <filesize>112629</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00bc42f43fdaf440e88d583fa47e29cc</md5>
          <size>112629</size>
          <filedate>1471859639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>27de0d9ee3745c6f78a68cdd0d6d54d8</md5>
          <size>143813</size>
          <filedate>1471859639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-rc3</name>
      <version>7.x-2.0-rc3</version>
      <tag>7.x-2.0-rc3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc3.tar.gz</download_link>
      <date>1470736739</date>
      <mdhash>dca088b6ea913d5a1e23cafb2da99ba3</mdhash>
      <filesize>112368</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>dca088b6ea913d5a1e23cafb2da99ba3</md5>
          <size>112368</size>
          <filedate>1470736739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>270066cca89a1baf0924e57eb1592f0b</md5>
          <size>143624</size>
          <filedate>1470736739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc2.tar.gz</download_link>
      <date>1462625639</date>
      <mdhash>66d2bcb70ad6040f3f9514075b51e193</mdhash>
      <filesize>111740</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66d2bcb70ad6040f3f9514075b51e193</md5>
          <size>111740</size>
          <filedate>1462625639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>03e27fd51d3ce6dacb9eafc6870fc9a5</md5>
          <size>142871</size>
          <filedate>1462625639</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc1.tar.gz</download_link>
      <date>1434547680</date>
      <mdhash>3a1a7a6fc98018c475135dd191290ec2</mdhash>
      <filesize>110935</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3a1a7a6fc98018c475135dd191290ec2</md5>
          <size>110935</size>
          <filedate>1434547680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fc4a367910276796c9df9ba85100eef5</md5>
          <size>142553</size>
          <filedate>1434547680</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta8</name>
      <version>7.x-2.0-beta8</version>
      <tag>7.x-2.0-beta8</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta8</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta8.tar.gz</download_link>
      <date>1415009328</date>
      <mdhash>d4a9686f8909cbfdff9a397610359679</mdhash>
      <filesize>110576</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d4a9686f8909cbfdff9a397610359679</md5>
          <size>110576</size>
          <filedate>1415009328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta8.zip</url>
          <archive_type>zip</archive_type>
          <md5>12e0713794bafb2b5c545b8add45eb92</md5>
          <size>142049</size>
          <filedate>1415009328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta7</name>
      <version>7.x-2.0-beta7</version>
      <tag>7.x-2.0-beta7</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta7</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta7.tar.gz</download_link>
      <date>1412893428</date>
      <mdhash>95bb4614c2ab3cac23cf235970f6e781</mdhash>
      <filesize>110354</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>95bb4614c2ab3cac23cf235970f6e781</md5>
          <size>110354</size>
          <filedate>1412893428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta7.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f5505ee38512c7a52f5887269616fdc</md5>
          <size>141807</size>
          <filedate>1412893428</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta6</name>
      <version>7.x-2.0-beta6</version>
      <tag>7.x-2.0-beta6</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta6.tar.gz</download_link>
      <date>1408520328</date>
      <mdhash>3275f473e2d30bd72385f5c077e11f53</mdhash>
      <filesize>110150</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3275f473e2d30bd72385f5c077e11f53</md5>
          <size>110150</size>
          <filedate>1408520328</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>91318b828b05d3caf2d24e2a8ac485c2</md5>
          <size>141585</size>
          <filedate>1408520328</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta5</name>
      <version>7.x-2.0-beta5</version>
      <tag>7.x-2.0-beta5</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta5.tar.gz</download_link>
      <date>1395914043</date>
      <mdhash>08c90ef4f40b70653075bf56ce0fc234</mdhash>
      <filesize>110371</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>08c90ef4f40b70653075bf56ce0fc234</md5>
          <size>110371</size>
          <filedate>1395914043</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>b4a11c99da2ce2e83aaf681181530a13</md5>
          <size>140853</size>
          <filedate>1395914043</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta4</name>
      <version>7.x-2.0-beta4</version>
      <tag>7.x-2.0-beta4</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta4.tar.gz</download_link>
      <date>1394137705</date>
      <mdhash>16c036b239aed0d6049a5a5348d1db68</mdhash>
      <filesize>110276</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>16c036b239aed0d6049a5a5348d1db68</md5>
          <size>110276</size>
          <filedate>1394137705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>6d8c2b4e6dd47cbda77d7ca5cec83b41</md5>
          <size>140742</size>
          <filedate>1394137705</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta3.tar.gz</download_link>
      <date>1393223306</date>
      <mdhash>619e151beef3867b8cd63cfae2512074</mdhash>
      <filesize>109884</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>619e151beef3867b8cd63cfae2512074</md5>
          <size>109884</size>
          <filedate>1393223306</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>dab4a2b40f0d1952f44c1b1ce5ad46b0</md5>
          <size>140339</size>
          <filedate>1393223306</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta2.tar.gz</download_link>
      <date>1392669505</date>
      <mdhash>e4993f1dfaa1b175b0cd440e01ba64d3</mdhash>
      <filesize>109678</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e4993f1dfaa1b175b0cd440e01ba64d3</md5>
          <size>109678</size>
          <filedate>1392669505</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>500ad9a36ed84d7e7c6ab1f54a44de37</md5>
          <size>140059</size>
          <filedate>1392669505</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta1.tar.gz</download_link>
      <date>1392586104</date>
      <mdhash>3618f9df6ce4cf3c87402305ac68b96a</mdhash>
      <filesize>109518</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3618f9df6ce4cf3c87402305ac68b96a</md5>
          <size>109518</size>
          <filedate>1392586104</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7dfb34ebde68d5ffe263c97222a84c7b</md5>
          <size>139891</size>
          <filedate>1392586104</filedate>
        </file>
      </files>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1392330504</date>
      <mdhash>3669ff9315dad90003cb2f0400826544</mdhash>
      <filesize>108718</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3669ff9315dad90003cb2f0400826544</md5>
          <size>108718</size>
          <filedate>1392330504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>39a74336092480cd83ca961269aadaf6</md5>
          <size>139076</size>
          <filedate>1392330504</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1392198506</date>
      <mdhash>fa942f4f85699914e1f79a42553835fe</mdhash>
      <filesize>105935</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fa942f4f85699914e1f79a42553835fe</md5>
          <size>105935</size>
          <filedate>1392198506</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5f44e4a65e1ce0117aadc8f3e47d3a10</md5>
          <size>135154</size>
          <filedate>1392198506</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.10</name>
      <version>7.x-1.10</version>
      <tag>7.x-1.10</tag>
      <version_major>1</version_major>
      <version_patch>10</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.10</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.10.tar.gz</download_link>
      <date>1392630205</date>
      <mdhash>c49ee5155562a4be45364e5bb6d71460</mdhash>
      <filesize>59268</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.10.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c49ee5155562a4be45364e5bb6d71460</md5>
          <size>59268</size>
          <filedate>1392630205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.10.zip</url>
          <archive_type>zip</archive_type>
          <md5>6a90d03e716d7def7526f26ffff1a678</md5>
          <size>73958</size>
          <filedate>1392630205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.9</name>
      <version>7.x-1.9</version>
      <tag>7.x-1.9</tag>
      <version_major>1</version_major>
      <version_patch>9</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.9</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.9.tar.gz</download_link>
      <date>1364843419</date>
      <mdhash>ac75d8e699c02fe592a5efaf4ca79be1</mdhash>
      <filesize>58554</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.9.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ac75d8e699c02fe592a5efaf4ca79be1</md5>
          <size>58554</size>
          <filedate>1364843419</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.9.zip</url>
          <archive_type>zip</archive_type>
          <md5>3202ce5a09452c796382a97ab7177a26</md5>
          <size>72276</size>
          <filedate>1364843419</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.8.tar.gz</download_link>
      <date>1358611421</date>
      <mdhash>6fa78fbe6605df0a6d4024a4f7d764b7</mdhash>
      <filesize>54695</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6fa78fbe6605df0a6d4024a4f7d764b7</md5>
          <size>54695</size>
          <filedate>1358611421</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>f906f82599f99a894043a6575f5634af</md5>
          <size>67273</size>
          <filedate>1358611421</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.7.tar.gz</download_link>
      <date>1350764529</date>
      <mdhash>8075036368dd143d15fc26cb23b486d0</mdhash>
      <filesize>54699</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8075036368dd143d15fc26cb23b486d0</md5>
          <size>54699</size>
          <filedate>1350764529</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>a98b2ba87ee73d43927ebaa37ad45da1</md5>
          <size>67271</size>
          <filedate>1350764529</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.6.tar.gz</download_link>
      <date>1323006060</date>
      <mdhash>9eaec55d614ce0aba8dfb6f490616698</mdhash>
      <filesize>45407</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9eaec55d614ce0aba8dfb6f490616698</md5>
          <size>45407</size>
          <filedate>1323006060</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>1ad58378160475aa343f1eaa63bc965d</md5>
          <size>57146</size>
          <filedate>1323006060</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5.tar.gz</download_link>
      <date>1320038138</date>
      <mdhash>f3fa5f5fa85ce95cc0be9d0f103e918c</mdhash>
      <filesize>45188</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3fa5f5fa85ce95cc0be9d0f103e918c</md5>
          <size>45188</size>
          <filedate>1320038138</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>90987b35e8616f30e65b11a1bebb5985</md5>
          <size>56781</size>
          <filedate>1320038138</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.5-rc1</name>
      <version>7.x-1.5-rc1</version>
      <tag>7.x-1.5-rc1</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.5-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5-rc1.tar.gz</download_link>
      <date>1319990440</date>
      <mdhash>fe3eac50b4a6180c7278e80904cebc3a</mdhash>
      <filesize>45196</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fe3eac50b4a6180c7278e80904cebc3a</md5>
          <size>45196</size>
          <filedate>1319990440</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.5-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c8b52f207b314a8e303a0296f69f4c87</md5>
          <size>56784</size>
          <filedate>1319990440</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.4.tar.gz</download_link>
      <date>1319008836</date>
      <mdhash>7542886c8fbcdffbc003609a127071ed</mdhash>
      <filesize>44756</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7542886c8fbcdffbc003609a127071ed</md5>
          <size>44756</size>
          <filedate>1319008836</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>0423dde049aec1faca7555af5c4d64ac</md5>
          <size>56340</size>
          <filedate>1319008836</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.3.tar.gz</download_link>
      <date>1318126013</date>
      <mdhash>79142b3636fe58f95a6de1e1fa9dc2c6</mdhash>
      <filesize>39180</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>79142b3636fe58f95a6de1e1fa9dc2c6</md5>
          <size>39180</size>
          <filedate>1318126013</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>75f6aa1afe8b76180244dd88b2a5d5b2</md5>
          <size>44918</size>
          <filedate>1318126013</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.2.tar.gz</download_link>
      <date>1318027909</date>
      <mdhash>1ae03c8ed7095dae6a61d7cef6dc60d3</mdhash>
      <filesize>37954</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1ae03c8ed7095dae6a61d7cef6dc60d3</md5>
          <size>37954</size>
          <filedate>1318027909</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f42cc2d14cd50b2cf665e739e7be770</md5>
          <size>43077</size>
          <filedate>1318027909</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.1.tar.gz</download_link>
      <date>1317760608</date>
      <mdhash>f29b06c3193a501fb42b4f58ada5edb6</mdhash>
      <filesize>37990</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f29b06c3193a501fb42b4f58ada5edb6</md5>
          <size>37990</size>
          <filedate>1317760608</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8cc777ddc1e071a296e318e5869c0f2c</md5>
          <size>43054</size>
          <filedate>1317760608</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.0.tar.gz</download_link>
      <date>1317478607</date>
      <mdhash>474d6edbcb23ef1b610a5e1a72f901f3</mdhash>
      <filesize>37767</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>474d6edbcb23ef1b610a5e1a72f901f3</md5>
          <size>37767</size>
          <filedate>1317478607</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>e9297b435dc3a87bc9f36b413a897fc2</md5>
          <size>42868</size>
          <filedate>1317478607</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.x-dev.tar.gz</download_link>
      <date>1732519486</date>
      <mdhash>c2781dacc882d412369d2c7ffe37a4a9</mdhash>
      <filesize>110023</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c2781dacc882d412369d2c7ffe37a4a9</md5>
          <size>110023</size>
          <filedate>1732519486</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>78978f189a0a09b2acc4e1a16dd85e4f</md5>
          <size>145082</size>
          <filedate>1732519486</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>ultimate_cron 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/ultimate_cron/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.x-dev.tar.gz</download_link>
      <date>1391080705</date>
      <mdhash>48a3fb7d4a2630038cd4b2dfab19287e</mdhash>
      <filesize>59263</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>48a3fb7d4a2630038cd4b2dfab19287e</md5>
          <size>59263</size>
          <filedate>1391080705</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/ultimate_cron-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>2baa9d2b8e593f54d049a5413ddb0c58</md5>
          <size>73963</size>
          <filedate>1391080705</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
