<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>File (Field) Paths</title>
  <short_name>filefield_paths</short_name>
  <dc:creator>Deciphered</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/filefield_paths</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Media</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>filefield_paths 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.2.tar.gz</download_link>
      <date>1671027069</date>
      <mdhash>5a5dc2d5f4d87c676e4636c1a03ca78e</mdhash>
      <filesize>30991</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5a5dc2d5f4d87c676e4636c1a03ca78e</md5>
          <size>30991</size>
          <filedate>1671027069</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c3cf78fd218c4458acbb88736f92a725</md5>
          <size>44707</size>
          <filedate>1671027069</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.1.tar.gz</download_link>
      <date>1534256580</date>
      <mdhash>f34f678c4b3052eb700c278a24611c67</mdhash>
      <filesize>30192</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f34f678c4b3052eb700c278a24611c67</md5>
          <size>30192</size>
          <filedate>1534256580</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>e123119d13b0ca62d5507aae4fd8f2cd</md5>
          <size>43730</size>
          <filedate>1534256580</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0.tar.gz</download_link>
      <date>1447729441</date>
      <mdhash>6c334eff9e70deff12c555c2bea2016d</mdhash>
      <filesize>30657</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6c334eff9e70deff12c555c2bea2016d</md5>
          <size>30657</size>
          <filedate>1447729441</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>df35dc3de9f71ed06d7fecfb660d477c</md5>
          <size>43618</size>
          <filedate>1447729441</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc3.tar.gz</download_link>
      <date>1447220041</date>
      <mdhash>48b9a9d58c2697057e0b6157b063ac12</mdhash>
      <filesize>29926</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>48b9a9d58c2697057e0b6157b063ac12</md5>
          <size>29926</size>
          <filedate>1447220041</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>0b7039198610e624cbacf3dce3609a2b</md5>
          <size>42545</size>
          <filedate>1447220041</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc2.tar.gz</download_link>
      <date>1446011056</date>
      <mdhash>d8baca78990105f4e0e121b16f441087</mdhash>
      <filesize>28729</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d8baca78990105f4e0e121b16f441087</md5>
          <size>28729</size>
          <filedate>1446011056</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>22c416d2d5ac879fea8bbb82e8e5cd5d</md5>
          <size>40325</size>
          <filedate>1446011056</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc1.tar.gz</download_link>
      <date>1442310839</date>
      <mdhash>53863d7c6345d8fb6deaeb3d24a3b7ac</mdhash>
      <filesize>28117</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>53863d7c6345d8fb6deaeb3d24a3b7ac</md5>
          <size>28117</size>
          <filedate>1442310839</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b0e63117743bb94c0cf91ea08dec149e</md5>
          <size>37072</size>
          <filedate>1442310839</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta4.tar.gz</download_link>
      <date>1366871711</date>
      <mdhash>1aca88c5b1b995f61867f9c3c5412b79</mdhash>
      <filesize>17686</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1aca88c5b1b995f61867f9c3c5412b79</md5>
          <size>17686</size>
          <filedate>1366871711</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f1e03b52822fa67452ca9d2bf658793c</md5>
          <size>21797</size>
          <filedate>1366871712</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta3.tar.gz</download_link>
      <date>1328655041</date>
      <mdhash>3f11255e4a208662dae18866225ce686</mdhash>
      <filesize>16325</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3f11255e4a208662dae18866225ce686</md5>
          <size>16325</size>
          <filedate>1328655041</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>630ce6bfc4256d648db5d38cb152f34a</md5>
          <size>20305</size>
          <filedate>1328655041</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta2.tar.gz</download_link>
      <date>1328437541</date>
      <mdhash>49de6bf450960623bb2cec2f936fbe38</mdhash>
      <filesize>19551</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>49de6bf450960623bb2cec2f936fbe38</md5>
          <size>19551</size>
          <filedate>1328437541</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b1a51c35e512e64d6f7fd234788f8694</md5>
          <size>25441</size>
          <filedate>1328437541</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta1.tar.gz</download_link>
      <date>1320701736</date>
      <mdhash>2332d956c34f554c0ed35e5b3793bec1</mdhash>
      <filesize>16267</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2332d956c34f554c0ed35e5b3793bec1</md5>
          <size>16267</size>
          <filedate>1320701736</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b5e520cbc4a774c4f45e60f3421d1f5a</md5>
          <size>19998</size>
          <filedate>1320701736</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1307867224</date>
      <mdhash>5d3c36c11876cc2ce4c441eb02fb568a</mdhash>
      <filesize>12484</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d3c36c11876cc2ce4c441eb02fb568a</md5>
          <size>12484</size>
          <filedate>1307867224</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>ca0532f6549970ee1d10165ee52b47c3</md5>
          <size>14922</size>
          <filedate>1307867224</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>filefield_paths 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/filefield_paths/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.x-dev.tar.gz</download_link>
      <date>1671030686</date>
      <mdhash>bf4fde993f0df448b999ee628eed08f3</mdhash>
      <filesize>31000</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf4fde993f0df448b999ee628eed08f3</md5>
          <size>31000</size>
          <filedate>1671030686</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/filefield_paths-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9ff2dc2726b7e36c46cb13a6b4cf7443</md5>
          <size>44749</size>
          <filedate>1671030686</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
