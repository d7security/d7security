<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Module Filter</title>
  <short_name>module_filter</short_name>
  <dc:creator>greenskin</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/module_filter</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Administration tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site search</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>module_filter 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.3.tar.gz</download_link>
      <date>1704470449</date>
      <mdhash>26faedcd8bc65064f65e32db32953a10</mdhash>
      <filesize>30703</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>26faedcd8bc65064f65e32db32953a10</md5>
          <size>30703</size>
          <filedate>1704470449</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>704b70fa108053d79ea6aaa9712418e4</md5>
          <size>39865</size>
          <filedate>1704470449</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.2.tar.gz</download_link>
      <date>1553698381</date>
      <mdhash>329ce9645bf58fc27abdd6dd9328fc6e</mdhash>
      <filesize>29772</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>329ce9645bf58fc27abdd6dd9328fc6e</md5>
          <size>29772</size>
          <filedate>1553698381</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8dee797be4c16d8181047b8774db8724</md5>
          <size>38885</size>
          <filedate>1553698381</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.1.tar.gz</download_link>
      <date>1497029343</date>
      <mdhash>66a4998f63f2cefe77c18713054a42cc</mdhash>
      <filesize>29757</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>66a4998f63f2cefe77c18713054a42cc</md5>
          <size>29757</size>
          <filedate>1497029343</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9823ec2b6a4c62221761f1b550b2858b</md5>
          <size>38865</size>
          <filedate>1497029343</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0.tar.gz</download_link>
      <date>1424631181</date>
      <mdhash>3e2ebdd2f2ec028252e1cf2a547d7d7e</mdhash>
      <filesize>29130</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3e2ebdd2f2ec028252e1cf2a547d7d7e</md5>
          <size>29130</size>
          <filedate>1424631181</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b775b30391b2e8eaae85f10496d5b391</md5>
          <size>36978</size>
          <filedate>1424631181</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-2.0-alpha2</name>
      <version>7.x-2.0-alpha2</version>
      <tag>7.x-2.0-alpha2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha2.tar.gz</download_link>
      <date>1386356904</date>
      <mdhash>342e0e3963e64c39f86061a10bbdaf2e</mdhash>
      <filesize>27898</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>342e0e3963e64c39f86061a10bbdaf2e</md5>
          <size>27898</size>
          <filedate>1386356904</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>2093c11e335ff88344958a956e4c0f2a</md5>
          <size>35480</size>
          <filedate>1386356904</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>module_filter 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1384832005</date>
      <mdhash>3bfb306828c12830c0ee3678f675f124</mdhash>
      <filesize>27962</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3bfb306828c12830c0ee3678f675f124</md5>
          <size>27962</size>
          <filedate>1384832005</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>68e041f221e5252f2e2941e3b0b2c3b9</md5>
          <size>35518</size>
          <filedate>1384832005</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>module_filter 7.x-1.8</name>
      <version>7.x-1.8</version>
      <tag>7.x-1.8</tag>
      <version_major>1</version_major>
      <version_patch>8</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.8</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.8.tar.gz</download_link>
      <date>1375995221</date>
      <mdhash>53a1b995dee29e1ab202fa1547dc1266</mdhash>
      <filesize>14605</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.8.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>53a1b995dee29e1ab202fa1547dc1266</md5>
          <size>14605</size>
          <filedate>1375995221</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.8.zip</url>
          <archive_type>zip</archive_type>
          <md5>ffcdbdfc2a15ae96a83d59bb128946a8</md5>
          <size>18222</size>
          <filedate>1375995221</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.7</name>
      <version>7.x-1.7</version>
      <tag>7.x-1.7</tag>
      <version_major>1</version_major>
      <version_patch>7</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.7</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.7.tar.gz</download_link>
      <date>1341518501</date>
      <mdhash>f10e62aa5346d5cd82854d66c359f199</mdhash>
      <filesize>14646</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.7.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f10e62aa5346d5cd82854d66c359f199</md5>
          <size>14646</size>
          <filedate>1341518501</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.7.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a638e8cee7865969a36cec2877dcb92</md5>
          <size>18239</size>
          <filedate>1341518501</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.6.tar.gz</download_link>
      <date>1316105205</date>
      <mdhash>77ca515021372066e2b19906adddb94e</mdhash>
      <filesize>13177</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>77ca515021372066e2b19906adddb94e</md5>
          <size>13177</size>
          <filedate>1316105205</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f31da40703a0e680491927104845211</md5>
          <size>16707</size>
          <filedate>1316105205</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.5.tar.gz</download_link>
      <date>1313598120</date>
      <mdhash>f3a6a44ec3e35e91870f8391a9f04ff5</mdhash>
      <filesize>13039</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f3a6a44ec3e35e91870f8391a9f04ff5</md5>
          <size>13039</size>
          <filedate>1313598120</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>578073ac59a163e980fcee02ff1ced24</md5>
          <size>16564</size>
          <filedate>1313598120</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.4.tar.gz</download_link>
      <date>1299525068</date>
      <mdhash>90d9ec10df0b9ee07707f87b96711bca</mdhash>
      <filesize>11570</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>90d9ec10df0b9ee07707f87b96711bca</md5>
          <size>11570</size>
          <filedate>1299525068</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>f59a4ab7edd8c831ab940b8a2c64c8ba</md5>
          <size>14803</size>
          <filedate>1299525068</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.3.tar.gz</download_link>
      <date>1299519370</date>
      <mdhash>8a075e4932db8cd4e35371ac276bcb4b</mdhash>
      <filesize>11555</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8a075e4932db8cd4e35371ac276bcb4b</md5>
          <size>11555</size>
          <filedate>1299519370</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>7bda0d1a4359d3eb00c981109c10c6b1</md5>
          <size>14794</size>
          <filedate>1299519370</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.2.tar.gz</download_link>
      <date>1299516669</date>
      <mdhash>afbb784aeb4029294e5877654385cc8c</mdhash>
      <filesize>11446</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>afbb784aeb4029294e5877654385cc8c</md5>
          <size>11446</size>
          <filedate>1299516669</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>24171ad9a22efac406f7e90cad4f9e0b</md5>
          <size>14633</size>
          <filedate>1299516669</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.1.tar.gz</download_link>
      <date>1299515169</date>
      <mdhash>45546c1f5bc473389ffc36f0e38a2bc1</mdhash>
      <filesize>13033</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>45546c1f5bc473389ffc36f0e38a2bc1</md5>
          <size>13033</size>
          <filedate>1299515169</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>dbe698324408d400f9615380f86d712f</md5>
          <size>16571</size>
          <filedate>1299515169</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.0.tar.gz</download_link>
      <date>1294104365</date>
      <mdhash>a482aef54751a8fd947bafc2d5ef1ecd</mdhash>
      <filesize>11523</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a482aef54751a8fd947bafc2d5ef1ecd</md5>
          <size>11523</size>
          <filedate>1294104365</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>cb6cee2856235daf966481a640ef3203</md5>
          <size>15399</size>
          <filedate>1294104365</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>module_filter 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-2.x-dev.tar.gz</download_link>
      <date>1704293208</date>
      <mdhash>90ac412d29777d6cfb7e45ab922ae8f7</mdhash>
      <filesize>30706</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>90ac412d29777d6cfb7e45ab922ae8f7</md5>
          <size>30706</size>
          <filedate>1704293208</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>51e8bf9ba76bf7a36f8126c8e676b0ec</md5>
          <size>39872</size>
          <filedate>1704293208</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>module_filter 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/module_filter/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/module_filter-7.x-1.x-dev.tar.gz</download_link>
      <date>1406917428</date>
      <mdhash>62b7a0ab716b2798d9e60462314d95c5</mdhash>
      <filesize>14743</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>62b7a0ab716b2798d9e60462314d95c5</md5>
          <size>14743</size>
          <filedate>1406917428</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/module_filter-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8165aaeb26b2646a15ad7960608d2f3d</md5>
          <size>18379</size>
          <filedate>1406917428</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
