<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Node clone</title>
  <short_name>node_clone</short_name>
  <dc:creator>pwolanin</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/node_clone</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>node_clone 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0.tar.gz</download_link>
      <date>1447900440</date>
      <mdhash>3d5b4c029cb20347ee9c374e5b0f4819</mdhash>
      <filesize>14980</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3d5b4c029cb20347ee9c374e5b0f4819</md5>
          <size>14980</size>
          <filedate>1447900440</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>68d346c43ee0f92db28576d5916ced2b</md5>
          <size>18654</size>
          <filedate>1447900440</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>node_clone 7.x-1.0-rc4</name>
      <version>7.x-1.0-rc4</version>
      <tag>7.x-1.0-rc4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0-rc4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc4.tar.gz</download_link>
      <date>1447628939</date>
      <mdhash>73b46b9eff36f29e4bcf1be461618ef5</mdhash>
      <filesize>14778</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>73b46b9eff36f29e4bcf1be461618ef5</md5>
          <size>14778</size>
          <filedate>1447628939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc4.zip</url>
          <archive_type>zip</archive_type>
          <md5>fb78f93fafde77bbb8e9a88a2b3569bf</md5>
          <size>18432</size>
          <filedate>1447628939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_clone 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc3.tar.gz</download_link>
      <date>1447385940</date>
      <mdhash>b00eecedc71e43c8b192daf6c7cac07f</mdhash>
      <filesize>14861</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b00eecedc71e43c8b192daf6c7cac07f</md5>
          <size>14861</size>
          <filedate>1447385940</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>6f213d56e452b76115cc324e4aa90158</md5>
          <size>18537</size>
          <filedate>1447385940</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_clone 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc2.tar.gz</download_link>
      <date>1386176905</date>
      <mdhash>ab0e93c573ecf4dfeb2451aa8c7fe50e</mdhash>
      <filesize>14185</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ab0e93c573ecf4dfeb2451aa8c7fe50e</md5>
          <size>14185</size>
          <filedate>1386176905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d4fd7a4bc869247be902ada2af8429cf</md5>
          <size>17495</size>
          <filedate>1386176905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_clone 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc1.tar.gz</download_link>
      <date>1344129444</date>
      <mdhash>d9256fc2e0650d083ddb1f4de50263c0</mdhash>
      <filesize>12848</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9256fc2e0650d083ddb1f4de50263c0</md5>
          <size>12848</size>
          <filedate>1344129444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>a15dfb5b2dd27438ea756b41ad2bd081</md5>
          <size>15576</size>
          <filedate>1344129444</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_clone 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-beta1.tar.gz</download_link>
      <date>1296525949</date>
      <mdhash>399a194e61ad3e88e65d09a7c0ea726e</mdhash>
      <filesize>15755</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>399a194e61ad3e88e65d09a7c0ea726e</md5>
          <size>15755</size>
          <filedate>1296525949</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4bb0ac49877717a4c7c98e794094755d</md5>
          <size>22328</size>
          <filedate>1296525949</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>node_clone 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/node_clone/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/node_clone-7.x-1.x-dev.tar.gz</download_link>
      <date>1477055236</date>
      <mdhash>74bbf7d52b4adddbf3d343fdb7e395e1</mdhash>
      <filesize>15110</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>74bbf7d52b4adddbf3d343fdb7e395e1</md5>
          <size>15110</size>
          <filedate>1477055236</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/node_clone-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a04e1bc3c269d3d78d82c22dcffe8b01</md5>
          <size>18780</size>
          <filedate>1477055236</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
