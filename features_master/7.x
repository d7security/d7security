<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Features Master</title>
  <short_name>features_master</short_name>
  <dc:creator>frankcarey</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/features_master</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>features_master 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/features_master/-/releases/7.x-1.2</release_link>
      <download_link>https://gitlab.com/api/v4/projects/66144587/packages/generic/features_master/7.x-1.2/features_master-7.x-1.2.tar.gz</download_link>
      <date>1736941349</date>
      <mdhash>ad8e3194d982da26ea621c11d6dd0ce7</mdhash>
      <filesize>10632</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/66144587/packages/generic/features_master/7.x-1.2/features_master-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ad8e3194d982da26ea621c11d6dd0ce7</md5>
          <size>10632</size>
          <filedate>1736941349</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/66144587/packages/generic/features_master/7.x-1.2/features_master-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>04873b2a9a76c98eb2f38fd1fcb26d3c</md5>
          <size>11555</size>
          <filedate>1736941349</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>features_master 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_master/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_master-7.x-1.1.tar.gz</download_link>
      <date>1469228939</date>
      <mdhash>ff39e8fc7d488aa9b83364dbc0a7e4f4</mdhash>
      <filesize>10601</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ff39e8fc7d488aa9b83364dbc0a7e4f4</md5>
          <size>10601</size>
          <filedate>1469228939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>918b571f23ec35e76b474aa4f2ff7a91</md5>
          <size>11252</size>
          <filedate>1469228939</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features_master 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_master/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_master-7.x-1.0.tar.gz</download_link>
      <date>1454000039</date>
      <mdhash>b3ca9895fbef954aadc19bb0c9b9d887</mdhash>
      <filesize>10566</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b3ca9895fbef954aadc19bb0c9b9d887</md5>
          <size>10566</size>
          <filedate>1454000039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>85deb007d9225909cf05651052ac5841</md5>
          <size>11211</size>
          <filedate>1454000039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>features_master 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_master/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-rc1.tar.gz</download_link>
      <date>1448467148</date>
      <mdhash>cc65dd3d118ff5a09167cac7cbead881</mdhash>
      <filesize>10569</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>cc65dd3d118ff5a09167cac7cbead881</md5>
          <size>10569</size>
          <filedate>1448467148</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1d0600eeae3cd792f2c65ad2da7feb6a</md5>
          <size>11209</size>
          <filedate>1448467148</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features_master 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_master/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-beta2.tar.gz</download_link>
      <date>1448467148</date>
      <mdhash>2804911cd546de9ba4c100ff904e0d77</mdhash>
      <filesize>8923</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2804911cd546de9ba4c100ff904e0d77</md5>
          <size>8923</size>
          <filedate>1448467148</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d7f496565261d4ddf17b63726e7ac979</md5>
          <size>9548</size>
          <filedate>1448467148</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>features_master 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/features_master/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/features_master-7.x-1.x-dev.tar.gz</download_link>
      <date>1469228939</date>
      <mdhash>61942af340273822cd0f0229b630aec0</mdhash>
      <filesize>10609</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>61942af340273822cd0f0229b630aec0</md5>
          <size>10609</size>
          <filedate>1469228939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/features_master-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>46f5ba38290c5fb3be5470b9b2f16a52</md5>
          <size>11258</size>
          <filedate>1469228939</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
