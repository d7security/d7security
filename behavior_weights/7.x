<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Behavior weights</title>
  <short_name>behavior_weights</short_name>
  <dc:creator>donquixote</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/behavior_weights</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>behavior_weights 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/behavior_weights/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0.tar.gz</download_link>
      <date>1343307724</date>
      <mdhash>19e53a3cf335eff88404a1db78fb1df5</mdhash>
      <filesize>8700</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>19e53a3cf335eff88404a1db78fb1df5</md5>
          <size>8700</size>
          <filedate>1343307724</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>2420f7f11a66aeffcd7a172de6d3507a</md5>
          <size>9691</size>
          <filedate>1343307724</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>behavior_weights 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/behavior_weights/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1317966701</date>
      <mdhash>6048bba0da5da8b4d2afaf660b24abdc</mdhash>
      <filesize>8691</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6048bba0da5da8b4d2afaf660b24abdc</md5>
          <size>8691</size>
          <filedate>1317966701</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>152f078df7f10c50e1efef8a91f3fa59</md5>
          <size>9694</size>
          <filedate>1317966701</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>behavior_weights 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/behavior_weights/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.x-dev.tar.gz</download_link>
      <date>1380555838</date>
      <mdhash>c69e6112e148a8b131f165c48c8a3d9c</mdhash>
      <filesize>8703</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c69e6112e148a8b131f165c48c8a3d9c</md5>
          <size>8703</size>
          <filedate>1380555838</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/behavior_weights-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ee21407d1fb340cfe44d9cc616d64014</md5>
          <size>9698</size>
          <filedate>1380555838</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
