<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Field Validation</title>
  <short_name>field_validation</short_name>
  <dc:creator>g089h515r806</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/field_validation</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content editing experience</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>field_validation 7.x-2.6</name>
      <version>7.x-2.6</version>
      <tag>7.x-2.6</tag>
      <version_major>2</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.6.tar.gz</download_link>
      <date>1434802380</date>
      <mdhash>42c1211578d7eefe306f1b5f589bff76</mdhash>
      <filesize>39854</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42c1211578d7eefe306f1b5f589bff76</md5>
          <size>39854</size>
          <filedate>1434802380</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.6.zip</url>
          <archive_type>zip</archive_type>
          <md5>9d13383fde0367f66c5e7757d79041ae</md5>
          <size>83373</size>
          <filedate>1434802380</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.5.tar.gz</download_link>
      <date>1433602080</date>
      <mdhash>91bd97a59d801cbd5efb2fdd5384ac3c</mdhash>
      <filesize>39868</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>91bd97a59d801cbd5efb2fdd5384ac3c</md5>
          <size>39868</size>
          <filedate>1433602080</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>d08ed482c14a46f08102ead5395f962c</md5>
          <size>83369</size>
          <filedate>1433602080</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.4.tar.gz</download_link>
      <date>1390724604</date>
      <mdhash>0733777e81fe18db94611a98189eab88</mdhash>
      <filesize>38588</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0733777e81fe18db94611a98189eab88</md5>
          <size>38588</size>
          <filedate>1390724604</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>eb69d26d3a47c9821ce8c4dfc8b8f95b</md5>
          <size>80840</size>
          <filedate>1390724604</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.3.tar.gz</download_link>
      <date>1359027110</date>
      <mdhash>a3b407ca37199a06d4fa29c72a2343e3</mdhash>
      <filesize>38521</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a3b407ca37199a06d4fa29c72a2343e3</md5>
          <size>38521</size>
          <filedate>1359027110</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>42e093be3ee1bad0bc3fca09d27b2ae2</md5>
          <size>80689</size>
          <filedate>1359027110</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.2.tar.gz</download_link>
      <date>1352727710</date>
      <mdhash>ace0bc26877b1bfeca5e699c58bc9bb9</mdhash>
      <filesize>37486</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ace0bc26877b1bfeca5e699c58bc9bb9</md5>
          <size>37486</size>
          <filedate>1352727710</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>80f02c39cfb28ffa76737514d009da0c</md5>
          <size>78874</size>
          <filedate>1352727710</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.1.tar.gz</download_link>
      <date>1344142024</date>
      <mdhash>9720b12e19e9a222dbce3ef76342ce75</mdhash>
      <filesize>29889</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9720b12e19e9a222dbce3ef76342ce75</md5>
          <size>29889</size>
          <filedate>1344142024</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b64b6280b91080c828ba00cf21e516cb</md5>
          <size>62739</size>
          <filedate>1344142024</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0.tar.gz</download_link>
      <date>1341896527</date>
      <mdhash>bd2cb0da0c8f77b35bd2af9b4defe8c3</mdhash>
      <filesize>21995</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bd2cb0da0c8f77b35bd2af9b4defe8c3</md5>
          <size>21995</size>
          <filedate>1341896527</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d05a26a326c11872ce89a7aabb7410f</md5>
          <size>39698</size>
          <filedate>1341896527</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-rc1.tar.gz</download_link>
      <date>1340024780</date>
      <mdhash>faaa8fe9462798f3a449036e24f4d12c</mdhash>
      <filesize>24198</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>faaa8fe9462798f3a449036e24f4d12c</md5>
          <size>24198</size>
          <filedate>1340024780</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>6798c91e9f58229dc586a078d25ef227</md5>
          <size>43866</size>
          <filedate>1340024780</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta2.tar.gz</download_link>
      <date>1335766862</date>
      <mdhash>89c1d83468fd718d6d4bee8dec8eee33</mdhash>
      <filesize>24105</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>89c1d83468fd718d6d4bee8dec8eee33</md5>
          <size>24105</size>
          <filedate>1335766862</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8295367c18fa75a9eb46c789d0ba56a2</md5>
          <size>43770</size>
          <filedate>1335766862</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta1.tar.gz</download_link>
      <date>1334569601</date>
      <mdhash>7c1de04e2d625cd5be43badd5985ce5e</mdhash>
      <filesize>22124</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7c1de04e2d625cd5be43badd5985ce5e</md5>
          <size>22124</size>
          <filedate>1334569601</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>fd00b9a1b9f02c3650a3947cee14911c</md5>
          <size>39245</size>
          <filedate>1334569601</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1333984246</date>
      <mdhash>77f3673151b8ef604b7a20cda6976251</mdhash>
      <filesize>10515</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>77f3673151b8ef604b7a20cda6976251</md5>
          <size>10515</size>
          <filedate>1333984246</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>770eb569c985ae9eae32ed1c28e5ea6a</md5>
          <size>14046</size>
          <filedate>1333984246</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0.tar.gz</download_link>
      <date>1335587455</date>
      <mdhash>a0f93e372ae85e5abcb2ceae92ec45a8</mdhash>
      <filesize>21233</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a0f93e372ae85e5abcb2ceae92ec45a8</md5>
          <size>21233</size>
          <filedate>1335587455</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>664b3d46e4a88bc14f7c6ac0b715b65f</md5>
          <size>26601</size>
          <filedate>1335587455</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-rc1.tar.gz</download_link>
      <date>1333623946</date>
      <mdhash>42d9ee38ad892d49f906091898f2cd45</mdhash>
      <filesize>21604</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42d9ee38ad892d49f906091898f2cd45</md5>
          <size>21604</size>
          <filedate>1333623946</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>17079fbd5c3e5d01528886293930fe45</md5>
          <size>26985</size>
          <filedate>1333623946</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta6</name>
      <version>7.x-1.0-beta6</version>
      <tag>7.x-1.0-beta6</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta6</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta6</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta6.tar.gz</download_link>
      <date>1333583446</date>
      <mdhash>f110083cba2bef5b65d727f01b77c609</mdhash>
      <filesize>18589</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f110083cba2bef5b65d727f01b77c609</md5>
          <size>18589</size>
          <filedate>1333583446</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta6.zip</url>
          <archive_type>zip</archive_type>
          <md5>b1eb9990eaa79bf729ff485b852b2d0d</md5>
          <size>22498</size>
          <filedate>1333583446</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta5</name>
      <version>7.x-1.0-beta5</version>
      <tag>7.x-1.0-beta5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta5.tar.gz</download_link>
      <date>1329135038</date>
      <mdhash>084da008e0e6410f5e39469a2b93a0bc</mdhash>
      <filesize>18708</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>084da008e0e6410f5e39469a2b93a0bc</md5>
          <size>18708</size>
          <filedate>1329135038</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta5.zip</url>
          <archive_type>zip</archive_type>
          <md5>40aa50b84a08236f6295bf1b95a52e85</md5>
          <size>22665</size>
          <filedate>1329135038</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta4</name>
      <version>7.x-1.0-beta4</version>
      <tag>7.x-1.0-beta4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta4.tar.gz</download_link>
      <date>1318602996</date>
      <mdhash>d9dcde9f3107e814c61db551864e5dcf</mdhash>
      <filesize>16439</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9dcde9f3107e814c61db551864e5dcf</md5>
          <size>16439</size>
          <filedate>1318602996</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta4.zip</url>
          <archive_type>zip</archive_type>
          <md5>442dbc01937715b10f1aa70797de8fb1</md5>
          <size>18421</size>
          <filedate>1318602996</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta3.tar.gz</download_link>
      <date>1317772004</date>
      <mdhash>f19b9e84f25ba5c88825f7414a082118</mdhash>
      <filesize>14850</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f19b9e84f25ba5c88825f7414a082118</md5>
          <size>14850</size>
          <filedate>1317772004</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>a18b9c308d1c2b8a92ccc1d8304ae4ea</md5>
          <size>16819</size>
          <filedate>1317772004</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta2.tar.gz</download_link>
      <date>1316264202</date>
      <mdhash>983094d6a06db59243a4ffcc383c256b</mdhash>
      <filesize>12422</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>983094d6a06db59243a4ffcc383c256b</md5>
          <size>12422</size>
          <filedate>1316264202</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>33ecc994950106f7de2e333b860d3687</md5>
          <size>14461</size>
          <filedate>1316264202</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta1.tar.gz</download_link>
      <date>1305547015</date>
      <mdhash>b8e410eadb9bd36358cd3f41e2a10f33</mdhash>
      <filesize>11014</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b8e410eadb9bd36358cd3f41e2a10f33</md5>
          <size>11014</size>
          <filedate>1305547015</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>25c70348c989290c3a1d535cd05eab0d</md5>
          <size>13103</size>
          <filedate>1305547015</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1304650916</date>
      <mdhash>ae3e8c46ddd070d06c6385890049ca8d</mdhash>
      <filesize>12039</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ae3e8c46ddd070d06c6385890049ca8d</md5>
          <size>12039</size>
          <filedate>1304650916</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>352acc80bfe2de67abe1528d9a86f3b9</md5>
          <size>14212</size>
          <filedate>1304650916</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-2.x-dev.tar.gz</download_link>
      <date>1687607311</date>
      <mdhash>42dbf49d021f5e77716bf9ff6a16dac4</mdhash>
      <filesize>40370</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>42dbf49d021f5e77716bf9ff6a16dac4</md5>
          <size>40370</size>
          <filedate>1687607311</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>fbde5496d8cec17ea05eb8d0b65b2a72</md5>
          <size>84584</size>
          <filedate>1687607311</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>field_validation 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/field_validation/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/field_validation-7.x-1.x-dev.tar.gz</download_link>
      <date>1380579010</date>
      <mdhash>8242ddeaca8218e1c3dbc46a27d17e01</mdhash>
      <filesize>21314</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8242ddeaca8218e1c3dbc46a27d17e01</md5>
          <size>21314</size>
          <filedate>1380579010</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/field_validation-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>137c6ecf8bc78f4b1f4dfe568357ed58</md5>
          <size>26678</size>
          <filedate>1380579011</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
