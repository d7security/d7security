<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Panelizer</title>
  <short_name>panelizer</short_name>
  <dc:creator>damienmckenna</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/panelizer</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>No further development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Content display</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>panelizer 7.x-3.4</name>
      <version>7.x-3.4</version>
      <tag>7.x-3.4</tag>
      <version_major>3</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.4.tar.gz</download_link>
      <date>1471635539</date>
      <mdhash>4665c933e84054ab202dfb21c7ae222b</mdhash>
      <filesize>106897</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4665c933e84054ab202dfb21c7ae222b</md5>
          <size>106897</size>
          <filedate>1471635539</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ff7b211628a64a3105c71f33cba59d92</md5>
          <size>138232</size>
          <filedate>1471635539</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-3.3</name>
      <version>7.x-3.3</version>
      <tag>7.x-3.3</tag>
      <version_major>3</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.3.tar.gz</download_link>
      <date>1471449504</date>
      <mdhash>97557f0018c7d34f2f3546285b8d5f72</mdhash>
      <filesize>104107</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>97557f0018c7d34f2f3546285b8d5f72</md5>
          <size>104107</size>
          <filedate>1471449504</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>e24489fca0683ea382ced0d302aaa6e2</md5>
          <size>134165</size>
          <filedate>1471449504</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-3.2</name>
      <version>7.x-3.2</version>
      <tag>7.x-3.2</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2.tar.gz</download_link>
      <date>1467340739</date>
      <mdhash>23fc9a32c53d3e3d1249fe9846f26f57</mdhash>
      <filesize>101389</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>23fc9a32c53d3e3d1249fe9846f26f57</md5>
          <size>101389</size>
          <filedate>1467340739</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>c6b02ec698da92f4f99d5511945e0ffd</md5>
          <size>129552</size>
          <filedate>1467340739</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-3.2-beta1</name>
      <version>7.x-3.2-beta1</version>
      <tag>7.x-3.2-beta1</tag>
      <version_major>3</version_major>
      <version_patch>2</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.2-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2-beta1.tar.gz</download_link>
      <date>1422878581</date>
      <mdhash>92d13fe040146e70a81eeeddad5f5fda</mdhash>
      <filesize>75805</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>92d13fe040146e70a81eeeddad5f5fda</md5>
          <size>75805</size>
          <filedate>1422878581</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.2-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>caa4693b86fa826e01ce2d71d4673846</md5>
          <size>94643</size>
          <filedate>1422878581</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-3.1</name>
      <version>7.x-3.1</version>
      <tag>7.x-3.1</tag>
      <version_major>3</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.1.tar.gz</download_link>
      <date>1360785942</date>
      <mdhash>463d79d78caac3ad1c016f3d1977e640</mdhash>
      <filesize>56244</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>463d79d78caac3ad1c016f3d1977e640</md5>
          <size>56244</size>
          <filedate>1360785942</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>149f26db125ef8b7b23c47db9c0d5b13</md5>
          <size>71445</size>
          <filedate>1360785942</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-3.0</name>
      <version>7.x-3.0</version>
      <tag>7.x-3.0</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0.tar.gz</download_link>
      <date>1359576339</date>
      <mdhash>5ecf636f9492b80c7626d2f758be71fc</mdhash>
      <filesize>56170</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5ecf636f9492b80c7626d2f758be71fc</md5>
          <size>56170</size>
          <filedate>1359576339</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>8f31a01fb9b5602ff97354b06a29ff47</md5>
          <size>71402</size>
          <filedate>1359576339</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-3.0-rc1</name>
      <version>7.x-3.0-rc1</version>
      <tag>7.x-3.0-rc1</tag>
      <version_major>3</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0-rc1.tar.gz</download_link>
      <date>1345319286</date>
      <mdhash>6a3d519c3fc09bb964ff2bfffcffac5e</mdhash>
      <filesize>51650</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6a3d519c3fc09bb964ff2bfffcffac5e</md5>
          <size>51650</size>
          <filedate>1345319286</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c48aab55bea9b31835f851d12f9f1873</md5>
          <size>65540</size>
          <filedate>1345319286</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0.tar.gz</download_link>
      <date>1332005747</date>
      <mdhash>da686dcab10a5c29c857189db77c6d1c</mdhash>
      <filesize>38734</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>da686dcab10a5c29c857189db77c6d1c</md5>
          <size>38734</size>
          <filedate>1332005747</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>2eace9a12cf58aaa2ce39d40b651e456</md5>
          <size>50536</size>
          <filedate>1332005747</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>panelizer 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-rc1.tar.gz</download_link>
      <date>1326674314</date>
      <mdhash>2aa59ddab9ef20ff19382ac1a3912609</mdhash>
      <filesize>37377</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2aa59ddab9ef20ff19382ac1a3912609</md5>
          <size>37377</size>
          <filedate>1326674314</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>24b0079bba645fec58ca3cd2a005f44f</md5>
          <size>49201</size>
          <filedate>1326674314</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta2.tar.gz</download_link>
      <date>1323126344</date>
      <mdhash>d9d848753ebe8c980704991f7fc00765</mdhash>
      <filesize>35562</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9d848753ebe8c980704991f7fc00765</md5>
          <size>35562</size>
          <filedate>1323126344</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6199269da45d8806743a6a4de3901aa4</md5>
          <size>46703</size>
          <filedate>1323126344</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta1.tar.gz</download_link>
      <date>1322716245</date>
      <mdhash>845d30ce28e545786b8adf81a122d59d</mdhash>
      <filesize>35074</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>845d30ce28e545786b8adf81a122d59d</md5>
          <size>35074</size>
          <filedate>1322716245</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>4f16826f9bca799399414495476bbef1</md5>
          <size>46136</size>
          <filedate>1322716245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-3.x-dev.tar.gz</download_link>
      <date>1548477484</date>
      <mdhash>019b4ae6da849c6801f9511767201147</mdhash>
      <filesize>112739</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>019b4ae6da849c6801f9511767201147</md5>
          <size>112739</size>
          <filedate>1548477484</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>ad436c1462ffde82199d8b6c4d836258</md5>
          <size>144853</size>
          <filedate>1548477484</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>panelizer 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/panelizer/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/panelizer-7.x-2.x-dev.tar.gz</download_link>
      <date>1380621997</date>
      <mdhash>bbb59733e0bbe9b93e130ad34a5f9050</mdhash>
      <filesize>45418</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bbb59733e0bbe9b93e130ad34a5f9050</md5>
          <size>45418</size>
          <filedate>1380621997</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/panelizer-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a7ecfaf0716dfc9425637fff8b979e14</md5>
          <size>58700</size>
          <filedate>1380621998</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
