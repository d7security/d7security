<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Encrypt</title>
  <short_name>encrypt</short_name>
  <dc:creator>rlhawk</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/encrypt</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Developer tools</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Security</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>encrypt 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.3.tar.gz</download_link>
      <date>1448047439</date>
      <mdhash>2d82760c75ab9402552ecc65ca9e0d55</mdhash>
      <filesize>28739</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2d82760c75ab9402552ecc65ca9e0d55</md5>
          <size>28739</size>
          <filedate>1448047439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>c321739f56f18c48ef663abd5b6e0cb0</md5>
          <size>41033</size>
          <filedate>1448047439</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.2.tar.gz</download_link>
      <date>1447870139</date>
      <mdhash>9edfb8c476fbb928e7b89050dac3d42a</mdhash>
      <filesize>28734</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9edfb8c476fbb928e7b89050dac3d42a</md5>
          <size>28734</size>
          <filedate>1447870139</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>69b3b9d026ef8787f8de32bcf984165b</md5>
          <size>41032</size>
          <filedate>1447870139</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1.tar.gz</download_link>
      <date>1440191039</date>
      <mdhash>24b940f624038082e1623770a75413b5</mdhash>
      <filesize>27052</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24b940f624038082e1623770a75413b5</md5>
          <size>27052</size>
          <filedate>1440191039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d51aca9b829a0723f75696421da36e75</md5>
          <size>38248</size>
          <filedate>1440191039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-2.1-rc1</name>
      <version>7.x-2.1-rc1</version>
      <tag>7.x-2.1-rc1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.1-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1-rc1.tar.gz</download_link>
      <date>1440013439</date>
      <mdhash>1b0af739ef24f7a269a1838d615cb104</mdhash>
      <filesize>27050</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>1b0af739ef24f7a269a1838d615cb104</md5>
          <size>27050</size>
          <filedate>1440013439</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.1-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1264ef3e652fce0c7d89be2c5c380751</md5>
          <size>38258</size>
          <filedate>1440013439</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0.tar.gz</download_link>
      <date>1412192629</date>
      <mdhash>9aa565aa69fde400cc9c68bdf154e3b2</mdhash>
      <filesize>26078</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9aa565aa69fde400cc9c68bdf154e3b2</md5>
          <size>26078</size>
          <filedate>1412192629</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a3bdde0859bbfc8431e482ccf1ead917</md5>
          <size>36791</size>
          <filedate>1412192629</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-2.0-rc1</name>
      <version>7.x-2.0-rc1</version>
      <tag>7.x-2.0-rc1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-rc1.tar.gz</download_link>
      <date>1408462342</date>
      <mdhash>578fb5985fbfec38fc2d4ed8230c747d</mdhash>
      <filesize>26082</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>578fb5985fbfec38fc2d4ed8230c747d</md5>
          <size>26082</size>
          <filedate>1408462342</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>d75d8665f2c41dd0a4cb96c0ca86feaf</md5>
          <size>36799</size>
          <filedate>1408462342</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-2.0-beta3</name>
      <version>7.x-2.0-beta3</version>
      <tag>7.x-2.0-beta3</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.0-beta3</release_link>
      <download_link/>
      <date>1349108467</date>
      <mdhash>620a87ef3713a95fadbc05c9e979cc18</mdhash>
      <filesize>22101</filesize>
      <files>
        <file>
          <url/>
          <archive_type>tar.gz</archive_type>
          <md5>620a87ef3713a95fadbc05c9e979cc18</md5>
          <size>22101</size>
          <filedate>1349108467</filedate>
        </file>
        <file>
          <url/>
          <archive_type>zip</archive_type>
          <md5>e2dbc4bae5de27916e4bf63ec8549c92</md5>
          <size>34370</size>
          <filedate>1349108467</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-2.0-beta2</name>
      <version>7.x-2.0-beta2</version>
      <tag>7.x-2.0-beta2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta2.tar.gz</download_link>
      <date>1332733245</date>
      <mdhash>06c9ca069b3c9863aa0afbf21221f5a3</mdhash>
      <filesize>22067</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>06c9ca069b3c9863aa0afbf21221f5a3</md5>
          <size>22067</size>
          <filedate>1332733245</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>3f351e4eab9134781cd9c8ad2e4a7d4d</md5>
          <size>34247</size>
          <filedate>1332733245</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta1.tar.gz</download_link>
      <date>1313443617</date>
      <mdhash>24fc9d48a27d61f6140436c015e773a6</mdhash>
      <filesize>18532</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>24fc9d48a27d61f6140436c015e773a6</md5>
          <size>18532</size>
          <filedate>1313443617</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>b2e569ceb551c9ce1dcd66f552a7db29</md5>
          <size>30145</size>
          <filedate>1313443617</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.1.tar.gz</download_link>
      <date>1337399455</date>
      <mdhash>c782fcce0453121212b87116fb4a09a4</mdhash>
      <filesize>16947</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c782fcce0453121212b87116fb4a09a4</md5>
          <size>16947</size>
          <filedate>1337399455</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>94ba0fb3705c0f089a2a8e6b39eff16d</md5>
          <size>21586</size>
          <filedate>1337399455</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0.tar.gz</download_link>
      <date>1315793803</date>
      <mdhash>2a1a54fb84a9579fe89fb63c5d79d18c</mdhash>
      <filesize>15826</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>2a1a54fb84a9579fe89fb63c5d79d18c</md5>
          <size>15826</size>
          <filedate>1315793803</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>a1cb1b2ebb428adac1120d9929fe5393</md5>
          <size>20478</size>
          <filedate>1315793803</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>encrypt 7.x-1.0-rc3</name>
      <version>7.x-1.0-rc3</version>
      <tag>7.x-1.0-rc3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.0-rc3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc3.tar.gz</download_link>
      <date>1314665161</date>
      <mdhash>4f1275a7c5b12b66ac50d4df093a8415</mdhash>
      <filesize>15822</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4f1275a7c5b12b66ac50d4df093a8415</md5>
          <size>15822</size>
          <filedate>1314665161</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc3.zip</url>
          <archive_type>zip</archive_type>
          <md5>9049a6ec8206c8ff997810645cfdd50b</md5>
          <size>20483</size>
          <filedate>1314665161</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc2.tar.gz</download_link>
      <date>1313626962</date>
      <mdhash>a493228edbe5e9a08a24077fabacfab2</mdhash>
      <filesize>15173</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a493228edbe5e9a08a24077fabacfab2</md5>
          <size>15173</size>
          <filedate>1313626962</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>6500b6fb19e0af2444f62b31321b7ba9</md5>
          <size>19807</size>
          <filedate>1313626962</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc1.tar.gz</download_link>
      <date>1310080916</date>
      <mdhash>99c12b14dd40882d538e885d74ed9a4d</mdhash>
      <filesize>11311</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>99c12b14dd40882d538e885d74ed9a4d</md5>
          <size>11311</size>
          <filedate>1310080916</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>818b0d86e62cb1702f90559284ed073c</md5>
          <size>14697</size>
          <filedate>1310080916</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-3.x-dev</name>
      <version>7.x-3.x-dev</version>
      <tag>7.x-3.x</tag>
      <version_major>3</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-3.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-3.x-dev.tar.gz</download_link>
      <date>1440008939</date>
      <mdhash>0baf39f8106c51939286f1a0345f6480</mdhash>
      <filesize>27018</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-3.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>0baf39f8106c51939286f1a0345f6480</md5>
          <size>27018</size>
          <filedate>1440008939</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-3.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>1246f6c63064615e12d5053b8d5e9fdb</md5>
          <size>38238</size>
          <filedate>1440008939</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-2.x-dev.tar.gz</download_link>
      <date>1553611385</date>
      <mdhash>f5c76ed6a86289c98f0396e05df2e2bf</mdhash>
      <filesize>29820</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f5c76ed6a86289c98f0396e05df2e2bf</md5>
          <size>29820</size>
          <filedate>1553611385</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>582db34aa2c35bfb88b06c7c4ac17ca8</md5>
          <size>44183</size>
          <filedate>1553611385</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>encrypt 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/encrypt/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/encrypt-7.x-1.x-dev.tar.gz</download_link>
      <date>1433360880</date>
      <mdhash>c493435722b2992bef2f1c4c73dd62dc</mdhash>
      <filesize>16692</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c493435722b2992bef2f1c4c73dd62dc</md5>
          <size>16692</size>
          <filedate>1433360880</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/encrypt-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>9eb6a357140e11bdc3edf7b1e89a0d68</md5>
          <size>21924</size>
          <filedate>1433360880</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
