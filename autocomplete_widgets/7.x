<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Autocomplete Widgets for Text and Number Fields</title>
  <short_name>autocomplete_widgets</short_name>
  <dc:creator>markus_petrux</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/autocomplete_widgets</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Maintenance fixes only</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Multilingual</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>autocomplete_widgets 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_widgets/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-rc1.tar.gz</download_link>
      <date>1366135723</date>
      <mdhash>f661c0f395efbd743bdc2d3d127b00ff</mdhash>
      <filesize>16555</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f661c0f395efbd743bdc2d3d127b00ff</md5>
          <size>16555</size>
          <filedate>1366135723</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>5a959b32b1741f8cc55d3a8c5f586d6e</md5>
          <size>19883</size>
          <filedate>1366135724</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>autocomplete_widgets 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_widgets/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta2.tar.gz</download_link>
      <date>1332436840</date>
      <mdhash>7efc231c8c982600251a560b5e406d6b</mdhash>
      <filesize>13944</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7efc231c8c982600251a560b5e406d6b</md5>
          <size>13944</size>
          <filedate>1332436840</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>44abe149e41ce44db66e40e92555834b</md5>
          <size>16494</size>
          <filedate>1332436840</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>autocomplete_widgets 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_widgets/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta1.tar.gz</download_link>
      <date>1319988329</date>
      <mdhash>bb0191b2c1a4a01e7ae8deb87b433bc9</mdhash>
      <filesize>12989</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bb0191b2c1a4a01e7ae8deb87b433bc9</md5>
          <size>12989</size>
          <filedate>1319988329</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>1c4dae9fe3ea09fbb759af030fd5d398</md5>
          <size>15469</size>
          <filedate>1319988329</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>autocomplete_widgets 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_widgets/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1313362914</date>
      <mdhash>873ed947670d66e8ba294f3d3b60774e</mdhash>
      <filesize>12646</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>873ed947670d66e8ba294f3d3b60774e</md5>
          <size>12646</size>
          <filedate>1313362914</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>8054773094cb64d9d2d4580d08431617</md5>
          <size>16097</size>
          <filedate>1313362914</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Insecure</value>
        </term>
      </terms>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
    <release>
      <name>autocomplete_widgets 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/autocomplete_widgets/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.x-dev.tar.gz</download_link>
      <date>1440851639</date>
      <mdhash>7d9be4543f4ac8ed77bacfa427bdebc5</mdhash>
      <filesize>16678</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7d9be4543f4ac8ed77bacfa427bdebc5</md5>
          <size>16678</size>
          <filedate>1440851639</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/autocomplete_widgets-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>7457a13263f243d2e60caf3abc39d1ba</md5>
          <size>20113</size>
          <filedate>1440851639</filedate>
        </file>
      </files>
      <security>Project has not opted into security advisory coverage!</security>
    </release>
  </releases>
</project>
