<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Views Raw SQL</title>
  <short_name>views_raw_sql</short_name>
  <dc:creator>sreynen</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/views_raw_sql</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Seeking co-maintainer(s)</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>views_raw_sql 7.x-1.2-rc1</name>
      <version>7.x-1.2-rc1</version>
      <tag>7.x-1.2-rc1</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.2-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.2-rc1.tar.gz</download_link>
      <date>1543369084</date>
      <mdhash>6372b6bee45f0fef7e4c394f151fe7ac</mdhash>
      <filesize>9883</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.2-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6372b6bee45f0fef7e4c394f151fe7ac</md5>
          <size>9883</size>
          <filedate>1543369084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.2-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3dd2d27a2c5e658348df840bed817336</md5>
          <size>12473</size>
          <filedate>1543369084</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_raw_sql 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1.tar.gz</download_link>
      <date>1543369084</date>
      <mdhash>7dc3581270324f6327a332acbb2656ea</mdhash>
      <filesize>8359</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>7dc3581270324f6327a332acbb2656ea</md5>
          <size>8359</size>
          <filedate>1543369084</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cf7f14a5887babaf3a27f62d6d940d72</md5>
          <size>10803</size>
          <filedate>1543369084</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_raw_sql 7.x-1.1-rc1</name>
      <version>7.x-1.1-rc1</version>
      <tag>7.x-1.1-rc1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.1-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1-rc1.tar.gz</download_link>
      <date>1454441039</date>
      <mdhash>04a11dce73723b8ec60676ee1fe67af5</mdhash>
      <filesize>9328</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>04a11dce73723b8ec60676ee1fe67af5</md5>
          <size>9328</size>
          <filedate>1454441039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.1-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>45c04847cadc6ff2671364f3701be7af</md5>
          <size>11850</size>
          <filedate>1454441039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_raw_sql 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0.tar.gz</download_link>
      <date>1454441039</date>
      <mdhash>29bcf134619bdb9eb74250b571708fd1</mdhash>
      <filesize>8351</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>29bcf134619bdb9eb74250b571708fd1</md5>
          <size>8351</size>
          <filedate>1454441039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>6b6b541f0a5408ca589b66e9c1014fc9</md5>
          <size>10783</size>
          <filedate>1454441039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>views_raw_sql 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0-rc1.tar.gz</download_link>
      <date>1347151781</date>
      <mdhash>e5a13733d005249fcdaec104d3d0376b</mdhash>
      <filesize>8258</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e5a13733d005249fcdaec104d3d0376b</md5>
          <size>8258</size>
          <filedate>1347151781</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>c49e8f95aa1b1f3f6054f2c4e03556e1</md5>
          <size>9774</size>
          <filedate>1347151781</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>views_raw_sql 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/views_raw_sql/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.x-dev.tar.gz</download_link>
      <date>1544893680</date>
      <mdhash>fe6bea0d9167e506b201439f6f17d575</mdhash>
      <filesize>9862</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>fe6bea0d9167e506b201439f6f17d575</md5>
          <size>9862</size>
          <filedate>1544893680</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/views_raw_sql-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>07603a25d1a0f41eeb639256ae388549</md5>
          <size>12479</size>
          <filedate>1544893680</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
