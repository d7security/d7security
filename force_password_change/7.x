<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Force Password Change</title>
  <short_name>force_password_change</short_name>
  <dc:creator>jaypan</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/force_password_change</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>force_password_change 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.2.tar.gz</download_link>
      <date>1491843242</date>
      <mdhash>c11933fc58ba99296f2e0b888677e875</mdhash>
      <filesize>20798</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>c11933fc58ba99296f2e0b888677e875</md5>
          <size>20798</size>
          <filedate>1491843242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>47a1b59b90fdd7ba733b4c455f406515</md5>
          <size>24270</size>
          <filedate>1491843242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>force_password_change 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.1.tar.gz</download_link>
      <date>1484898184</date>
      <mdhash>8326efbd1be64c72d70abec7e10e9eb6</mdhash>
      <filesize>20716</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8326efbd1be64c72d70abec7e10e9eb6</md5>
          <size>20716</size>
          <filedate>1484898184</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>3d5da58dc9def32152f1da7eb5c8dd51</md5>
          <size>24200</size>
          <filedate>1484898184</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>force_password_change 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0.tar.gz</download_link>
      <date>1482372487</date>
      <mdhash>6f001dbf0a21f1910033bb462f8e88f4</mdhash>
      <filesize>20496</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6f001dbf0a21f1910033bb462f8e88f4</md5>
          <size>20496</size>
          <filedate>1482372487</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>579f4550b11cf191a20750a80366cc21</md5>
          <size>23740</size>
          <filedate>1482372487</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>force_password_change 7.x-2.0-rc2</name>
      <version>7.x-2.0-rc2</version>
      <tag>7.x-2.0-rc2</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-2.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0-rc2.tar.gz</download_link>
      <date>1481165885</date>
      <mdhash>82c0656c80a6387f31face1d8cc756ae</mdhash>
      <filesize>20498</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>82c0656c80a6387f31face1d8cc756ae</md5>
          <size>20498</size>
          <filedate>1481165885</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>b65c331ac7d79ebaba1a514aa4b9d43f</md5>
          <size>23744</size>
          <filedate>1481165885</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>force_password_change 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0.tar.gz</download_link>
      <date>1449157440</date>
      <mdhash>b0056a8a89679c3c3aa1ba6d8fd5bbff</mdhash>
      <filesize>19803</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b0056a8a89679c3c3aa1ba6d8fd5bbff</md5>
          <size>19803</size>
          <filedate>1449157440</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>762d4154884c7a103468e107b9a9a23c</md5>
          <size>22934</size>
          <filedate>1449157440</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>force_password_change 7.x-1.0-rc2</name>
      <version>7.x-1.0-rc2</version>
      <tag>7.x-1.0-rc2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-1.0-rc2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc2.tar.gz</download_link>
      <date>1296546103</date>
      <mdhash>e0edd8916a2a463bed572a162cb18400</mdhash>
      <filesize>18772</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e0edd8916a2a463bed572a162cb18400</md5>
          <size>18772</size>
          <filedate>1296546103</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc2.zip</url>
          <archive_type>zip</archive_type>
          <md5>450fcf4aac7e43733e861334cac724a4</md5>
          <size>22083</size>
          <filedate>1296546103</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>force_password_change 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc1.tar.gz</download_link>
      <date>1295496248</date>
      <mdhash>5df35450ee7ee04520e32bec135035f5</mdhash>
      <filesize>18748</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5df35450ee7ee04520e32bec135035f5</md5>
          <size>18748</size>
          <filedate>1295496248</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>48e8b089cebfb544aa9a3042dbb934e2</md5>
          <size>22083</size>
          <filedate>1295496248</filedate>
        </file>
      </files>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>force_password_change 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.x-dev.tar.gz</download_link>
      <date>1492792444</date>
      <mdhash>de800549354482e9bf632cf66d62ffa6</mdhash>
      <filesize>20864</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>de800549354482e9bf632cf66d62ffa6</md5>
          <size>20864</size>
          <filedate>1492792444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>de164b4e4e9f6dbaa3b334a30c621d38</md5>
          <size>24319</size>
          <filedate>1492792444</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>force_password_change 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/force_password_change/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.x-dev.tar.gz</download_link>
      <date>1478622543</date>
      <mdhash>8f4623882a94a93c30a88f8bfd03a13f</mdhash>
      <filesize>19816</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8f4623882a94a93c30a88f8bfd03a13f</md5>
          <size>19816</size>
          <filedate>1478622543</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/force_password_change-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>02d9cfbea2a71421b30afffe5596f274</md5>
          <size>22952</size>
          <filedate>1478622543</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
