<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Variable</title>
  <short_name>variable</short_name>
  <dc:creator>jose reyero</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://www.drupal.org/project/variable</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>variable 7.x-2.5</name>
      <version>7.x-2.5</version>
      <tag>7.x-2.5</tag>
      <version_major>2</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.5.tar.gz</download_link>
      <date>1398250127</date>
      <mdhash>19ef79edd10ef87e471775cf34a27386</mdhash>
      <filesize>53993</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>19ef79edd10ef87e471775cf34a27386</md5>
          <size>53993</size>
          <filedate>1398250127</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>57ecfe10f77a1e437b332aa4070bce9b</md5>
          <size>74700</size>
          <filedate>1398250127</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.4.tar.gz</download_link>
      <date>1390310305</date>
      <mdhash>593343d361833c14f6bf8da3cc93332c</mdhash>
      <filesize>53722</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>593343d361833c14f6bf8da3cc93332c</md5>
          <size>53722</size>
          <filedate>1390310305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>3d72658196378941aa60f640b375387b</md5>
          <size>74488</size>
          <filedate>1390310305</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.3.tar.gz</download_link>
      <date>1376034994</date>
      <mdhash>4a053c4479a0a540225dc040e822b6ac</mdhash>
      <filesize>53717</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>4a053c4479a0a540225dc040e822b6ac</md5>
          <size>53717</size>
          <filedate>1376034994</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>add84db6d632a40f5b68719abcf87754</md5>
          <size>76212</size>
          <filedate>1376034994</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.2.tar.gz</download_link>
      <date>1358075138</date>
      <mdhash>07d1633a9df95a66374c70eb9b539ef1</mdhash>
      <filesize>53545</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>07d1633a9df95a66374c70eb9b539ef1</md5>
          <size>53545</size>
          <filedate>1358075138</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0921bd18f1d4dc277c60bacb7f1400bb</md5>
          <size>76001</size>
          <filedate>1358075138</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.1.tar.gz</download_link>
      <date>1341236242</date>
      <mdhash>ec6e03acf71fdb65471dcf3f71439a2b</mdhash>
      <filesize>52510</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ec6e03acf71fdb65471dcf3f71439a2b</md5>
          <size>52510</size>
          <filedate>1341236242</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>7e817916db6e43cdfdb86e39585f7d6c</md5>
          <size>75007</size>
          <filedate>1341236242</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.0.tar.gz</download_link>
      <date>1340224919</date>
      <mdhash>8360a99fe685553a71ebb8f2b2de9537</mdhash>
      <filesize>52286</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8360a99fe685553a71ebb8f2b2de9537</md5>
          <size>52286</size>
          <filedate>1340224919</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>7d1ea9e819a5c9d47b47440b266d2dc4</md5>
          <size>74754</size>
          <filedate>1340224919</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-2.0-beta1</name>
      <version>7.x-2.0-beta1</version>
      <tag>7.x-2.0-beta1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.0-beta1.tar.gz</download_link>
      <date>1338039697</date>
      <mdhash>f06bde35794978e2114b064b8fe6a26a</mdhash>
      <filesize>50599</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>f06bde35794978e2114b064b8fe6a26a</md5>
          <size>50599</size>
          <filedate>1338039697</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>cce2d9e547311a884c233e70e9d47a05</md5>
          <size>74385</size>
          <filedate>1338039697</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-2.0-alpha1</name>
      <version>7.x-2.0-alpha1</version>
      <tag>7.x-2.0-alpha1</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.0-alpha1.tar.gz</download_link>
      <date>1337602905</date>
      <mdhash>a969eb4605ed8d1b46d5a7f615894d97</mdhash>
      <filesize>49073</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>a969eb4605ed8d1b46d5a7f615894d97</md5>
          <size>49073</size>
          <filedate>1337602905</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>093699442d995c7dc37e27c63c435a01</md5>
          <size>72387</size>
          <filedate>1337602905</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.2.tar.gz</download_link>
      <date>1334999498</date>
      <mdhash>d22b461760eb97e010a29ab443d26582</mdhash>
      <filesize>39619</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d22b461760eb97e010a29ab443d26582</md5>
          <size>39619</size>
          <filedate>1334999498</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>acd65f41f9d762b2b3720932cb69b508</md5>
          <size>59594</size>
          <filedate>1334999498</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.1.tar.gz</download_link>
      <date>1313088721</date>
      <mdhash>e25c594c052b1973402c90ed896b40aa</mdhash>
      <filesize>30548</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e25c594c052b1973402c90ed896b40aa</md5>
          <size>30548</size>
          <filedate>1313088721</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>722ed19a741b27026b5e893a154db190</md5>
          <size>43801</size>
          <filedate>1313088721</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0.tar.gz</download_link>
      <date>1304158917</date>
      <mdhash>715a7eb15fb721ce32f243a0cbe014a2</mdhash>
      <filesize>29109</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>715a7eb15fb721ce32f243a0cbe014a2</md5>
          <size>29109</size>
          <filedate>1304158917</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b40e3ecf4be73dbf8779a9b3ffeab353</md5>
          <size>41225</size>
          <filedate>1304158917</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>variable 7.x-1.0-beta3</name>
      <version>7.x-1.0-beta3</version>
      <tag>7.x-1.0-beta3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-beta3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta3.tar.gz</download_link>
      <date>1303033317</date>
      <mdhash>b7b99e2a5fee44e3ba2bc9162826576c</mdhash>
      <filesize>29116</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b7b99e2a5fee44e3ba2bc9162826576c</md5>
          <size>29116</size>
          <filedate>1303033317</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta3.zip</url>
          <archive_type>zip</archive_type>
          <md5>114888e35ac6f557e35279196a508253</md5>
          <size>41229</size>
          <filedate>1303033317</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-beta2</name>
      <version>7.x-1.0-beta2</version>
      <tag>7.x-1.0-beta2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-beta2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta2.tar.gz</download_link>
      <date>1298891771</date>
      <mdhash>91d7116b5ef8b9a0b32d47c913e93a9c</mdhash>
      <filesize>28339</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>91d7116b5ef8b9a0b32d47c913e93a9c</md5>
          <size>28339</size>
          <filedate>1298891771</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta2.zip</url>
          <archive_type>zip</archive_type>
          <md5>d824c9c829b723fa890ae92bfce846a7</md5>
          <size>40483</size>
          <filedate>1298891771</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-beta1</name>
      <version>7.x-1.0-beta1</version>
      <tag>7.x-1.0-beta1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>beta1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-beta1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta1.tar.gz</download_link>
      <date>1298575040</date>
      <mdhash>316aa9de8723f9e42d235340d5e81cd8</mdhash>
      <filesize>27140</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>316aa9de8723f9e42d235340d5e81cd8</md5>
          <size>27140</size>
          <filedate>1298575040</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-beta1.zip</url>
          <archive_type>zip</archive_type>
          <md5>9907d30f3698ce8850d24b39a9869bee</md5>
          <size>40761</size>
          <filedate>1298575040</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Beta releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-alpha5</name>
      <version>7.x-1.0-alpha5</version>
      <tag>7.x-1.0-alpha5</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha5</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-alpha5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha5.tar.gz</download_link>
      <date>1298138218</date>
      <mdhash>562eb1e1bc3ed33f3bf7d3cf3af5713c</mdhash>
      <filesize>25613</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>562eb1e1bc3ed33f3bf7d3cf3af5713c</md5>
          <size>25613</size>
          <filedate>1298138218</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha5.zip</url>
          <archive_type>zip</archive_type>
          <md5>ce40f9a73c8442274e8f51e5d0ae0101</md5>
          <size>38519</size>
          <filedate>1298138218</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-alpha4</name>
      <version>7.x-1.0-alpha4</version>
      <tag>7.x-1.0-alpha4</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha4</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-alpha4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha4.tar.gz</download_link>
      <date>1297961280</date>
      <mdhash>d9057a4880e128bf379a261985cee7b9</mdhash>
      <filesize>25316</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d9057a4880e128bf379a261985cee7b9</md5>
          <size>25316</size>
          <filedate>1297961280</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha4.zip</url>
          <archive_type>zip</archive_type>
          <md5>ff74640dad16cd5e0ad2baef3510b8b2</md5>
          <size>38225</size>
          <filedate>1297961280</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-alpha3</name>
      <version>7.x-1.0-alpha3</version>
      <tag>7.x-1.0-alpha3</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha3</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-alpha3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha3.tar.gz</download_link>
      <date>1297683744</date>
      <mdhash>d5e5cc33ed8d0c29409fa945d3107c4e</mdhash>
      <filesize>23820</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>d5e5cc33ed8d0c29409fa945d3107c4e</md5>
          <size>23820</size>
          <filedate>1297683744</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha3.zip</url>
          <archive_type>zip</archive_type>
          <md5>ed9f1ba974d6eace54b98b4915de8083</md5>
          <size>34591</size>
          <filedate>1297683744</filedate>
        </file>
      </files>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-alpha2</name>
      <version>7.x-1.0-alpha2</version>
      <tag>7.x-1.0-alpha2</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha2</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-alpha2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha2.tar.gz</download_link>
      <date>1295708628</date>
      <mdhash>5d8f8134368f319788c6b2f86d812171</mdhash>
      <filesize>23271</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5d8f8134368f319788c6b2f86d812171</md5>
          <size>23271</size>
          <filedate>1295708628</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha2.zip</url>
          <archive_type>zip</archive_type>
          <md5>9634c6fc65581fa40251771e0fecdf1d</md5>
          <size>34018</size>
          <filedate>1295708628</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.0-alpha1</name>
      <version>7.x-1.0-alpha1</version>
      <tag>7.x-1.0-alpha1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>alpha1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.0-alpha1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha1.tar.gz</download_link>
      <date>1295460731</date>
      <mdhash>e42b293024da523516f8e92e26ede356</mdhash>
      <filesize>19401</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e42b293024da523516f8e92e26ede356</md5>
          <size>19401</size>
          <filedate>1295460731</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.0-alpha1.zip</url>
          <archive_type>zip</archive_type>
          <md5>f552e2e33e46739c870778ad26fb272e</md5>
          <size>27290</size>
          <filedate>1295460731</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Alpha releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-2.x-dev.tar.gz</download_link>
      <date>1397240053</date>
      <mdhash>9e8a3fec31297fdc79a71dbba82e5472</mdhash>
      <filesize>53993</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>9e8a3fec31297fdc79a71dbba82e5472</md5>
          <size>53993</size>
          <filedate>1397240053</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>8e405e045359307e6c06099f59d2a4cd</md5>
          <size>74728</size>
          <filedate>1397240053</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>variable 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/variable/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/variable-7.x-1.x-dev.tar.gz</download_link>
      <date>1382151333</date>
      <mdhash>5251f764b10e313bcb36b83eaaafd77b</mdhash>
      <filesize>48137</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>5251f764b10e313bcb36b83eaaafd77b</md5>
          <size>48137</size>
          <filedate>1382151333</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/variable-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>87b5cacd8b3daf859a68bc4d4283e259</md5>
          <size>72712</size>
          <filedate>1382151333</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
